This is a simple 2D game, inspired by Stardew Valley, Minecraft and Moonlighter. The player gets to explore an infinite world that's generated randomly at every game launch, made with over over a dozen biomes, that coherently appear depending on the local humidity and temperature. These biomes are populated with multiple plants, as well as bushes where the player can gather fruits. If they dislike nature, they can go out and find a mountain, where ores such as coal or iron can be found, and melted with a furnace to obtain iron ingots (which is of no use for now).

The game supports a system of world loading/saving, to allow the player to keep some kind of progression as they explore the map.

It currently doesn't have much of a goal for the player, and they cannot do much apart from exploring the map and breaking blocks, but many more things are planned to being added!

Ideas of additions:
* Block and item crafting
* More animals and ennemies
* More flaura in some biomes (Savanna, jungle, dead lands, taiga, flowery forest and toundra)
* More blocks and furniture
* A tutorial screen/UI
* Sub-worlds, like caves accessible through cave entrances
* Character customisation
* Villages, with npcs and houses
* Entity interaction

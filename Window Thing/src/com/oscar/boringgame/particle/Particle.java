package com.oscar.boringgame.particle;

import com.oscar.boringgame.util.EquationType;
import com.oscar.boringgame.util.EquationType.Equation;

public enum Particle {
	FLAME( 
			textureArray(0, 0),
			0.3f, 
			1f, 0.2f, 
			null, 
			new Equation(EquationType.RANDX, 0.015f, 0.05f)
	),
	DROPLET(
			textureArray(1, 0),
			0.2f, 
			0.5f, 0.1f, 
			new Equation(EquationType.RAND, -0.04f, 0.04f), 
			new Equation(EquationType.AXPOWEBXC, 0.72f, 2, 0, -0.064f)
	),
	BLOCK_PIECE(
			null,
			0.125f,
			0.3f, 0.1f,
			new Equation(EquationType.RAND, -0.04f, 0.04f),
			new Equation(EquationType.AXPOWEBXC, 0.72f, 2, 0, -0.064f)
	);
	
	protected final float[] texture;
	protected final float lifespan;
	protected final float aproximation;
	protected final Equation pathX;
	protected final Equation pathY;
	protected final float width;
	

	Particle(float[] texture, float width, float life, float apro, Equation px, Equation py){
		this.texture = texture;
		lifespan = life;
		aproximation = apro;
		pathX = px;
		pathY = py;
		this.width = width;
	}
	
	/**
	 * @return The particle's expected lifespan in seconds.
	 */
	public float getLifespan() {
		return lifespan;
	}
	
	/**
	 * @return An value that delimits the maximum distance from the default lifespan a ParticleEntity's lifespan can have.
	 */
	public float getAproximation() {
		return aproximation;
	}
	
	/**
	 * @return The Equation used for the Particle's movement on the X axis.
	 */
	public Equation getEquationX() {
		return pathX;
	}
	
	/**
	 * @return The Equation used for the Particle's movement on the Y axis.
	 */
	public Equation getEquationY() {
		return pathY;
	}
	
	/**
	 * @return The width in blocks of the Particle, knowing that it is square.
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return The amount of the ParticleEntity's texture that's used, on the x and y axis. The values should range from 0 to 1.
	 */
	public float[] getTextureCoordinates() {
		return texture;
	}
	
	/**
	 * Creates an array for the texture coordinates at the specified coordinates in the atlas.
	 * @param x The x coordinate, in blocks.
	 * @param y The y coordinate, in blocks.
	 * @return The array used for a particle with a size of 1 at the specified coordinates.
	 */
	public static float[] textureArray(int x, int y) {
		float halfPixel = 0.0001f; // we use this to avoid textureBleeding, because floating point operations are shit!
		return new float[] { x * (6f/32) + halfPixel, y * (6f/32) + halfPixel, (x+1) * (6f/32) - halfPixel, (y+1) * (6f/32) - halfPixel};

	}
}

package com.oscar.boringgame.particle;

import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.BoringMath;

public class BlockBreakingParticle extends ParticleEntity {

	private final float[] textureCoords;
	private final Block block;
	
	public BlockBreakingParticle(Block block, World w, float x, float y) {
		super(Particle.BLOCK_PIECE, w, x, y);		
		this.block = block;
		int blockWidth = block instanceof SurfaceHitboxBlock ? (int) ((SurfaceHitboxBlock) block).getDisplay().getWidth() : 1;
		int blockHeight = block instanceof SurfaceHitboxBlock ? (int) ((SurfaceHitboxBlock) block).getDisplay().getHeight() : 1;
		float texSize = 2f/256 - 0.0001f;
		float xt = BoringMath.nextInt(0, blockWidth*16-2)/256f;
		float yt = BoringMath.nextInt(0, blockHeight*16-2)/256f;
		this.textureCoords = block.getTextureCoordinates().clone();
		textureCoords[0] += xt;
		textureCoords[1] += yt;
		textureCoords[2] = textureCoords[0] + texSize;
		textureCoords[3] = textureCoords[1] + texSize;
	}
	
	@Override
	public float[] getTextureCoordinates() {
		return textureCoords;
	}
	
	@Override
	public Texture getTexture() {
		return Artist.BLOCK_ATLAS;
	}
	
	/**
	 * @return The block that this particle is associated to.
	 */
	public Block getBlock() {
		return block;
	}
	
	
}

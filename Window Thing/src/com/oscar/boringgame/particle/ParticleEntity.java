package com.oscar.boringgame.particle;

import com.oscar.boringgame.map.World;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.EquationType.Equation;
import com.oscar.boringgame.util.Location;

public class ParticleEntity {
	protected final Particle type;
	protected final Location location;
	protected final float height;
	protected final long deathTime;
	protected final long spawnTime;
	protected final Equation px;
	protected final Equation py;
	
	public ParticleEntity(Particle p, World w, float x, float y) {
		type = p;
		this.location = new Location(x, y, w);
		this.height = y+0.9f;
		// I could just use the Equation from the Particle, but I clone it to regenerate the random numbers!
		this.px = p.pathX == null ? null : p.pathX.clone(); 
		this.py = p.pathY == null ? null : p.pathY.clone(); 
		spawnTime = System.currentTimeMillis();
		deathTime = spawnTime + (long) (1000 * BoringMath.nextFloat(p.lifespan - p.aproximation, p.lifespan + p.aproximation));
	}
	
	/**
	 * @return This particle's texture coordinates.
	 */
	public float[] getTextureCoordinates() {
		return type.getTextureCoordinates();
	}
	
	/**
	 * @return The texture atlas used to draw this particle.
	 */
	public Texture getTexture() {
		return Artist.PARTICLE_ATLAS;
	}
	
	/**
	 * @return This ParticleEntity's location.
	 */
	public Location getLocation() {
		return location;
	}
	
	/**
	 * @return The type of this Particle.
	 */
	public Particle getType() {
		return type;
	}
	
	/**
	 * @return The depth of the particle, that corresponds to a bit above the Location where it was spawned.
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * Will manage the lifetime and the position of this Particle.
	 */
	public void tick() {
		if(deathTime <= System.currentTimeMillis()) {
			location.getWorld().remove(this);
			return;
		}
		float timeLived = (spawnTime - System.currentTimeMillis())/1000f;
		if(px != null)
			location.x += px.calc(timeLived);
		if(py != null)
			location.y += py.calc(timeLived);
	}
}

package com.oscar.boringgame.blocks.datablocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.oscar.boringgame.blocks.CosmeticBlock;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.InteractableBlock;
import com.oscar.boringgame.blocks.blocktypes.NoHitboxSurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.entity.DroppedItem;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.IntLocation;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public class StrawberryBush extends DataHolderBlockInstance implements NoHitboxSurfaceBlock, InteractableBlock {
	
	/** The maximum growth of a bush, before it can start trying to get fruits. */
	private static final short growthGoal = 2500;
	
	/** The odds of growing, when the growthGoal has been reached. */
	private static final float oddsOfGrowing = 0.003f;
	
	/** The block's texture when it has fruits. */
	private static final float[] textureWithFruits = textureArray(1, 2);
	
	/** The random generator, to know when the bush is grown. */
	private static final Random rand = new Random();
	
	/** The growth of the bush. */
	private short growth;
	
	/** If the bush is fully grown. */
	private boolean isDone;

	public StrawberryBush(IntLocation loc, World world) {
		this(loc, world, false);
	}
	
	public StrawberryBush(IntLocation loc, World world, boolean grew) {
		super(loc, world);
		growth = 0;
		isDone = grew;
	}
	
	@Override
	public String getName() {
		return "Strawberry Bush";
	}
		
	@Override
	public short getID() {
		return 48;
	}
	
	@Override
	public int getDurability() {
		return 50;
	}
	
	@Override
	public boolean isFlamable() {
		return true;
	}

	@Override
	public SurfaceBlock burnedResult() {
		return AIR.AIR;
	}

	@Override
	public BlockCategory getCategory() {
		return BlockCategory.GENERIC;
	}

	@Override
	public List<ItemStack> getDrops() {
		List<ItemStack> drops = new ArrayList<>();
		if(isDone) {
			short amount = (short) (2 + rand.nextInt(3));
			for(short i = 0; i < amount; i++)
				drops.add(new ItemStack(CommonItem.STRAWBERRY));
		}
		return drops;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return isDone ? textureWithFruits : CosmeticBlock.BUSH.getTextureCoordinates();
	}
	
	@Override
	public boolean canPlaceOn(SolidBlock b) {
		return b == SolidBlock.GRASS;
	}

	@Override
	public boolean isFlat() {
		return false;
	}
	
	@Override
	public float getYOffset() {
		return 0.8f;
	}

	@Override
	public boolean interact(Player p) {
		if(!isDone) return false;
		
		for(ItemStack item : getDrops()) {
			DroppedItem dropped = world.dropItem(item, new Location(loc, world).add(.5f, .5f));
			PVector vel = PVector.fromAngle(rand.nextFloat() * 2 * Math.PI);
			vel.mult(0.1f);
			dropped.setVelocity(vel);
		}
		
		growth = 0;
		isDone = false;
		world.blockUpdate(loc.x, loc.y);
		return true;
	}

	@Override
	public void tick(int counter) {
		if(!isDone) {
			if(growth < growthGoal)
				growth++;
			else if(rand.nextDouble() < oddsOfGrowing)
				isDone = true;

			world.blockUpdate(loc.x, loc.y);
		}
	}

	@Override
	public DataHolderBlockInstance newInstance(IntLocation loc, World world) {
		return new StrawberryBush(loc, world);
	}
	
	@Override
	public int neededExtraBytes() {
		return Short.BYTES + IntLocation.BYTES + Short.BYTES + 1;
	}
	
	@Override
	public byte[] toBytesWithExtras() {
		byte[] idBytes = BoringUtil.toBytes(getID());
		byte[] locationBytes = loc.toBytes();
		byte[] growthBytes = BoringUtil.toBytes(growth);
		byte[] isDoneBytes = BoringUtil.toBytes(isDone);

		return BoringUtil.concatAll(idBytes, locationBytes, growthBytes, isDoneBytes);
	}
	
	@Override
	public StrawberryBush fromBytes(World world, ByteBuffer bytes) {
		if(bytes.remaining() < neededBytes())
			throw new IllegalArgumentException("The given byte buffer's length (" + bytes.remaining() + ") should be equal or larger than neededBytes() (" + neededBytes() + ")!");
		
		IntLocation location = new IntLocation(bytes);
		short growth = bytes.getShort();
		boolean isDone = bytes.get() == 1;
		
		StrawberryBush straberryBush = new StrawberryBush(location, world);
		straberryBush.growth = growth;
		straberryBush.isDone = isDone;
		
		return straberryBush;
	}
}

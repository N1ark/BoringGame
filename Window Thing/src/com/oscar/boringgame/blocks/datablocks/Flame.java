package com.oscar.boringgame.blocks.datablocks;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.NoHitboxSurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.particle.Particle;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.IntLocation;

public class Flame extends DataHolderBlockInstance implements NoHitboxSurfaceBlock {

	/** A random object used to know where the next flame should be placed. */
	private static final Random random = new Random();
	/** The amount of ticks between each attempt at spreading. */
	private static final short spreadGoal = 70;
	/** The odds of the flame spreading to a nearby block. */
	private static final float spreadOdds = 0.03f;
	/** The amount of ticks before the flame should consider burning the block it's on. */
	private static final short ticksForBurning = 65;
	/** The oods of the flame consuming the block below itself and disappearing. */
	private static final float burnOdds = 0.006f;
	/** The amount of frames the flame spends in each animation frame. */
	private static final byte framesPerAnim = 6;
	/** The amount of ticks between each created particle. */
	private static final byte ticksForParticle = 10;
	/** The odds of making a particle appear. */
	private static final float particleOdds = 0.6f;
	/** The flame's textures. */
	private static final float[][] textures = new float[][] {
		Block.textureArray(9, 1), Block.textureArray(8, 1),
		Block.textureArray(9, 1), Block.textureArray(10, 1)
		};
	
	private short spread;
	private short ticksLived;
	
	public Flame(IntLocation loc, World world) {
		super(loc, world);
		spread = 0;
		ticksLived = 0;
	}

	@Override
	public void tick(int counter) {
		ticksLived ++;
		
		if(spread < spreadGoal)
			spread++;
		else if(random.nextFloat() < spreadOdds) {
			int attempts = 0;
			while(attempts < 10) {
				attempts++;
				int pos = random.nextInt(5*5);
				int pX = loc.x - 2 + pos % 5;
				int pY = loc.y - 2 + pos / 5;
				SurfaceBlock surface = world.getUpperBlockIf(pX, pY);
				
				if(surface != null && surface.isFlamable()) {
					world.setblock(pX, pY, surface.burnedResult());
					break;
				}
				if(surface != AIR.AIR)
					continue;
				
				SolidBlock ground = world.getBlockIf(pX, pY);
				if(ground.isFlamable() && world.getVisibleUpperBlock(pX, pY) == AIR.AIR) {
					IntLocation fLoc = new IntLocation(pX, pY);
					Flame flame = new Flame(fLoc, world);
					world.setblock(pX, pY, flame);
					spread = 0;
					break;
				}
			}
		}
		
		if(ticksLived > ticksForBurning && random.nextFloat() < burnOdds) {
			world.setblock(loc.x, loc.y, world.getBlock(loc.x, loc.y).burnedResult());
			world.setblock(loc.x, loc.y, AIR.AIR);
			return;
		}
		
		if(ticksLived % ticksForParticle == 0 && random.nextFloat() < particleOdds) { 
			world.add(new ParticleEntity(Particle.FLAME, world, loc.x + random.nextFloat(), loc.y + random.nextFloat()));
		}
		
		world.blockUpdate(loc.x, loc.y);
	}

	@Override
	public DataHolderBlockInstance newInstance(IntLocation loc, World world) {
		return new Flame(loc, world);
	}

	@Override
	public boolean canPlaceOn(SolidBlock b) {
		return b.isFlamable();
	}

	@Override
	public int getDurability() {
		return -1;
	}

	@Override
	public List<ItemStack> getDrops() {
		return new ArrayList<>();
	}

	@Override
	public String getName() {
		return "Flame";
	}

	@Override
	public short getID() {
		return 45;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return textures[frame/framesPerAnim%textures.length];
	}

	@Override
	public boolean isFlat() {
		return false;
	}

	@Override
	public float getYOffset() {
		return 0.9f;
	}

	@Override
	public boolean isFlamable() {
		return false;
	}

	@Override
	public SurfaceBlock burnedResult() {
		return null;
	}

	@Override
	public BlockCategory getCategory() {
		return BlockCategory.GENERIC;
	}

	@Override
	public int neededExtraBytes() {
		return Short.BYTES + IntLocation.BYTES + Short.BYTES + Short.BYTES;
	}

	@Override
	public byte[] toBytesWithExtras() {
		byte[] idBytes = BoringUtil.toBytes(getID());
		byte[] locationBytes = loc.toBytes();
		byte[] spreadBytes = BoringUtil.toBytes(spread);
		byte[] ticksLivedBytes = BoringUtil.toBytes(ticksLived);

		return BoringUtil.concatAll(idBytes, locationBytes, spreadBytes, ticksLivedBytes);
	}
	
	@Override
	public Flame fromBytes(World world, ByteBuffer bytes) {
		if(bytes.remaining() < neededBytes())
			throw new IllegalArgumentException("The given byte array's length (" + bytes.remaining() + ") should be equal or greater than neededBytes() (" + neededBytes() + ")!");
		
		IntLocation location = new IntLocation(bytes);
		short spread = bytes.getShort();
		short ticksLived = bytes.getShort();
		
		Flame flame = new Flame(location, world);
		flame.spread = spread;
		flame.ticksLived = ticksLived;
		
		return flame;
	}
}

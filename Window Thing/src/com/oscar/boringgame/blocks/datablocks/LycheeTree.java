package com.oscar.boringgame.blocks.datablocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.InteractableBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.entity.DroppedItem;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.IntLocation;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public class LycheeTree extends DataHolderBlockInstance implements SurfaceHitboxBlock, InteractableBlock {

	/** The tree's normal texture, when it is fruitless. */
	private static final float[] texture = textureArray(12, 1, 1, 2);
	
	/** The tree's texture when it is fully grown and has fruits. */
	private static final float[] grownTexture = textureArray(13, 1, 1, 2);
	
	/** The maximum growth of a bush, before it can start trying to get fruits. */
	private static final short growthGoal = 2500;
	
	/** The odds of growing, when the growthGoal has been reached. */
	private static final float oddsOfGrowing = 0.003f;
	
	/** The tree's hitbox. */
	private static final Area hitbox = new Area(0.3, 0.5, 0.7, 0.9);
	
	/** The dimension of the tree. */
	private static final Area dimension = new Area(1, 1);

	/** The display size of the tree. */
	private static final Area display = Area.getAreaRelative(0, -1, 1, 2);
	
	/** The random generator, to know when the tree is grown. */
	private static final Random rand = new Random();
	
	/** The growth of the tree in ticks. */
	private short growth;
	
	/** If the tree is done growing and beared fruits. */
	private boolean isDone;
	
	public LycheeTree(IntLocation loc, World world) {
		this(loc, world, false);
	}
	
	public LycheeTree(IntLocation loc, World world, boolean isDone) { 
		super(loc, world);
		this.isDone = isDone;
		this.growth = 0;
	}
	
	@Override
	public String getName() {
		return "Lychee Tree";
	}
	
	@Override
	public short getID() {
		return 47;
	}

	@Override
	public boolean canPlaceOn(SolidBlock b) {
		return b == SolidBlock.GRASS;
	}

	@Override
	public int getDurability() {
		return 110;
	}
	
	@Override
	public boolean isFlamable() {
		return true;
	}

	@Override
	public SurfaceBlock burnedResult() {
		return AIR.AIR;
	}

	@Override
	public BlockCategory getCategory() {
		return BlockCategory.WOODEN;
	}

	@Override
	public List<ItemStack> getDrops() {
		List<ItemStack> drops = new ArrayList<>();
		if(isDone) {
			short amount = (short) (1 + rand.nextInt(3));
			for(short i = 0; i < amount; i++)
				drops.add(new ItemStack(CommonItem.LYCHEE));
		}
		return drops;
	}

	@Override
	public float[] getTextureCoordinates(int frame) {
		return isDone ? grownTexture : texture;
	}

	@Override
	public Area getHitbox() {
		return hitbox;
	}

	@Override
	public Area getDimension() {
		return dimension;
	}

	@Override
	public Area getDisplay() {
		return display;
	}
	
	@Override
	public boolean interact(Player p) {
		if(!isDone) return false;
		
		for(ItemStack item : getDrops()) {
			DroppedItem dropped = world.dropItem(item, new Location(loc, world).add(.5f, 1.3f));
			PVector vel = PVector.fromAngle(rand.nextFloat() * 2 * Math.PI);
			vel.mult(0.1f);
			dropped.setVelocity(vel);
		}
		
		growth = 0;
		isDone = false;
		world.blockUpdate(loc.x, loc.y);
		return true;	}

	@Override
	public void tick(int counter) {
		if(!isDone) {
			if(growth < growthGoal)
				growth++;
			else if(rand.nextDouble() < oddsOfGrowing)
				isDone = true;

			world.blockUpdate(loc.x, loc.y);
		}
	}

	@Override
	public DataHolderBlockInstance newInstance(IntLocation loc, World world) {
		return new LycheeTree(loc, world);
	}
	
	@Override
	public int neededExtraBytes() {
		return Short.BYTES + IntLocation.BYTES + Short.BYTES + 1;
	}
	
	@Override
	public byte[] toBytesWithExtras() {
		byte[] idBytes = BoringUtil.toBytes(getID());
		byte[] locationBytes = loc.toBytes();
		byte[] growthBytes = BoringUtil.toBytes(growth);
		byte[] isDoneBytes = BoringUtil.toBytes(isDone);

		return BoringUtil.concatAll(idBytes, locationBytes, growthBytes, isDoneBytes);
	}
	
	@Override
	public LycheeTree fromBytes(World world, ByteBuffer bytes) {
		if(bytes.remaining() < neededBytes())
			throw new IllegalArgumentException("The given byte buffer's length (" + bytes.remaining() + ") should be equal or larger than neededBytes() (" + neededBytes() + ")!");
		
		IntLocation location = new IntLocation(bytes);
		short growth = bytes.getShort();
		boolean isDone = bytes.get() == 1;
		
		LycheeTree lycheeTree = new LycheeTree(location, world);
		lycheeTree.growth = growth;
		lycheeTree.isDone = isDone;
		
		return lycheeTree;
	}
}

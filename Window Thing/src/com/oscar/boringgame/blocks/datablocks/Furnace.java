package com.oscar.boringgame.blocks.datablocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.ExtraInfoDisplayBlock;
import com.oscar.boringgame.blocks.blocktypes.InteractableBlock;
import com.oscar.boringgame.blocks.blocktypes.ItemHolderBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.particle.Particle;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.IntLocation;

public class Furnace extends DataHolderBlockInstance implements SurfaceHitboxBlock, ItemHolderBlock, InteractableBlock, ExtraInfoDisplayBlock {
	
	/** A random object to generate the particles' position. */
	private static final Random random = new Random();
	
	/** The texture the furnace has when not lit. */
	private static final float[] textureWhenIdle = textureArray(5, 2);
	
	/** The texture the furnace has when burning something. */
	private static final float[] textureWhenBurning = textureArray(6, 2);
	
	/** The display for the extra bubble, used to display smelted/cooked items. */
	private static final ExtraInfoDisplay infoBubble = new ExtraInfoDisplay(Area.getAreaRelative(0, -1, 1, 1.25f), textureArray(10, 3, 1, 1.25f));
	
	/** The maximum value of the burning progress, to consider an item "ready". */
	private static final short burningProgressGoal = 200;
	
	/** The furnace's hitbox. */
	private static final Area hitbox = new Area(0.05, 0.25, 0.95, 0.95);
	
	/** The dimension and display size of the furnace. */
	private static final Area dimension = new Area(1, 1);
	
	/** The item currently inside the furnace. */
	private ItemStack item;
	
	/** The furnace's current progress. */
	private short burningProgress;
	
	/** The display object to show the item inside the furnace. */
	private final ExtraInfoDisplay extra;
	
	public Furnace(IntLocation loc, World world) { 
		super(loc, world);
		this.burningProgress = 0;
		this.item = null;
		this.extra = new ExtraInfoDisplay(Area.getAreaRelative(0.25f, -0.75f, 0.5f, 0.5f), null);
	}
	
	@Override
	public String getName() {
		return "Furnace";
	}
	
	@Override
	public short getID() {
		return 46;
	}
	
	@Override
	public int getDurability() {
		return 300;
	}
	
	@Override
	public boolean isFlamable() {
		return false;
	}

	@Override
	public SurfaceBlock burnedResult() {
		return null;
	}

	@Override
	public BlockCategory getCategory() {
		return BlockCategory.STONY;
	}

	@Override
	public List<ItemStack> getDrops() {
		List<ItemStack> drops = new ArrayList<>();
		drops.add(new ItemStack(DataHolderBlockInstance.FURNACE));
		if(hasResultReady())
			drops.add(item);
		return drops;
	}

	@Override
	public float[] getTextureCoordinates(int frame) {
		return burningProgress > 0 ? textureWhenBurning : textureWhenIdle;
	}
	
	@Override
	public Area getHitbox() {
		return hitbox;
	}

	@Override
	public Area getDimension() {
		return dimension;
	}

	@Override
	public Area getDisplay() {
		return dimension;
	}

	@Override
	public ItemStack getItem() {
		return item;
	}

	@Override
	public boolean canPlaceOn(SolidBlock b) {
		return !b.canBeReplaced();
	}
	
	/**
	 * @return If the furnace has its result ready to be picked.
	 */
	public boolean hasResultReady() {
		return burningProgress == 0 && item != null;
	}
	
	@Override
	public Furnace newInstance(IntLocation loc, World world) {
		return new Furnace(loc, world);
	}
	
	@Override
	public boolean interact(Player p) {
		ItemStack hand = p.getHandItem();
		if(
			item == null &&
			hand != null &&
			hand.getType() == CommonItem.IRON_NUGGET && 
			hand.getAmount() >= 3 && 
			p.getInventory().has(CommonItem.COAL)
		) {
			if(hand.getAmount() == 3)
				p.getInventory().setItem(null, p.getSlot());
			else
				hand.setAmount(hand.getAmount()-3);
			p.getInventory().remove(CommonItem.COAL);
			burningProgress = 1;
			item = new ItemStack(CommonItem.IRON_INGOT);
			p.getWorld().blockUpdate(loc.x, loc.y);
			return true;
		}
		
		else if (
			hasResultReady() &&
			p.getInventory().hasSpace(item)
		) {
			p.getInventory().addItem(item);
			burningProgress = 0;
			item = null;
			return true;
		}
		return false;
	}

	@Override
	public void tick(int counter) {
		if(burningProgress > 0) {
			if(counter % 7 == 0)
				world.add(new ParticleEntity(Particle.FLAME, world, loc.x + random.nextFloat(), loc.y + random.nextFloat() * 0.3f));
			burningProgress ++;
			if(burningProgress > burningProgressGoal) {
				burningProgress = 0;
				extra.texture = item.getType().getTextureCoordinates();
			}
			else
				world.blockUpdate(loc.x, loc.y);
		}
	}

	@Override
	public ExtraInfoDisplay[] getExtraInfo() {
		if(hasResultReady()) {
			return new ExtraInfoDisplay[] {
					infoBubble,
					extra
			};
		}
		return new ExtraInfoDisplay[0];
	}
	
	@Override
	public int neededExtraBytes() {
		return Short.BYTES + IntLocation.BYTES + (item == null ? 2 : item.neededBytes()) + Short.BYTES;
	}

	@Override
	public byte[] toBytesWithExtras() {
		byte[] idBytes = BoringUtil.toBytes(getID());
		byte[] locationBytes = loc.toBytes();
		byte[] itemBytes = item == null ? new byte[] {Byte.MIN_VALUE, Byte.MIN_VALUE} : item.toBytes();
		byte[] burningProgressBytes = BoringUtil.toBytes(burningProgress);
		
		return BoringUtil.concatAll(idBytes, locationBytes, itemBytes, burningProgressBytes);
	}
	
	@Override
	public Furnace fromBytes(World world, ByteBuffer bytes) {
		if(bytes.remaining() < neededBytes())
			throw new IllegalArgumentException("The given byte array's length (" + bytes.remaining() + ") should be equal or larger than neededBytes() (" + neededBytes() + ")!");
		
		IntLocation location = new IntLocation(bytes);
		ItemStack item;
		
		if(bytes.get(bytes.position()) == Byte.MIN_VALUE) {
			item = null;
			bytes.position(bytes.position()+2);
		} else {
			 item = new ItemStack(bytes);
		}
		
		short burningProgress = bytes.getShort();
		
		Furnace furnace = new Furnace(location, world);
		furnace.item = item;
		furnace.burningProgress = burningProgress;
		
		if(furnace.hasResultReady())
			furnace.extra.texture = item.getType().getTextureCoordinates();
		
		return furnace;
	}
}

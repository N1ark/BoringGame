package com.oscar.boringgame.blocks.blocktypes;

import com.oscar.boringgame.blocks.SolidBlock;

public interface SurfaceBlock extends Block {
	/**
	 * Will see is this Block can be placed on top of the specified Block, by looping through all the placing-enabled blocks.
	 * @param b The SolidBlock on which this should be placed.
	 * @return If this Block can be placed on it.
	 */
	public boolean canPlaceOn(SolidBlock b);
	
	@Override
	SurfaceBlock burnedResult();
}
package com.oscar.boringgame.blocks.blocktypes;

import com.oscar.boringgame.entity.Player;

public interface InteractableBlock extends DataHolderBlock {
	/**
	 * Will make everything needed for when the given Player interacts (right click) with the block.
	 * @param p The Player who interacted.
	 * @return If the interaction was a success. If it was, all other interactions and block placings will be cancelled.
	 */
	public boolean interact(Player p);
}
package com.oscar.boringgame.blocks.blocktypes;

import com.oscar.boringgame.util.Area;

public interface ExtraInfoDisplayBlock extends DataHolderBlock {
	
	public ExtraInfoDisplay[] getExtraInfo();
	
	public static final class ExtraInfoDisplay {		
		public final Area position;
		public float[] texture;
		
		public ExtraInfoDisplay(Area position, float[] texture) {
			this.position = position;
			this.texture = texture;
		}
		
	}
	
}

package com.oscar.boringgame.blocks.blocktypes;

import static java.lang.Math.floor;

import com.oscar.boringgame.map.World;
import com.oscar.boringgame.util.IntLocation;

public abstract class DataHolderBlockInstance implements DataHolderBlock {
	
	/** The location at which this block is placed. */
	protected final IntLocation loc;
	/** The world in which this block is placed. */
	protected final World world;
	
	public DataHolderBlockInstance(IntLocation loc, World world) {
		this.loc = loc;
		this.world = world;
	}
	
	@Override
	public byte[] toBytes() {
		if(loc == null)
			return DataHolderBlock.super.toBytes();
		return toBytesWithExtras();
	}
	
	@Override
	public final DataHolderBlockInstance newInstance(int x, int y, World world) {
		return newInstance(new IntLocation(x, y), world);
	}
	
	@Override
	public final DataHolderBlockInstance newInstance(float x, float y, World world) {
		return newInstance(new IntLocation((int) floor(x), (int) floor(y)), world);
	}
}
package com.oscar.boringgame.blocks.blocktypes;

import java.util.List;

import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.item.ItemStack;

public interface Block extends Item {
	
	final float[][] BREAKING_ANIMATION_TEXTURE = new float[][] {
		textureArray(0, 1),
		textureArray(1, 1),
		textureArray(2, 1),
		textureArray(3, 1)
	};
	final float[] SELECTED_BLOCK_TEXTURE = textureArray(4, 1);
	
	/**
	 * @return The durability of the block.
	 */
	int getDurability();
	
	/**
	 * @return If this block can catch fire.
	 */
	boolean isFlamable();
	
	/**
	 * @return The block that should replace this once it burned,
	 */
	Block burnedResult();
	
	/**
	 * @return The BlockCategory of this block, that will determine what 
	 */
	BlockCategory getCategory();
	
	
	/**
	 * @return A list with the generated ItemStacks dropped.
	 */
	List<ItemStack> getDrops();
	
	@Override
	default int getMaxStackSize(){
		return 50;
	}
	
	/**
	 * Creates an array for the texture coordinates at the specified coordinates in the atlas.
	 * @param x The x coordinate, in blocks.
	 * @param y The y coordinate, in blocks.
	 * @return The array used for a block with a size of 1 at the specified coordinates.
	 */
	public static float[] textureArray(int x, int y) {
		return textureArray(x, y, 1, 1);
	}
	
	/**
	 * Creates an array for the texture coordinates at the specified coordinates in the atlas.
	 * @param x The x coordinate, in blocks.
	 * @param y The y coordinate, in blocks.
	 * @param width The width of the texture in blocks.
	 * @param height The height of the texture in blocks.
	 * @return The array used for a block with a size of 1 at the specified coordinates.
	 */
	public static float[] textureArray(float x, float y, float width, float height) {
		float halfPixel = 0.0001f; // we use this to avoid textureBleeding, because floating point operations are shit!
		return new float[] { x/16 + halfPixel, y/16 + halfPixel, (x+width)/16 - halfPixel, (y+height)/16 - halfPixel};

	}
}

package com.oscar.boringgame.blocks.blocktypes;

import java.nio.ByteBuffer;

import com.oscar.boringgame.blocks.datablocks.Flame;
import com.oscar.boringgame.blocks.datablocks.Furnace;
import com.oscar.boringgame.blocks.datablocks.LycheeTree;
import com.oscar.boringgame.blocks.datablocks.StrawberryBush;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.util.IntLocation;

public interface DataHolderBlock extends SurfaceBlock {

	public static final Furnace FURNACE = new Furnace(null, null);
	public static final StrawberryBush STRAWBERRY_BUSH = new StrawberryBush(null, null, true);
	public static final LycheeTree LYCHEE_TREE = new LycheeTree(null, null, true);
	public static final Flame FLAME = new Flame(null, null);
	
	public static final DataHolderBlockInstance[] values = {FURNACE, STRAWBERRY_BUSH, LYCHEE_TREE, FLAME};
	
	/**
	 * Will tick the block.
	 * @param counter The tick counter.
	 */
	public void tick(int counter);
	
	/**
	 * Will simply create a new instance of this class. This method is in no way impacted by the used instance.
	 * @param The location of the block.
	 * @return The new instance created.
	 */
	public DataHolderBlockInstance newInstance(IntLocation loc, World world);
	
	/**
	 * Will simply create a new instance of this class. This method is in no way impacted by the used instance.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @param world The world in which the block is.
	 * @return The new instance created.
	 */
	public DataHolderBlockInstance newInstance(int x, int y, World world);
	
	/**
	 * Will simply create a new instance of this class. This method is in no way impacted by the used instance.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @param world The world in which the block is.
	 * @return The new instance created.
	 */
	public DataHolderBlockInstance newInstance(float x, float y, World world);
	
	/**
	 * Will go through all registered DataHolderBlocks, and see if one of them is from the same class as the given instance.
	 * @param instance The class to look for.
	 * @return The static final value associated to this type of DataHolderBlockInstance.
	 */
	public static DataHolderBlockInstance getByInstance(DataHolderBlockInstance instance) {
		for(DataHolderBlockInstance value : values)
			if(value.getClass().equals(instance.getClass()))
				return value;
		return null;
	}
	
	/**
	 * @return The bytes needed to have a full representation of this block.
	 */
	public int neededExtraBytes();
	
	/**
	 * @return The full representation of this block, with all the variables too.
	 */
	public byte[] toBytesWithExtras();
	
	/**
	 * Will create a new instance of this class, with its values taken from the given bytes. This method is in no way impacted by the used instance.
	 * @param world The world in which the block should be placed.
	 * @param bytes The extra bytes attached to this block.
	 * @return A new instance of this DataHolderBlock type.
	 */
	public DataHolderBlock fromBytes(World world, ByteBuffer bytes);
}

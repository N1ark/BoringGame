package com.oscar.boringgame.blocks.blocktypes;

import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.item.SpecialItem;

public enum BlockCategory {
	WOODEN(SpecialItem.AXE),
	STONY(SpecialItem.PICKAXE),
	GRANULOUS(SpecialItem.SHOVEL),
	GENERIC();
	
	private final Item[] tools;
	
	BlockCategory(Item... tools){
		this.tools = tools;
	}
	
	/**
	 * @param item The tool that would be used to break the block.
	 * @return If this block category is weak against the given tool.
	 */
	public boolean isWeakAgainst(Item item) {
		for(Item i : tools)
			if(i == item)
				return true;
		return false;
	}
	
}

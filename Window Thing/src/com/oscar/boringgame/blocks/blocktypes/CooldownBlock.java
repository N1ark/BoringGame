package com.oscar.boringgame.blocks.blocktypes;

public interface CooldownBlock extends DataHolderBlock {
	/**
	 * @return The current cooldown state.
	 */
	int getCooldown();
	
	/**
	 * Will decrease the cooldown by 1.
	 * @return If the cooldown reached 0.
	 */
	boolean tickCooldown();
	
	/**
	 * Will set the cooldown.
	 * @param cooldown The new cooldown.
	 */
	void setCooldown(int cooldown);
}
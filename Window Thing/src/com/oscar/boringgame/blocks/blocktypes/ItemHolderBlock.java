package com.oscar.boringgame.blocks.blocktypes;

import com.oscar.boringgame.item.ItemStack;

public interface ItemHolderBlock extends DataHolderBlock {
	/**
	 * @return The ItemStack contained in the block.
	 */
	ItemStack getItem();
}
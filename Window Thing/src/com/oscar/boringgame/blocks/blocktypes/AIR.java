package com.oscar.boringgame.blocks.blocktypes;

import static com.oscar.boringgame.blocks.blocktypes.BlockCategory.GENERIC;

import java.util.List;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.item.ItemStack;

public class AIR implements SurfaceBlock{
	
	private AIR() {};
	
	public static final AIR AIR = new AIR();
	
	@Override public int getDurability()				 {return 0;	  	}
	@Override public boolean canPlaceOn(SolidBlock b)	 {return false;	}
	@Override public List<ItemStack> getDrops()			 {return null; 	}
	@Override public String getName()					 {return null; 	}
	@Override public float[] getTextureCoordinates(int x){return null;	}
	@Override public boolean isFlamable() 				 {return false;	}
	@Override public SurfaceBlock burnedResult() 		 {return null;	}
	@Override public BlockCategory getCategory() 		 {return GENERIC;}
	@Override public short getID() 						 {return 0;		}
}
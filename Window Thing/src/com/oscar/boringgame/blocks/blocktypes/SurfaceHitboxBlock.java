package com.oscar.boringgame.blocks.blocktypes;

import com.oscar.boringgame.util.Area;

public interface SurfaceHitboxBlock extends SurfaceBlock{
	/**
	 * @return The hitbox of the block, for the colision with entities.
	 */
	Area getHitbox();
	
	/**
	 * @return The "rounded" hitbox of the block, that shows were the Player can break the block.
	 */
	Area getDimension();
	
	/**
	 * @return The display rule of the block, that has the limits of the image.
	 */
	Area getDisplay();

	@Override
	default int getMaxStackSize(){
		return 1;
	}
}
package com.oscar.boringgame.blocks.blocktypes;

public interface NoHitboxSurfaceBlock extends SurfaceBlock {
	/**
	 * @return If this Block is flat, which means that it has the same "depth" as a SolidBlock and shouldn't be displayed in front of anything except
	 * the SolidBlock behind it.
	 */
	public boolean isFlat();
	
	/**
	 * @return If this Block isn't flat, will return the depth offset of the Block, to know if it should be displayed in front or behind other entities and blocks.
	 * If this Block is flat, this value shouldn't be used and means nothing.
	 */
	public float getYOffset();
}
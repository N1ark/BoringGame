package com.oscar.boringgame.blocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.util.ArrayList;
import java.util.List;

import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.util.Loot;

public enum SolidBlock implements Block {
	DEEP_WATER(
			13,
			"Deep Water", textureArray(5, 0),
			BlockCategory.GENERIC
	),
	ERROR(
			14,
			"Error", textureArray(6, 0),
			BlockCategory.GENERIC
	),
	WATER(
			15,
			"Water", textureArray(13, 0),
			BlockCategory.GENERIC, true
	),
	ASHES(
			16,
			"Ashes", textureArray(11, 1),
			BlockCategory.GRANULOUS, 50, null,
			new Loot(CommonItem.COAL, 0, 2)
	),
	DRY_DIRT(
			17,
			"Dry Dirt", textureArray(2, 0),
			BlockCategory.GRANULOUS, 60, null
	), 
	DIRT(
			18,
			"Dirt", textureArray(1, 0),
			BlockCategory.GRANULOUS, 80, DRY_DIRT
	), 
	GRASS(
			19,
			"Grass", textureArray(0,0),
			BlockCategory.GRANULOUS, 100, DIRT
	),
	WOOD(
			20,
			"Wood Planks", textureArray(14, 0),
			BlockCategory.WOODEN, 220, ASHES
	), 
	BEDROCK(
			21,
			"Bedrock", textureArray(3, 0),
			BlockCategory.STONY, -1, null
	),
	SAND(
			22, 
			"Sand", textureArray(10, 0),
			BlockCategory.GRANULOUS, 100, null
	), 
	SNOW(
			23,
			"Snow", textureArray(11, 0),
			BlockCategory.GRANULOUS, 140, null
	),
	ICE(
			24,
			"Ice", textureArray(7, 0),
			BlockCategory.STONY, 70, null
	),
	OAK_LOG(
			25,
			"Oak Log", textureArray(9, 0),
			BlockCategory.WOODEN, 250, ASHES
	),
	STONE(
			26,
			"Stone", textureArray(12, 0),
			BlockCategory.STONY, 500, null,
			new Loot(CommonItem.ROCK, 1, 5)
	),
	COAL_ORE(
			27,
			"Coal Ore", textureArray(4, 0),
			BlockCategory.STONY, 600, STONE,
			new Loot(CommonItem.ROCK, 1, 3), new Loot(CommonItem.COAL, 1, 2)
	),
	IRON_ORE(
			28,
			"Iron Ore", textureArray(8, 0),
			BlockCategory.STONY, 900, null,
			new Loot(CommonItem.ROCK, 1, 3), new Loot(CommonItem.IRON_NUGGET, 1, 3)
	),
	;
	
	private final short id;
	private final String name;
	private final float[] texture;
	private final BlockCategory category;
	private final int durability; // if -1 won't break
	private final boolean flamable;
	private final SolidBlock burnedResult;
	private final boolean canPlace;
	private final boolean walkable; 
	private final Loot[] loot;

	SolidBlock(int id, String name, float[] texture, BlockCategory category) {
		this(id, name, texture, category, false);
	}
	
	SolidBlock(int id, String name, float[] texture, BlockCategory category, boolean walk) {
		this.id = (short) id;
		this.name = name;
		this.category = category;
		this.durability = -1;
		this.walkable = walk;
		this.canPlace = true;
		this.loot = null;
		this.texture = texture;
		this.flamable = false;
		this.burnedResult = null;
	}

	SolidBlock(int id, String name, float[] texture, BlockCategory category, int dur, SolidBlock burnedResult, Loot... loot) {
		this.id = (short) id;
		this.name = name;
		this.category = category;
		this.durability = dur;
		this.walkable = true;
		this.canPlace = (dur < 0);
		this.loot = loot;
		this.texture = texture;
		this.flamable = burnedResult != null;
		this.burnedResult = burnedResult;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public short getID() {
		return id;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return texture;
	}

	@Override
	public int getDurability() {
		return durability;
	}

	public boolean isWalkable() {
		return walkable;
	}
	
	public boolean canBeReplaced() {
		return canPlace;
	}
	
	@Override
	public boolean isFlamable() {
		return flamable;
	}
	
	@Override
	public SolidBlock burnedResult() {
		return burnedResult;
	}
	
	@Override
	public BlockCategory getCategory() {
		return category;
	}
	
	@Override
	public List<ItemStack> getDrops() {
		List<ItemStack> drops = new ArrayList<>();
		if(loot.length == 0)
			drops.add(new ItemStack(this));
		else
			for (Loot l : loot) {
				ItemStack dropped = l.generateItem();
				if(dropped != null)
					drops.add(dropped);
			}
		return drops;
	}
}

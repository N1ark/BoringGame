package com.oscar.boringgame.blocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.util.Arrays;
import java.util.List;

import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.HitBox;

public enum ComplexFurnitureBlock implements SurfaceHitboxBlock{
	BED(
			44,
			"Large Bed",
			BlockCategory.WOODEN,
			new Area(2, 4),
			new Area(2, 4),
			200,
			new ComplexFurniturePart(textureArray(2, 3, 2, 4), new Area(0.15, 0.6, 1.85, 0.95)), //top
			new ComplexFurniturePart(textureArray(0, 3, 2, 4), new Area(0.15, 3.5, 1.85, 3.8)) //bottom
	);

	private final String name;
	private final short id;
	private final BlockCategory category;
	private ComplexFurniturePart[] parts;
	private final int durability;
	private final Area display;
	private final Area dimension;
	private final HitBox hitbox;
	
	ComplexFurnitureBlock(int id, String name, BlockCategory category, Area dim, int dur, ComplexFurniturePart... parts){
		this(id, name, category, new Area(1,1), dim, dur, parts);
	}
	
	ComplexFurnitureBlock(int id, String name, BlockCategory category, Area display, Area dim, int dur, ComplexFurniturePart... parts){
		this.id = (short) id;
		this.name = name;
		this.category = category;
		this.parts = parts;
		this.display = display;
		this.dimension = dim;
		this.hitbox = new HitBox();
		this.durability = dur;
		for(int x = 0; x < parts.length; x++)
			hitbox.addPart(parts[x].hitbox);
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public short getID() {
		return id;
	}
	
	@Override
	public int getDurability() {
		return durability;
	}
	
	/**
	 * @return The block's hitbox.
	 */
	public HitBox getContactHitbox() {
		return hitbox;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return parts[0].texture;
	}
	
	@Override
	public boolean isFlamable() {
		return false;
	}
	
	@Override
	public SurfaceBlock burnedResult() {
		return null;
	}
	
	@Override
	public BlockCategory getCategory() {
		return category;
	}
	
	/**
	 * @return The ComplexFurnitureParts making this ComplexFurnitureBlock.
	 */
	public ComplexFurniturePart[] getParts() {
		return parts;
	}
	
	@Override
	public Area getHitbox() {
		return null;
	}
	
	@Override
	public Area getDimension() {
		return dimension;
	}
	
	@Override
	public Area getDisplay() {
		return display;
	}
	
	@Override
	public boolean canPlaceOn(SolidBlock b) {
		return !b.canBeReplaced();
	}
	
	@Override
	public List<ItemStack> getDrops() {
		return Arrays.asList(new ItemStack(this));
	}
	
	public static class ComplexFurniturePart{
		
		private final Area hitbox;
		private final float[] texture;
		
		private ComplexFurniturePart(float[] tex, Area h){
			this.texture = tex;
			hitbox = h;
		}
		
		/**
		 * @return The part's hitbox, relative to the block's location.
		 */
		public Area getHitbox() {
			return hitbox;
		}
		
		/**
		 * @return The coordinates of this part's texture on the texture atlas.
		 */
		public float[] getTextureCoordinates() {
			return texture;
		}
	}
}

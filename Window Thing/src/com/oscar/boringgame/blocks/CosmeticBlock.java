package com.oscar.boringgame.blocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.util.ArrayList;
import java.util.List;

import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.NoHitboxSurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.util.Loot;

public enum CosmeticBlock implements NoHitboxSurfaceBlock {

	RED_FLOWERS(
			36,
			"Red Flowers", textureArray(8, 2),
			BlockCategory.GENERIC, 20, 
			SolidBlock.GRASS
	),
	MIXED_FLOWERS(
			37,
			"Mixed Flowers", textureArray(7, 2),
			BlockCategory.GENERIC, 20, 
			SolidBlock.GRASS
	),
	TULIPS(
			38,
			"Tulips", textureArray(10, 2),
			BlockCategory.GENERIC, 20, 
			SolidBlock.GRASS
	),
	FROST_FLAKES(
			39,
			"Frost Flakes", textureArray(11, 2),
			BlockCategory.GENERIC, 20,
			SolidBlock.SNOW, SolidBlock.GRASS
	),
	SAPLING(
			40,
			"Sapling", textureArray(9, 2),
			BlockCategory.WOODEN, 40, 
			false, 0.8f, 
			SolidBlock.GRASS
	),
	BUSH(
			41,
			"Bush", textureArray(0, 2),
			BlockCategory.GENERIC, 50, 
			false, 0.8f, 
			SolidBlock.GRASS
	),
	DEAD_BUSH(
			42,
			"Dead Bush", textureArray(4, 2),
			BlockCategory.WOODEN, 20, 
			false, 0.8f, 
			SolidBlock.GRASS, SolidBlock.SAND, SolidBlock.DIRT, SolidBlock.DRY_DIRT
	),
	HIBICUS_BUSH(
			43,
			"Hibiscus Bush", textureArray(7, 1),
			BlockCategory.GENERIC, 55,
			false, 0.8f,
			new Loot[] {new Loot(CommonItem.HIBISCUS_FLOWER, 1, 4)},
			SolidBlock.GRASS
	)
	;
	
	private final String name;
	private final short id;
	private final float[] texture;
	private final BlockCategory category;
	private final boolean flat;
	private final float yOffset;
	private final int durability;
	private final SolidBlock[] placableOn;
	private final Loot[] loot;

	CosmeticBlock(int id, String name, float[] texture, BlockCategory category, int dur, SolidBlock... placable) {
		this(id, name, texture, category, dur, true, 1, null, placable);
	}
	
	CosmeticBlock(int id, String name, float[] texture, BlockCategory category, int dur, boolean flat, float yOffset, SolidBlock... placable) {
		this(id, name, texture, category, dur, flat, yOffset, null, placable);
	}
	
	CosmeticBlock(int id, String name, float[] texture, BlockCategory category, int dur, boolean flat, float yOffset, Loot[] loot, SolidBlock... placable) {
		this.id = (short) id;
		this.name = name;
		this.texture = texture;
		this.category = category;
		this.flat = flat;
		this.durability = dur;
		this.yOffset = yOffset;
		this.placableOn = placable;
		this.loot = loot == null ? new Loot[] {new Loot(this)} : loot;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public short getID() {
		return id;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return texture;
	}
	
	@Override
	public int getDurability(){
		return durability;
	}
	
	@Override
	public List<ItemStack> getDrops(){
		ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
		for (Loot l : loot) {
			ItemStack dropped = l.generateItem();
			if(dropped != null)
				drops.add(dropped);
		}
		return drops;
	}
	
	@Override
	public boolean canPlaceOn(SolidBlock b) {
		for(SolidBlock a : placableOn)
			if(a == b)
				return true;
		return false;
	}
	
	@Override
	public boolean isFlat() {
		return flat;
	}
	
	@Override
	public float getYOffset() {
		return yOffset;
	}

	@Override
	public boolean isFlamable() {
		return true;
	}

	@Override
	public SurfaceBlock burnedResult() {
		return AIR.AIR;
	}

	@Override
	public BlockCategory getCategory() {
		return category;
	}
}

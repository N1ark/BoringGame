package com.oscar.boringgame.blocks;

import static com.oscar.boringgame.blocks.blocktypes.Block.textureArray;

import java.util.ArrayList;
import java.util.List;

import com.oscar.boringgame.blocks.blocktypes.BlockCategory;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.Loot;

public enum FurnitureBlock implements SurfaceHitboxBlock{

	CHAIR(
			29,
			"Chair",
			textureArray(2, 2),
			BlockCategory.WOODEN, 50, 
			new Area(1,1), new Area(0, 0.5, 1, 0.9)
	),
	TABLE(
			30,
			"Large Table", 
			textureArray(7, 3, 3, 2),
			BlockCategory.WOODEN, 200,
			new Area(3, 2),
			new Area(3, 2), new Area(0.1, 0.5, 2.9, 1.9)
	),
	OAK(
			31,
			"Oak",
			textureArray(4, 3, 3, 5),
			BlockCategory.WOODEN, 400, 
			Area.getAreaRelative(0, -4, 3, 5), 
			new Area(3, 1), new Area(0.3, -0.1, 2.7, 0.9),
			new SolidBlock[] {SolidBlock.DRY_DIRT, SolidBlock.DIRT, SolidBlock.GRASS},
			new Loot(CosmeticBlock.SAPLING, 1, 3), new Loot(SolidBlock.OAK_LOG, 3, 6)
	),
	PINE(
			32,
			"Pine",
			textureArray(11, 3, 3, 5),
			BlockCategory.WOODEN, 400,
			Area.getAreaRelative(0, -4, 3, 5),
			new Area(3, 1), new Area(0.74, 0.1, 2.26, 0.85),
			new SolidBlock[] {SolidBlock.DRY_DIRT, SolidBlock.DIRT, SolidBlock.GRASS},
			new Loot(CosmeticBlock.SAPLING, 1, 3), new Loot(SolidBlock.OAK_LOG, 3, 6)
	),
	BAOBAB(
			33,
			"Baobab",
			textureArray(0, 7, 4, 6),
			BlockCategory.WOODEN, 550,
			Area.getAreaRelative(0, -5, 4, 6),
			new Area(4, 1), new Area(0.625, -0.15, 3, 0.8),
			new SolidBlock[] {SolidBlock.DRY_DIRT, SolidBlock.DIRT, SolidBlock.GRASS},
			new Loot(CosmeticBlock.SAPLING, 1, 2), new Loot(SolidBlock.OAK_LOG, 4, 9)
	),
	CHEST(
			34,
			"Chest",
			textureArray(3, 2),
			BlockCategory.WOODEN, 150, 
			new Area(1,1), new Area(0.1, 0.2, 0.9, 1)
	),
	CACTUS(
			35,
			"Cactus",
			textureArray(14, 1, 1, 2),
			BlockCategory.WOODEN, 80,
			Area.getAreaRelative(0, -1, 1, 2),
			new Area(1, 1), new Area(0.3, 0.55, 0.8, 0.85),
			new SolidBlock[] {SolidBlock.DRY_DIRT, SolidBlock.SAND}
	)
	;

	private final short id;
	private final String name;
	private final float[] texture;
	private final BlockCategory category;
	private final int durability;
	private final Area display;
	private final Area dimension;
	private final Area hitbox;
	private final Loot[] loot;
	private final SolidBlock[] placableOn;

	FurnitureBlock(int id, String name, float[] texture, BlockCategory category, int dur, Area dim, Area h) {
		this(id, name, texture, category, dur, new Area(1,1), dim, h, null);
	}
	
	FurnitureBlock(int id, String name, float[] texture, BlockCategory category, int dur, Area dim, Area h, Loot... l) {
		this(id, name, texture, category, dur, new Area(1,1), dim, h, null, l);
	}
	
	FurnitureBlock(int id, String name, float[] texture, BlockCategory category, int dur, Area display, Area dim, Area h) {
		this(id, name, texture, category, dur, display, dim, h, null);
	}
	
	FurnitureBlock(int id, String name, float[] texture, BlockCategory category, int dur, Area display, Area dim, Area h, SolidBlock[] placableOn, Loot... l) {
		this.id = (short) id;
		this.name = name;
		this.texture = texture;
		this.category = category;
		this.durability = dur;
		this.dimension = dim;
		this.display = display;
		this.hitbox = h;
		this.loot = l != null ? l : new Loot[]{new Loot(this)};
		this.placableOn = placableOn;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public short getID() {
		return id;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return texture;
	}
	
	@Override
	public boolean isFlamable() {
		return false;
	}
	
	@Override
	public SurfaceBlock burnedResult() {
		return null;
	}
	
	@Override
	public BlockCategory getCategory() {
		return category;
	}
	
	@Override
	public Area getHitbox() {
		return hitbox;
	}
	
	@Override
	public int getDurability() {
		return durability;
	}
	
	@Override
	public Area getDisplay() {
		return display;
	}
	
	@Override
	public Area getDimension() {
		return dimension;
	}
	
	@Override
	public boolean canPlaceOn(SolidBlock b) {
		if(placableOn == null)
			return !b.canBeReplaced();
		for(SolidBlock block : placableOn)
			if(block == b)
				return true;
		return false;
	}
	
	@Override
	public List<ItemStack> getDrops() {
		ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
		for (Loot l : loot) {
			ItemStack dropped = l.generateItem();
			if(dropped != null)
				drops.add(dropped);
		}
		return drops;
	}
}

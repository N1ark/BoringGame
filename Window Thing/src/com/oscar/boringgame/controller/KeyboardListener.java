package com.oscar.boringgame.controller;

public interface KeyboardListener {

	/**
	 * Called when the keyboard typed a character.
	 * @param c The typed character.
	 */
	public void typedChar(char c);
	
	/**
	 * Called when the keyboard pressed a key. This doesn't mean a key was typed; to get typed characters, use <code>typedChar(char)</code>.
	 * @param keyCode The key code of the pressed key.
	 */
	public void pressedKey(int keyCode);
	
	/**
	 * Called when the keyboard released a key.
	 * @param keyCode The key code of the released key.
	 */
	public void releasedKey(int keyCode);
}

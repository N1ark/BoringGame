package com.oscar.boringgame.controller;

public interface MouseListener {

	/**
	 * Called when a mouse button is pressed.
	 * @param button The pressed mouse button.
	 */
	public void pressedButton(int button);
	
	/**
	 * Called when a mouse button is released.
	 * @param button The released mouse button.
	 */
	public void releasedButton(int button);
	
}

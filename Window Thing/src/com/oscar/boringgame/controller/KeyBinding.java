package com.oscar.boringgame.controller;

import static org.lwjgl.glfw.GLFW.*;

import com.oscar.boringgame.renderer.Window;

public enum KeyBinding {
	LEFT_CLICK(GLFW_MOUSE_BUTTON_LEFT, true),
	RIGHT_CLICK(GLFW_MOUSE_BUTTON_RIGHT, true),
	BACKSPACE(GLFW_KEY_BACKSPACE, false),
	
	ATTACK(0, GLFW_MOUSE_BUTTON_LEFT, true), 
	PLACE(1, GLFW_MOUSE_BUTTON_RIGHT, true),

	WALK_FORWARD(2, GLFW_KEY_W, false), 
	WALK_LEFT(3, GLFW_KEY_A, false), 
	WALK_BACKWARDS(4, GLFW_KEY_S, false),
	WALK_RIGHT(5, GLFW_KEY_D, false),

	SPRINTING(6, GLFW_KEY_Q, false),
	CHEAT_SPRINT(7, GLFW_KEY_Z, false),
	CHEAT_RECEIVE_ITEMS(23, GLFW_KEY_EQUAL, false),

	HIGHLIGHT_FACED_BLOCK(8, GLFW_KEY_TAB, false), 
	ZOOM_FASTER(9, GLFW_KEY_LEFT_SHIFT, false), 
	SHOW_DEBUG(10, GLFW_KEY_F3, false),
	HIDE_UI(11, GLFW_KEY_F1, false),

	HOTBAR_1(12, GLFW_KEY_1, false), 
	HOTBAR_2(13, GLFW_KEY_2, false), 
	HOTBAR_3(14, GLFW_KEY_3, false),
	HOTBAR_4(15, GLFW_KEY_4, false),
	HOTBAR_5(16, GLFW_KEY_5, false), 
	HOTBAR_6(17, GLFW_KEY_6, false), 
	HOTBAR_7(18, GLFW_KEY_7, false), 
	HOTBAR_8(19, GLFW_KEY_8, false),
	HOTBAR_9(20, GLFW_KEY_9, false), 
	HOTBAR_10(21, GLFW_KEY_0, false),

	OPEN_SETTINGS(GLFW_KEY_ESCAPE, false),
	OPEN_INVENTORY(22, GLFW_KEY_E, false),
	
	INVENTORY_PICK_ITEM(GLFW_MOUSE_BUTTON_LEFT, true),
	INVENTORY_PICK_HALF_ITEM(GLFW_MOUSE_BUTTON_RIGHT, true),
	;

	private static Window usedWindow;

	/**
	 * Will set the used window for this enum to fetch the button presses/releases.
	 * @param window The used window.
	 */
	public static void setUsedWindow(Window window) {
		usedWindow = window;
	}

	private final short id;
	private final int defaultBinding;
	private final boolean defaultIsMouseBinding;
	private int binding;
	private boolean isMouseBinding;
	
	private boolean isFinal;

	private boolean isPressed;
	private boolean isFirstPressed;
	private boolean isFirstRelease;
	private boolean isActivated;

	/**
	 * A constructor used for a final KeyBinding. The ID will be set to -1.
	 * @param binding The ID of the binded key.
	 * @param mouseBinding If this is a mouse binding or a keyboard binding.
	 */
	KeyBinding(int binding, boolean mouseBinding){
		this(-1, binding, mouseBinding);
	}
	
	/**
	 * A constructor used for KeyBindings.
	 * @param id The KeyBinding's ID. If it is negative, the KeyBinding will vary.
	 * @param defaultBinding The keybinding set by default.
	 * @param defaultIsMouseBinding If, by default, this is a mouse binding.
	 */
	KeyBinding(int id, int defaultBinding, boolean defaultIsMouseBinding) {
		this.id = (short) id;
		this.defaultBinding = defaultBinding;
		this.defaultIsMouseBinding = defaultIsMouseBinding;

		this.binding = defaultBinding;
		this.isMouseBinding = defaultIsMouseBinding;

		this.isFinal = id == -1;
		
		this.isPressed = false;
		this.isFirstPressed = false;
		this.isActivated = false;
		this.isFirstRelease = false;
	}

	/**
	 * Will update all inputs for the keybindings.
	 */
	public static void update() {
		glfwPollEvents();

		for (KeyBinding key : values()) {
			boolean newState = key.isMouseBinding ? isButtonDown(key.binding) : isKeyDown(key.binding);

			key.isFirstPressed =  newState && !key.isPressed;
			key.isFirstRelease = !newState &&  key.isPressed;
			if (key.isFirstPressed)
				key.isActivated ^= true;

			key.isPressed = newState;
		}
	}
	
	/**
	 * @return This KeyBinding's ID.
	 */
	public short getID() {
		return id;
	}
	
	/**
	 * @return If this KeyBinding is final. If it is true, this KeyBinding cannot be edited by the user.
	 */
	public boolean isFinal() {
		return isFinal;
	}

	/**
	 * @return If this KeyBinding is currently pressed.
	 */
	public boolean isPressed() {
		return isPressed;
	}

	/**
	 * @return If this KeyBinding was just pressed "for the first time" (before this, it wasn't pressed). 
	 */
	public boolean isFirstPressed() {
		return isFirstPressed;
	}
	
	/**
	 * Will set the isFirstPressed value of this key to false. This should only be used when the input was already used and shouldn't be
	 * used again in the same frame.
	 */
	public void usedFirstPress() {
		isFirstPressed = false;
	}

	/**
	 * @return If this KeyBinding was just released "for the first time" (befor this, it was pressed).
	 */
	public boolean isFirstRelease() {
		return isFirstRelease;
	}
	
	/**
	 * Will set the isFirstRelease value of this key to false. This should only be used when the input was already used and shouldn't be
	 * used again in the same frame.
	 */
	public void usedFirstRelease() {
		isFirstRelease = false;
	}
	
	/**
	 * @return If this KeyBinding is activated. This is a state that will simply change between false and true
	 * at each first press.
	 */
	public boolean isActivated() {
		return isActivated;
	}

	/**
	 * @param key The keyboard key observed.
	 * @return If the given keyboard key is down.
	 */
	private static boolean isKeyDown(int key) {
		return glfwGetKey(usedWindow.getID(), key) == GLFW_PRESS;
	}

	/**
	 * @param key The keyboard key observed.
	 * @return If the given keyboard key is down.
	 */
	private static boolean isButtonDown(int key) {
		return glfwGetMouseButton(usedWindow.getID(), key) == GLFW_PRESS;
	}

	/**
	 * @return The binding associated to this value.
	 */
	public int getBinding() {
		return binding;
	}

	/**
	 * @return If this binding is associated with the mouse. If false, it is associated to the keyboard.
	 */
	public boolean isMouseBinding() {
		return isMouseBinding;
	}

	/**
	 * Will set the new binding associated to this value.
	 * @param binding The new key value.
	 * @param isMouseBinding If this binding is associated to the mouse or to the keyboard.
	 */
	public void setBinding(int binding, boolean isMouseBinding) {
		if(isFinal)
			throw new IllegalAccessError("Tried to change the keybinding " + this + ", but it is final!");
		
		this.binding = binding;
		this.isMouseBinding = isMouseBinding;
	}

	/**
	 * Will reset the key binding to its default value.
	 */
	public void resetBinding() {
		this.binding = defaultBinding;
		this.isMouseBinding = defaultIsMouseBinding;
	}
	
	/**
	 * @param id The id looked for.
	 * @return The Key associated to this id. If the key is final, there may be multiple keys associated to the same ID. If none is
	 * found, will return null.
	 */
	public static KeyBinding getByID(short id) {
		for(KeyBinding kb : values())
			if(kb.id == id)
				return kb;
		return null;
	}
}
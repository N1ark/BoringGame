package com.oscar.boringgame.controller;

import static com.oscar.boringgame.controller.KeyBinding.*;
import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWCursorEnterCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import com.oscar.boringgame.entity.DroppedItem;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.entity.Player.WalkState;
import com.oscar.boringgame.item.Inventory;
import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.main.GameState;
import com.oscar.boringgame.renderer.Camera;
import com.oscar.boringgame.renderer.Window;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.PVector;

public class Controller {
	private final Window window;
	private final PVector direction;
	private Camera camera;
	
	private final PVector mousePosition;
	private boolean mouseInWindow;
	
	private Inventory hoveredInventory;
	private ItemStack hoveredItem;
	private int hoveredSlot;
	
	private final List<KeyboardListener> keyboardListeners = new ArrayList<>();
	private final List<MouseListener> mouseListeners = new ArrayList<>();
	
	private static final KeyBinding[] hotbarKeys = new KeyBinding[]{
			HOTBAR_1,
			HOTBAR_2,
			HOTBAR_3,
			HOTBAR_4,
			HOTBAR_5,
			HOTBAR_6,
			HOTBAR_7,
			HOTBAR_8,
			HOTBAR_9,
			HOTBAR_10
	};

	/** Name for keys that aren't alphanumeric. */
	private static final Map<Integer, String> specialKeyNames = new HashMap<Integer, String>(){
		private static final long serialVersionUID = -3777476418869426139L;

		{
			put(GLFW_KEY_SPACE, "Space");
			//put(GLFW_KEY_ESCAPE, "Escape"); <--- The escape key shouldn't be allowed as a key binding, as it could lock the user in the settings menu.
			put(GLFW_KEY_TAB, "Tab");
			put(GLFW_KEY_BACKSPACE, "Backspace");
			put(GLFW_KEY_ENTER, "Enter");
			put(GLFW_KEY_CAPS_LOCK, "Caps Lock");
			put(GLFW_KEY_LEFT_SHIFT, "L Shift");
			put(GLFW_KEY_LEFT_CONTROL, "L Control");
			put(GLFW_KEY_LEFT_ALT, "L Alt");
			put(GLFW_KEY_LEFT_SUPER, "L Super");
			put(GLFW_KEY_RIGHT_SHIFT, "R Shift");
			put(GLFW_KEY_RIGHT_CONTROL, "R Control");
			put(GLFW_KEY_RIGHT_ALT, "R Alt");
			put(GLFW_KEY_RIGHT_SUPER, "R Super");
			put(GLFW_KEY_UP, "Up");
			put(GLFW_KEY_LEFT, "Left");
			put(GLFW_KEY_RIGHT, "Right");
			put(GLFW_KEY_DOWN, "Down");
			put(GLFW_KEY_F1, "F1");
			put(GLFW_KEY_F2, "F2");
			put(GLFW_KEY_F3, "F3");
			put(GLFW_KEY_F4, "F4");
			put(GLFW_KEY_F5, "F5");
			put(GLFW_KEY_F6, "F6");
			put(GLFW_KEY_F7, "F7");
			put(GLFW_KEY_F8, "F8");
			put(GLFW_KEY_F9, "F9");
			put(GLFW_KEY_F10, "F10");
			put(GLFW_KEY_F11, "F11");
			put(GLFW_KEY_F12, "F12");
			put(GLFW_KEY_F13, "F13");
			put(GLFW_KEY_F14, "F14");
			put(GLFW_KEY_F15, "F15");
			put(GLFW_KEY_F16, "F16");
			put(GLFW_KEY_F17, "F17");
			put(GLFW_KEY_F18, "F18");
			put(GLFW_KEY_F19, "F19");
			put(GLFW_KEY_F20, "F20");
			put(GLFW_KEY_F21, "F21");
			put(GLFW_KEY_F22, "F22");
			put(GLFW_KEY_F23, "F23");
			put(GLFW_KEY_F24, "F24");
			put(GLFW_KEY_F25, "F25");
		}
	};
	
	
	public Controller(Window window) {
		this(window, null);
	}
	
	public Controller(Window window, Camera c) {
		this.window = window;
		this.camera = c;
		this.direction = new PVector();
		this.mousePosition = new PVector();
		this.mouseInWindow = false;
		
		glfwSetScrollCallback(window.getID(), new GLFWScrollCallback() {
			@Override
			public void invoke(long window, double xOff, double yOff) {
				if(camera == null)
					return;
				float zoomingValue = (float) yOff;
				if(ZOOM_FASTER.isPressed())
					zoomingValue *= 10;
				camera.setZoom(camera.getZoom()-zoomingValue);
			}
		});
		glfwSetCursorEnterCallback(window.getID(), new GLFWCursorEnterCallback() {
			@Override
			public void invoke(long window, boolean entered) {
				mouseInWindow = entered;
			}
		});
		glfwSetCursorPosCallback(window.getID(), new GLFWCursorPosCallback() {
			@Override
			public void invoke(long window, double x, double y) {
				mousePosition.x = (float) x;
				mousePosition.y = (float) y;
			}
		});
		glfwSetCharCallback(window.getID(), new GLFWCharCallback() {
			@Override
			public void invoke(long window, int codepoint) {
				for(KeyboardListener kl : keyboardListeners)
					kl.typedChar((char) codepoint);
			}
		});
		glfwSetKeyCallback(window.getID(), new GLFWKeyCallback() {
			
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if(action == GLFW_PRESS)
					for(KeyboardListener kl : keyboardListeners)
						kl.pressedKey(key);
				else if(action == GLFW_RELEASE)
					for(KeyboardListener kl : keyboardListeners)
						kl.releasedKey(key);
			}
		});
		glfwSetMouseButtonCallback(window.getID(), new GLFWMouseButtonCallback() {
			@Override
			public void invoke(long window, int button, int action, int mods) {
				if(action == GLFW_PRESS)
					for(MouseListener ml : mouseListeners)
						ml.pressedButton(button);
				else if(action == GLFW_RELEASE)
					for(MouseListener ml : mouseListeners)
						ml.releasedButton(button);
			}
		});
		
		KeyBinding.setUsedWindow(window);
	}
	
	/**
	 * @param keyCode The key code that needs a user friendly name.
	 * @return A user friendly name for the key with the given key code.
	 */
	public static String getKeyName(int keyCode) {
		String name = glfwGetKeyName(keyCode, GLFW_KEY_UNKNOWN);
		return name == null ? specialKeyNames.get(keyCode) : name;
	}
	
	/**
	 * @param button The mouse button that needs a user friendly name.
	 * @return A user friendly name for the given mouse button.
	 */
	public static String getButtonName(int button) {
		switch(button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			return "Left Click";
		case GLFW_MOUSE_BUTTON_RIGHT:
			return "Right Click";
		case GLFW_MOUSE_BUTTON_MIDDLE:
			return "Middle Click";
		default:
			return "Mouse "+button;
		}
	}
	
	/**
	 * Will add a KeyboardListener to this controller, to receive any typed character or key press and release.
	 * @param listener The KeyboardListener to add.
	 */
	public void addKeyboardListener(KeyboardListener listener) {
		if(!keyboardListeners.contains(listener))
			keyboardListeners.add(listener);
	}
	
	/**
	 * Will remove the given listener from the list of KeyboardListeners.
	 * @param listener The listener to remove.
	 * @return If the listener was in the list before removing it.
	 */
	public boolean removeKeyboardListener(KeyboardListener listener) {
		return keyboardListeners.remove(listener);
	}
	
	/**
	 * Will add a MouseListener to this controller, to receive any mouse action.
	 * @param listener The MouseListener to add.
	 */
	public void addMouseListener(MouseListener listener) {
		if(!mouseListeners.contains(listener))
			mouseListeners.add(listener);
	}
	
	/**
	 * Will remove the given listener from the list of MouseListeners.
	 * @param listener The listener to remove.
	 * @return If the listener was in the list before removing it.
	 */
	public boolean removeMouseListener(MouseListener listener) {
		return mouseListeners.remove(listener);
	}
	
	/**
	 * Will set the camera used by this controller.
	 * @param camera The camera used, or null if no camera is currently active.
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	
	/**
	 * @return The ItemStack currently hovered by the user, or null if they aren't hovering any.
	 */
	public ItemStack getHoveredItemStack() {
		return hoveredItem;
	}
	
	/**
	 * Will set the ItemStack currently hovered by the user.
	 * @param item The hoveredItemStack.
	 */
	public void setHoveredItemStack(ItemStack item) {
		this.hoveredItem = item;
	}
	
	/**
	 * @return The slot currently hovered by the user in the hovered inventory, of -1 if they aren't hovering any.
	 */
	public int getHoveredSlot() {
		return hoveredSlot;
	}
	
	/**
	 * Will set the inventory slot currently hovered by the user.
	 * @param slot The hovered slot.
	 */
	public void setHoveredSlot(int slot) {
		this.hoveredSlot = slot;
	}
	
	/**
	 * @return The Inventory currently hovered by the user, of null if they aren't hovering any.
	 */
	public Inventory getHoveredInventory() {
		return hoveredInventory;
	}
	
	/**
	 * Will set the inventory currently hovered by the user.
	 * @param inv The hovered inventory.
	 */
	public void setHoveredInventory(Inventory inv) {
		this.hoveredInventory = inv;
	}
	
	/**
	 * Will take the user's inputs, and apply them to the Player's acceleration.
	 * @param p The Player to who the controls should be applied.
	 */
	public void applyControls(Player p) {
		// Speed / Movement
		direction.nullify();
		if(WALK_FORWARD.isPressed())
			direction.y -= 1;
		if(WALK_BACKWARDS.isPressed())
			direction.y += 1;
		if(WALK_LEFT.isPressed())
			direction.x -= 1;
		if(WALK_RIGHT.isPressed())
			direction.x += 1;
		direction.normalize();
		
		boolean sprinting = p.getWalkState() == WalkState.SPRINT && !direction.isNull() ? true : SPRINTING.isPressed();
		p.applyControlsToVelocity(direction, CHEAT_SPRINT.isPressed() ? 10 : 1, sprinting);
		
		// Hotbar slot
		for(int i = 0; i < 10; i++) {
			if(hotbarKeys[i].isPressed()) {
				p.setSlot(i);
				break;
			}
		}
		
		// Open inventory
		if(KeyBinding.OPEN_INVENTORY.isFirstPressed())
			p.setOpenedInventory(p.getCurrentInventory() == p.getInventory() ? null : p.getInventory());
		if(KeyBinding.OPEN_SETTINGS.isFirstPressed()) {
			if(p.getCurrentInventory() != null)
				p.setOpenedInventory(null);
			else
				GameState.openSettings();
		}
		if(KeyBinding.CHEAT_RECEIVE_ITEMS.isFirstPressed()) {
			for(Item item : Item.ALL_ITEMS) {
				p.getInventory().addItem(new ItemStack(item));
			}
		}
		
		// Inventory interaction
		UserAction action = ATTACK.isPressed() ? UserAction.ATTACK : PLACE.isPressed() ? UserAction.PLACE : UserAction.NONE;
		if(KeyBinding.INVENTORY_PICK_ITEM.isFirstPressed() || KeyBinding.INVENTORY_PICK_HALF_ITEM.isFirstPressed()) {
			boolean isFullPick = KeyBinding.INVENTORY_PICK_ITEM.isFirstPressed();
			ItemStack cursor = p.getCursor();
			
			if(hoveredSlot == -1 && cursor != null) { // Drop the item
				Area cameraView = camera.getFieldOfView(null, window);
				float xMouse = window.getView().mapX(mousePosition.x, cameraView);
				float yMouse = window.getView().mapY(mousePosition.y, cameraView);
				float angle = BoringMath.getRadAngle(p.getLocation().x, p.getLocation().y, xMouse, yMouse);
				float distance = BoringMath.distance(xMouse, yMouse, p.getLocation().x, p.getLocation().y);
				float speed = Math.min(0.5f * distance / 2.45f, 0.5f); // here 0.5 is the max speed, and 2.45 is the distance travelled by the item when at max speed.
				
				ItemStack droppedItem = new ItemStack(cursor.getType(), isFullPick ? cursor.getAmount() : 1);
				DroppedItem item = p.getWorld().dropItem(droppedItem, p.getLocation().clone());
				item.setVelocity((float) Math.cos(angle)*speed, (float) Math.sin(angle)*speed);
				
				if(cursor.getAmount() - droppedItem.getAmount() < 1)
					p.setCursor(null);
				else
					cursor.setAmount(cursor.getAmount()-droppedItem.getAmount());
				action = UserAction.NONE;
			}
			
			else if(cursor != null && hoveredItem != null && cursor.getType() == hoveredItem.getType()) { // Balance amounts
				int amount = hoveredItem.getAmount() + (isFullPick ? cursor.getAmount() : 1);
				int total = hoveredItem.getAmount() + cursor.getAmount();
				hoveredItem.setAmount(amount);
				
				int excess = total - hoveredItem.getAmount();
				if(excess < 1)
					p.setCursor(null);
				else
					cursor.setAmount(excess);
				action = UserAction.NONE;
			}
			
			else if(cursor != null && hoveredItem == null){ // Place 1 or all element(s)
				hoveredItem = new ItemStack(cursor.getType(), isFullPick ? cursor.getAmount() : 1);
				hoveredInventory.setItem(hoveredItem, hoveredSlot);
				
				if(cursor.getAmount() - hoveredItem.getAmount() < 1)
					p.setCursor(null);
				else
					cursor.setAmount(cursor.getAmount()-hoveredItem.getAmount());
				action = UserAction.NONE;
			} 
			
			else if(cursor == null && hoveredItem != null) { // Get half or all the elements
				cursor = new ItemStack(hoveredItem.getType(), isFullPick ? hoveredItem.getAmount() : hoveredItem.getAmount()/2);
				p.setCursor(cursor);
				hoveredInventory.addAmount(hoveredSlot, -cursor.getAmount());
				action = UserAction.NONE;
			} 
			
			else if(cursor != null && hoveredItem != null){ // Swap items
				p.setCursor(hoveredItem);
				hoveredInventory.setItem(cursor, hoveredSlot);
				action = UserAction.NONE;
			} 
			
			else if(p.getCurrentInventory() != null){ // No inventory interaction happened
				action = UserAction.NONE;
			}
		} else if(p.getCurrentInventory() != null){ // No inventory interaction happened
			action = UserAction.NONE;
		}
		
		p.setCurrentlyDoneAction(action);
	}

	/**
	 * @return The mouse position in pixels.
	 */
	public PVector getMousePosition() {
		return mousePosition;
	}
	
	/**
	 * @return If the mouse is in the window or not.
	 */
	public boolean isMouseInWindow() {
		return mouseInWindow;
	}
	
	/**
	 * An enum used to designate the different actions taken by the Player.
	 * @author oscar
	 *
	 */
	public static enum UserAction {
		ATTACK, PLACE, NONE;
	}
}
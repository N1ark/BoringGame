package com.oscar.boringgame.main;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.oscar.boringgame.files.OperatingSystem;
import com.oscar.boringgame.files.WorldIO;

public class Boot {
	
	/** If this version of the program is being exported as a JAR. */
	public static boolean IS_JAR = false;
	
	/** The start of a path, that can vary depending on wether this is a JAR file and on the OS. */
	public static String PATH_EXTRA = "";
	
	public static void main(String[] args) {
		GameState.checkValidityOfItems();
		GameState.checkValidityOfBiomes();
		GameState.checkValidityOfEntities();
		GameState.checkValidityOfKeybindings();
		
		if(!IS_JAR)
			WorldIO.setSavePath(Paths.get("C:", "Users", "oscar", "Documents", "BoringGame Files"));
		else {
			try {
				File file = new File(Boot.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile().toPath().resolve("BoringGame Save Files").toFile();
				if(!file.exists()) {
					Files.createDirectory(file.toPath());
					System.out.println("Created save folder \"" + file.getPath() + "\"");
				}
				WorldIO.setSavePath(file.toPath());
			} catch (URISyntaxException | IOException e) {
				System.err.println("Couldn't create save folder:");
				e.printStackTrace();
			}
		}
		
		if(IS_JAR && OperatingSystem.getCurrentOS() == OperatingSystem.WINDOWS)
			PATH_EXTRA = "src/";
		
		GameState.initialize();
	}
}

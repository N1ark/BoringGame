package com.oscar.boringgame.main;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DataFormatException;

import com.oscar.boringgame.controller.Controller;
import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.entity.Entity;
import com.oscar.boringgame.entity.EntityType;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.files.WorldIO;
import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.map.generator.DefaultWorldGenerator;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.renderer.Camera;
import com.oscar.boringgame.renderer.MenuRenderer;
import com.oscar.boringgame.renderer.RenderingInfo;
import com.oscar.boringgame.renderer.SettingsRenderer;
import com.oscar.boringgame.renderer.Window;
import com.oscar.boringgame.renderer.WorldRenderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Sync;
import com.oscar.boringgame.util.Location;

public enum GameState {
	/** When the user is in the main menu. */
	MAIN_MENU,
	/** When the user is playing in a world. */
	PLAYING,
	/** When the user is in the settings menu. */
	SETTINGS;
	
	private static boolean initialised = false;

	/** The current game state. */
	private static GameState currentState;
	
	/** The window on which everything is rendered. */
	private static Window window;
	
	/** The currently used World. This value can be set even if the current state isn't <code>PLAYING</code>, if the user
	 * is in a menu but can go back to the <code>PLAYING</code> state later. */
	private static World world;
	
	/** The player used by the user. This value can be set even if the current state isn't <code>PLAYING</code>, if the user
	 * is in a menu but can go back to the <code>PLAYING</code> state later. */
	private static Player player;
	
	/** The camera used by the user. This value can be set even if the current state isn't <code>PLAYING</code>, if the user
	 * is in a menu but can go back to the <code>PLAYING</code> state later. */
	private static Camera camera;
	
	/** The controller to receive inputs. */
	private static Controller controller;
	
	/**
	 * Will initialize everything needed for the game to properly work, by creating the window and loading textures.
	 */
	public static void initialize() {
		if(initialised)
			throw new IllegalAccessError("Tried initializing the game, but it already is initialized!");
		
		window = new Window("SuperBoringGame", false);
		window.setIcon();

		Artist.beginSession();
		Artist.setWindow(window);
		Artist.loadTextures();
		Entity.load();
		
		try {
			WorldIO.loadSettings();
		} catch (IllegalStateException | IOException | DataFormatException e) {
			System.err.println("Something went wrong when loading the settings.");
			e.printStackTrace();
		}
		KeyBinding.setUsedWindow(window);
		controller = new Controller(window);
		
		currentState = MAIN_MENU;
		
		while(!window.shouldClose())
			chooseNewLoop();
		
		if(world != null)
			closeAndSaveWorld();
		saveSettings();
		
		window.close();
	}
	
	/**
	 * @return The game's current state.
	 */
	public static GameState getCurrentState() {
		return currentState;
	}
	
	/**
	 * Will open the main menu.
	 */ 
	public static void openMainMenu() {
		currentState = MAIN_MENU;
	}
	
	/**
	 * Will try to save the current user's settings.
	 */
	public static void saveSettings() {
		try {
			WorldIO.saveSettings();
		} catch (IllegalStateException | IOException e) {
			System.err.println("Something went wrong when trying to save the settings.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Will try to save the current World (if it exists), and then remove the reference to the current world (and to the current Player).
	 */
	public static void closeAndSaveWorld() {
		if(world != null) {
			try {
				WorldIO.saveWorld(world);
			} catch (IllegalStateException | IOException e) {
				System.err.println("Something went wrong when trying to save the world.");
				e.printStackTrace();
			}
		}
		world = null;
		player = null;
		camera = null;
	}
	
	/**
	 * Will make the user play in the world with the given name by loading it from the files.
	 * @param name The world's name.
	 */
	public static void openWorld(String name) {
		try {
			world = WorldIO.openWorld(name);
			for(Entity e : world.getEntities()) {
				if(e instanceof Player) {
					player = (Player) e;
					break;
				}
			}
			if(player == null)
				player = new Player(new Location(0, 0, world));
			
		} catch (IllegalArgumentException | IllegalStateException | IOException | DataFormatException e) {
			System.err.println("Got an exception when trying to open world " + name + ":");
			System.err.println(e.getMessage());
			System.err.println("Will instead generate a world with this name!");

			createNewWorld(name);
		}
		currentState = PLAYING;
	}
	
	/**
	 * Will make the user play in a newly generated world with the given name.
	 * @param name The world's name.
	 */
	public static void createNewWorld(String name) {
		world = new World(name);
		player = new Player(new Location(0, 0, world));
		world.setGenerator(new DefaultWorldGenerator(world));
		currentState = PLAYING;
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					WorldIO.saveWorld(world);
				} catch (IllegalStateException | IOException e) {
					System.err.println("Error when trying to save created world:");
					e.printStackTrace();
				}
			}
		}).start();
	}
	

	/**
	 * Will open the settings menu to the user.
	 */
	public static void openSettings() {
		currentState = SETTINGS;
	}
	
	/**
	 * Will make the game return to the WorldRenderer if a world is loaded.
	 * @throws IllegalAccessError If no world is loaded.
	 */
	public static void goBackToWorld() {
		if(world != null)
			currentState = PLAYING;
		else
			throw new IllegalAccessError("Tried going back to world, but no world is currently loaded!");
	}
	
	/**
	 * Will start a new loop depending on the current game state. 
	 * <br><strong>This must only be used in the main thread.</strong>
	 */
	private static void chooseNewLoop() {
		switch(currentState) {
		case MAIN_MENU:
			mainMenuLoop();
			break;
		case PLAYING:
			worldLoop();
			break;
		case SETTINGS:
			settingsLoop();
			break;
		}
	}
	
	/**
	 * The game loop for when the user is in the main menu.
	 */
	private static void mainMenuLoop() {
		MenuRenderer menu = new MenuRenderer(window, controller);
	
		while(!window.shouldClose() && currentState == MAIN_MENU) {
			// Take inputs
			KeyBinding.update();
			
			// Render
			menu.drawFrame();
			window.update();

			// Wait
			Sync.sync(60);
		}
		menu.willClose();
	}
	
	/**
	 * The game loop for when the user is in the settings.
	 */
	private static void settingsLoop() {
		SettingsRenderer menu = new SettingsRenderer(window, controller);
		
		while(!window.shouldClose() && currentState == SETTINGS) {
			// Take inputs
			KeyBinding.update();

			// Render
			menu.drawFrame();
			window.update();

			// Wait
			Sync.sync(60);
		}
		menu.willClose();
	}
	
	/**
	 * The game loop for when the user is playing in the given World, with the given Player entity.
	 */
	private static void worldLoop() {
		world.setTickingEntity(player);
		world.startTicking();

		if(camera == null)
			camera = new Camera(player.getLocation().x, player.getLocation().y, 15);
		controller.setCamera(camera);
		camera.follow(player);
		
		// Setting up Renderer
		RenderingInfo debug = new RenderingInfo();
		WorldRenderer renderer = new WorldRenderer(world, camera, player, window, controller, debug);
		world.setTickingInfo(debug);
				
		long chrono = System.currentTimeMillis();
		int x = 0;
		
		while(!window.shouldClose() && currentState == PLAYING) {
			// Take inputs
			KeyBinding.update();
			
			// Tick/Update everything
			controller.applyControls(player);
			for(Entity e : world.getEntities())
				if(world.isInLoadedChunk(e.getLocation()))
					e.tick();
			for(ParticleEntity p : world.getParticles())
				p.tick();
			camera.tick();
			
			// Render
			renderer.drawFrame();
			window.update();

			// Extra + Wait
			x++;
			if(System.currentTimeMillis()-chrono > 1000) {
				debug.framesPerSecond = x;
				x = 0;
				chrono = System.currentTimeMillis();
			}

			Sync.sync(60);
		}
		world.stopTicking();
		
		renderer.willClose();
		controller.setCamera(null);
	}
	
	/**
	 * Will look through all items to see if there are duplicate ids.
	 * @throws IllegalStateException If two duplicate ids were found.
	 */
	public static void checkValidityOfItems() throws IllegalStateException {
		Map<Short, Item> ids = new HashMap<>();
		for(Item item : Item.ALL_ITEMS) {
			short id = item.getID();
			if(ids.containsKey(id))
				throw new IllegalStateException("Two items ("+ids.get(id).getName()+" and "+item.getName()+") have the same ID ("+id+"), something is wrong!");
			ids.put(id, item);
		}
		
		short max = -1;
		for(short id : ids.keySet())
			if(id > max)
				max = id;
		System.out.println("Loaded Items, the largest ID is " + max + " (" + ids.get(max).getName() + ")");
	}
	
	/**
	 * Will look through all biomes to see if there are duplicate ids.
	 * @throws IllegalStateException If two duplicate ids were found.
	 */
	public static void checkValidityOfBiomes() throws IllegalStateException {
		Map<Byte, Biome> ids = new HashMap<>();
		for(Biome biome : Biome.values()) {
			byte id = biome.getID();
			if(ids.containsKey(id))
				throw new IllegalStateException("Two biomes ("+ids.get(id).getName()+" and "+biome.getName()+") have the same ID ("+id+"), something is wrong!");
			ids.put(id, biome);
		}
		
		byte max = -1;
		for(byte id : ids.keySet())
			if(id > max)
				max = id;
		System.out.println("Loaded Biomes, the largest ID is " + max + " (" + ids.get(max).getName() + ")");
	}
	
	/**
	 * Will look through all biomes to see if there are duplicate ids.
	 * @throws IllegalStateException If two duplicate ids were found.
	 */
	public static void checkValidityOfEntities() throws IllegalStateException {
		Map<Short, EntityType> ids = new HashMap<>();
		for(EntityType entity : EntityType.values()) {
			short id = entity.getID();
			if(ids.containsKey(id))
				throw new IllegalStateException("Two entities ("+ids.get(id).name()+" and "+entity.name()+") have the same ID ("+id+"), something is wrong!");
			ids.put(id, entity);
		}
		
		short max = -1;
		for(short id : ids.keySet())
			if(id > max)
				max = id;
		System.out.println("Loaded Entities, the largest ID is " + max + " (" + ids.get(max).toString() + ")");
	}
	
	/**
	 * Will look through all keybindings to see if there are duplicate ids (final keybindings are ignored).
	 * @throws IllegalStateException If thw duplicate ids were found.
	 */
	public static void checkValidityOfKeybindings() throws IllegalStateException {
		Map<Short, KeyBinding> ids = new HashMap<>();
		for(KeyBinding key : KeyBinding.values()) {
			if(key.isFinal())
				continue;
			short id = key.getID();
			if(ids.containsKey(id))
				throw new IllegalStateException("Two keybindings ("+ids.get(id).name()+" and "+key.name()+") have the same ID ("+id+"), something is wrong!");
			ids.put(id, key);
		}
		
		short max = -1;
		for(short id : ids.keySet())
			if(id > max)
				max = id;
		System.out.println("Loaded KeyBindings, the largest ID is " + max + " (" + ids.get(max).name() + ")");
	}
}

package com.oscar.boringgame.main;

import static java.lang.Math.floor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.util.OpenSimplexNoise;
import com.oscar.boringgame.util.PVector;
import com.oscar.boringgame.util.PoissonDisk;

public class Test {

	public static void main(String[] args) {
		mapAnalysis(2500);
	}
	
	private static int colorFromBiome(Biome b) {
		float[] colors = b.getGrassColor();
		return new Color(colors[0], colors[1], colors[2]).getRGB();
	}
	
	private static boolean first = true;
	private static long chrono;
	
	private static void mapAnalysis(int size) {
		chrono("Declaring variables");

		final Font font = new Font("Comic Sans MS", Font.PLAIN, 50);
		long seed = new Random().nextLong();
		Random rand = new Random(seed);
		int 
			tempSize = 150,
			waterSize = 48,
			biomeSize = 10;
		OpenSimplexNoise
			temperature = new OpenSimplexNoise(rand.nextLong()),
			humidity = new OpenSimplexNoise(rand.nextLong()),
			water = new OpenSimplexNoise(rand.nextLong());
		
		PVector[][] biomeCenters = PoissonDisk.fastPoissonDiskSampling(seed, biomeSize, size);
		float poissonSize = PoissonDisk.gridUnitSize(biomeSize);
		int gridSize = PoissonDisk.gridSize(biomeSize, size);
		PVector[][] closestBiomes = new PVector[size][size];
		BufferedImage image = new BufferedImage(size * 3, size * 3, BufferedImage.TYPE_INT_RGB);
		HashMap<PVector, List<Float>> temperaturesPerBiome = new HashMap<>();
		HashMap<PVector, List<Float>> humidityPerBiome = new HashMap<>();
		
		chrono("Assign Voronoi");
		for(PVector[] p1 : biomeCenters)
			for(PVector p : p1) {
				temperaturesPerBiome.put(p, new ArrayList<>());
				humidityPerBiome.put(p, new ArrayList<>());
			}
		for(int x = 0; x < size; x++) {
			for(int y = 0; y < size; y++) {
				int gridX = (int) floor(x/poissonSize);
				int gridY = (int) floor(y/poissonSize);
				PVector best = null;
				float bestDist = Float.MAX_VALUE;
				for(int a = 0; a < 9; a++) {
					int nGridX = gridX - 1 + a/3;
					int nGridY = gridY - 1 + a%3;
					if(nGridX < 0 || gridSize <= nGridX || nGridY < 0 || gridSize <= nGridY)
						continue;
					
					PVector point = biomeCenters[nGridX][nGridY];
					if(point == null)
						continue;
					float dist = point.dist(x, y);
					if(dist < bestDist) {
						bestDist = dist;
						best = point;
					}
				}
				closestBiomes[x][y] = best;
			}
		}
		
		chrono("Drawing biome stats");
		{
			Graphics gra = image.getGraphics();
			int y = 50;
			gra.setFont(font);
			for(Biome biome : Biome.values()) {
				gra.drawString(biome.getName() + "-> H = " + biome.getHumidity() + "% / T = " + biome.getTemperature() + "°C", 10, y);
				y += 60;
			}
			y = 0;
			for(Biome biome : Biome.values()) {
				int col = colorFromBiome(biome);
				for(int x = size-100; x < size; x++)
					for(int yp = y; yp < y+60; yp++)
						image.setRGB(x, yp, col);
				y += 60;
			}
		}
		
		chrono("Biome Scheme");
		{
			for(int x = 0; x < size; x++) {
				float temp = ((float) x/size)*30;
				for(int y = 0; y < size; y++) {
					float hum = ((float) y/size)*100;
					Biome b = Biome.biomeFor(temp, hum);
					image.setRGB(x+size, y, colorFromBiome(b));
				}
			}
			
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.BLACK);
			gra.drawString("0/0", size, 50);
			gra.drawString("30°C", size*2-120, 50);
			gra.drawString("100% Hum", size, size-40);
		}
		
		chrono("Random Temperature");
		{
			for(int x = 0; x < size; x++) {
				for(int y = 0; y < size; y++) {
					float temp = (float) temperature.eval((float) x/tempSize, (float) y/tempSize);
					int col = Color.HSBtoRGB((1+temp)*0.23f+0.6f, 1, 1);
					image.setRGB(x, y+size, col);
				}
			}
			
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.BLACK);
			gra.drawString("Temperature map", 80, 50+size);
			int minTemp = Color.HSBtoRGB(0.6f, 1, 1);
			int maxTemp = Color.HSBtoRGB(2*0.23f+0.6f, 1, 1);
			for(int x = 0; x < 50; x++)
				for(int y = 0; y < 50; y++) {
					image.setRGB(x, y+size, minTemp);
					image.setRGB(x, y+size+50, maxTemp);
				}
			
			gra.drawString("0°C", 2, size+40);
			gra.drawString("30°C", 2, size+90);
		}
		
		chrono("Random Humidity");
		{
			for(int x = 0; x < size; x++) {
				for(int y = 0; y < size; y++) {
					float humi = (float) humidity.eval((float) x/tempSize, (float) y/tempSize);
					int col = Color.HSBtoRGB((1+humi)*0.23f+0.15f, 1, 1);
					image.setRGB(x+size, y+size, col);
				}
			}
			
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.BLACK);
			gra.drawString("Humidity map", size+80, 50+size);
			int minHum = Color.HSBtoRGB(0.14f, 1, 1);
			int maxHum = Color.HSBtoRGB(2*0.23f+0.14f, 1, 1);
			for(int x = 0; x < 50; x++)
				for(int y = 0; y < 50; y++) {
					image.setRGB(x+size, y+size, minHum);
					image.setRGB(x+size, y+size+50, maxHum);
				}
			
			gra.drawString("0%", 2+size, size+40);
			gra.drawString("100%", 2+size, size+90);
		}
		
		chrono("Water Spots");
		{
			for(int x = 0; x < size; x++) {
				for(int y = 0; y < size; y++) {
					float waterValue = (float) water.eval((float) x/waterSize, (float) y/waterSize);
					//waterValue -= (float) temperature.eval((float) x/tempSize, (float) y/tempSize); 
					if(waterValue < 0.4)
						continue;
					int col = Color.HSBtoRGB(0.65f, 1, 0.4f+waterValue);
					image.setRGB(x+size*2, y+size, col);
				}
			}
			
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.WHITE);
			gra.drawString("Water level map", size*2+80, 50+size);
		}
		
		chrono("Biomes");
		{
			for(int x = 0; x < size; x++) {
				for(int y = 0; y < size; y++) {
					float temp = (float) temperature.eval((float) x/tempSize, (float) y/tempSize);
					float humi = (float) humidity.eval((float) x/tempSize, (float) y/tempSize);
					temp = Biome.convertTemperature(temp);
					humi = Biome.convertHumidity(humi);
					Biome b = Biome.biomeFor(temp, humi);
					image.setRGB(x, y+size*2, colorFromBiome(b));
					
					PVector closest = closestBiomes[x][y];
					temperaturesPerBiome.get(closest).add(temp);
					humidityPerBiome.get(closest).add(humi);
				}
			}
			
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.BLACK);
			gra.drawString("Biomes", 80, 50+size*2);
		}
		
		for(PVector[] array : biomeCenters) 
			for(PVector p : array) {
				int count = 0;
				float total = 0;
				for(Float f : temperaturesPerBiome.get(p)) {
					count++;
					total += f;
				}
				total /= count;
				temperaturesPerBiome.get(p).clear();
				temperaturesPerBiome.get(p).add(total);
				
				count = 0;
				total = 0;
				for(Float f : humidityPerBiome.get(p)) {
					count++;
					total += f;
				}
				total /= count;
				humidityPerBiome.get(p).clear();
				humidityPerBiome.get(p).add(total);
			}
		
		chrono("Voronoi Cells");
		{
			HashMap<PVector, Integer> colorsPerCell = new HashMap<>();
			for(PVector[] array : biomeCenters) 
				for(PVector p : array) {
					if(p == null)
						continue;
					colorsPerCell.put(p, Color.HSBtoRGB(rand.nextFloat(), rand.nextFloat()*0.5f+0.5f, rand.nextFloat()*0.5f+0.5f));
				}
			
			for(int x = 0; x < size; x++) {
				for(int y = 0; y < size; y++) {
					image.setRGB(x+size, y+size*2, colorsPerCell.get(closestBiomes[x][y]));
				}
			}
			
			for(PVector[] array : biomeCenters) 
				for(PVector p : array) {
					if(p == null)
						continue;
					image.setRGB((int) floor(p.x)+size, (int) floor(p.y)+size*2, 0);
				}
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.BLACK);
			gra.drawString("Voronoi Cells", size+80, 50+size*2);
		}
		
		chrono("Final Result");
		{
			for(int x = 0; x < size; x++) {
				for(int y = 0; y < size; y++) {
					PVector closest = closestBiomes[x][y];
					Biome b = Biome.biomeFor(temperaturesPerBiome.get(closest).get(0), humidityPerBiome.get(closest).get(0));
					image.setRGB(x+size*2, y+size*2, colorFromBiome(b));
				}
			}
			
			Graphics gra = image.getGraphics();
			gra.setFont(font);
			gra.setColor(Color.BLACK);
			gra.drawString("Final", size*2+80, 50+size*2);
		}
		
		chrono("Exporting");
		try {
			ImageIO.write(image, "png", new File("test.png"));
		} catch (IOException e) {/**/}
		chrono("Done!");
	}
	
	private static void chrono(String text) {
		if(first)
			first = false;
		else
			System.out.println((System.currentTimeMillis()-chrono) + "ms");
		chrono = System.currentTimeMillis();
		System.out.print(text + " - ");
	}
}

package com.oscar.boringgame.item;

import static com.oscar.boringgame.item.Item.textureArray;

public enum CommonItem implements Item {
	ROCK(
			1,
			"Rock", textureArray(0, 1)
	),
	COAL(
			2,
			"Coal", textureArray(1, 1)
	),
	IRON_NUGGET(
			3,
			"Iron Nugget", textureArray(2, 1)
	),
	IRON_INGOT(
			4,
			"Iron Ingot", textureArray(3, 1)
	),
	STRAWBERRY(
			5,
			"Strawberry", textureArray(0, 2), 10
	),
	BLUEBERRY(
			6,
			"Blueberry", textureArray(1, 2), 8
	),
	CARROT(
			7,
			"Carrot", textureArray(2, 2), 25
	),
	LYCHEE(
			8,
			"Lychee", textureArray(4, 2), 18
	),
	HIBISCUS_FLOWER(
			9,
			"Hibiscus Flower", textureArray(3, 2)
	),
	DYNAMITE(
			49,
			"Dynamite", textureArray(3, 0)
	)
	;

	private final short id;
	private final String name;
	private final float[] texture;
	private final int hunger;
	 
	CommonItem(int id, String name, float[] texture){
		this(id, name, texture, -1);
	}
	
	// Here, id can be given as an int to avoid having lots of casting in constructors.
	CommonItem(int id, String name, float[] texture, int hunger){
		this.name = name;
		this.id = (short) id;
		this.texture = texture;
		this.hunger = hunger;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return texture;
	}
	
	@Override
	public int getMaxStackSize() {
		return 50;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public short getID() {
		return id;
	}
	
	/**
	 * @return The hunger restored when eating this Item. If it cannot be eaten, this will return -1.
	 */
	public int getEatingValue() {
		return hunger;
	}
	
	/**
	 * @return If this Item can be eaten.
	 */
	public boolean canBeEaten() {
		return hunger < 0;
	}
}

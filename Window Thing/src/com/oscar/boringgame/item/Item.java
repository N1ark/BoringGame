	package com.oscar.boringgame.item;

import java.nio.ByteBuffer;

import com.oscar.boringgame.blocks.ComplexFurnitureBlock;
import com.oscar.boringgame.blocks.CosmeticBlock;
import com.oscar.boringgame.blocks.FurnitureBlock;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlock;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.util.BoringUtil;

public interface Item {
	
	/**
	 * All the existing items in the game
	 */
	public static final Item[] ALL_ITEMS = getAllItems();
	
	/**
	 * A method that will generate an array with all the Items in the game. Even if this method can be used, it is recommended
	 * to directly use <code>Item.ALL_ITEMS</code>.
	 * @return All the existing items in the game.
	 */
	public static Item[] getAllItems() {
		return BoringUtil.concatAll(
				new Item[0], // needs this otherwise won't work, because it will try to create an array of SolidBlock.
				SolidBlock.values(), 
				CosmeticBlock.values(),
				FurnitureBlock.values(),
				DataHolderBlock.values,
				ComplexFurnitureBlock.values(),
				CommonItem.values(),
				SpecialItem.values()
		);
	}
	
	/**
	 * @return The name of the Item.
	 */
	String getName();
	
	/**
	 * @return The max amount of times this Item can be stacked on itself.
	 */
	int getMaxStackSize();
	
	/**
	 * @param frame The "index" of the frame (aka the number of frames drawn before this one). This is mainly used for animations.
	 * @return The coordinates of the texture on the texture atlas.
	 * <br>Indexes are: 
	 * <br> - 0: X1
	 * <br> - 1: Y1
	 * <br> - 2: X2
	 * <br> - 3: Y2
	 */
	float[] getTextureCoordinates(int frame);
	
	/**
	 * @return The coordinates of the texture on the texture atlas. This doesn't take animation into account, so it is equivalent
	 * to calling getTextureCoordinates(0);
	 * <br>Indexes are: 
	 * <br> - 0: X1
	 * <br> - 1: Y1
	 * <br> - 2: X2
	 * <br> - 3: Y2
	 */
	default float[] getTextureCoordinates() {
		return getTextureCoordinates(0);
	}
	
	/**
	 * @return This item's ID.
	 */
	short getID();
	
	/**
	 * @return The amount of bytes needed to represent this Item.
	 */
	default int neededBytes() {
		return Short.BYTES;
	}
	
	/**
	 * @return This Item as a byte array.
	 */
	default byte[] toBytes() {
		return BoringUtil.toBytes(getID());
	}
	
	/**
	 * Will extract an id from the two first bytes of the given data, and find what Item has the same id.
	 * @param bytes The bytes to get the data.
	 * @return The Item represented by these bytes. If no Item is found, will return null.
	 */
	public static Item getItemFrom(ByteBuffer bytes) {
		if(bytes.remaining() < Short.BYTES)
			throw new IllegalArgumentException("The given array of bytes ("+bytes.capacity()+") must have a length of at least Short.BYTES ("+Short.BYTES+")");
		
		short id = bytes.getShort();
		if(id == 0)
			return AIR.AIR;

		Item item = null;
		for(Item i : ALL_ITEMS)
			if(i.getID() == id)
				item = i;
		
		if(item == null)
			System.err.println("Tried looking for an item with the ID " + id + ", but found nothing!");
		return item;
	}
	
	/**
	 * Will extract an id from the two first bytes of the given data, and find what Item has the same id. If the loaded item needs to get more
	 * data from the bytes, it will do so, possibly using the world given to load some variables.
	 * @param bytes The bytes to get the data.
	 * @param world The world that can possibly be used to get more data.
	 * @return The Item represented by these bytes. If no item is found, will return null.
	 */
	public static Item getItemFrom(ByteBuffer bytes, World world) {
		Item item = getItemFrom(bytes);
		if(item instanceof DataHolderBlock)
			item = ((DataHolderBlock) item).fromBytes(world, bytes);
		return item;
	}
	
	/**
	 * Creates an array for the texture coordinates at the specified coordinates in the atlas.
	 * @param x The x coordinate, in items.
	 * @param y The y coordinate, in items, <strong>starting from the bottom</strong>.
	 * @return The array used for an item with a size of 1 at the specified coordinates.
	 */
	public static float[] textureArray(int x, int y) {
		float halfPixel = 0.0001f; // we use this to avoid textureBleeding, because floating point operations are shit :D
		return new float[] { x * (10f/256) + halfPixel, 1 - (y+1f)*(10f/256) + halfPixel, (x+1)*(10f/256) - halfPixel, 1 - y*(10f/256) - halfPixel};
	}
}

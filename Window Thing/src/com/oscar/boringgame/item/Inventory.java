package com.oscar.boringgame.item;

import java.nio.ByteBuffer;

import com.oscar.boringgame.util.BoringUtil;

public class Inventory {
	private final InventoryType type;
	private ItemStack[] content;

	public Inventory() {
		this(InventoryType.PLAYER);
	}

	Inventory(InventoryType i) {
		type = i;
		content = new ItemStack[i.size];
	}

	/**
	 * @return This inventory's type.
	 */
	public InventoryType getType() {
		return type;
	}

	/**
	 * @return A copy of the array with content of this Inventory.
	 */
	public ItemStack[] getContent() {
		return content.clone();
	}

	/**
	 * @param slot The slot to look at.
	 * @return The item at the specified slot. Null if there is no Item or if the slot isn't contained in the inventory.
	 */
	public ItemStack getItem(int slot) {
		if (slot < 0 || content.length <= slot)
			return null;
		return content[slot];
	}

	/**
	 * Will set the Item at the given slot.
	 * @param item The Item to set.
	 * @param slot The slot at which the Item will be set. If this slot isn't contained in the inventory, nothing will happen.
	 */
	public void setItem(ItemStack item, int slot) {
		if (0 <= slot && slot < content.length)
			content[slot] = item;
	}
	
	/**
	 * Will change the amount of items in the given slot, by adding the given value to the ItemStack at the given slot.
	 * @param slot The slot of the modified ItemStack.
	 * @param value The value to add to the ItemStack's amount. This value can be negative. If the ItemStack's amount is smaller
	 * than one, the ItemStack is replaced with null.
	 */
	public void addAmount(int slot, int value) {
		ItemStack it = content[slot];
		if(it.getAmount()+value < 1)
			content[slot] = null;
		else
			it.setAmount(it.getAmount()+value);
	}

	/**
	 * @param i The Item type to look for.
	 * @return If this inventory contains at least one ItemStack instance with the given Item type.
	 */
	public boolean has(Item i) {
		for (ItemStack it : content)
			if (it != null && it.getType() == i)
				return true;
		return false;
	}

	/**
	 * Will remove once the given Item from the inventory.
	 * @param i The Item to remove once.
	 */
	public void remove(Item i) {
		for (int j = 0; j < content.length; j++)
			if (content[j] != null && content[j].getType() == i) {
				if (content[j].getAmount() == 1)
					content[j] = null;
				else
					content[j].setAmount(content[j].getAmount() - 1);
				break;
			}
	}

	/**
	 * Will remove all ItemStacks that have this Item from the inventory.
	 * @param i The Item to get rid of.
	 */
	public void clear(Item i) {
		for (int j = 0; j < content.length; j++)
			if (content[j].getType() == i)
				content[j] = null;
	}

	/**
	 * Will add the given ItemStack to the Inventory, by first trying to add it to an already existing ItemStack, and if all of the Items
	 * can't be dispatched in ItemStack, will then place whats remaining in a new slot.
	 * @param item The Item to place in the inventory.
	 */
	public void addItem(ItemStack item) {
		int amount = item.getAmount();
		for (ItemStack i : content) {
			if (i != null && i.getType() == item.getType()) {
				int added = i.getAmount();
				i.setAmount(i.getAmount() + amount);
				added = i.getAmount() - added;
				amount -= added;
				if (amount < 1)
					return;
			}
		}
		for (int x = 0; x < content.length; x++) {
			if (content[x] == null) {
				content[x] = new ItemStack(item.getType(), amount);
				amount -= content[x].getAmount();
				if (amount < 1)
					return;
			}
		}
	}

	/**
	 * Will loop through the Inventory and see if it has enough space to get this item.
	 * @param add The ItemStack to add.
	 * @return If it has space for it.
	 */
	public boolean hasSpace(ItemStack add) {
		int amount = add.getAmount();
		for (ItemStack it : content) {
			if (it == null)
				return true;
			else if (it.getType() == add.getType()) {
				amount -= Math.max(1, Math.min(add.getAmount() + it.getAmount(), it.getType().getMaxStackSize()))
						- it.getAmount();
				if (amount < 1)
					return true;
			}
		}

		return false;
	}
	
	/**
	 * @return The bytes to represent this Inventory.
	 */
	public byte[] toBytes() {
		byte[] emptySlot = new byte[] {Byte.MIN_VALUE, Byte.MIN_VALUE};
		byte[] bytes = new byte[1];
		
		bytes[0] = type.getID();
		for(ItemStack item : content) {
			byte[] itemBytes = item == null ? emptySlot : item.toBytes();
			bytes = BoringUtil.concatAll(bytes, itemBytes);
		}
		
		return bytes;
	}
	
	/**
	 * Will create an Inventory corresponding to the given bytes.
	 * @param bytes The bytes representing this Inventory.
	 */
	public Inventory(ByteBuffer bytes) {
		byte typeID = bytes.get();
		InventoryType type = null;
		for(InventoryType t : InventoryType.values()) {
			if(t.id == typeID) {
				type = t;
				break;
			}
		}
		if(type == null)
			throw new IllegalArgumentException("The given byte array has an inventory id ("+ typeID +") that doesn't exist!");
		
		this.type = type;
		this.content = new ItemStack[type.size];
		
		for(int slot = 0; slot < type.size; slot++) {
			if(bytes.get(bytes.position()) == Byte.MIN_VALUE && bytes.get(bytes.position()+1) == Byte.MIN_VALUE) {
				bytes.position(bytes.position()+2);
				continue;
			}
			content[slot] = new ItemStack(bytes);
		}
		
	}

	public static enum InventoryType {
		PLAYER(
				0, 70
		), 
		CHEST(
				1, 30
		);
		
		private final byte id;
		private final int size;

		InventoryType(int id, int size) {
			this.id = (byte) id;
			this.size = size;
		}

		/**
		 * @return The size of this inventory type.
		 */
		public int getSize() {
			return size;
		}
		
		/**
		 * @return This inventory type's ID.
		 */
		public byte getID() {
			return id;
		}
	}
}

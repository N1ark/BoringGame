package com.oscar.boringgame.item;

public class ItemData {
	public static enum ItemDataType{
		MINING_BONUS("Mining Bonus"),
		;
		private final String name;
		ItemDataType(String n){
			name = n;
		}
		public String getName() {
			return name;
		}
	}
	
	private final ItemDataType type;
	private final float value;
	
	public ItemData(ItemDataType i, float value) {
		 type = i;
		 this.value = value;
	}
	
	public ItemDataType getType() {
		return type;
	}
	public float getValue() {
		return value;
	}
}

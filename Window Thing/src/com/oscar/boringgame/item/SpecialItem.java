package com.oscar.boringgame.item;

import static com.oscar.boringgame.item.Item.textureArray;

import com.oscar.boringgame.item.ItemData.ItemDataType;

public enum SpecialItem implements Item {

	SHOVEL(
			10,
			"Shovel", textureArray(0, 0), 
			new ItemData(ItemDataType.MINING_BONUS, 2f)
	),
	PICKAXE(
			11,
			"Pickaxe", textureArray(1, 0),
			new ItemData(ItemDataType.MINING_BONUS, 2f)
	),
	AXE(
			12,
			"Axe", textureArray(2, 0),
			new ItemData(ItemDataType.MINING_BONUS, 2f)
	),
	;

	private final short id;
	private final String name;
	private final float[] texture;
	private final ItemData[] data;
	
	SpecialItem(int id, String n, float[] texture, ItemData... d){
		this.id = (short) id;
		this.name = n;
		this.texture = texture;
		this.data = d;
	}
	
	@Override
	public float[] getTextureCoordinates(int frame) {
		return texture;
	}

	@Override
	public int getMaxStackSize() {
		return 1;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public short getID() {
		return id;
	}
	
	/**
	 * @return The array containing all the data about the Item.
	 */
	public ItemData[] getData() {
		return data;
	}
	
	/**
	 * This method can be used to get the specific ItemData corresponding to the given DataType.
	 * @param i The ItemDataType that's searched.
	 * @return If there is one, the corresponding ItemData. Otherwise, null.
	 */
	public ItemData getData(ItemDataType i) {
		for(ItemData d : data)
			if(d.getType() == i)
				return d;
		return null;
	}
}

package com.oscar.boringgame.item;

import java.nio.ByteBuffer;

import com.oscar.boringgame.util.BoringUtil;

public class ItemStack {
	private Item type;
	private short amount;
	
	/**
	 * Will create an ItemStack with this item and an amount of 1.
	 * @param type The ItemStack's type.
	 */
	public ItemStack(Item type) {
		this(type, 1);
	}
	
	/**
	 * Will create an ItemStack with this item and the given amount.
	 * @param type The ItemStack's type.
	 * @param amount The amount of items contained. If this value is smaller than 1 or larger than the max stack size of the Item, it will be changed.
	 */
	public ItemStack(Item type, int amount) {
		this.type = type;
		this.amount = (short) Math.max(1, Math.min(amount, type.getMaxStackSize()));
	}
	
	/**
	 * Will create an ItemStack, represented by the given bytes.
	 * @param bytes The bytes representing this ItemStack.
	 */
	public ItemStack(ByteBuffer bytes) {
		if(bytes.remaining() < Short.BYTES * 2)
			throw new IllegalArgumentException("The given byte array is too small ("+bytes.remaining()+") to possibly contain an ItemStack!");
		
		amount = bytes.getShort();
		type = Item.getItemFrom(bytes);
	}
	
	/**
	 * @return This ItemStack's item type.
	 */
	public Item getType() {
		return type;
	}
	
	/**
	 * @return The amount of items contained in this ItemStack.
	 */
	public int getAmount() {
		return amount;
	}
	
	/**
	 * Will set the amount of items in this ItemStack.
	 * @param value The new amount. If it is below 1 or over this type's max stack size, it will be changed.
	 */
	public void setAmount(int value) {
		amount = (short) Math.max(1, Math.min(value, type.getMaxStackSize()));
	}
	
	/**
	 * Will set this ItemStack's item type.
	 * @param i The new type.
	 */
	public void setType(Item i) {
		type = i;
	}
	
	/**
	 * @return The amount of bytes necessary to save this ItemStack.
	 */
	public int neededBytes() {
		return Short.BYTES + type.neededBytes();
	}
	
	/**
	 * @return The bytes to represent this ItemStack.
	 */
	public byte[] toBytes() {
		byte[] amountBytes = BoringUtil.toBytes(amount);
		byte[] itemBytes = type.toBytes();
		return BoringUtil.concatAll(amountBytes, itemBytes);
	}
}

package com.oscar.boringgame.files;

public interface FileNameValidator {
	
	/** All the characters that can be in a file name, but that cannot be at the end of it. */
	public static final char[] forbiddenSuffixes = new char[] {'.', ' '};
	
	/** All the characters that cannot be used in a file name, whatever their position. */
	public static final char[] forbiddenChars = new char[] {'/', '<', '>', ':', '"', '\\', '|', '?', '*'};
	
	/** All the file names that are not allowed. This list is only true for Windows, but to make sure that files are compatible
	 * between all operating systems, I would recommend forbiding them regardless of the file system. */
	public static final String[] forbiddenNames = new String[] {
			"CON", "PRN", "AUX", "NUL", 
			"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
			"LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
	};
	
	/**
	 * @param c The character to check.
	 * @return If the given character is allowed for file names. This doesn't take into account forbidden file name suffixes.
	 */
	public static boolean isAllowed(char c) {
		for(char d : forbiddenChars)
			if(d == c)
				return false;
		return true;
	}
	
	/**
	 * Will use all the registered invalid characters, and check if the given file name can be considered valid.
	 * @param name The name to check.
	 * @return If the given file name can be used.
	 */
	public static boolean isValidFileName(String name) {
		for(char c : forbiddenChars)
			if(name.contains(Character.toString(c)))
				return false;
		
		for(char c : forbiddenSuffixes)
			if(name.endsWith(Character.toString(c)))
				return false;
		
		for(String s : forbiddenNames)
			if(name.equals(s))
				return false;
		
		return true;
	}
	
	/**
	 * Will make sure the given String can be used as a file name.
	 * @param name The file name to make valid.
	 * @return The valid file name derived from the given file name. If the given file name is valid, this function should return name.
	 */
	public static String makeValid(String name) {
		for(char c : forbiddenChars)
			name.replace(c, '_');
		
		for(char c : forbiddenSuffixes)
			if(name.endsWith(Character.toString(c))) {
				name += '_';
				break;
			}
		
		for(String s : forbiddenNames)
			if(name.equals(s)) {
				name += '_';
				break;
			}
		
		return name;
	}
	
}

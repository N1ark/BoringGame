package com.oscar.boringgame.files;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.entity.Entity;
import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.map.Chunk;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.map.generator.WorldGenerator;
import com.oscar.boringgame.util.BoringUtil;

public class WorldIO {
	
	private WorldIO() {}
	
	/** The version of the currently used converter for the world file. */
	private static final byte worldFileVersion = 1;
	
	/** The version of the currently used converter for the chunk file. */
	private static final byte chunkFileVersion = 1;
	
	/** The version of the currently used converter for the settings file. */
	private static final byte settingsFileVersion = 1;
	
	/** The name of the file that saves general data about the world. */
	private static final String worldFileName = "info";
	
	/** The extension used for the file that saves general data about the world. */
	private static final String worldFileExtension = ".wrld";
	
	/** The extension used for the file that saves data about a single chunk. */
	private static final String chunkFileExtension = ".chnk";
	
	/** The name of the file that saves general data about the game, such as keybindings. */
	private static final String settingsFileName = "settings";
	
	/** The extension used for the file that saves general data about the game. */
	private static final String settingsFileExtension = ".optn";

	/** An array of bytes used to indicate a separation. */
	private static final byte[] separator = new byte[] {Byte.MIN_VALUE, Byte.MIN_VALUE};
	
	/** The path were the files are saved. */
	private static Path savePath;
	
	/**
	 * Will set the save path, were files will be saved.
	 * @param path The new used path.
	 */
	public static void setSavePath(Path path) {
		if(!Files.exists(path)) {
			System.err.println("The given path for the save path (" + path.toString() + ") doesn't exist!");
			return;
		}
		savePath = path;
		System.out.println("Set save path to \"" + path.toString() + "\"" );
	}
	
	/**
	 * Will save a file that contains all the the user's settings, that should be kept between game launches.
	 * @throws IllegalStateException If the save path wasn't set.
	 * @throws IOException If there was an error when creating the file or when writing to it.
	 */
	public static void saveSettings() throws IOException, IllegalStateException{
		System.out.print("Saving settings... ");
		if(savePath == null)
			throw new IllegalStateException("The save path wasn't set!");
		
		long chrono = System.currentTimeMillis();
		Path settingsFile = savePath.resolve(settingsFileName + settingsFileExtension);
		writeFile(settingsFile, generateSettingsFile());
		
		System.out.println("Done! Took " + (System.currentTimeMillis()-chrono) + "ms");		
	}
	
	/**
	 * Will generate the bytes needed to represent a file with all the keybindings.
	 * @return The generated bytes.
	 */
	private static byte[] generateSettingsFile() {
		byte[] bytes = new byte[] {settingsFileVersion};
		
		for(KeyBinding binding : KeyBinding.values()) {
			if(binding.isFinal())
				continue;
			
			byte[] idBytes = BoringUtil.toBytes(binding.getID());
			byte[] isMouseBytes = BoringUtil.toBytes(binding.isMouseBinding());
			byte[] bindingBytes = BoringUtil.toBytes(binding.getBinding());
			bytes = BoringUtil.concatAll(bytes, idBytes, isMouseBytes, bindingBytes);
		}
		
		return bytes;
	}
	
	/**
	 * Will look in the save file if it exists, and try to find all valid sub-folders that represent worlds.
	 * @return The list with the found worlds. If an exception occurs, this will return an empty list and will simply print the exception.
	 */
	public static List<String> getExistingSavedWorlds() {
		List<String> list = new ArrayList<>();
		if(savePath == null || !Files.exists(savePath)) {
			new IllegalStateException("The save path wasn't set or doesn't exist!").printStackTrace();;
			return list;
		}
			
		try (DirectoryStream<Path> entries = Files.newDirectoryStream(savePath)) {
			for (Path entry : entries) {
				if(Files.isDirectory(entry) && Files.exists(entry.resolve(worldFileName+worldFileExtension))) {
					list.add(entry.getFileName().toString());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * Will save the world using this format to the returned File list. There is one file per chunk, plus one extra file
	 * with general information about the world.
	 * @param world The world to save.
	 * @throws IOException If something bad ocurred when generating the files and writing to them.
	 * @throws IllegalStateException If the save path wasn't set.
	 */
	public static void saveWorld(World world) throws IOException, IllegalStateException {
		System.out.println("Saving world \"" + world.getName() + "\"...");
		if(savePath == null)
			throw new IllegalStateException("The save path wasn't set!");
		
		Path directory = savePath.resolve(world.getName());
		if(!Files.exists(directory))	
			Files.createDirectory(directory);
		
		Path mainWorldFile = directory.resolve(worldFileName+worldFileExtension);
		writeFile(mainWorldFile, generateWorldFile(world));
		
		long chrono = System.currentTimeMillis();
		int chunksMade = 0;
		System.out.print("Saving chunk files... ");
		for(int x : world.getChunkXKeys()) {
			for(int y : world.getChunkYKeys(x)) {
				Chunk chunk = world.getChunkOfCoordinatesIf(x, y);
				if(chunk == null) // There shouldn't be any case of null chunks but just in case...
					continue;
				
				Path chunkFile = directory.resolve(x+"."+y+chunkFileExtension);
				writeFile(chunkFile, generateChunkFile(chunk));
				chunksMade ++;
			}
		}
		System.out.println("Done! Created " + chunksMade + " chunk files in " + (System.currentTimeMillis()-chrono) + "ms");
	}
	
	/**
	 * Will save the given chunk that's owned by the given chunk at the given coordinates.
	 * @param world The world that owns the chunk.
	 * @param x The X coordinate of the chunk in chunk coordinates.
	 * @param y The Y coordinate of the chunk in chunk coordinates.
	 * @param chunk The chunk to save.
	 * @throws IllegalStateException If the save path wasn't set.
	 * @throws IOException If there was an error when creating or writing the file.
	 */
	public static void saveChunk(World world, int x, int y, Chunk chunk) throws IllegalStateException, IOException {
		System.out.print("Saving chunk of \"" + world.getName() + "\" at " + x + "/" + y + "... ");
		if(savePath == null)
			throw new IllegalStateException("The save path wasn't set!");
		
		Path worldFolder = savePath.resolve(world.getName());
		if(!Files.exists(worldFolder))
			Files.createDirectory(worldFolder);
		
		Path chunkFile = worldFolder.resolve(x+"."+y+chunkFileExtension);
		
		long chrono = System.currentTimeMillis();
		writeFile(chunkFile, generateChunkFile(chunk));
		System.out.println("Done! Took " + (System.currentTimeMillis()-chrono) + "ms");
	}
	
	/**
	 * Will delete the given file, and if it happens to be a folder, will also delete everything it contains.
	 * @param path The path to delete.
	 * @param removeFolder If the folder should also be removed, or only its contents.
	 * @throws IOException If an error ocurred when creating a directory stream.
	 */
	@SuppressWarnings("unused")
	private static void recursiveDeleteFolder(Path path, boolean removeFolder) throws IOException {
		if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
			try (DirectoryStream<Path> entries = Files.newDirectoryStream(path)) {
				for (Path entry : entries) {
					recursiveDeleteFolder(entry, true);
				}
			}
		}
		if(removeFolder)
			Files.delete(path);
	}
	
	/**
	 * Will generate the bytes needed to represent the world file, that should contain this world's seed,
	 * its name, the id of its WorldGenerator and all the entities in the world.
	 * @param world The world that will have its data transformed to a byte array.
	 * @return The generated byte array.
	 */
	private static byte[] generateWorldFile(World world) {
		System.out.print("Generating world file for \"" + world.getName() + "\", version " + worldFileVersion + "...");
		long chrono = System.currentTimeMillis();
		
		byte[] versionByte = new byte[] {worldFileVersion};
		byte[] seedBytes = BoringUtil.toBytes(world.seed);
		byte[] nameBytes = BoringUtil.toBytes(world.getName());
		byte[] idByte = new byte[] {world.getGenerator().getType().getID()};
		
		byte[] allWorldInfo = BoringUtil.concatAll(versionByte, seedBytes, nameBytes, separator, idByte);
		
		for(Entity e : world.getEntities()) {
			allWorldInfo = BoringUtil.concatAll(allWorldInfo, e.toBytes());
		}
		
		System.out.println(" Done creating data! Took " + (System.currentTimeMillis()-chrono) + "ms");
		
		return allWorldInfo;
	}
	
	/**
	 * Will generate the bytes needed to represent the chunk file that should contain all this chunk's information, which
	 * includes its ground blocks, its surface blocks, the biomes it has and the blocks that need a block update.
	 * @param chunk The chunk from which the data will be used.
	 * @return The generated byte array.
	 * @throws IOException If there was an error when creting the data.
	 */
	private static byte[] generateChunkFile(Chunk chunk) throws IOException {
		ByteArrayOutputStream biomeBytes = new ByteArrayOutputStream(Chunk.SIZE * Chunk.SIZE);
		ByteArrayOutputStream groundBytes = new ByteArrayOutputStream();
		ByteArrayOutputStream surfaceBytes = new ByteArrayOutputStream();
		boolean[] updatesBooleans = new boolean[Chunk.SIZE * Chunk.SIZE];
		
		for(int x = 0; x < Chunk.SIZE; x++) {
			for(int y = 0; y < Chunk.SIZE; y++) {
				SolidBlock ground = chunk.getBlock(x, y);
				SurfaceBlock surface = chunk.getUpperBlock(x, y);
				byte[] groundData = ground.toBytes();
				byte[] surfaceData = surface instanceof DataHolderBlock ? ((DataHolderBlock) surface).toBytesWithExtras() : surface.toBytes();
				
				groundBytes.write(groundData);
				surfaceBytes.write(surfaceData);
				biomeBytes.write(chunk.getBiome(x, y).getID());
				updatesBooleans[x * Chunk.SIZE + y] = chunk.needsBlockUpdate(x, y);
			}
		}
		
		byte[] updatesBytes = BoringUtil.bytesFromBooleans(updatesBooleans);
		
		ByteArrayOutputStream data = new ByteArrayOutputStream(1 + biomeBytes.size() + groundBytes.size() + surfaceBytes.size() + updatesBytes.length);
		
		data.write(chunkFileVersion);
		groundBytes.writeTo(data);
		surfaceBytes.writeTo(data);
		biomeBytes.writeTo(data);
		data.write(updatesBytes);

		return data.toByteArray();
	}
	
	/**
	 * Will look for the settings file, and load all its data.
	 * @throws IOException If something went wrong when reading the file.
	 * @throws IllegalStateException If the save path wasn't set.
	 * @throws DataFormatException If there was an error when decompressing the file.
	 */
	public static void loadSettings() throws IOException, IllegalStateException, DataFormatException {
		System.out.print("Loading settings... ");
		if(savePath == null)
			throw new IllegalStateException("The save path wasn't set!");
		
		long chrono = System.currentTimeMillis();
		Path settingsFile = savePath.resolve(settingsFileName + settingsFileExtension);
		if(!Files.exists(settingsFile)) {
			System.out.println("No settings file found!");
			return;
		}
		getDataFromSettingsFile(readFile(settingsFile));
		
		System.out.println("Took " + (System.currentTimeMillis() - chrono) + "ms");
	}
	
	/**
	 * Will load data from the given bytes, considering they are bytes associated to a settings file.
	 * @param bytes The bytes to rea.d
	 */
	private static void getDataFromSettingsFile(byte[] bytes) {
		ByteBuffer data = ByteBuffer.wrap(bytes);
		byte version = data.get();
		if(version != settingsFileVersion)
			throw new IllegalArgumentException("The settings file's version (" + version + ") isn't the current file version (" + settingsFileVersion + ")!");
		
		int total = 0;
		while(data.hasRemaining()) {
			short keybindingID = data.getShort();
			boolean isMouseBinding = data.get() == 1;
			int binding = data.getInt();
			KeyBinding kb = KeyBinding.getByID(keybindingID);
			if(kb != null) {
				if(kb.isFinal()) {
					System.err.println("Tried setting the value of the final keybinding " + kb);
					continue;
				}
				kb.setBinding(binding, isMouseBinding);
				total++;
			}
		}
		System.out.print("Loaded " + total + " key bindings! ");
	}
	
	/**
	 * Will read in the given file that must be a folder, and load the world contained in it.
	 * @param path The folder with the world files.
	 * @return The created world.
	 * @throws IllegalStateException If the save folder isn't set or the the world folder exists but there is no world file in it.
	 * @throws IllegalArgumentException If the given world doesn't exist in the save folder.
	 * @throws IOException If something bad happened when trying to read from the files.
	 * @throws DataFormatException If there was an error when decompressing the file.
	 */
	public static World openWorld(String name) throws IOException, IllegalArgumentException, IllegalStateException, DataFormatException {
		System.out.println("Loading world \""+name+"\"...");
		if(savePath == null)
			throw new IllegalStateException("The save path wasn't set!");
		
		Path path = savePath.resolve(name);
		if(!Files.exists(path))
			throw new IllegalArgumentException("The given world doesn't exist!");
		
		Path worldFile = path.resolve(worldFileName+worldFileExtension);
		if(!Files.exists(worldFile))
			throw new IllegalStateException("The given world exists, but the main world file (" + worldFileName+worldFileExtension + ") doesn't!");
		
		byte[] worldInfoBytes = readFile(worldFile);
		WorldInformation info = getDataFromWorldFile(worldInfoBytes);
		
		World world = new World(info.name, info.seed);
		world.setGenerator(WorldGenerator.getWorldGenerator(world, info.generatorID));
		for(Entity e : info.entities) {
			world.add(e);
			e.getLocation().setWorld(world);
		}
		
		System.out.println("Done loading world \"" + name + "\"!");
		
		return world;
	}
	

	/**
	 * Will see in the current save path if the chunk for the given world at the given position exists.
	 * @param world The world that owns the chunk.
	 * @param x The chunk's X coordinate in chunk coordinates.
	 * @param y The chunk's Y coordinate in chunk coordinates.
	 * @return If the file for the chunk exists. This doesn't guarantee that the file is readable.
	 */
	public static boolean chunkIsSaved(World world, int x, int y) {
		if(savePath == null)
			return false;
		Path chunkFile = savePath.resolve(world.getName()).resolve(x+"."+y+chunkFileExtension);
		return Files.exists(chunkFile);
	}
	
	/**
	 * Will try to load the chunk owned by the given world at the given chunk coordinates.
	 * @param world The world that owns the chunk.
	 * @param x The chunk's X coordinate in chunk coordinates.
	 * @param y The chunk's Y coordinate in chunk coordinates.
	 * @return The generated chunk.
	 * @throws IllegalStateException If the save path isn't set.
	 * @throws IllegalArgumentException If the chunk's file doesn't exist.
	 * @throws IOException If there was an error when reading the file's content.
	 * @throws DataFormatException If there was an error when decompressing the file 
	 */
	public static Chunk loadChunk(World world, int x, int y) throws IllegalStateException, IllegalArgumentException, IOException, DataFormatException {
		System.out.print("Loading chunk in \""+world.getName()+"\" at " + x + "/" + y + "... ");
		if(savePath == null)
			throw new IllegalStateException("The save path wasn't set!");
		
		Path chunkFile = savePath.resolve(world.getName()).resolve(x+"."+y+chunkFileExtension);
		if(!Files.exists(chunkFile))
			throw new IllegalArgumentException("The world folder or the chunk file don't exist!");
		
		long chrono = System.currentTimeMillis();
		Chunk chunk = getDataFromChunkFile(readFile(chunkFile), world);
		System.out.println("Done! Took " + (System.currentTimeMillis()-chrono)+ "ms");
		
		return chunk;
	}
	
	/**
	 * Will read the given bytes, assuming they correspond to a world file, and extract all the necessary information from it, such as the seed,
	 * the world's name, the generator type and all the entities in it.
	 * @param bytes The file's bytes.
	 * @return The read information.
	 */
	private static WorldInformation getDataFromWorldFile(byte[] bytes) {
		System.out.println("Reading world file... ");
		long chrono = System.currentTimeMillis();
		
		WorldInformation info = new WorldInformation();
		ByteBuffer dataReader = ByteBuffer.wrap(bytes);
		
		byte version = dataReader.get();
		if(version != WorldIO.worldFileVersion)
			throw new IllegalArgumentException("The given world file has been saved with version " + version + " of world files, currently at version " + WorldIO.worldFileVersion + "!" );
		
		info.seed = dataReader.getLong();
		System.out.println("\t> Seed is " + info.seed);
		
		info.name = BoringUtil.getString(dataReader, separator);
		System.out.println("\t> Name is " + info.name);
		
		info.generatorID = dataReader.get();
		System.out.println("\t> Generator id is " + info.generatorID);
		
		
		while(dataReader.position() < bytes.length-1) {
			Entity entity = Entity.getEntityFromBytes(dataReader);
			if(entity != null) {
				info.entities.add(entity);
			}
		}
		System.out.println("\t> Loaded " + info.entities.size() + " entities");
		
		System.out.println("Done reading file! Took " + (System.currentTimeMillis()-chrono) + "ms");
		return info;
	}
	
	/**
	 * Will read the given bytes, assuming they correspond to a chunk file, and extract all the necessary information from it, such as the ground
	 * blocks, the surface blocks, the biomes and the blocks that need block updates.
	 * @param bytes The file's bytes.
	 * @param world The world in which the chunk will be placed. This is used by some of the SurfaceBlocks.
	 * @return The chunk read from the file.
	 */
	private static Chunk getDataFromChunkFile(byte[] bytes, World world) {
		Chunk chunk = new Chunk();
		ByteBuffer dataReader = ByteBuffer.wrap(bytes);

		byte version = dataReader.get();
		if(version != WorldIO.chunkFileVersion) {
			throw new IllegalArgumentException("The given chunk file has been saved with version " + version + " of chunk files, currently at version " + WorldIO.chunkFileExtension + "!" );
		}
		
		for(int x = 0; x < Chunk.SIZE; x++) {
			for(int y = 0; y < Chunk.SIZE; y++) {
				Item item = Item.getItemFrom(dataReader, world);
				if(item instanceof SolidBlock)
					chunk.setblock((SolidBlock) item, x, y);
			}
		}
		
		for(int x = 0; x < Chunk.SIZE; x++) {
			for(int y = 0; y < Chunk.SIZE; y++) {
				Item item = Item.getItemFrom(dataReader, world);
				if(item instanceof SurfaceBlock)
					chunk.setblock((SurfaceBlock) item, x, y);
			}
		}
		
		for(int x = 0; x < Chunk.SIZE; x++) {
			for(int y = 0; y < Chunk.SIZE; y++) {
				Biome biome = Biome.getFromID(dataReader.get());
				if(biome != null)
					chunk.setBiome(biome, x, y);
			}
		}
		
		byte[] blockUpdateBytes = new byte[dataReader.remaining()];
		dataReader.get(blockUpdateBytes);
		boolean[] blockUpdateBooleans = BoringUtil.booleansFromBytes(blockUpdateBytes);
		int x = 0;
		int y = 0;
		for(boolean b : blockUpdateBooleans) {
			chunk.blockUpdate(b, x, y);
			y++;
			if(y == Chunk.SIZE) {
				x++;
				y = 0;
			}
		}
		
		return chunk;
	}
	
	/**
	 * Will try to write the given data to the given file, in a compressed format.
	 * @param path The path to write to.
	 * @param data The data that will be written.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	private static void writeFile(Path path, byte[] data) throws FileNotFoundException, IOException {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[128]; // Most files are small enough to be contained in ~128 or ~256 bytes, so no need to go overboard with 1024 bytes of buffer
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		outputStream.close();
		byte[] output = outputStream.toByteArray();
		
		Files.write(path, output, CREATE, WRITE, TRUNCATE_EXISTING);
	}

	/**
	 * Will try to read the given file, and get all the bytes from it by decompressing the deflate algorithm.
	 * @param path The file to read from.
	 * @return The read bytes.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws DataFormatException 
	 */
	private static byte[] readFile(Path path) throws FileNotFoundException, IOException, DataFormatException {
		byte[] data = Files.readAllBytes(path);
		Inflater inflater = new Inflater();   
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[128];
		while (!inflater.finished()) {
			int count = inflater.inflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		outputStream.close();
		byte[] output = outputStream.toByteArray();
		return output;
	}
	
	private static class WorldInformation {
		private long seed;
		private String name;
		private byte generatorID;
		private final List<Entity> entities = new ArrayList<>();
	}
	
}

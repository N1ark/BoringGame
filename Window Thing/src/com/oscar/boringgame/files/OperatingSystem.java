package com.oscar.boringgame.files;

public enum OperatingSystem {
	
	WINDOWS,
	MACOS,
	UNIX,
	SOLARIS,
	UNKNOWN;
	
	static {
		String osName = System.getProperty("os.name").toLowerCase();
		
		if(osName.contains("win"))
			currentOS = WINDOWS;
		else if(osName.contains("mac"))
			currentOS = MACOS;
		else if(osName.contains("nix") || osName.contains("nux") || osName.contains("aix"))
			currentOS = UNIX;
		else if(osName.contains("sunos"))
			currentOS = SOLARIS;
		else 
			currentOS = UNKNOWN;
	}

	private static OperatingSystem currentOS;
	
	/**
	 * @return The current OS.
	 */
	public static OperatingSystem getCurrentOS() {
		return currentOS;
	}
	
}

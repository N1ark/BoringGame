package com.oscar.boringgame.renderer.helper;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_LEQUAL;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDepthFunc;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;

import java.awt.FontFormatException;
import java.io.IOException;

import org.lwjgl.opengl.GL;

import com.oscar.boringgame.renderer.Camera;
import com.oscar.boringgame.renderer.Window;
import com.oscar.boringgame.util.Area;

public class Artist {

	/** The Atlas with the texture for all blocks and items. */
	public static Texture BLOCK_ATLAS;
	/** The Atlas with the texture for all particles. */
	public static Texture PARTICLE_ATLAS;
	/** The Atlas with the texture for all gui related textures. */
	public static Texture GUI_ATLAS;
	/** The default font for writing text. */
	public static Font DEFAULT_FONT;
	/** A smaller font, for less important information. */
	public static Font SMALL_FONT;
	
	private static Window window;
	
	/**
	 * Will set the Window use to calculate the screen ratio.
	 */
	public static void setWindow(Window window) {
		Artist.window = window;
	}
	
	/**
	 * Will start all the required values for rendering, by creating the OpenGL capabilities, and setting all blending, alpha and depth options.
	 */
	public static void beginSession() {
		GL.createCapabilities();
		glEnable(GL_TEXTURE_2D); 

		glEnable(GL_BLEND);
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	    
	    glEnable(GL_ALPHA_TEST);
	    glAlphaFunc (GL_GREATER, 0.1f);

	    glEnable(GL_DEPTH_TEST);
    	glDepthFunc(GL_LEQUAL);
	}
	
	/**
	 * Will load the necessary textures, such as the Block texture atlas.
	 */
	public static void loadTextures() {
		BLOCK_ATLAS = new Texture("atlas");
		PARTICLE_ATLAS = new Texture("particle_atlas");
		GUI_ATLAS = new Texture("gui_atlas");
		
		
		try {
			DEFAULT_FONT = new Font("munro", 10);
			SMALL_FONT = new Font("munro-small", 10);
		} catch (FontFormatException | IOException e) {
			System.out.println("Couldn't load font.");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	/**
	 * Will set the amount of blocks that are displayed on the width of the screen.
	 * @param camera The "camera" used to specify from where and at what distance the world is currently being seen.
	 * @param view A storage Area object, that will be used for setting the returned value. If set to null, a new object will be created!
	 * @return An Area representing the current edges of the screen.
	 */
	public static void setView(Camera camera, Area view) {
		camera.getFieldOfView(view, window);
		
		float zNear = -(view.getHeight()+10);
		
        glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(
				view.x1, view.x2,
				view.y2, view.y1, 
				zNear, 0); 
	
        glMatrixMode(GL_MODELVIEW);
	}
	
	/**
	 * Will prepare the OpenGL matrix to work for GUI, by "creating" a square of coordinates (-1;-1)/(1;1) fill a maximal part of the screen.
	 */
	public static void setGUIView(Area view) {
        glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		view.x1 = -1;
		view.x2 = 1;
		view.y1 = -1;
		view.y2 = 1;
		if(window.getWidth() > window.getHeight()) {
			float extra = ((float) window.getWidth() - window.getHeight()) / window.getHeight();
			view.x1 -= extra;
			view.x2 += extra;
			
		} else {
			float extra = ((float) window.getHeight() - window.getWidth()) / window.getWidth();
			view.y1 -= extra;
			view.y2 += extra;
		}
		glOrtho(
				view.x1, view.x2,
				view.y2, view.y1, 
				-1, 0); 
        glMatrixMode(GL_MODELVIEW);

	}
}

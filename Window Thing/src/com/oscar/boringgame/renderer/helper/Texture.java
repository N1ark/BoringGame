package com.oscar.boringgame.renderer.helper;

import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

import com.oscar.boringgame.main.Boot;

public class Texture {
	
	/** The last texture to have been bound. */
	private static int currentTexture;
	
	/** This texture's ID. */
	private int textureObject;
	
	/** If this texture has been cleared from memory. */
	private boolean cleared = false;
	
	/**
	 * Will create a texture object for the texture with the given ID.
	 * @param id The already existing texture's ID.
	 */
	public Texture(int id) {
		this.textureObject = id;
	}
	
	/**
	 * Will create a texture from the given file.
	 * @param fileName The name of the file in the assets directory. The extension (must be .png) is automatically added, so it musn't be specified.
	 */
	public Texture(String fileName) {
		long chrono = System.currentTimeMillis();
		String path = Boot.PATH_EXTRA + "assets/" + fileName + ".png"; // If exported to .jar, add "src/" at the beginning of the path!

		System.out.print("Loading texture \""+path+"\"...");
		
		ClassLoader load = Boot.class.getClassLoader();
		try(InputStream file = load.getResourceAsStream(path)) {
			BufferedImage bufferedImage = ImageIO.read(file);
		
			int width = bufferedImage.getWidth();
			int height = bufferedImage.getHeight();
			
			int[] pixels_raw = new int[width * height * 4];
			pixels_raw = bufferedImage.getRGB(0, 0, width, height, null, 0, width);
			
			ByteBuffer pixels = BufferUtils.createByteBuffer(width * height * 4);
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					int pixel = pixels_raw[i * width + j];
					pixels.put((byte) ((pixel >> 16) & 0xFF)); // RED
					pixels.put((byte) ((pixel >> 8) & 0xFF));  // GREEN
					pixels.put((byte) (pixel & 0xFF));		  // BLUE
					pixels.put((byte) ((pixel >> 24) & 0xFF)); // ALPHA
				}
			}
			
			pixels.flip();
			
			textureObject = glGenTextures();
			
			glBindTexture(GL_TEXTURE_2D, textureObject);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
			
			System.out.println(" Loaded! Took " + (System.currentTimeMillis()-chrono) + "ms");
		}
		catch (IOException | IllegalArgumentException e) {
			System.out.println();
			System.err.println("Couldn't load texture \"" + fileName + "\"");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Will activate this texture.
	 */
	public void bind() {
		glBindTexture(GL_TEXTURE_2D, textureObject);
		currentTexture = textureObject;
	}
	
	/**
	 * @return If this texture is currently binded.
	 */
	public boolean isBound() {
		return currentTexture == textureObject;
	}
	
	/**
	 * Will activate this texture, if it isn't activated yet.
	 */
	public void bindIfNeeded() {
		if(!isBound())
			bind();
	}
	
	/**
	 * Will clear this texture, by deleting it from memory.
	 */
	public void clear() {
		if(!cleared)
			glDeleteTextures(textureObject);
		cleared = true;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Texture && ((Texture) obj).textureObject == textureObject;
	}
	
}
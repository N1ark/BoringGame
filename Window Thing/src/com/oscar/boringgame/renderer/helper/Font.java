package com.oscar.boringgame.renderer.helper;

import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.oscar.boringgame.main.Boot;

public class Font {

	// Constants
	private static final Map<Integer, String> CHARS = new HashMap<Integer, String>() {
		private static final long serialVersionUID = -8094674993839841894L;
		{
			put(0, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			put(1, "abcdefghi");
			put(4, "jklmnopqrstuvwxyz");
			put(2, "0123456789");
			put(3, " $+-*/=%\"'#@&_(),.;:?!\\|<>[]§`^~");
		}
	};

	// Variables
	/** Align the text to the left. This is the default option. */
	public static final byte ALIGN_LEFT 		   = 0b00001;
	/** Align the text to the right. */
	public static final byte ALIGN_RIGHT 		   = 0b00010;
	/** Align the text by the center. */
	public static final byte ALIGN_CENTER 		   = 0b00100;
	/** Vertically align the text by its baseline. This is the default option. */
	public static final byte VERTICAL_ALIGN_NORMAL = 0b01000;
	/** Vertically align the text by its center. */
	public static final byte VERTICAL_ALIGN_CENTER = 0b10000;
	
	private java.awt.Font font;
	private FontMetrics fontMetrics;
	private BufferedImage bufferedImage;
	private Texture texture;
	
	private float charHeight;
	private float fontImageWidth;
	private float fontImageHeight;

	private Map<Character, Float> charX = new HashMap<>();
	private Map<Character, Float> charY = new HashMap<>();

	
	// Constructors
	public Font(String path, float size) throws FontFormatException, IOException {
		path = Boot.PATH_EXTRA + "assets/gui/" + path + ".ttf";
		long chrono = System.currentTimeMillis();
		System.out.print("Loading font \"" + path + "\"... ");

		try (InputStream input = Boot.class.getClassLoader().getResourceAsStream(path)) {
			this.font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, input).deriveFont(size);
			
			// Generate buffered image
			Graphics2D graphics = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics();
			graphics.setFont(font);

			// Pre-calculate all meaningful data
			this.fontMetrics = graphics.getFontMetrics();
			this.charHeight = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();
			this.fontImageWidth = (float) CHARS.values().stream().mapToDouble(e -> fontMetrics.getStringBounds(e, null).getWidth()).max().getAsDouble();
			this.fontImageHeight = CHARS.keySet().size() * this.charHeight;
			
			// Calculate all characters' coordinates
			for(Entry<Integer, String> s : CHARS.entrySet()) {
				for(char c : s.getValue().toCharArray()) {
					String originStr = CHARS.values().stream().filter(e -> e.contains(Character.toString(c))).findFirst().orElse(Character.toString(c));
					float x = (float) fontMetrics.getStringBounds(originStr.substring(0, originStr.indexOf(c)), null).getWidth();
					charX.put(c, x);
					
					float lineId = CHARS.keySet().stream().filter(i -> CHARS.get(i).contains(Character.toString(c))).findFirst().orElse(0);
					float y = charHeight * lineId;
					charY.put(c, y);
				}
			}
			
			this.bufferedImage = graphics.getDeviceConfiguration().createCompatibleImage((int) fontImageWidth,
					(int) fontImageHeight, Transparency.BITMASK);
			
			// Generate texture
			int id = glGenTextures();
			this.texture = new Texture(id);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int) fontImageWidth, (int) fontImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, asByteBuffer());

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			
			
			System.out.println("Loaded! Took " + (System.currentTimeMillis()-chrono) + "ms");
		}
	}

	// Getters
	public float getCharX(char c) {
		//String originStr = CHARS.values().stream().filter(e -> e.contains(Character.toString(c))).findFirst()
//				.orElse(Character.toString(c));
		//return (float) fontMetrics.getStringBounds(originStr.substring(0, originStr.indexOf(c)), null).getWidth();
				return charX.get(c);
	}

	public float getCharY(char c) {
		return charY.get(c);
	}

	public float getCharWidth(char c) {
		return fontMetrics.charWidth(c);
	}

	public float getStringWidth(String text) {
		return fontMetrics.stringWidth(text);
	}
	
	public boolean isValid(char c) {
		for(Entry<Integer, String> string : CHARS.entrySet())
			for(char d : string.getValue().toCharArray())
				if(d == c)
				return true;
		return false;
	}

	public float getCharHeight() {
		return charHeight;
	}
	
	// Functions
	public void enable() {
		texture.bind();
	}

	/**
	 * Will draw with this font in the current context, writing at the given position.
	 * @param text The text to draw.
	 * @param x The text's x position.
	 * @param y The text's y position. 
	 */
	public void drawText(String text, float x, float y) {
		drawText(text, x, y, 1, ALIGN_LEFT, VERTICAL_ALIGN_NORMAL);
	}
	
	/**
	 * Will draw with this font in the current context, writing at the given position.
	 * @param text The text to draw.
	 * @param x The text's x position.
	 * @param y The text's y position. 
	 * @param scale The text's scale.
	 */
	public void drawText(String text, float x, float y, float scale) {
		drawText(text, x, y, scale, ALIGN_LEFT, VERTICAL_ALIGN_NORMAL);
	}
	
	/**
	 * Will draw with this font in the current context, writing at the given position and with the given alignement.
	 * @param text The text to draw.
	 * @param x The text's x position.
	 * @param y The text's y position.
	 * @param scale The text's scale.
	 * @param alignement The alignement mode of the text. 
	 * See <code>ALIGN_LEFT</code>, <code>ALIGN_RIGHT</code>, <code>ALIGN_CENTER</code>.
	 */
	public void drawText(String text, float x, float y, float scale, byte alignement) {
		drawText(text, x, y, scale, alignement, VERTICAL_ALIGN_NORMAL);
	}

	/**
	 * Will draw with this font in the current context, writing at the given position and with the given alignement.
	 * @param text The text to draw.
	 * @param x The text's x position.
	 * @param y The text's y position.
	 * @param scale The text's scale.
	 * @param alignement The alignement mode of the text. 
	 * See <code>ALIGN_LEFT</code>, <code>ALIGN_RIGHT</code>, <code>ALIGN_CENTER</code>.
	 * @param vertAlignement The vertical alignement mode of the text.
	 * See <code>VERTICAL_ALIGN_NORMAL</code>, <code>VERTICAL_ALIGN_CENTER</code>
	 */
	public void drawText(String text, float x, float y, float scale, byte alignement, byte vertAlignement) {	
		if(alignement == ALIGN_RIGHT)
			x -= getStringWidth(text)*scale;
		else if(alignement == ALIGN_CENTER)
			x -= getStringWidth(text)*scale/2;
		
		if(vertAlignement == VERTICAL_ALIGN_CENTER)
			y -= charHeight*scale/2;
		
		final float halfPixel = 0.0001f;
		glBegin(GL_QUADS);

		for (char c : text.toCharArray()) {
			if(!isValid(c))
				c = '?';
			
			float width = getCharWidth(c);
			float height = charHeight;
			float cw = 1f / fontImageWidth * width;
			float ch = 1f / fontImageHeight * height;
			float cx = 1f / fontImageWidth * getCharX(c);
			float cy = 1f / fontImageHeight * getCharY(c);

			width *= scale;
			height *= scale;

			glTexCoord2f(cx + halfPixel, cy + halfPixel);
			glVertex2f(x, y);

			glTexCoord2f(cx + cw - halfPixel, cy + halfPixel);
			glVertex2f(x + width, y);

			glTexCoord2f(cx + cw - halfPixel, cy + ch - halfPixel);
			glVertex2f(x + width, y + height);

			glTexCoord2f(cx + halfPixel, cy + ch - halfPixel);
			glVertex2f(x, y + height);
			x += width;
		}

		glEnd();
	}

	// Conversions
	public ByteBuffer asByteBuffer() {

		ByteBuffer byteBuffer;

		// Draw the characters on our image
		Graphics2D imageGraphics = (Graphics2D) bufferedImage.getGraphics();
		imageGraphics.setFont(font);
		imageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		imageGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		// draw every CHAR by line...
		imageGraphics.setColor(Color.WHITE);
		for(int i : CHARS.keySet()) {
			imageGraphics.drawString(CHARS.get(i), 0, fontMetrics.getMaxAscent() + this.charHeight * i);
		}

		// Generate texture data
		int[] pixels = new int[bufferedImage.getWidth() * bufferedImage.getHeight()];
		bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), pixels, 0,
				bufferedImage.getWidth());
		byteBuffer = ByteBuffer.allocateDirect((bufferedImage.getWidth() * bufferedImage.getHeight() * 4));

		for (int y = 0; y < bufferedImage.getHeight(); y++) {
			for (int x = 0; x < bufferedImage.getWidth(); x++) {
				int pixel = pixels[y * bufferedImage.getWidth() + x];
				byteBuffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
				byteBuffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
				byteBuffer.put((byte) (pixel & 0xFF)); // Blue component
				byteBuffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component. Only for RGBA
			}
		}

		byteBuffer.flip();

		return byteBuffer;
	}
}
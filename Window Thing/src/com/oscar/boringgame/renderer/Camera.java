package com.oscar.boringgame.renderer;

import com.oscar.boringgame.entity.Entity;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.PVector;

public class Camera {
	
	private Entity following;
	private final PVector location;
	private float zoom;
	private float followSpeed = 6;
	
	/**
	 * Will create a camera at the given location, with the given zoom level.
	 * @param x The camera's X coordinate.
	 * @param y The camera's Y coordinate.
	 * @param zoom The camera's zoom level, equating to the amount of blocks it can see in the width.
	 */
	public Camera(float x, float y, float zoom) {
		this.location = new PVector(x, y);
		this.zoom = zoom;
		this.following = null;
	}
	
	/**
	 * @return The camera's location.
	 */
	public PVector getLocation() {
		return location;
	}
	
	/**
	 * @return The camera's zoom level, equating to the amount of blocks it can see in the width.
	 */
	public float getZoom() {
		return zoom;
	}

	/**
	 * Will set the camera's zoom level, equating to the amount of blocks it can see in the width.
	 * @param zoom The zoom level. Minimum is 5.
	 */
	public void setZoom(float zoom) {
		if(zoom < 5)
			zoom = 5;
		this.zoom = zoom;
	}

	/**
	 * Will set the entity followed by the camera.
	 * @param entity The followed entity.
	 */
	public void follow(Entity entity) {
		this.following = entity;
	}
	
	/**
	 * @return The Entity currently followed by the camera.
	 */
	public Entity getFollow() {
		return following;
	}
	
	/**
	 * Will set the given Area's coordinates and size to match what would be seen bu this Camera in the given Window.
	 * @param view The area that will contain the coordinates. If null, the method will create a new Area.
	 * @param window The window in which this camera is placed.
	 * @return The given Area, or a new area if null was the given argument.
	 */
	public Area getFieldOfView(Area view, Window window) {
		if(view == null)
			view = new Area(0,0);
		float zoomX = zoom/2;
		float zoomY = zoomX/window.getRatio();
		
		view.x1 = location.x-zoomX;
		view.x2 = location.x+zoomX;
		view.y1 = location.y-zoomY;
		view.y2 = location.y+zoomY;
		
		return view;
	}
	
	/**
	 * Will do all that is needed for the camera, such as moving it.
	 */
	public void tick() {
		if(following != null) {
			location.x = (location.x * followSpeed + following.getLocation().x)/(followSpeed+1);
			location.y = (location.y * followSpeed + following.getLocation().y - following.getDisplay().getHeight()/2f)/(followSpeed+1);
		}
	}
	
}

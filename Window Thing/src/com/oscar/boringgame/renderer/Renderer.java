package com.oscar.boringgame.renderer;

import static com.oscar.boringgame.renderer.helper.Artist.setGUIView;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import com.oscar.boringgame.controller.Controller;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.PVector;

public abstract class Renderer {

	/** The coordinate system currently used. */
	protected final Area viewArea;
	
	/** The mouse's position in the current coordinate system. */
	protected final PVector mousePos;
	
	/** The window on which this renderer is painting. */
	protected final Window window;
	
	/** The controller used to get user input. */
	protected final Controller controller;
	
	/** The number of frames drawn by the renderer. */
	private int frame;
	
	public Renderer(Window window, Controller controller) {
		this.viewArea = new Area(0, 0);
		this.mousePos = new PVector();
		this.window = window;
		this.controller = controller;
	}
	
	/**
	 * @return The window that is painted by this renderer.
	 */
	public final Window getWindow() {
		return window;
	}
	
	/**
	 * @return The controller used by this renderer.
	 */
	public final Controller getController() {
		return controller;
	}
	
	/**
	 * @return The mouse's position in the current coordinate system.
	 */
	public final PVector getMousePos() {
		return mousePos;
	}
	
	/**
	 * @return The current view area, that dictates the current coordinate system.
	 */
	public final Area getViewArea() {
		return viewArea;
	}
	

	/**
	 * @return The number of frames drawn by the renderer.
	 */
	public final int getFrame() {
		return frame;
	}
	
	/**
	 * Will clear the screen and draw this renderer's content.
	 */
	public final void drawFrame() {
		clear();
		draw();
		frame++;
	}
	
	/**
	 * Will draw this renderer's content. This method should never be accessed from outside the Renderer class.
	 */
	protected abstract void draw();
	
	/**
	 * Will switch the coordinate system to the UI coordinate system, and will recalculate the mouse position.
	 */
	protected final void switchToGUIView() {
		setGUIView(viewArea);
		mousePos.set(controller.getMousePosition());
		mousePos.x = xFromPixelToView(mousePos.x);
		mousePos.y = yFromPixelToView(mousePos.y);
	}
	
	/**
	 * Function that should be called when a rendered will be closed (and won't be used again), so it can free some ressources.
	 */
	public void willClose() {/* Doesn't need to be overriden. */}
	
	/**
	 * Will clear the screen, to prepare for a new render cycle.
	 */
	public static final void clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0,0,0,0);
	}
	
	
	/**
	 * @param x The X pixel coordinate to convert.
	 * @return The view X coordinate equivalent for the given pixel X coordinate.
	 */
	public final float xFromPixelToView(float x) {
		return window.getView().mapX(x, viewArea);
	}
	
	/**
	 * @param y The Y pixel coordinate to convert.
	 * @return The view Y coordinate equivalent for the given pixel Y coordinate.
	 */
	public final float yFromPixelToView(float y) {
		return window.getView().mapY(y, viewArea);
	}
	
	/**
	 * @param x The X view coordinate to convert.
	 * @return The pixel X coordinate equivalent for the given view X coordinate.
	 */
	public final int xFromViewToPixel(float x) {
		return Math.round(viewArea.mapX(x, window.getView()));
	}
	
	/**
	 * @param y The Y view coordinate to convert.
	 * @return The pixel Y coordinate equivalent for the given view Y coordinate.
	 */
	public final int yFromViewToPixel(float y) {
		return Math.round(viewArea.mapY(y, window.getView()));
	}
	
	/**
	 * <code>width = a - b <=> widthFromViewToPixel(width) = yFromViewToPixel(a) - yFromViewToPixel(b) </code>
	 * @param width The width to convert.
	 * @return The equivalent on the X axis of the given length. This is slightly different from xFromViewToPixel(), as it doesn't take
	 * into account the origin offset.
	 */
	public final int widthFromViewToPixel(float width) {
		return Math.round(width * window.getWidth() / viewArea.getWidth());
	}
	
	/**
	 * <code>height = a - b <=> heightFromViewToPixel(height) = yFromViewToPixel(a) - yFromViewToPixel(b) </code>
	 * @param height The height to convert.
	 * @return The equivalent on the Y axis of the given length. This is slightly different from yFromViewToPixel(), as it doesn't take
	 * into account the origin offset.
	 */
	public final int heightFromViewToPixel(float height) {
		return Math.round(height * window.getHeight() / viewArea.getHeight());
	}
	
	/**
	 * Will add a quad to the current render at the given coordinates.
	 * @param x The x coordinate of the texture.
	 * @param y The y coordinate of the texture.
	 * @param width The width of the texture.
	 * @param height The height of the texture.
	 * @param texCo The texture coordinates in the currently binded texture.
	 */
	protected static void addQuad(float x, float y, float width, float height, float[] texCo) {
		glBegin(GL_QUADS); 
	    glTexCoord2f(texCo[0], texCo[1]);
	    glVertex2f(x, y);
	    glTexCoord2f(texCo[2], texCo[1]);
	    glVertex2f(x+width, y);
	    glTexCoord2f(texCo[2], texCo[3]);
	    glVertex2f(x+width, y+height);
	    glTexCoord2f(texCo[0], texCo[3]);
	    glVertex2f(x, y+height);
	    glEnd();
	}
	
}

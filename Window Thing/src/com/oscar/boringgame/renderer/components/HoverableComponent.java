package com.oscar.boringgame.renderer.components;

import com.oscar.boringgame.renderer.Renderer;

public interface HoverableComponent {

	/**
	 * This is an additional drawing call that should be called after all components have been drawn. It will see if the
	 * mouse is hovering this component, and if so, it will draw a hover box with information.
	 * @param renderer The renderer used.
	 * @return If a hover box was drawn. If it is true, no more calls to drawHoverIfNeeded() should be made.
	 */
	boolean drawHoverIfNeeded(Renderer renderer);
	
}

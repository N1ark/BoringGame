package com.oscar.boringgame.renderer.components;

import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.item.Inventory;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;

public class HotbarDisplay extends InventoryDisplay implements HoverableComponent {
	
	private static final float[] selectedSlotTexture = textureArray(10, 12, 12, 12);
	
	/** The player to know what's the selected slot. */
	private Player player;
	/** The bar's starting. */
	private int slot;
	
	/**
	 * Will create a HotbarDisplay defined by the given parameters.
	 * @param inventory The Inventory that will be displayed. This field is final.
	 * @param player The Player object to know what is the selected slot, or null if this shouldn't be done.
	 * @param slot The index of the first slot to display.
	 * @param x The x coordinate of the bar's center.
	 * @param y The y coordinate of the bar's center.
	 * @param width The bar's width. This will also define the bar's height.
	 */
	public HotbarDisplay(Inventory inventory, Player player, int slot, float x, float y, float width) {
		super(inventory, x, y, width);
		this.player = player;
		this.slot = slot;
	}
	
	/**
	 * @return The Player object used to know what is the currently selected slot.
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * Will set the Player object used to know what is the currently selected slot.
	 * @param player The player object used, or null if the selected slot shouldn't be displayed.
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	/**
	 * @return The bar's first displayed slot.
	 */
	public int getSlot() {
		return slot;
	}
	
	/**
	 * Will set the bar's first slot. This component will draw the following 10 items (including this one) in the bar.
	 * @param slot The first slot.
	 */
	public void setSlot(int slot) {
		this.slot = slot;
	}
	
	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		this.pixel = width / 111;
		this.barHeight = pixel * 12;
		this.height = barHeight;
	}
	
	@Override
	public void setHeight(float height) {
		super.setHeight(height);
		this.pixel = height / 12;
		this.barHeight = height;
		this.width = pixel * 111;
	}
	
	@Override
	public void draw(Renderer renderer) {
		float x = this.x - width/2;
		float y = this.y - height/2;
		ItemStack hoveredItem = null;
		int hoveredSlot = -1;
		
		Artist.GUI_ATLAS.bindIfNeeded();
		
		drawInventoryBar(x, y);
		if(player != null && slot <= player.getSlot() && player.getSlot() < slot+10)
			addQuad(x+(player.getSlot()-slot)*pixel*11, y, pixel*12, pixel*12, selectedSlotTexture);
		
		x += pixel*2;
		y += pixel*2;
		Artist.BLOCK_ATLAS.bind();
		for(int i = 0; i < 10; i++) {
			ItemStack item = inventory.getItem(slot+i);
			drawInventoryItem(x+i*pixel*11, y, item, renderer);
			if(isHovering(renderer.getMousePos(), x+i*pixel*11, y)) {
				hoveredItem = item;
				hoveredSlot = slot+i;
			}
		}

		Artist.SMALL_FONT.enable();
		drawInventoryNumbers(x, y, slot);
		
		if(hoveredSlot >= 0) {
			renderer.getController().setHoveredSlot(hoveredSlot);
			renderer.getController().setHoveredInventory(inventory);
			if(hoveredItem != null)
				renderer.getController().setHoveredItemStack(hoveredItem);
		}
	}
	
}

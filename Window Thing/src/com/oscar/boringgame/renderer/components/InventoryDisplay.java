package com.oscar.boringgame.renderer.components;

import static com.oscar.boringgame.renderer.helper.Artist.SMALL_FONT;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glColor3fv;

import com.oscar.boringgame.blocks.ComplexFurnitureBlock;
import com.oscar.boringgame.blocks.ComplexFurnitureBlock.ComplexFurniturePart;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.item.Inventory;
import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Font;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.PVector;

public class InventoryDisplay extends Component implements HoverableComponent {
	/** The texture for the hover box used when an item is hovered. */
	protected static final ResizableTextureComponents hoverBoxTexture = new ResizableTextureComponents(22, 12, 2, 2, 2, 2);

	/** The texture coordinates for a hotbar texture, made of 10 slots. */
	protected static final float[] hotbarCoordinates = textureArray(10, 0, 111, 12);

	/** The font scale for drawing item numbers. */
	protected final static float fontScale = 0.006f;
		
	/** The Inventory to draw. */
	protected final Inventory inventory;
	/** The height of the bar, depending on this bar's width. */
	protected float barHeight;
	/** The size of a pixel, depending on this bar's width. */
	protected float pixel;

	/**
	 * Will create an inventory display, that will print <strong>centered</strong> on the given screen coordinates (usual components take
	 * it as a reference for the upper left corner).
	 * @param inv The inventory to display. The first 10 items will be displayed in a separate bar, below the rest.
	 * @param x The inventory's center's x coordinate.
	 * @param y The inventory's center's y coordinate.
	 * @param width The inventory's width. The height will be determined from this width.
	 */
	public InventoryDisplay(Inventory inv, float x, float y, float width) {
		super(x, y, width, 0);
		this.inventory = inv;
		setWidth(width);
	}
	
	/**
	 * @return The Inventory rendered by this component.
	 */
	public Inventory getInventory() {
		return inventory;
	}
	
	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		this.pixel = width / 111;
		this.barHeight = pixel * 12;
		this.height = (barHeight - pixel) * inventory.getType().getSize()/10 + pixel*5 + pixel*2; 
		// add the height of a row for each row, plus the extra padding for the first row, and add two pixels that are skipped (because the totality of
		// the height should be counter for the last and first rows, as their bottom pixels aren't painted over.
	}
	
	@Override
	public void setHeight(float height) {
		super.setHeight(height);
		this.pixel = height / (11 * inventory.getType().getSize()/10 + 7); // Do the reverse operation from setWidth()
		this.barHeight = pixel * 12;
		this.width = pixel * 111;
	}
	
	@Override
	public void draw(Renderer renderer) {
		ItemStack hoveredItem = null;
		int hoveredSlot = -1;
		float y = this.y - height/2;
		float x = this.x - width/2;

		// Inventory bars drawing
		Artist.GUI_ATLAS.bindIfNeeded();

		for(int i = 1; i < inventory.getType().getSize()/10; i++) {
			drawInventoryBar(x, y);
			y += pixel * 11; // Increase Y position by 11 pixels rather than 12, to avoid double edges that make thick lines.
		}
		
		y += pixel * 6; // Add 5 pixels (not 6, because we have to count the left out pixel from before).
		drawInventoryBar(x, y);
		
		// Content drawing
		Artist.BLOCK_ATLAS.bind();
		x += pixel * 2;
		y = this.y - height/2 + pixel * 2;
		for(int i = 1; i < inventory.getType().getSize()/10; i++) {
			for(int j = 0; j < 10; j++) {
				ItemStack item = inventory.getItem(i*10+j);
				drawInventoryItem(x + j * pixel * 11, y, item, renderer);
				if(isHovering(renderer.getMousePos(), x + j * pixel * 11, y)) {
					hoveredItem = item;
					hoveredSlot = i*10+j;
				}
			}
			y += pixel * 11;
		}
		
		y += pixel * 6;
		for(int i = 0; i < 10; i++) {
			ItemStack item = inventory.getItem(i);
			drawInventoryItem(x + i * pixel * 11, y, item, renderer);
			if(isHovering(renderer.getMousePos(), x + i * pixel * 11, y)) {
				hoveredItem = item;
				hoveredSlot = i;
			}
		}
		
		
		
		// Number drawing
		Artist.SMALL_FONT.enable();
		y = this.y - height/2 + pixel * 2;
		for(int i = 1; i < inventory.getType().getSize()/10; i++) {
			drawInventoryNumbers(x, y, i*10);
			y += pixel * 11;
		}
		
		y += pixel * 6;
		drawInventoryNumbers(x, y, 0);
		
		if(hoveredSlot >= 0) {
			renderer.getController().setHoveredSlot(hoveredSlot);
			renderer.getController().setHoveredInventory(inventory);
			if(hoveredItem != null)
				renderer.getController().setHoveredItemStack(hoveredItem);
		}
	}
	
	@Override
	public boolean drawHoverIfNeeded(Renderer renderer) {
		if(renderer.getController().getHoveredInventory() != inventory || renderer.getController().getHoveredItemStack() == null)
			return false;
		
		drawItemHoverBox(renderer.getController().getHoveredItemStack(), renderer.getMousePos().x, renderer.getMousePos().y);
		return true;
	}
	
	/**
	 * Will draw an inventory bar texture at the given coordinates.
	 * @param x The x coordinate to draw at.
	 * @param y The y coordinate to draw at.
	 */
	protected void drawInventoryBar(float x, float y) {
		addQuad(x, y, width, barHeight, hotbarCoordinates);
	}
	
	/**
	 * Will draw the given ItemStack at the given coordinates.
	 * @param x The x coordinate to draw at.
	 * @param y The y coordinate to draw at.
	 * @param item The Item to draw.
	 * @param renderer The renderer object, used to draw animated textures.
	 */
	public void drawInventoryItem(float x, float y, ItemStack item, Renderer renderer) {
		if(item == null)
			return;
		
		Item type = item.getType();
		if(type == SolidBlock.GRASS)
			glColor3fv(Biome.PLAIN.getGrassColor());
		
		float width = pixel * 8;
		float height = pixel * 8;
		float xExtra = 0;
		float yExtra = 0;

		if(type instanceof SurfaceHitboxBlock) {
			Area dimension = ((SurfaceHitboxBlock) type).getDisplay();
			if(dimension.getWidth() > dimension.getHeight()) {
				float oldHeight = height;
				height *= dimension.getHeight()/dimension.getWidth();
				yExtra = (oldHeight-height)/2f;
			}
			else if(dimension.getHeight() > dimension.getWidth()) {
				float oldWidth = width;
				width *= dimension.getWidth()/dimension.getHeight();
				xExtra = (oldWidth-width)/2f;
			}
		}

		if(type instanceof ComplexFurnitureBlock)
			for(ComplexFurniturePart part : ((ComplexFurnitureBlock) type).getParts())
				addQuad(x + xExtra, y + yExtra, width, height, part.getTextureCoordinates());
		else
			addQuad(x + xExtra, y + yExtra, width, height, type.getTextureCoordinates(renderer.getFrame()));


		if(type == SolidBlock.GRASS)
			glColor3f(1, 1, 1);
	}
	
	/**
	 * @param mouse The mouse's position.
	 * @param x The X coordinate at which the Item would be drawn.
	 * @param y The Y coordinate at which the Item would be drawn.
	 * @return If the given mouse position is hovering the slot containing the item that would be drawn at the giving coordinates (which
	 * basically means that there is a 1.5 or 2 pixel outer margin to also take into account the slot's size.
	 */
	protected boolean isHovering(PVector mouse, float x, float y) {
		return 
				x - pixel*1.5 < mouse.x && 
				mouse.x < x + pixel*9.5 && 
				y - pixel*2 < mouse.y && 
				mouse.y < y + pixel*10;
	}
	
	/**
	 * Will draw the numbers for the current Inventory bar at the given coordinates, starting from the given slot. The given
	 * coordinates should be the upper left corner of the first Item of the row - this method will do the scaling and the positioning
	 * by itself.
	 * @param x The x coordinate to draw at.
	 * @param y The y coordinate to draw at.
	 * @param slot The starting slot. Will try to draw the 10 following numbers (including this one).
	 */
	protected void drawInventoryNumbers(float x, float y, int slot) {
		y += pixel*8 - SMALL_FONT.getCharHeight()*fontScale;
		x += pixel/2 + pixel * 7.5f;

		for(int i = slot; i < slot+10; i++) {
			ItemStack item = inventory.getItem(i);
			
			if(item != null && item.getAmount() > 1) {
				String amount = Integer.toString(item.getAmount());
				glColor3f(0.22f, 0.22f, 0.22f);
				SMALL_FONT.drawText(amount, x+fontScale, y+fontScale, fontScale, Font.ALIGN_RIGHT);
				glColor3f(1f, 1f, 1f);
				SMALL_FONT.drawText(amount, x, y, fontScale, Font.ALIGN_RIGHT);
			}
			x += pixel*11;
		}
	}
	
	/**
	 * Will draw the box that should hover over the given Item, at the given coordinates.
	 * @param item The Item whose data will be displayed in the box.
	 * @param x The box's upper left corner X coordinate.
	 * @param y The box's upper left corner Y coordinate.
	 */
	protected void drawItemHoverBox(ItemStack item, float x, float y) {
		Font font = Artist.DEFAULT_FONT;
		String text = item.getType().getName();
		float width = font.getStringWidth(text)*fontScale;
		float height = font.getCharHeight()*fontScale;
		
		Artist.GUI_ATLAS.bindIfNeeded();
		addResizableQuad(x, y, width+pixel*4, height+pixel*4, pixel*2, pixel*2, hoverBoxTexture);// have a 1 pixel padding
		
		font.enable();
		font.drawText(text, x+pixel*2, y+pixel*2, fontScale);
	}
	
}

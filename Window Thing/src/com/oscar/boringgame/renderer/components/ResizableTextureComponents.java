package com.oscar.boringgame.renderer.components;

public class ResizableTextureComponents {

	/** The upper left texture. */
	public final float[] UL;
	/** The upper texture. */
	public final float[] U;
	/** The upper right texture. */
	public final float[] UR;
	/** The left texture. */
	public final float[] L;
	/** The central texture. */
	public final float[] C;
	/** The right texture. */
	public final float[] R;
	/** The lower left texture. */
	public final float[] DL;
	/** The lower texture. */
	public final float[] D;
	/** The lower right texture. */
	public final float[] DR;
	
	
	/**
	 * Will create new texture components from the given texture coordinates.
	 * @param x The x coordinate in pixels of the upper left corner of the texture.
	 * @param y The y coordinate in pixels of the upper left corner of the texture.
	 * @param sideWidth The width in pixels of the side.
	 * @param sideHeight The height in pixels of the side.
	 * @param centerWidth The width in pixels of the central zone.
	 * @param centerHeight The height in pixels of the central zone.
	 */
	public ResizableTextureComponents(int x, int y, int sideWidth, int sideHeight, int centerWidth, int centerHeight) {
		int x0 = x;
		int x1 = x0+sideWidth;
		int x2 = x1+centerWidth;
		int y0 = y;
		int y1 = y0+sideHeight;
		int y2 = y1+centerHeight;
		
		UL = Component.textureArray(x0, y0, sideWidth,   sideHeight);
		U  = Component.textureArray(x1, y0, centerWidth, sideHeight);
		UR = Component.textureArray(x2, y0, sideWidth,   sideHeight);
		
		L  = Component.textureArray(x0, y1, sideWidth,   centerHeight);
		C  = Component.textureArray(x1, y1, centerWidth, centerHeight);
		R  = Component.textureArray(x2, y1, sideWidth,   centerHeight);
		
		DL = Component.textureArray(x0, y2, sideWidth,   sideHeight);
		D  = Component.textureArray(x1, y2, centerWidth, sideHeight);
		DR = Component.textureArray(x2, y2, sideWidth,   sideHeight);
		
	}
	
}

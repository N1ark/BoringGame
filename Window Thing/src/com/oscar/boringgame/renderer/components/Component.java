package com.oscar.boringgame.renderer.components;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_SCISSOR_TEST;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glScissor;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.util.PVector;

public abstract class Component {

	/** The size of a pixel. */
	protected static final float pixel = 0.014f;
	
	/** The component's X position. */
	public float x;
	/** The component's Y position. */
	public float y;
	/** The component's width. */
	protected float width;
	/** The component's height. */
	protected float height;
	
	
	protected Component(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	/**
	 * @return The component's width.
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * Will set the component's width.
	 * @param width The new width.
	 */
	public void setWidth(float width) {
		this.width = width;
	}
	
	/**
	 * @return The component's height.
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * Will set the component's height.
	 * @param height The new height.
	 */
	public void setHeight(float height) {
		this.height = height;
	}
	
	/**
	 * @param point The point to check.
	 * @return If the given point is inside this component.
	 */
	public final boolean isInside(PVector point) {
		return x <= point.x && point.x <= x + width &&
			   y <= point.y && point.y <= y + height;
				
	}
	
	/**
	 * Will draw this component at its location.
	 * @param renderer The renderer that's displaying this component.
	 */
	public abstract void draw(Renderer renderer);

	/**
	 * Will add a quad to the current render at the given coordinates.
	 * @param x The x coordinate of the texture.
	 * @param y The y coordinate of the texture.
	 * @param width The width of the texture.
	 * @param height The height of the texture.
	 * @param texCo The texture coordinates in the currently binded texture.
	 */
	protected static void addQuad(float x, float y, float width, float height, float[] texCo) {
		glBegin(GL_QUADS); 
	    glTexCoord2f(texCo[0], texCo[1]);
	    glVertex2f(x, y);
	    glTexCoord2f(texCo[2], texCo[1]);
	    glVertex2f(x+width, y);
	    glTexCoord2f(texCo[2], texCo[3]);
	    glVertex2f(x+width, y+height);
	    glTexCoord2f(texCo[0], texCo[3]);
	    glVertex2f(x, y+height);
	    glEnd();
	}
	
	/**
	 * Will display a resizable UI component.
	 * @param x The X coordinate for the upper left corner of the central zone.
	 * @param y The Y coordinate for the upper left corner of the central zone.
	 * @param totalWidth The width of the central zone.
	 * @param totalHeight The height of the central zone.
	 * @param sideWidth The width of the sides.
	 * @param sideHeight The height of the sides.
	 * @param cornerUpLeft The texture for the upper left corner.
	 * @param cornerUpRight The texture for the upper right corner.
	 * @param cornerDownLeft The texture for the lower left corner.
	 * @param cornerDownRight The texture for the lower right corner.
	 * @param sideLeft The texture for the left side.
	 * @param sideRight The texture for the right side.
	 * @param sideUp The texture for the upper side.
	 * @param sideDown The texture for the lower side.
	 * @param zoneCenter The texture for the central zone.
	 */
	protected static void addResizableQuad(float x, float y, float totalWidth, float totalHeight, float sideWidth, float sideHeight,
			ResizableTextureComponents texture) {
		x += sideWidth;
		y += sideHeight;
		totalWidth -= sideWidth * 2;
		totalHeight -= sideHeight * 2;
		
		addQuad(x - sideWidth,  y - sideHeight,  sideWidth,  sideHeight,  texture.UL);
		addQuad(x, 			    y - sideHeight,  totalWidth, sideHeight,  texture.U);
		addQuad(x + totalWidth, y - sideHeight,  sideWidth,  sideHeight,  texture.UR);
		addQuad(x - sideWidth,  y, 			     sideWidth,  totalHeight, texture.L);
		addQuad(x, 			    y, 			     totalWidth, totalHeight, texture.C);
		addQuad(x + totalWidth, y, 			     sideWidth,  totalHeight, texture.R);
		addQuad(x - sideWidth,  y + totalHeight, sideWidth,  sideHeight,  texture.DL);
		addQuad(x, 			    y + totalHeight, totalWidth, sideHeight,  texture.D);
		addQuad(x + totalWidth, y + totalHeight, sideWidth,  sideHeight,  texture.DR);
	}
	
	/**
	 * Creates an array for the texture coordinates at the specified coordinates in the atlas.
	 * @param x The x coordinate, in pixels.
	 * @param y The y coordinate, in pixels.
	 * @param width The width of the texture in pixels.
	 * @param height The height of the texture in pixels.
	 * @return The array used for the texture at the given pixels.
	 */
	protected static float[] textureArray(float x, float y, float width, float height) {
		float halfPixel = 0.0001f; // we use this to avoid texture bleeding, because floating point operations are shit!
		return new float[] { x/128 + halfPixel, y/128 + halfPixel, (x+width)/128 - halfPixel, (y+height)/128 - halfPixel};
	}
	

	/**
	 * Will mask th following drawn quads to the limits of this components (determined from it's x, y, width and height).
	 * To start drawing like normal again, use <code>stopMasking()</code>
	 * @param renderer The renderer used to convert view coordinates to pixel coordinates.
	 */
	protected final void maskToZone(Renderer renderer) {
		maskToZone(0,0, renderer);
	}
	
	
	/**
	 * Will mask th following drawn quads to the limits of this components (determined from it's x, y, width and height).
	 * @param edgeX The additionnal border to add to the masking on the width. A positive value will make the renderable zone smaller, 
	 * while a negative value will make it larger.
	 * @param edgeY The additionnal border to add to the masking on the height. A positive value will make the renderable zone smaller, 
	 * while a negative value will make it larger.
	 * @param renderer The renderer used to convert view coordinates to pixel coordinates.
	 */
	protected final void maskToZone(float edgeX, float edgeY, Renderer renderer) {
		glEnable(GL_SCISSOR_TEST);
		int H = renderer.getWindow().getHeight();
		int h = renderer.heightFromViewToPixel(height-edgeY*2);
		int sy = -renderer.yFromViewToPixel(y+edgeY) + H - h; 
		// I need to convert the coordinates to the upside down coordinates used for glScissor.
		// Otherwise, the scissor area would be symmetric to the correct area by the X acis passing through the center of the screen.
		 
		glScissor(
				renderer.xFromViewToPixel(x+edgeX),
				sy,
				renderer.widthFromViewToPixel(width-edgeX*2), 
				h
		);
	}
	
	/**
	 * Will stop masking the rendering to this component's dimensions.
	 */
	protected static void stopMasking() {
		glDisable(GL_SCISSOR_TEST);
	}
}

package com.oscar.boringgame.renderer.components;

import static com.oscar.boringgame.renderer.components.InventoryDisplay.fontScale;
import static com.oscar.boringgame.renderer.components.InventoryDisplay.hoverBoxTexture;
import static org.lwjgl.opengl.GL11.glColor3f;

import java.text.DecimalFormat;
import java.util.function.Function;

import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Font;

public class VerticalBar extends Component implements HoverableComponent {

	/** The bar's texture */
	protected static final ResizableTextureComponents barTexture = new ResizableTextureComponents(0, 0, 0, 5, 10, 1);
	/** A white texture used to fill the bar with. */
	protected static final float[] fillingTexture = textureArray(7, 12, 1, 1);
	
	private static final DecimalFormat numFormat = new DecimalFormat("#.#");
	
	/** This bar's type. */
	private VerticalBarType type;
	/** The Player used to get information for this bar. */
	private Player player;
	
	/**
	 * Will create a vertical bar of the given type, that will display the given player's information.
	 * @param x The x coordinate of the bar's upper left corner.
	 * @param y The y coordinate of the bar's upper left corner.
	 * @param width The bar's width.
	 * @param height The bar's height.
	 * @param barType The type of bar, that will dicate the bar's filling color, its icon and what kind of data it will use.
	 * @param player The Player from which the information will be used. This should only be null if the bar type doesn't require player data.
	 */
	public VerticalBar(float x, float y, float width, float height, VerticalBarType barType, Player player) {
		super(x, y, width, height);
		this.type = barType;
		this.player = player;
	}
	
	/**
	 * @return This bar's type, that will dicate the bar's filling color, its icon and what kind of data it will use.
	 */
	public VerticalBarType getType() {
		return type;
	}
	
	/**
	 * Will set this bar's type, that will dicate the bar's filling color, its icon and what kind of data it will use.
	 * @param type The new bar type.
	 */
	public void setType(VerticalBarType type) {
		this.type = type;
	}
	
	/**
	 * @return The Player used to get data for the bar.
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * Will set the Player that will be used to get data for the bar. This should only be set to null
	 * if the bar doesn't need information from a Player.
	 * @param player The new Player to get data from.
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Override
	public void draw(Renderer renderer) {
		Artist.GUI_ATLAS.bindIfNeeded();
		
		float fill = type.value.apply(player).floatValue() / type.maxValue.apply(player).floatValue();
		addResizableQuad(x, y, width, height, 0, pixel*5, barTexture);
		addQuad(x + (width-pixel*6)/2, y - pixel*7, pixel*6, pixel*6, type.iconTexture);
		float fillingHeight = (height - pixel*4) * fill;
		glColor3f(type.r, type.g, type.b);
		addQuad(x+pixel*2, y+height-pixel*2-fillingHeight, width-pixel*4, fillingHeight, fillingTexture);
		glColor3f(1, 1, 1);
	}
	



	@Override
	public boolean drawHoverIfNeeded(Renderer renderer) {
		if(!isInside(renderer.getMousePos()))
			return false;
		
		drawHoverBox(type.prefix + numFormat.format(type.value.apply(player)) + type.sufix, renderer.getMousePos().x, renderer.getMousePos().y);
		return true;
	}
	
	
	/**
	 * Will draw the box that should hover over the given Item, at the given coordinates.
	 * @param item The Item whose data will be displayed in the box.
	 * @param x The box's upper left corner X coordinate.
	 * @param y The box's upper left corner Y coordinate.
	 */
	protected void drawHoverBox(String text, float x, float y) {
		Font font = Artist.DEFAULT_FONT;
		float width = font.getStringWidth(text)*fontScale;
		float height = font.getCharHeight()*fontScale;
		
		Artist.GUI_ATLAS.bindIfNeeded();
		addResizableQuad(x, y, width+pixel*4, height+pixel*4, pixel*2, pixel*2, hoverBoxTexture);// have a 1 pixel padding
		
		font.enable();
		font.drawText(text, x+pixel*2, y+pixel*2, fontScale);
	}
	
	
	
	public static enum VerticalBarType {
		/**
		 * The type for a bar that will display the health of a player.
		 */
		HEALTH(
				"Health: ", "HP",
				189, 25, 25,
				textureArray(0, 17, 6, 6),
				Player::getHP,
				Player::getMaxHP
		),
		/**
		 * The type for a bar that will display the energy of a player.
		 */
		ENERGY(
				"Energy: ", "",
				241, 203, 49,
				textureArray(0, 11, 6, 6),
				Player::getEnergy,
				Player::getMaxEnergy
				),
		;
		
		/** The String that will be displayed when the bar is hovered, before its value. */
		private final String prefix;
		/** The String that will be display when the bar is hovered, after its value. */
		private final String sufix;
		/** The color values to fill this bar type. */
		private final float r, g, b;
		/** The texture for this type of bar's icon. */
		private final float[] iconTexture;
		/** The function used on a Player to get the current bar's value. */
		private final Function<Player, Number> value;
		/** The function used on a Player to get the max bar value. The minimum is considered to be 0. */
		private final Function<Player, Number> maxValue;
		
		VerticalBarType(String prefix, String sufix, int r, int g, int b, float[] iconTexture, Function<Player, Number> value, Function<Player, Number> maxValue){
			this.prefix = prefix;
			this.sufix = sufix;
			this.r = r/255f;
			this.g = g/255f;
			this.b = b/255f;
			this.iconTexture = iconTexture;
			this.value = value;
			this.maxValue = maxValue;
		}
	}
}

package com.oscar.boringgame.renderer.components;

import static org.lwjgl.opengl.GL11.glColor3f;

import java.util.Arrays;

import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Font;
import com.oscar.boringgame.util.PVector;

public class ScrollList extends Component{

	/** The texture for the back of the list. */
	protected static final ResizableTextureComponents textureBack = new ResizableTextureComponents(10, 58, 3, 3, 1, 1);;
	
	/** The texture for an item in the list. */
	protected static final ResizableTextureComponents textureItem = TextField.texture;
	
	/** The texture for an item in the list when it is selected. */
	protected static final ResizableTextureComponents textureItemSelected = TextField.textureSelected;

	/** The texture for the scroll button. */
	private static final float[] scrollButtonTexture = textureArray(24, 51, 3, 5);
	
	/** The texture for the upper part of the scroll bar. */
	private static final float[] scrollBarUp = textureArray(27, 51, 3, 2);
	
	/** The texture for the middle part of the scroll bar. */
	private static final float[] scrollBar = textureArray(27, 53, 3, 1);
	
	/** The texture for the lower part of the scroll bar. */
	private static final float[] scrollBarDown = textureArray(27, 54, 3, 2);
	
	private static final float sideSize = pixel * 3;
	
	private String[] values;
	private int selectedValue;
	private float scrollPosition;
	private float valueHeight;
	private Font font;
	
	private boolean isScrolling = false;
	
	public ScrollList(float x, float y, float width, float height, String[] values, Font font) {
		super(x, y, width, height);
		this.values = values;
		this.font = font;
		this.valueHeight = pixel * 14;
		this.scrollPosition = 0;
	}
	
	/**
	 * Will set the values displayed by this ScrollList.
	 * @param values The values displayed.
	 */
	public void setValues(String[] values) {
		this.values = values;
	}
	
	/**
	 * Will add the given value to the already existing values.
	 * @param value The value to add.
	 */
	public void addValue(String value){
		values = Arrays.copyOf(values, values.length+1);
		values[values.length-1] = value;
	}
	
	/**
	 * @return The array with all values displayed by the scroll list.
	 */
	public String[] getValues() {
		return values;
	}
	
	/**
	 * @return The index of the currently selected value.
	 */
	public int getSelectedValueIndex() {
		return selectedValue;
	}
	
	/**
	 * @return The currently selected value.
	 */
	public String getSelectedValue() {
		return values[selectedValue];
	}
	
	@Override
	public void draw(Renderer renderer) {
		Artist.GUI_ATLAS.bindIfNeeded();
		
		PVector mousePos = renderer.getMousePos();
		float padding = pixel*2;
		float contentHeight = values.length * (valueHeight + pixel);
		float contentY = y;
		boolean checkInteraction = isInside(mousePos);
		
		// Draw background
		addResizableQuad(x, y, width, height, sideSize, sideSize, textureBack);
		
		// If necessary, draw scroll bar
		if(contentHeight > height - pixel * 2) {
			// Place the content and the button
			float scrollY = y+(height-pixel*5)*scrollPosition;
			contentY -= (contentHeight - (height - pixel*3)) * scrollPosition;
			
			// Draw the bar and the button
			addQuad(x+width+pixel, y, 				pixel*3, pixel*2, scrollBarUp);
			addQuad(x+width+pixel, y+pixel*2, 		pixel*3, height-pixel*4, scrollBar);
			addQuad(x+width+pixel, y+height-pixel*2,pixel*3, pixel*2, scrollBarDown);
			addQuad(x+width+pixel, scrollY, pixel*3, pixel*5, scrollButtonTexture);
			
			// Get the scroll position from the mouse's position
			if(isScrolling) {
				scrollPosition = (mousePos.y - y - pixel*2.5f)/(height - pixel*5);
				scrollPosition = scrollPosition < 0 ? 0 : scrollPosition > 1 ? 1 : scrollPosition;
			}
			
			// Check if it is scrolling
			if(!isScrolling) {
				isScrolling = KeyBinding.LEFT_CLICK.isFirstPressed() &&  // add padding to have a larger and easier hitbox.
							  mousePos.x >= x+width+pixel - padding &&
							  mousePos.x <= x+width+pixel*3 + padding &&
							  mousePos.y >= scrollY - padding &&
							  mousePos.y <= scrollY + pixel*5 + padding;
			} else {
				isScrolling = KeyBinding.LEFT_CLICK.isPressed();
			}
		}
		
		maskToZone(0, pixel, renderer);
		// Add values' boxes
		for(int i = 0; i < values.length; i++) {
			if(
					checkInteraction &&
					KeyBinding.LEFT_CLICK.isFirstRelease() && 
					mousePos.x >= x+padding &&
					mousePos.x <= x+width-padding &&
					mousePos.y >= contentY + (valueHeight + pixel) * i + padding && 
					mousePos.y <= contentY + (valueHeight + pixel) * i + padding + valueHeight
			) {
				KeyBinding.LEFT_CLICK.usedFirstRelease();
				selectedValue = i;
			}
			
			addResizableQuad(x+padding, contentY + (valueHeight + pixel) * i + padding, width - padding*2, valueHeight, sideSize, sideSize, i == selectedValue ? textureItemSelected : textureItem);
		}
		
		// Draw value's text
		glColor3f(0, 0, 0);
		font.enable();
		float maxTextLength = width - padding*2 - sideSize*2;
		for(int i = 0; i < values.length; i++) {
			float scale = 0.01f;
			if(font.getStringWidth(values[i]) * scale > maxTextLength)
				scale = (maxTextLength)/font.getStringWidth(values[i]);
			
			font.drawText(values[i], x+sideSize+padding, contentY + (valueHeight + pixel) * i + padding + valueHeight/2, scale, Font.ALIGN_LEFT, Font.VERTICAL_ALIGN_CENTER);
		}
		glColor3f(1, 1, 1);
		stopMasking();
	}
	
}
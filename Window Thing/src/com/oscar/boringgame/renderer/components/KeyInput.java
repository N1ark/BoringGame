package com.oscar.boringgame.renderer.components;

import static org.lwjgl.opengl.GL11.glColor3f;

import org.lwjgl.glfw.GLFW;

import com.oscar.boringgame.controller.Controller;
import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.controller.KeyboardListener;
import com.oscar.boringgame.controller.MouseListener;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Font;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.PVector;

public class KeyInput extends Component implements KeyboardListener, MouseListener {
	
	private static final ResizableTextureComponents backgroundTexture = Button.texture;
	private static final ResizableTextureComponents keyTexture = TextField.texture;
	private static final ResizableTextureComponents keyTextureSelected = TextField.textureSelected;
	
	private final String bindingName;
	private boolean selected;
	private String keyName;
	private final Font font;
	private final KeyBinding binding;
	/** a variable used to avoid having the input being re-selected when the user uses the left click binding. */
	private boolean ignoreNextClick;
	
	/**
	 * Will create a new KeyInput at the given coordinates with the given size, that will affect the given KeyBinding.
	 * @param x The X coordinate of the KeyInput.
	 * @param y The Y coordinate of the KeyInput.
	 * @param width The total width of the KeyInput, including the button and the key display.
	 * @param height The height of the KeyInput.
	 * @param binding The KeyBInding this input will be affecting.
	 * @param font The font used to draw text.
	 * @throws IllegalArgumentException If the given KeyBInding is final, in which case it cannot be modified anyways.
	 */
	public KeyInput(float x, float y, float width, float height, KeyBinding binding, Font font) {
		super(x, y, width, height);
		
		if(binding.isFinal())
			throw new IllegalArgumentException("The given binding is final, so it cannot be modified!");

		this.binding = binding;
		this.bindingName = BoringUtil.capitalize(binding.name().replace('_', ' '));
		this.font = font;
		this.keyName = binding.isMouseBinding() ? Controller.getButtonName(binding.getBinding()) : Controller.getKeyName(binding.getBinding());
		this.ignoreNextClick = false;
	}
	
	@Override
	public void draw(Renderer renderer) {
		if(keyName == null) {
			System.err.println("No key name found for the keybinding " + binding + " (key " + binding.getBinding() + ", is mouse: " + binding.isMouseBinding() + ")");
			keyName = "[?]";
		}
		
		float buttonWidth = width - height - pixel;
		PVector pos = renderer.getMousePos();
		if(KeyBinding.LEFT_CLICK.isFirstRelease()) {
			if(ignoreNextClick)
				ignoreNextClick = false;
			else {
				selected = isInside(pos);
				if(selected)
					KeyBinding.LEFT_CLICK.isFirstRelease();
			}
		}

		Artist.GUI_ATLAS.bindIfNeeded();
		
		addResizableQuad(x, y, buttonWidth, height, pixel*13, pixel*4, backgroundTexture);
		addResizableQuad(x+buttonWidth+pixel, y, height, height, pixel*3, pixel*3, selected ? keyTextureSelected : keyTexture);
		
		font.enable();
		
		glColor3f(0, 0, 0);
		float scale = 0.01f;
		if(font.getStringWidth(bindingName) * scale > buttonWidth - pixel * 5) {
			scale = (buttonWidth - pixel * 5) / font.getStringWidth(bindingName);
		}
		font.drawText(bindingName, x+buttonWidth/2, y+height/2, scale, Font.ALIGN_CENTER, Font.VERTICAL_ALIGN_CENTER);
		
		scale = 0.01f;
		if(font.getStringWidth(keyName) * scale > height - pixel*4) {
			scale = (height - pixel*4) / font.getStringWidth(keyName);
		}
		font.drawText(keyName, x+buttonWidth+pixel+height/2, y+height/2, scale, Font.ALIGN_CENTER, Font.VERTICAL_ALIGN_CENTER);
		glColor3f(1, 1, 1);
	}
	
	@Override
	public void releasedKey(int keyCode) {
		if(selected) {
			selected = false;
			if(Controller.getKeyName(keyCode) != null) {
				binding.setBinding(keyCode, false);
				keyName = Controller.getKeyName(keyCode);
			}
		}
	}
	
	@Override
	public void releasedButton(int button) {
		if(selected) {
			selected = false;
			binding.setBinding(button, true);
			keyName = Controller.getButtonName(button);
			
			if(button == GLFW.GLFW_MOUSE_BUTTON_LEFT)
				ignoreNextClick = true;
		}
	}
	
	@Override
	public void typedChar(char c) {/*not used*/}
	
	@Override
	public void pressedKey(int keyCode) {/*not used*/}

	@Override
	public void pressedButton(int button) {/*not used*/}
	
	
}

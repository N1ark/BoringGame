package com.oscar.boringgame.renderer.components;

import static org.lwjgl.opengl.GL11.glColor3f;

import java.util.ArrayList;
import java.util.List;

import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Font;

public class Button extends Component {
	
	/** The button's base texture. */
	protected static final ResizableTextureComponents texture = new ResizableTextureComponents(10, 24, 13, 4, 2, 1); 

	/** The button's texture when it is hovered. */
	protected static final ResizableTextureComponents textureHovered = new ResizableTextureComponents(10, 33, 13, 4, 2, 1); 
	
	/** The button's texture when it is disabled. */
	protected static final ResizableTextureComponents textureDisabled = new ResizableTextureComponents(10, 42, 13, 4, 2, 1);
	
	private String label;
	private Font font;
	private float fontSize = 0.01f;
	private boolean isEnabled;
	
	private final List<ButtonListener> listeners = new ArrayList<>();
	
	/**
	 * Creates a button placed at the given position and with the given width.
	 * @param x The button's X coordinate.
	 * @param y The button's Y coordinate.
	 * @param width The button's width.
	 * @param height The button's height.
	 * @param label The button's label.
	 */
	public Button(float x, float y, float width, float height, String label, Font font) {
		super(x, y, width, height);
		this.label = label;
		this.font = font;
		this.isEnabled = true;
	}
	
	/**
	 * @return This button's label.
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * Will set this button's label.
	 * @param label The new label.
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/**
	 * Will set the button's label's font size.
	 * @param size The new font size.
	 */
	public void setFontSize(float size) {
		this.fontSize = size;
	}
	
	/**
	 * @return If this button is enabled.
	 */
	public boolean isEnabled() {
		return isEnabled;
	}
	
	/**
	 * Will set whether or not this button is enabled.
	 * @param enabled If this button's enabled.
	 */
	public void setEnabled(boolean enabled) {
		isEnabled = enabled;
	}
	
	/**
	 * Will add the given listener to this button's listeners.
	 * @param listener The object that's listening to this button's interactions.
	 */
	public void addListener(ButtonListener listener) {
		if(!listeners.contains(listener))
			listeners.add(listener);
	}
	
	/**
	 * Will try to remove the given listener from this button's listeners.
	 * @param listener The listener to remove.
	 * @return If it was successfully removed.
	 */
	public boolean removeListener(ButtonListener listener) {
		return listeners.remove(listener);
	}
	
	@Override
	public void draw(Renderer renderer) {
		
		Artist.GUI_ATLAS.bindIfNeeded();
		
		boolean isHovered = renderer.getController().isMouseInWindow() && isInside(renderer.getMousePos()); 
		
		addResizableQuad(x, y, width, height, pixel*13, pixel*4, isEnabled ? isHovered ? textureHovered : texture : textureDisabled);
		font.enable();
		glColor3f(0, 0, 0);
		font.drawText(label, x+width/2, y+height/2, fontSize, Font.ALIGN_CENTER, Font.VERTICAL_ALIGN_CENTER);
		glColor3f(1, 1, 1);
		
		if(isHovered && isEnabled && KeyBinding.LEFT_CLICK.isFirstRelease()) {
			KeyBinding.LEFT_CLICK.usedFirstRelease();
			for(ButtonListener listener : listeners)
				listener.pressedButton(this);
		}
	}

	public static interface ButtonListener {
		
		/**
		 * Called when the button this listener listens to is pressed with a left click.
		 * @param button The pressed button
		 */
		public void pressedButton(Button button);
		
	}
	
}
package com.oscar.boringgame.renderer.components;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.controller.KeyboardListener;
import com.oscar.boringgame.files.FileNameValidator;
import com.oscar.boringgame.renderer.Renderer;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Font;

public class TextField extends Component implements KeyboardListener {

	/** The text field's texture. */
	protected static final ResizableTextureComponents texture = new ResizableTextureComponents(10, 51, 3, 3, 1, 1);
	
	/** The text field's texture when it is selected. */
	protected static final ResizableTextureComponents textureSelected = new ResizableTextureComponents(17, 51, 3, 3, 1, 1);
	
	/** The size of the sides of the text field. */
	private static final float sideSize = pixel * 3;
	/** The max amount of characters that can be typed into the text field. */
	private static final int maxInputLength = 32;
	
	private String defaultLabel;
	private String currentLabel;
	private Font font;
	private boolean isSelected;
	private int backspaceCooldown;
	private boolean isForFileName;
	
	private final List<TextFieldListener> listeners = new ArrayList<>();
	
	/**
	 * Will create a new text field.
	 * @param x The field's X coordinate.
	 * @param y The field's Y coordinate.
	 * @param width The field's width.
	 * @param height The field's height.
	 * @param defaultLabel The String that appears when the current string is empty.
	 * @param font The font used.
	 */
	public TextField(float x, float y, float width, float height, String defaultLabel, Font font) {
		super(x, y, width, height);
		this.font = font;
		this.defaultLabel = defaultLabel;
		this.currentLabel = "";
		this.isForFileName = false;
	}
	
	/**
	 * @return If this text field is currently selected.
	 */
	public boolean isSelected() {
		return isSelected;
	}
	
	/**
	 * @return The text currently typed by the user. This value cannot be null.
	 */
	public String getCurrentText() {
		return currentLabel;
	}
	
	/**
	 * @return If this textbox is currently empty, which means that the default label is displayed instead.
	 */
	public boolean isEmpty() {
		return currentLabel.isEmpty();
	}
	
	/**
	 * @return This text field's default label, that's displayed when its content is empty.
	 */
	public String getDefaultLabel() {
		return defaultLabel;
	}
	
	/**
	 * Will set this text field's default label, that will be displayed if its content is empty.
	 * @param label The default label.
	 */
	public void setDefaultLabel(String label) {
		this.defaultLabel = label;
	}
	
	/**
	 * @return If this text field is for a file name, in which case some characters won't be allowed to be inputted.
	 */
	public boolean isForFileName() {
		return isForFileName;
	}
	
	/**
	 * Will set whether or not this text field is for a file name, in which case some characters won't be allowed to be inputted.
	 * @param isForFileName If it is for a file name.
	 */
	public void setIsForFileName(boolean isForFileName) {
		this.isForFileName = isForFileName;
	}
	
	/**
	 * Will make the given listener listen to any updates from this text field.
	 * @param listener The listener to add.
	 */
	public void addTextFieldListener(TextFieldListener listener) {
		if(!listeners.contains(listener))
			listeners.add(listener);
	}
	
	/**
	 * Will try to remove the given listener to the listeners that are observing this text field.
	 * @param listener The listener to remove.
	 * @return If the listener was actually listening to the text field before it was removed.
	 */
	public boolean removeTextFieldListener(TextFieldListener listener) {
		return listeners.remove(listener);
	}
	
	@Override
	public void draw(Renderer renderer) {
		Artist.GUI_ATLAS.bindIfNeeded();
		
		if(backspaceCooldown--<0 && KeyBinding.BACKSPACE.isPressed() && !currentLabel.isEmpty()) {
			currentLabel = currentLabel.substring(0, currentLabel.length()-1);
			backspaceCooldown = 5;
			for(TextFieldListener listener : listeners)
				listener.valueChanged(this);
		}
		
		

		if(KeyBinding.LEFT_CLICK.isFirstRelease()) {
			isSelected = renderer.getController().isMouseInWindow() && isInside(renderer.getMousePos());
			if(isSelected)
				KeyBinding.LEFT_CLICK.usedFirstRelease();
		}
		
		addResizableQuad(x, y, width, height, sideSize, sideSize, isSelected ? textureSelected : texture);
		
		font.enable();
		String text;
		if(currentLabel.isEmpty()) {
			GL11.glColor3f(0.25f, 0.25f, 0.25f);
			text = defaultLabel;
		} else {
			GL11.glColor3f(0, 0, 0);
			text = currentLabel;
		}
		float scale = 0.01f;
		float maxLength = width - sideSize*2;
		if(font.getStringWidth(text) * scale > maxLength) {
			scale = (maxLength) / font.getStringWidth(text);
		}
		font.drawText(text, x+sideSize, y+height/2, scale, Font.ALIGN_LEFT, Font.VERTICAL_ALIGN_CENTER);		
		GL11.glColor3f(1, 1, 1);
	}

	@Override
	public void typedChar(char c) {
		if(isSelected && currentLabel.length() < maxInputLength && font.isValid(c)) {
			if(c == ' ' && currentLabel.isEmpty())
				return;
			if(isForFileName && !FileNameValidator.isAllowed(c))
				return;
			
			currentLabel += c;
			for(TextFieldListener listener : listeners)
				listener.valueChanged(this);
		}
	}
	
	@Override
	public void pressedKey(int keyCode) {/*not used*/}
	
	@Override
	public void releasedKey(int keyCode) {/*not used*/}
	
	public static interface TextFieldListener {
		
		/**
		 * Called when the text field this listener listens to has had its value changer.
		 * @param textField The text field that changed.
		 */
		public void valueChanged(TextField textField);
		
	}
	
}

package com.oscar.boringgame.renderer;

import static com.oscar.boringgame.controller.KeyBinding.HIGHLIGHT_FACED_BLOCK;
import static com.oscar.boringgame.controller.KeyBinding.SHOW_DEBUG;
import static com.oscar.boringgame.renderer.helper.Artist.BLOCK_ATLAS;
import static com.oscar.boringgame.renderer.helper.Artist.DEFAULT_FONT;
import static com.oscar.boringgame.renderer.helper.Artist.GUI_ATLAS;
import static com.oscar.boringgame.renderer.helper.Artist.SMALL_FONT;
import static com.oscar.boringgame.renderer.helper.Artist.setView;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glColor3fv;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTranslatef;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.oscar.boringgame.blocks.ComplexFurnitureBlock;
import com.oscar.boringgame.blocks.ComplexFurnitureBlock.ComplexFurniturePart;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.ExtraInfoDisplayBlock;
import com.oscar.boringgame.blocks.blocktypes.ExtraInfoDisplayBlock.ExtraInfoDisplay;
import com.oscar.boringgame.blocks.blocktypes.NoHitboxSurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.controller.Controller;
import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.entity.DroppedItem;
import com.oscar.boringgame.entity.Entity;
import com.oscar.boringgame.entity.EntityType;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.map.Chunk;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.particle.BlockBreakingParticle;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.renderer.components.HotbarDisplay;
import com.oscar.boringgame.renderer.components.InventoryDisplay;
import com.oscar.boringgame.renderer.components.VerticalBar;
import com.oscar.boringgame.renderer.components.VerticalBar.VerticalBarType;
import com.oscar.boringgame.renderer.helper.Font;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public class WorldRenderer extends Renderer {
	
	/** The scale for the font in normal UI elements. */
	private static final float inventoryFontScale = 0.006f;
	
	/** The scale for the font in the debug UI. */
	private static final float debugFontScale = 0.0055f;

	/** A DecimalFormat for rendering user visible numbers */
	private static final DecimalFormat numFormat = new DecimalFormat("#.###;-#");

	/** The field of view used, specifying what blocks are rendered. */
	private final Area fieldOfView = new Area(0,0);
	
	/** The field of view used for the blocks, but converted to chunk positions. */
	private final Area fieldOfViewAsChunks = new Area(0,0);
	
	/** The field of view used to know where displayed Entities and Particles should stand. 
	 * It is usually 2 blocks larger than <code>fieldOfView</code> in every direction.*/
	private final Area entityFieldOfView = new Area(0,0);
	
	/** The object containing debug info. */
	private final RenderingInfo debug;
	
	/** The drawn world. */
	private World world;
	
	/** The camera, used to know what to renderer. */
	private Camera camera;
	
	/** The player whose informations will be rendered. */
	private Player player;
	
	/** The component that will draw the Player's inventory. */
	private InventoryDisplay invDisp;
	
	/** The component that will draw the Player's hotbar. */
	private HotbarDisplay hotbarDisp;
	
	/** The component that will draw the Player's health bar. */
	private VerticalBar healthBar;
	
	/** The component that will draw the Player's energy bar. */
	private VerticalBar energyBar;
	
	public WorldRenderer(World world, Camera camera, Player player, Window window, Controller controller, RenderingInfo debug) {
		super(window, controller);
		this.world = world;
		this.camera = camera;
		this.debug = debug;
		setPlayer(player);
	}
	
	/**
	 * Will set the world that will be drawn by this renderer.
	 * @param world The World that will be drawn.
	 */
	public void setWorld(World world) {
		this.world = world;
	}
	
	/**
	 * Will set the camera that specifies from where and how the World is seen.
	 * @param camera The camera used.
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	
	/**
	 * @return The player used by this renderer to draw its inventory etc.
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * Will set the Player rendered by this Renderer.
	 * @param player The Player to who extra information should be displayed. If no extra information is required, this can be set to null.
	 */
	public void setPlayer(Player player) {
		this.player = player;
		float pixel = 0.014f;
		invDisp = new InventoryDisplay(player.getInventory(), 0, 0, pixel*111);
		hotbarDisp = new HotbarDisplay(player.getInventory(), player, 0, -1+pixel*23+invDisp.getWidth()/2, 0.8f, invDisp.getWidth());
		hotbarDisp.y = 1 - hotbarDisp.getHeight()/2 - pixel;
		healthBar = new VerticalBar(-1+pixel, 0.35f-pixel, pixel*10, 0.65f, VerticalBarType.HEALTH, player);
		energyBar = new VerticalBar(healthBar.x+healthBar.getWidth()+pixel, 0.35f-pixel, pixel*10, 0.65f, VerticalBarType.ENERGY, player);
	}
	
	/**
	 * Will draw the given World and all its layers at the location specified by the Camera, and showing extra information to the given Player.
	 */
	@Override
	protected void draw() {
		setView(camera, fieldOfView);		
		entityFieldOfView.set(fieldOfView).expand(2);
		fieldOfViewAsChunks.set(fieldOfView).add(0, 6).divide(Chunk.SIZE).maximize();
		world.setCurrentlySeenChunks(fieldOfViewAsChunks);
		
		drawTerrain();
		drawAboveGround();
		if(player != null)
			drawTerrainBonus();
		
		drawEntities();
		drawParticles();
		
		if(!KeyBinding.HIDE_UI.isActivated()) {
			switchToGUIView();
			drawOverlay();
			if(SHOW_DEBUG.isActivated())
				drawDebug();
		}
	}
	
	
	/**
	 * Will draw the ground of the World, only made of SolidBlocks.
	 */
	private void drawTerrain() {
		BLOCK_ATLAS.bindIfNeeded();
		glPushMatrix();
		glTranslatef(0, 0, Float.MIN_VALUE);

		SolidBlock b;
		for(int yChunk = (int) fieldOfViewAsChunks.y1; yChunk < fieldOfViewAsChunks.y2; yChunk++) {
			int yGlobal = yChunk * Chunk.SIZE;
			for(int xChunk = (int) fieldOfViewAsChunks.x1; xChunk < fieldOfViewAsChunks.x2; xChunk++) {
				int xGlobal = xChunk * Chunk.SIZE;
				
				Chunk chunk = world.getChunkOfCoordinates(xChunk, yChunk);
				for(int x = 0; x < Chunk.SIZE; x++) {
					for(int y = 0; y < Chunk.SIZE; y++) {
						if((b = chunk.getBlock(x, y)) != SolidBlock.ERROR) {
							if (b == SolidBlock.GRASS)
								glColor3fv(chunk.getBiome(x, y).getGrassColor());
							addQuad(x+xGlobal, y+yGlobal, b);
							if (b == SolidBlock.GRASS)
								glColor3f(1, 1, 1);
						}
					}
				}
			}
		}
		
		glPopMatrix();
	}
	
	/**
	 * Will draw all the above ground blocks in the World, with the proper depth.
	 */
	private void drawAboveGround() {
		BLOCK_ATLAS.bindIfNeeded();
		glPushMatrix();
		
		SurfaceBlock b;
		for(int yChunk = (int) fieldOfViewAsChunks.y1; yChunk < fieldOfViewAsChunks.y2; yChunk++) {
			int yGlobal = yChunk * Chunk.SIZE;
			for(int xChunk = (int) fieldOfViewAsChunks.x1; xChunk < fieldOfViewAsChunks.x2; xChunk++) {
				int xGlobal = xChunk * Chunk.SIZE;
				
				Chunk chunk = world.getChunkOfCoordinates(xChunk, yChunk);
				for(int x = 0; x < Chunk.SIZE; x++) {
					for(int y = 0; y < Chunk.SIZE; y++) {
						if((b = chunk.getUpperBlock(x, y)) != AIR.AIR) {
							
							float z = depthFor(yGlobal + y);
							
							if(b instanceof NoHitboxSurfaceBlock) {
								if(((NoHitboxSurfaceBlock) b).isFlat())
									z = Float.MIN_VALUE+1;
								else
									z += ((NoHitboxSurfaceBlock) b).getYOffset();
		
								glTranslatef(0, 0, z);
								addQuad(xGlobal + x, yGlobal + y, b);
								glTranslatef(0, 0, -z);
							}
							
							else if(b instanceof ComplexFurnitureBlock) {
								Area size = ((ComplexFurnitureBlock) b).getDisplay();
								for(ComplexFurniturePart part : ((ComplexFurnitureBlock) b).getParts()) {
									glTranslatef(0, 0, z + part.getHitbox().y2);
									addQuad(xGlobal + x + size.x1, yGlobal + y + size.y1, size.getWidth(), size.getHeight(), part.getTextureCoordinates());
									glTranslatef(0, 0, -z - part.getHitbox().y2);
								}
							}
							
							else if(b instanceof SurfaceHitboxBlock) {
								Area disp = ((SurfaceHitboxBlock) b).getDisplay();
								z += ((SurfaceHitboxBlock) b).getHitbox().y2;
								glTranslatef(0, 0, z);
								addQuad(xGlobal + x + disp.x1, yGlobal + y + disp.y1, disp.getWidth(), disp.getHeight(), b);
								glTranslatef(0, 0, -z);
							}
							
							else {
								System.err.println("Something fell through the tests... -> " + b);
							}
							
							
							if(b instanceof ExtraInfoDisplayBlock) {
								for(ExtraInfoDisplay extra : ((ExtraInfoDisplayBlock) b).getExtraInfo()) {
									Area disp = extra.position;
									glTranslatef(0, 0, z);
									addQuad(xGlobal + x + disp.x1, yGlobal + y + disp.y1, disp.getWidth(), disp.getHeight(), extra.texture);
									glTranslatef(0, 0, -z);
								}
							}
							
						}
					}
				}
			}
		}
		
		glPopMatrix();
	}
	
	/**
	 * Will draw additional information of the world for the Player, such as the faced block's breaking value, or the highlight for the faced Block.
	 */
	private void drawTerrainBonus() {
		BLOCK_ATLAS.bindIfNeeded();
		glPushMatrix();
		
		Location loc = player.getFacingBlock();
		float z = depthFor(loc.y);
		glTranslatef(0, 0, z);

		int breakState = world.getBreakState(loc.x, loc.y) - 1;
		if(breakState >= 0)
			addQuad(loc.x, loc.y, 1, 1, Block.BREAKING_ANIMATION_TEXTURE[breakState]);
		
		if(HIGHLIGHT_FACED_BLOCK.isPressed())
			addQuad(loc.x, loc.y, 1, 1, Block.SELECTED_BLOCK_TEXTURE);			
		
		glPopMatrix();
	}
	
	/**
	 * Will draw all visible Entities in the given World.
	 */
	private void drawEntities() {
		List<Entity> entities = world.visibleEntities(entityFieldOfView);
		Collections.sort(entities, Comparator.comparing(Entity::getType));
		
		glPushMatrix();

		for(Entity e : entities) {
			e.getTexture().bindIfNeeded();
			
			Location loc = e.getLocation();
			Area disp = e.getDisplay();
			float z = depthFor(loc.y);
			
			
			glTranslatef(0, 0, z);
			if(e.getType() == EntityType.DROPPED_ITEM)
				drawDroppedItem((DroppedItem) e);
			else
				addQuad(loc.x + disp.x1, loc.y + disp.y1, disp.getWidth(), disp.getHeight(), e.getTextureCoordinates());
		
			glTranslatef(0, 0, -0.01f);
			addQuad(loc.x + disp.x1, loc.y + disp.y1 + disp.getHeight()*0.8f, disp.getWidth(), disp.getHeight() * 0.3f, e.getShadowOffsets());
			glTranslatef(0, 0, -z + 0.01f);
		}
		glPopMatrix();
	}
	
	/**
	 * Will draw all visible Particles in the given World.
	 */
	private void drawParticles() {
		List<ParticleEntity> particles = world.visibleParticles(entityFieldOfView);
		particles.sort(Comparator.comparing(ParticleEntity::getType));
		glPushMatrix();
		
		for(ParticleEntity p : particles) {
			p.getTexture().bindIfNeeded();
			float size = p.getType().getWidth();
			Location loc = p.getLocation();
			float z = depthFor(p.getHeight());
			
			if(p instanceof BlockBreakingParticle && ((BlockBreakingParticle) p).getBlock() == SolidBlock.GRASS)
				glColor3fv(p.getLocation().getBiome().getGrassColor());
			glTranslatef(0, 0, z);
			addQuad(loc.x-size/2f, loc.y-size/2f, size, size, p.getTextureCoordinates());
			glTranslatef(0, 0, -z);
			glColor3f(1, 1, 1);
		}
		
		glPopMatrix();
	}
	
	/**
	 * Will draw the overlay with all the necessary information to the given Player.
	 */
	private void drawOverlay() {
		GUI_ATLAS.bindIfNeeded();
		glPushMatrix();
		glTranslatef(0, 0, 1);

		// Add drawing info for hotbar/inventory
		controller.setHoveredSlot(-1);
		controller.setHoveredItemStack(null);
		controller.setHoveredInventory(null);
		if(player.getCurrentInventory() == null) {
			hotbarDisp.draw(this); // This is only valid if the open inventory is the player's
			healthBar.draw(this);
			energyBar.draw(this);
		}
		else {
			invDisp.draw(this);
		}	
			
		if(controller.isMouseInWindow() && player.getCursor() != null) {
			PVector pos = controller.getMousePosition();
			float x = xFromPixelToView(pos.x)-0.057f;
			float y = yFromPixelToView(pos.y)-0.057f;
			BLOCK_ATLAS.bind();
			invDisp.drawInventoryItem(x, y, player.getCursor(), this);

			if(player.getCursor().getAmount() > 1) {
				SMALL_FONT.enable();
				String amount = Integer.toString(player.getCursor().getAmount());
				y -= SMALL_FONT.getCharHeight()*inventoryFontScale;
				glColor3f(0.22f, 0.22f, 0.22f);
				SMALL_FONT.drawText(amount, x+0.112f+inventoryFontScale, y+0.114f+inventoryFontScale, inventoryFontScale, Font.ALIGN_RIGHT);
				glColor3f(1f, 1f, 1f);
				SMALL_FONT.drawText(amount, x+0.112f, y+0.114f, inventoryFontScale, Font.ALIGN_RIGHT);
			}
		} 
		else {
			if(player.getCurrentInventory() == null) {
				if(
						hotbarDisp.drawHoverIfNeeded(this) || // Since these are lazy operators, it will only draw the
						healthBar.drawHoverIfNeeded(this) ||  // hover box for one component, and then stop!
						energyBar.drawHoverIfNeeded(this)
				);
			}
			else {
				invDisp.drawHoverIfNeeded(this);
			}
		}
		
		
		glPopMatrix();
	}
	
	/**
	 * Will draw the given DroppedItem at its corresponding location.
	 * @param dropped The drawn item.
	 */
	private static void drawDroppedItem(DroppedItem dropped) {
		Item type = dropped.getItem().getType();
		Area disp = dropped.getDisplay();
		Location loc = dropped.getLocation();
		if(type == SolidBlock.GRASS)
			glColor3fv(Biome.PLAIN.getGrassColor());
		
		float width = dropped.getDisplay().getWidth();
		float height = dropped.getDisplay().getHeight();
		float xExtra = 0;
		float yExtra = 0;

		if(type instanceof SurfaceHitboxBlock) {
			Area dimension = ((SurfaceHitboxBlock) type).getDisplay();
			if(dimension.getWidth() > dimension.getHeight()) {
				float oldHeight = height;
				height *= dimension.getHeight()/dimension.getWidth();
				yExtra = (oldHeight-height)/2f;
			}
			else if(dimension.getHeight() > dimension.getWidth()) {
				float oldWidth = width;
				width *= dimension.getWidth()/dimension.getHeight();
				xExtra = (oldWidth-width)/2f;
			}
		}

		if(type instanceof ComplexFurnitureBlock)
			for(ComplexFurniturePart part : ((ComplexFurnitureBlock) type).getParts())
				addQuad(loc.x + disp.x1 + xExtra, loc.y + disp.y1 + yExtra, width, height, part.getTextureCoordinates());
		else
			addQuad(loc.x + disp.x1 + xExtra, loc.y + disp.y1 + yExtra, width, height, type.getTextureCoordinates(0)); //TODO add texture animations

		if(type == SolidBlock.GRASS)
			glColor3f(1, 1, 1);
	}
	
	/**
	 * Will draw debug information relative to the given player on the screen.
	 */
	private void drawDebug() {
		World world = player.getWorld();
		int mb = 1024 * 1024; 
		Runtime instance = Runtime.getRuntime();
		
		glPushMatrix();
		glTranslatef(0, 0, 1);
		
		DEFAULT_FONT.enable();
		
		String[] linesLeft = new String[] {
				"FPS: " + debug.framesPerSecond,
				"Zoom: " + camera.getZoom() + " blocks",
				"",
				"X: " + numFormat.format(player.getLocation().x),
				"Y: " + numFormat.format(player.getLocation().y),
				"Chunk: " + (int) Math.floor(player.getLocation().x / Chunk.SIZE) 
					+ "/" + (int) Math.floor(player.getLocation().y / Chunk.SIZE),
				"Biome: " + player.getLocation().getBiome().getName(),
				"",
				"VX: " + numFormat.format(player.getVelocity().x),
				"VY: " + numFormat.format(player.getVelocity().y),
				"",
				"Entities: " + world.getEntities().size(),
				"Particles: " + world.getParticles().size(),
				"Block Updates: " + world.amountOfBlockUpdates(),
				"Tick Speed: " + debug.tickSpeed + "ms",
				"Loaded Chunks: " + world.getLoadedChunks(),
				"Seed: " + world.seed 
		};
		String[] linesRight = new String[] {
				"Free Memory: " + (instance.freeMemory()/mb) + "/" + (instance.totalMemory()/mb) + "Mo",
				
		};

		float height = DEFAULT_FONT.getCharHeight()*debugFontScale;
		float x = -1;
		float y = -1f;
		
		for(String line : linesLeft) {
			glColor3f(0.3f, 0.3f, 0.3f);
			DEFAULT_FONT.drawText(line, x+debugFontScale, y+debugFontScale, debugFontScale, Font.ALIGN_LEFT);
			glColor3f(1, 1, 1);
			DEFAULT_FONT.drawText(line, x, y, debugFontScale, Font.ALIGN_LEFT);
			
			y += height;
		}
		
		x = 1;
		y = -1f;
		
		for(String line : linesRight) {
			glColor3f(0.3f, 0.3f, 0.3f);
			DEFAULT_FONT.drawText(line, x+debugFontScale, y+debugFontScale, debugFontScale, Font.ALIGN_RIGHT);
			glColor3f(1, 1, 1);
			DEFAULT_FONT.drawText(line, x, y, debugFontScale, Font.ALIGN_RIGHT);
			
			y += height;
		}
		
		glPopMatrix();
	}
	
	/**
	 * @param x The X pixel coordinate to convert.
	 * @return The world X coordinate equivalent for the given pixel X coordinate. 
	 * <br><strong>Not be confused with <code>xFromPixelToView(float)</code></strong>
	 */
	public float xFromPixelToWorld(float x) {
		return fieldOfView.x1 + x * fieldOfView.getWidth() / window.getWidth();
	}
	
	/**
	 * @param y The Y pixel coordinate to convert.
	 * @return The view Y coordinate equivalent for the given pixel Y coordinate.
	 * <br><strong>Not be confused with <code>yFromPixelToView(float)</code></strong>
	 */
	public float yFromPixelToWorld(float y) {
		return fieldOfView.y1 + y * fieldOfView.getHeight() / window.getHeight();
	}
	

	/**
	 * @param y The Y coordinate.
	 * @return The depth value for something at the given Y coordinate.
	 */
	private float depthFor(float y) {
		return y - fieldOfView.y1;
	}
	
	/**
	 * Will add a quad to the current render at the given coordinates.
	 * @param x The x coordinate of the Block.
	 * @param y The y coordinate of the Block.
	 * @param block The block from which the texture will be used.
	 */
	private void addQuad(float x, float y, Block block) {
		addQuad(x, y, 1, 1, block.getTextureCoordinates(getFrame()));
	}

	/**
	 * Will add a quad to the current render at the given coordinates.
	 * @param x The x coordinate of the Block.
	 * @param y The y coordinate of the Block.
	 * @param width The width of the Block.
	 * @param height The height of the Block.
	 * @param block The block from which the texture will be used.
	 */
	private void addQuad(float x, float y, float width, float height, Block block) {
		addQuad(x, y, width, height, block.getTextureCoordinates(getFrame()));
	}
}
package com.oscar.boringgame.renderer;

import static org.lwjgl.opengl.GL11.glClearColor;

import java.util.List;

import com.oscar.boringgame.controller.Controller;
import com.oscar.boringgame.files.FileNameValidator;
import com.oscar.boringgame.files.WorldIO;
import com.oscar.boringgame.main.GameState;
import com.oscar.boringgame.renderer.components.Button;
import com.oscar.boringgame.renderer.components.Button.ButtonListener;
import com.oscar.boringgame.renderer.components.ScrollList;
import com.oscar.boringgame.renderer.components.TextField;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.PVector;

public class MenuRenderer extends Renderer {
	
	private final float[] fullTexture = new float[] {0,0,1,1};
	private final Texture[] backgroundTextures;
	private final Texture skyTexture;
	
	private final PVector panoramaPos;
	
	private final Button generateButton;
	private final Button openButton;
	private final TextField genWorldName;
	private final ScrollList worldList;

	/**
	 * Will create a new main menu, with the needed components, and load all its needed textures.
	 */
	public MenuRenderer(Window window, Controller controller) {
		super(window, controller);
		panoramaPos = new PVector();
		backgroundTextures = new Texture[] {new Texture("panorama_1"), new Texture("panorama_2"), new Texture("panorama_3"), new Texture("panorama_4")};
		skyTexture = new Texture("panorama_sky");
		
		generateButton = new Button(0.2f, 0.05f, 0.6f, 0.2f, "Generate", Artist.DEFAULT_FONT);
		generateButton.addListener(new ButtonListener() {
		@Override
			public void pressedButton(Button button) {
				String name = genWorldName.getCurrentText();
				
				// Make name valid
				name = FileNameValidator.makeValid(name);
				
				// Make sure name is unique
				List<String> worldNames = WorldIO.getExistingSavedWorlds();
				if(worldNames.contains(name)) {
					String original = name + "-";
					int x = 0;
					while(worldNames.contains(name)) {						
						x++;
						name = original + x;
					}
				}
					
				GameState.createNewWorld(name);
			}
		});

		genWorldName = new TextField(0.1f, -0.25f, 0.8f, 0.2f, "World name...", Artist.DEFAULT_FONT);
		genWorldName.setIsForFileName(true);
		controller.addKeyboardListener(genWorldName);
		
		worldList = new ScrollList(-0.9f, -0.8f, 0.8f, 1.4f, WorldIO.getExistingSavedWorlds().toArray(new String[0]), Artist.DEFAULT_FONT);
		openButton = new Button(-0.8f, 0.7f, 0.6f, 0.2f, "Open", Artist.DEFAULT_FONT);
		openButton.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				String name = worldList.getSelectedValue();
				GameState.openWorld(name);
			}
		});
		if(worldList.getValues().length == 0)
			openButton.setEnabled(false);
	}
	
	@Override
	protected void draw() {
		switchToGUIView();
		
		// Do background moving
		float maxDispX = viewArea.getWidth()/10;
		float maxDispY = viewArea.getHeight()/10;
		float skyDistance = 0.8f;
		
		panoramaPos.set(mousePos);
		panoramaPos.div(10);
		
		float panoramaSize = (viewArea.getWidth() + maxDispX * 2) / backgroundTextures.length;
		float x = viewArea.x1 - maxDispX + panoramaPos.x;
		float y = viewArea.y2 - panoramaSize + maxDispY + panoramaPos.y - 0.1f;
		
		glClearColor(67f/255, 107f/255, 163f/255, 1);
		skyTexture.bind();
		addQuad(x - panoramaPos.x * skyDistance, y - panoramaPos.y * skyDistance - panoramaSize * 3, panoramaSize * backgroundTextures.length, panoramaSize * backgroundTextures.length, fullTexture);
		for(int i = 0; i < backgroundTextures.length; i++) {
			backgroundTextures[i].bind();
			addQuad(x + panoramaSize * i , y, panoramaSize, panoramaSize, fullTexture);
		}
		
		// Draw components
		generateButton.setEnabled(!genWorldName.isEmpty());
		genWorldName.draw(this);
		generateButton.draw(this);
		worldList.draw(this);
		openButton.draw(this);
	}
	
	@Override
	public void willClose() {
		controller.removeKeyboardListener(genWorldName);
		for(Texture texture : backgroundTextures)
			texture.clear();
		skyTexture.clear();
	}
	

	
}
package com.oscar.boringgame.renderer;

import static org.lwjgl.opengl.GL11.glClearColor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.oscar.boringgame.controller.Controller;
import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.main.GameState;
import com.oscar.boringgame.renderer.components.Button;
import com.oscar.boringgame.renderer.components.Button.ButtonListener;
import com.oscar.boringgame.renderer.components.KeyInput;
import com.oscar.boringgame.renderer.helper.Artist;

public class SettingsRenderer extends Renderer {

	private SettingsMenu currentMenu;
	private int page;
	
	private final Button buttonMainMenu;
	private final Button buttonResume;
	
	private final Button buttonKeybindings;
	private final Button buttonBack;
	
	private final Button buttonBackPage;
	private final Button buttonNextPage;
	
	private final HashMap<Integer, List<KeyInput>> keybindingInputs;
	
	public SettingsRenderer(Window window, Controller controller) {
		super(window, controller);
		currentMenu = SettingsMenu.MAIN;
		
		buttonMainMenu = new Button(-0.3f, 0.6f, 0.6f, 0.2f, "Main Menu", Artist.DEFAULT_FONT);
		buttonMainMenu.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				GameState.closeAndSaveWorld();
				GameState.openMainMenu();
			} 
		});
		
		buttonResume = new Button(-0.3f, 0.35f, 0.6f, 0.2f, "Resume", Artist.DEFAULT_FONT);
		buttonResume.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				GameState.goBackToWorld();
			}
		});
		
		buttonKeybindings = new Button(-0.3f, -0.2f, 0.6f, 0.2f, "Keybindings", Artist.DEFAULT_FONT);
		buttonKeybindings.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				currentMenu = SettingsMenu.KEYBINDINGS;
			}
		});
		buttonBack = new Button(-0.3f, 0.6f, 0.6f, 0.2f, "Back", Artist.DEFAULT_FONT);
		buttonBack.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				currentMenu = SettingsMenu.MAIN;
			}
		});
		buttonBackPage = new Button(-0.55f, 0.6f, 0.2f, 0.2f, "<", Artist.DEFAULT_FONT);
		buttonBackPage.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				page--;
			}
		});
		buttonNextPage = new Button(0.35f, 0.6f, 0.2f, 0.2f, ">", Artist.DEFAULT_FONT);
		buttonNextPage.addListener(new ButtonListener() {
			@Override
			public void pressedButton(Button button) {
				page++;
			}
		});
		
		keybindingInputs = new HashMap<>();
		int i = 0;
		for(KeyBinding key : KeyBinding.values()) {
			if(key.isFinal())
				continue;
			
			
			if(!keybindingInputs.containsKey(i/10))
				keybindingInputs.put(i/10, new ArrayList<>());
			
			KeyInput input = new KeyInput((i%10 < 5) ? -0.85f : 0.05f, -0.8f + (i%5) * 0.25f, 0.8f, 0.2f, key, Artist.DEFAULT_FONT);
			keybindingInputs.get(i/10).add(input);
			
			controller.addKeyboardListener(input);
			controller.addMouseListener(input);
			
			i++;
		}
	}
	
	@Override
	protected void draw() {
		switchToGUIView();
		
		glClearColor(0.4f, 0.5f, 0.8f, 1);
		
		
		if(currentMenu == SettingsMenu.MAIN) {
			buttonResume.draw(this);
			buttonMainMenu.draw(this);
			buttonKeybindings.draw(this);
		}
		else if(currentMenu == SettingsMenu.KEYBINDINGS) {
			buttonBack.draw(this);
			for(KeyInput input : keybindingInputs.get(page)) {
				input.draw(this);
			}
			
			if(page > 0)
				buttonBackPage.draw(this);
			if(keybindingInputs.containsKey(page+1))
				buttonNextPage.draw(this);
		}
		
		
		if(KeyBinding.OPEN_SETTINGS.isFirstPressed()) {
			if(currentMenu == SettingsMenu.MAIN)
				GameState.goBackToWorld();
			else if(currentMenu == SettingsMenu.KEYBINDINGS)
				currentMenu = SettingsMenu.MAIN;
		}
	}
	
	@Override
	public void willClose() {
		for(Entry<Integer, List<KeyInput>> inputs : keybindingInputs.entrySet())
			for(KeyInput input : inputs.getValue()) {
				controller.removeKeyboardListener(input);
				controller.removeMouseListener(input);
			}
	}
	
	private static enum SettingsMenu {
		MAIN, KEYBINDINGS
	}
	
}

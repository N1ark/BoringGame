package com.oscar.boringgame.renderer;

import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetWindowIcon;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallbackI;
import org.lwjgl.opengl.GL20;
import org.lwjgl.stb.STBImage;

import com.oscar.boringgame.main.Boot;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.IOUtil;

public class Window implements GLFWWindowSizeCallbackI {
	
	private long window;
	
	private final Area view;
	private int width;
	private int height;
	
	private boolean fullscreen;
	
	public Window(String title, boolean fullscreen) {
		if(!glfwInit()) {
			throw new IllegalStateException("Failed to initialize GLFW!");
		}
		
		GLFWVidMode vid = glfwGetVideoMode(glfwGetPrimaryMonitor());
		this.fullscreen = fullscreen;
		this.height = fullscreen ? vid.height() : 720;
		this.width = fullscreen ? vid.width() : 1080;
		this.view = new Area(width, height);
		
		window = glfwCreateWindow(width, height, title, fullscreen ? glfwGetPrimaryMonitor() : 0, 0);
		if(window == 0)
			throw new IllegalStateException("Failed to initialize window!");
		
		if(!fullscreen)
			glfwSetWindowPos(window, (vid.width()-width)/2, (vid.height()-height)/2);
		
		glfwSetWindowSizeCallback(window, this);
		
		glfwShowWindow(window);
		glfwMakeContextCurrent(window);
	}
	
	public void setIcon() {
		IntBuffer w = BufferUtils.createIntBuffer(1);
		IntBuffer h = BufferUtils.createIntBuffer(1);
		IntBuffer comp = BufferUtils.createIntBuffer(1);

		// Icons
		{
			ByteBuffer icon16;
			ByteBuffer icon32;
			try {
				icon16 = IOUtil.ioResourceToByteBuffer(Boot.PATH_EXTRA + "assets/gui/icon16.png", 2048);
				icon32 = IOUtil.ioResourceToByteBuffer(Boot.PATH_EXTRA + "assets/gui/icon32.png", 4096);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			try (GLFWImage.Buffer icons = GLFWImage.malloc(2)) {
				ByteBuffer pixels16 = STBImage.stbi_load_from_memory(icon16, w, h, comp, 4);
				icons.position(0).width(w.get(0)).height(h.get(0)).pixels(pixels16);

				ByteBuffer pixels32 = STBImage.stbi_load_from_memory(icon32, w, h, comp, 4);
				icons.position(1).width(w.get(0)).height(h.get(0)).pixels(pixels32);

				icons.position(0);
				glfwSetWindowIcon(window, icons);

				STBImage.stbi_image_free(pixels32);
				STBImage.stbi_image_free(pixels16);
			}
		}
	}
	
	/**
	 * @return The Window's ID.
	 */
	public long getID() {
		return window;
	}
	
	/**
	 * Will set the Window's dimensions.
	 * @param width The new width of the window, in pixels.
	 * @param height
	 */
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * @return The width of the window.
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * @return The height of the window.
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * @return An area representing this window's view. This method returns the same as <code>new Area(getWidth(), getHeight())</code>.
	 */
	public Area getView() {
		return view;
	}
	
	/**
	 * @return The width to height ratio of this Window.
	 */
	public float getRatio() {
		return (float) width/height;
	}
	
	/**
	 * Will set wether or not the Window is fullscreen, which might change its size.
	 * @param b If this Window is fullscreen.
	 */
	public void setFullscreen(boolean b) {
		fullscreen = b;
	}
	
	/**
	 * @return If this Window is displayed in fullscreen.
	 */
	public boolean isFullscreen() {
		return fullscreen;
	}
	
	/**
	 * Updates the Window's graphics.
	 */
	public void update() {
		glfwSwapBuffers(window);
	}
	
	/**
	 * @return If this Window should close.
	 */
	public boolean shouldClose() {
		return glfwWindowShouldClose(window);
	}
	
	/**
	 * Closes this Window.
	 */
	public void close() {
		glfwDestroyWindow(window);
		glfwTerminate();
		System.exit(1);
	}

	@Override
	public void invoke(long window, int width, int height) {
		this.width = width;
		this.height = height;
		view.x2 = width;
		view.y2 = height;
		GL20.glViewport(0, 0, width, height);
	}
}

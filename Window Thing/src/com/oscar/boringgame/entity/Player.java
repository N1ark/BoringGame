package com.oscar.boringgame.entity;

import static java.lang.Math.ceil;
import static java.lang.Math.floor;

import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.List;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.InteractableBlock;
import com.oscar.boringgame.blocks.blocktypes.NoHitboxSurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.controller.Controller.UserAction;
import com.oscar.boringgame.controller.KeyBinding;
import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.item.Inventory;
import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.item.ItemData.ItemDataType;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.item.SpecialItem;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.particle.BlockBreakingParticle;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.IntLocation;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;
import com.oscar.boringgame.util.Side;

public class Player extends Entity implements ItemHoldingEntity {
	
	/** The player's hitbox. */
	private static final Area hitbox = new Area(-0.2,-0.1,0.2,0.1);
	/** The player's display area. */
	private static final Area display = new Area(-0.375, -1.60, 0.375, 0.15);
	
	/** The texture atlas containing all the player's textures. */
	private static Texture textureAtlas;
	/** The texture coordinates for the player, formatted as such: texture = textures[facedDirection.index][walkAnimation] */
	private static final float[][][] textures;
	/** The texture coordinates for the player's shadow. */
	private static final float[] shadow = {108f/128, 118f/128, 1,1};

	/** The slot currently selected by the player in its hotbar. */
	private int selectedSlot;
	/** The max health the player can have. */
	private int maxHp = 50;
	/** The player's energy. */
	private float energy;
	/** The max energy the player can have. */
	private int maxEnergy = 100;

	/** The vector with the player's acceleration. This vector is nullified every tick, so it shouldn't be used to store data. */
	private final PVector acceleration;
	/** If the player is trying to sprint, which isn't linked to wether or not they're sprinting. */
	private boolean isTryingToSprint;
	
	/** The side currently faced by the player. */
	private Side facing;
	/** The speed the player has when walking. */
	private float speed;
	/** The speed the player has when sprinting. */
	private float sprintSpeed;
	/** The animation frame for the player's texture. */
	private float animStep;
	/** The player's current walking state. */
	private WalkState state;
	/** The player's default mining speed, that will dictate how much damage is dealt on a block when the Player breaks it with their hand. */
	private int miningSpeed = 1;
	
	/** The player's inventory. */
	private final Inventory inventory;
	/** The inventory currently opened by the player. */
	private Inventory currentInventory;
	/** The ItemStack the player currently has in its mouse cursor. */
	private ItemStack cursor;
	
	// Initialize textures
	static {
		textures = new float[4][4][4];
		for(Side s : Side.values()) {
			int sId = s.ordinal();
			for(int i = 0; i < 3; i++) {
				int x = (sId%2)*3 + i;
				int y = sId/2;
				textures[sId][i] = textureCoordinates(x, y);
			}
			
			textures[sId][3] = textures[sId][2];
			textures[sId][2] = textures[sId][0];
		}
	}

	/**
	 * Will create a sample Player, with no information attached to it.
	 */
	protected Player() {
		super();
		acceleration = null;
		inventory = null;
	}
	
	/**
	 * Will create a Player at the given coordinates, with the default name and the default stats. Its Inventory will be
	 * filled with 10 items of each type.
	 * @param location The Location at which it will appear.
	 */
	public Player(Location location) {
		super(location);
		name = "Player";
		inventory = new Inventory();
		for(int i = 0; i < Item.ALL_ITEMS.length && i < inventory.getType().getSize(); i++)
			inventory.setItem(new ItemStack(Item.ALL_ITEMS[i], 10), i);
		speed = .013f;
		sprintSpeed = .019f;
		hp = maxHp;
		state = WalkState.IDLE;
		energy = maxEnergy;		
		facing = Side.SOUTH;
		acceleration = new PVector();
		isTryingToSprint = false;
	}
	
	/**
	 * Will create a Player from the given bytes.
	 * @param bytes The bytes to read from.
	 */
	protected Player(ByteBuffer bytes) {
		super(BoringUtil.getUUID(bytes));
		location.set(new Location(bytes));
		velocity.set(new PVector(bytes));
		name = BoringUtil.getString(bytes, separator);
		hp = bytes.getFloat();
		maxHp = bytes.getInt();
		energy = bytes.getFloat();
		maxEnergy = bytes.getInt();
		speed = bytes.getFloat();
		sprintSpeed = bytes.getFloat();
		inventory = new Inventory(bytes);
		selectedSlot = 0;
		acceleration = new PVector();
		isTryingToSprint = false;
		facing = Side.SOUTH;
		animStep = 0;
		state = WalkState.IDLE;
	}
	
	@Override
	public short getID() {
		return 0;
	}
	
	@Override
	public Area getHitbox() {
		return hitbox;
	}

	@Override
	public Area getDisplay() {
		return display;
	}
	
	@Override
	public EntityType getType() {
		return EntityType.PLAYER;
	}
	
	@Override
	public float[] getTextureCoordinates() {
		return textures[facing.ordinal()][(int) animStep];
	}
	
	@Override
	public float[] getShadowOffsets() {
		return shadow;
	}
		
	/**
	 * @return The player's current speed.
	 */
	public float getSpeed() {
		return speed;
	}
	
	/**
	 * @return The player's current sprint speed.
	 */
	public float getSprintSpeed() {
		return sprintSpeed;
	}
	
	/**
	 * @return The player's current used speed.
	 */
	public float getCurrentSpeed() {
		return state == WalkState.SPRINT ? sprintSpeed : speed;
	}
	
	@Override
	public int getMaxHP() {
		return maxHp;
	}
	
	/**
	 * @return The energy of the Player. This value is ceiled, and is in reality a float.
	 */
	public float getEnergy() {
		return energy;
	}
	
	/**
	 * @return The player's max energy.
	 */
	public int getMaxEnergy() {
		return maxEnergy;
	}
	
	/**
	 * Will set the Player's energy.
	 * @param x The energy's new value.
	 */
	public void setEnergy(int x) {
		energy = x;
	}
	
	/**
	 * Will add a value to the player's energy.
	 * @param x The value to add.
	 */
	public void addEnergy(int x) {
		energy += x;
	}
	
	/**
	 * @return The direction currently faced by the Player.
	 */
	public Side getFacing() {
		return facing;
	}
	
	/**
	 * Will set the player's faced direction.
	 * @param s The new faced direction.
	 */
	public void setFacing(Side s) {
		facing = s;
	}
	
	/**
	 * @return The player's walking state.
	 */
	public WalkState getWalkState() {
		return state;
	}
	
	/**
	 * Will increment the Player's walking animation, and if they're running, remove some energy.
	 */
	public void incrementWalk() {
		if(state == WalkState.SPRINT) {
			energy -= 0.02;
			if(energy <= 0) {
				state = WalkState.WALK;
				energy = 0;
			}
		}
		animStep += velocity.mag();
		animStep %= 4;
	}
	
	/**
	 * Will make the Player's walking start, setting the animation to 0 and checking if they're running or walking.
	 */
	public void toggleWalking() {
		state = isTryingToSprint && energy > 0 ? WalkState.SPRINT : WalkState.WALK;
	}
	
	/**
	 * Will disable the player's walking.
	 */
	public void untoggleWalking() {
		state = WalkState.IDLE;
		animStep = 0;
	}
	
	@Override
	public Texture getTexture() {
		return textureAtlas;
	}
	
	/**
	 * Will teleport the Player to the specified Location.
	 * @param l The Player's new location.
	 */
	public void teleport(Location l) {
		location.set(l);
	}
	
	/**
	 * Will set the Player's currently opened inventory.
	 * @param i The newly opened Inventory.
	 */
	public void setOpenedInventory(Inventory i) {
		currentInventory = i;
	}
	
	/**
	 * @return The Inventory currently opened by the Player.
	 */
	public Inventory getCurrentInventory() {
		return currentInventory;
	}
	
	/**
	 * @return The current hotbar slot selected by the Player.
	 */
	public int getSlot() {
		return selectedSlot;
	}
	
	/**
	 * Will set the player's current selected slot in the hotbar.
	 * <br> If the value is below 0, it will be incremented by 10 until in the range [0;10].
	 * <br> If the value is above 10, it will be modulo'ed by 10.
	 * @param slot The newly selected slot.
	 */
	public void setSlot(int slot) {
		while(slot < 0) slot += 10;
		slot %= 10;
		selectedSlot = slot;
	}
	
	/**
	 * @return The Player's Inventory.
	 */
	public Inventory getInventory() {
		return inventory;
	}
	
	/**
	 * Will set the current ItemStack in the Player's cursor.
	 * @param it The ItemStack in the Player's cursor. This can be null.
	 */
	public void setCursor(ItemStack it) {
		cursor = it;
	}
	
	/**
	 * @return The current ItemStack in the Player's cursor.
	 */
	public ItemStack getCursor() {
		return cursor;
	}
	
	/**
	 * @return The Location of the Block currently faced by the Player.
	 */
	public Location getFacingBlock() {
		return location.clone().add(facing.x, facing.y).floor();
	}
	
	@Override
	public ItemStack getHandItem() {
		return inventory.getContent()[selectedSlot];
	}
	
	@Override
	public void tick() {
		checkCollectNearItems();
		
		velocity.add(acceleration);
		controlSpeed();
		
		if(velocity.mag() < 0.001)
			untoggleWalking();
		else
			if(tryMoving())
				checkWaterParticles();
		
		manageBlockInteractions();
	}
	
	/**
	 * Will make the Player collect all the Items near him
	 */
	private void checkCollectNearItems() {
		List<DroppedItem> near = getWorld().visibleItems(this);
		if(!near.isEmpty()) {
			near.sort(Comparator.comparing(item -> item.location.distance(location)));
			for(DroppedItem droppedItem : near)
				if(
						droppedItem.canBePickedUp() && 
						droppedItem.getLocation().distance(location) <= 1.3 && 
						inventory.hasSpace(droppedItem.getItem())
				) {
					inventory.addItem(droppedItem.getItem());
					droppedItem.remove();
				}
		}
	}
	
	/**
	 * Will add the input from the user to the Player's velocity.
	 * @param direction The direction the Player is going: a vector of length 1, from which the X and Y coordinates will be used.
	 * @param intensity The intensity at which the Player is walking: a float ranging from 0 to 1, that indicates at which percentage of speed the Player's going.
	 * @param sprinting If the Player is sprinting.
	 */
	public void applyControlsToVelocity(PVector direction, float intensity, boolean sprinting) {
		acceleration.nullify();
		isTryingToSprint = sprinting;
		
		if (currentInventory == null) {
			if(state == WalkState.IDLE)
				toggleWalking();
			else
				state = sprinting && energy > 0 ? WalkState.SPRINT : WalkState.WALK;
			
			float currentSpeed = convertWalkSpeed(getCurrentSpeed());
			acceleration.set(direction);
			acceleration.mult(currentSpeed * intensity);
			
			if(acceleration.x != 0 || acceleration.y != 0)
				facing = acceleration.getSide();
		}
	}
	
	/**
	 * Will set the current action done by the user.
	 * @param action The action done by the user.
	 */
	public void setCurrentlyDoneAction(UserAction button) {
		currentPressedButton = button;
	}

	@Override
	protected boolean tryMoving() {
		boolean moved = super.tryMoving();
		if(moved)
			incrementWalk();
		else
			untoggleWalking();
		return moved;
	}	
	
	/** The faced block in the last tick. */
	private Location lastFacedBlock;
	/** The currently faced block. */
	private Location facedBlock;
	/** The user action performed in the last tick. */
	private UserAction lastPressedButton = UserAction.NONE;
	/** The user action performed in this tick. */
	private UserAction currentPressedButton = UserAction.NONE;
	/** The remaining block durability for the faced block. */
	private float blockDurability;
	/** The faced block's type. */
	private Block facedBlockType;
	/** A cooldown that is applied whenever the Player interacts with a block/Item, to avoid having 60 interactions per second. */
	private int interactionCooldown = 0;
	
	/**
	 * Will set <code>facedBlockType</code> to the currently faced block.
	 */
	private void resetFacedBlock() {
		facedBlockType = facedBlock.getVisibleUpperBlock();
		if(facedBlockType == AIR.AIR)
			facedBlockType = facedBlock.getBlock();
		blockDurability = facedBlockType.getDurability(); 
	}
	
	
	/**
	 * Will manage the Player's placing and breaking of blocks.
	 */
	private void manageBlockInteractions() {
		interactionCooldown --;
		boolean buttonChanged = lastPressedButton != currentPressedButton;
		facedBlock = getFacingBlock();
		
		if(buttonChanged && lastPressedButton == UserAction.ATTACK) { // Released left click
			getWorld().resetBreakState(lastFacedBlock.x, lastFacedBlock.y);
		}
		
		if(currentPressedButton == UserAction.ATTACK && !facedBlock.equals(lastFacedBlock) && lastFacedBlock != null) { // Changed breaking block
			getWorld().resetBreakState(lastFacedBlock.x, lastFacedBlock.y);
			resetFacedBlock();
		}
		
		else if(buttonChanged) { // Started clicking
			resetFacedBlock();
		}
		
		else if(currentPressedButton == UserAction.PLACE && !facedBlock.equals(lastFacedBlock)) { // Changed placing/interacting block
			resetFacedBlock();
		}
		
		if(currentPressedButton == UserAction.ATTACK && facedBlockType.getDurability() > 0) {
			manageBlockBreaking();
		}
		else if(currentPressedButton == UserAction.PLACE) {
			boolean interacted = manageBlockInteracting();
			if(!interacted)
				manageBlockPlacing();
		}

		lastFacedBlock = facedBlock;
		lastPressedButton = currentPressedButton;
	}
	
	/**
	 * Will do everything needed to control the Player's ability to break a block.
	 */
	private void manageBlockBreaking() {
		Item heldItem = getHandItem() == null ? AIR.AIR : getHandItem().getType();
		float damageDealt = miningSpeed;
		if(facedBlockType.getCategory().isWeakAgainst(heldItem)) {
			damageDealt *= ((SpecialItem) heldItem).getData(ItemDataType.MINING_BONUS).getValue();
		}			
		
		if(KeyBinding.CHEAT_SPRINT.isPressed())
			damageDealt = 5 * damageDealt;
		
		blockDurability -= damageDealt;

		int breakState = (int) Math.floor(5 - blockDurability * 5f / facedBlockType.getDurability());
		boolean broke = getWorld().setBreakState(breakState, (int) facedBlock.x, (int) facedBlock.y);
		if(broke)
			resetFacedBlock();
		else {
			if(BoringMath.random().nextFloat() < damageDealt * 0.15) {
				ParticleEntity particle = new BlockBreakingParticle(
						facedBlockType, getWorld(), 
						facedBlock.x+BoringMath.random().nextFloat(), 
						facedBlock.y+BoringMath.random().nextFloat());
				getWorld().add(particle);
			}
		}
	}
	
	/**
	 * Will do everything needed to control the Player's ability to interact with blocks.
	 * @return If an interaction happened.
	 */
	private boolean manageBlockInteracting() {
		if(interactionCooldown > 0)
			return false;
		
		boolean interacted = false;
		
		if(getHandItem() != null && getHandItem().getType() == CommonItem.DYNAMITE) {
			Dynamite dynamite = new Dynamite(location.clone());
			dynamite.setVelocity(velocity.mag() < 0.01f ? facing.toVector() : velocity.get().normalize());
			inventory.addAmount(selectedSlot, -1);
			interacted = true;
		}
		
		if(!interacted && facedBlockType instanceof InteractableBlock)
			interacted = ((InteractableBlock) facedBlockType).interact(this);
		
		if(!interacted && location.getUpperBlock() instanceof InteractableBlock)
			interacted = ((InteractableBlock) location.getUpperBlock()).interact(this);
		
		
		if(interacted)
			interactionCooldown = 10;
		return interacted;
	}
	
	/**
	 * Will do everything needed to control the Player's ability to place blocks.
	 */
	private void manageBlockPlacing() {
		ItemStack heldItem = getHandItem();
		if(heldItem == null || !(heldItem.getType() instanceof Block)) 
			return;
		
		Block hand = (Block) heldItem.getType();
		if(hand instanceof SurfaceBlock && facedBlock.getVisibleUpperBlock() != AIR.AIR) 
			return;
		
		World world = getWorld();
		Location placedLocation = facedBlock.clone();
		boolean canBuild = false;
		
		if(hand instanceof SolidBlock) {
			if(facedBlockType instanceof SolidBlock && ((SolidBlock) facedBlockType).canBeReplaced())
				canBuild = true;
		} 
		else if(hand instanceof NoHitboxSurfaceBlock) {
			if(facedBlockType instanceof SolidBlock && ((NoHitboxSurfaceBlock) hand).canPlaceOn((SolidBlock) facedBlockType)) {
				canBuild = true;
			}
		}
		else if(hand instanceof SurfaceHitboxBlock) {
			if(facing == Side.NORTH)
				placedLocation.y -= ((SurfaceHitboxBlock) hand).getDimension().y2-1;
			else if(facing == Side.WEST)
				placedLocation.x -= ((SurfaceHitboxBlock) hand).getDimension().x2-1;
			
			canBuild = true;
			
			for(int x = (int) floor(placedLocation.x); x < ceil(placedLocation.x + ((SurfaceHitboxBlock) hand).getDimension().x2); x++) {
				for(int y = (int) floor(placedLocation.y); y < ceil(placedLocation.y + ((SurfaceHitboxBlock) hand).getDimension().y2); y++) {
					if(!((SurfaceBlock) hand).canPlaceOn(world.getBlock(x, y)) || world.getVisibleUpperBlock(x, y) != AIR.AIR) {
						canBuild = false;
						break; // here a return could work, but just in case I'll leave this instead
					}
				}
			}
		}
		
		if(canBuild) {
			if(hand instanceof DataHolderBlockInstance)
				hand = ((DataHolderBlockInstance) hand).newInstance(new IntLocation(placedLocation), world);
			
			world.setblock(placedLocation, hand);
			resetFacedBlock();
			if(heldItem.getAmount() > 1)
				heldItem.setAmount(heldItem.getAmount()-1);
			else
				getInventory().setItem(null, selectedSlot);
		}
	}
	
	@Override
	public byte[] toBytes() {
		byte[] idBytes = BoringUtil.toBytes(getType().getID());
		byte[] uuidBytes = BoringUtil.toBytes(uuid);
		byte[] locationBytes = location.toBytes();
		byte[] velocityBytes = velocity.toBytes();
		byte[] nameBytes = BoringUtil.toBytes(name);
		byte[] hpBytes = BoringUtil.toBytes(hp);
		byte[] maxHpBytes = BoringUtil.toBytes(maxHp);
		byte[] energyBytes = BoringUtil.toBytes(energy);
		byte[] maxEnergyBytes = BoringUtil.toBytes(maxEnergy);
		byte[] speedBytes = BoringUtil.toBytes(speed);
		byte[] sprintSpeedBytes = BoringUtil.toBytes(sprintSpeed);
		byte[] inventoryBytes = inventory.toBytes();
		
		return BoringUtil.concatAll(idBytes, uuidBytes, locationBytes, velocityBytes, nameBytes, separator, hpBytes, maxHpBytes, energyBytes,
				maxEnergyBytes, speedBytes, sprintSpeedBytes, inventoryBytes);
	}
	
	@Override
	public Player fromBytes(ByteBuffer bytes) {
		return new Player(bytes);
	}
	
	/**
	 * Will load all the textures for the Player.
	 */
	public static void load() {
		textureAtlas = new Texture("entity/player_atlas");
	}
	
	private static float[] textureCoordinates(int x, int y) {
		float halfPixel = 0.0001f;
		return new float[] {x * 15f / 128 + halfPixel, y * 35f / 128 + halfPixel, (x+1) * 15f / 128 - halfPixel, (y+1) * 35f / 128 - halfPixel};
	}
	
	/**
	 * A simple enum, used to indicate the walking state of the Player: idle, walking or sprinting!s
	 */
	public static enum WalkState {
		IDLE, WALK, SPRINT;
	}
}

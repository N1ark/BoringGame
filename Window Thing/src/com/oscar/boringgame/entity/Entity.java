package com.oscar.boringgame.entity;

import java.nio.ByteBuffer;
import java.util.UUID;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.particle.Particle;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public abstract class Entity {

	protected static final byte[] separator = new byte[] {Byte.MIN_VALUE, Byte.MIN_VALUE};
	
	protected String name;
	protected float hp;
	protected final UUID uuid;
	protected final PVector velocity;
	protected final Location location;
	
	protected float waterParticleCounter;
	
	/**
	 * Will create a sample entity, with no data attached to it.
	 */
	protected Entity() {
		uuid = null;
		velocity = null;
		location = null;
	}
	
	/**
	 * Will create an entity at the given location and will spawn it in the world specified in the location.
	 * @param l The location at which the entity will be spawned.
	 */
	public Entity(Location l) {
		this.location = l;
		this.velocity = new PVector();
		this.uuid = UUID.randomUUID();
		this.hp = getMaxHP();
		if(l != null && l.getWorld() != null)
			l.getWorld().add(this);
	}
	
	/**
	 * Will create an entity at the coordinates 0/0 with the given UUID.
	 * @param uuid The entity's UUID.
	 */
	protected Entity(UUID uuid) {
		this.location = new Location(0, 0, null);
		this.velocity = new PVector();
		this.uuid = uuid;
		this.hp = getMaxHP();
	}
	
	/**
	 * @return The entity´s name.
	 */
	public final String getName() {
		return name;
	}
	
	/**
	 * @return The entity´s health. If it drops below 0 it is removed from the world.
	 */
	public final float getHP() {
		return hp;
	}
	
	/**
	 * Will set the player's health to the given value.
	 * @param hp The entity's health.
	 */
	public void setHP(float hp) {
		this.hp = hp;
		if(hp <= 0) {
			remove();
			hp = 0;
		}
	}
	
	/**
	 * Will change this entity's hp by the given value.
	 * @param value The value to add to this entity's health.
	 */
	public final void addHP(float value) {
		setHP(hp + value);
	}
	
	/**
	 * @return Will return this entity's max HP.
	 */
	public abstract int getMaxHP();
	
	/**
	 * @return The entity's location (the starting point of it's hitbox and display area).
	 */
	public final Location getLocation() {
		return location;
	}
	
	/**
	 * @return The entity's velocity, that is added to its position everytime it's ticked.
	 */
	public final PVector getVelocity() {
		return velocity;
	}
	
	/**
	 * @return The entity's current world.
	 */
	public final World getWorld() {
		return location.getWorld();
	}
	
	/**	
	 * @return The entity's UUID. This should normally be unique.
	 */
	public final UUID getUUID() {
		return uuid;
	}
	
	/**
	 * Will set the Entity's velocity vector.
	 * @param x The X coordinate of the vector.
	 * @param y The Y coordinate of the vector.
	 */
	public void setVelocity(float x, float y) {
		velocity.set(x, y);
	}
	
	/**
	 * Will set the Entity's velocity vector.
	 * @param v The velocity vector from which the values will be copied.
	 */
	public void setVelocity(PVector v) {
		velocity.set(v);
	}
	
	/**
	 * @return The current Entity's hitbox, translated to it's Location.
	 */
	public Area getCurrentHitbox() {
		return getHitbox().copyTranslate(location);
	}
	
	/**
	 * @return The current texture displayed by the Entity.
	 */
	public abstract Texture getTexture();
	
	/**
	 * @return The collision box of the entity.
	 */
	public abstract Area getHitbox();
	
	/**
	 * @return The area that will contain the displayed entity.
	 */
	public abstract Area getDisplay();
	
	/**
	 * @return The coordinates of the texture on the texture atlas.
	 * <br>Indexes are: 
	 * <br> - 0: X1
	 * <br> - 1: Y1
	 * <br> - 2: X2
	 * <br> - 3: Y2
	 */
	public abstract float[] getTextureCoordinates();
	
	/**
	 * @return The coordinates of the shadow texture on the texture atlas.
	 * <br>Indexes are: 
	 * <br> - 0: X1
	 * <br> - 1: Y1
	 * <br> - 2: X2
	 * <br> - 3: Y2
	 */
	public abstract float[] getShadowOffsets();
	
	/**
	 * @return This entity's ID.
	 */
	public abstract short getID();
	
	/**
	 * @return This Entity's EntityType.
	 */
	public abstract EntityType getType();
	
	/**
	 * The ticking method of the entity, that will manage everthing needed.
	 */
	public void tick() {
		controlSpeed();
		
		if(velocity.mag() > 0.001)
			if(tryMoving())
				checkWaterParticles();
	}
	
	/**
	 * Will control the current Entity's velocity, by slowing it by a certain amount.
	 */
	protected void controlSpeed() {
		SolidBlock block = location.getBlock();
		float slow;
		switch(block) {
		case ICE:
			slow = 0.95f;
			break;
		default: 
			slow = 0.8f;
			break;
		}
		velocity.mult(slow); // The max speed reachable is: (SEED * SLOW) / (1 - SLOW)
	}
	
	/**
	 * Will try to add the velocity to the Entity's position, and then animate it.
	 * @return If the entity moved in at least one of the axis.
	 */
	protected boolean tryMoving() {
		Area hitboxX = getCurrentHitbox().translateX(velocity.x);
		boolean movedX = false;
		boolean movedY = false;

		if(getWorld().isWalkable(hitboxX)) {
			movedX = true;
			if(getWorld().isWalkable(hitboxX.translateY(velocity.y)))
				movedY = true;
		} else { 
			Area hitboxY = getCurrentHitbox().translateY(velocity.y);
			if (getWorld().isWalkable(hitboxY))
				movedY = true;
		}
		location.x += movedX ? velocity.x : 0;
		location.y += movedY ? velocity.y : 0;
		return movedX || movedY;
	}
	
	/**
	 * Will convert the given walk speed (on normal blocks) to what it should be depending on the block on which the Entity is.
	 * <br>For instance, if the Entity is on ice, it will return a lower value, to make up for the ice's deceleration being lighter.
	 * @param speed The default walking speed on "normal" blocks.
	 * @return The walking speed that must be used in the velocity.
	 */
	// The max speed reachable is: (SPEED * SLOW) / (1 - SLOW), use this to determine by how much it should be slowed!
	protected float convertWalkSpeed(float speed) {
		SolidBlock block = location.getBlock();
		
		switch(block) {
		case ICE:
			speed *= .22f;
			break;
		case WATER:
			speed *= .5f;
			break;
		default:
			break;
		}
		return speed;
	}
		
	/**
	 * If the entity has a velocity and is in the water, this will try to make water particles appear around it.
	 */
	protected final void checkWaterParticles() {
		if(location.getBlock() == SolidBlock.WATER) {
			if(waterParticleCounter > 0.4) {
				Area hitbox = getCurrentHitbox();
				int amount = 1 + BoringMath.random().nextInt(3);
				for(int i = 0; i < amount; i++)
					location.getWorld().add(new ParticleEntity(
							Particle.DROPLET, 
							location.getWorld(),
							BoringMath.nextFloat(hitbox.x1, hitbox.x2), 
							BoringMath.nextFloat(hitbox.y1, hitbox.y2)
					));
				
				waterParticleCounter = 0;
			}
			else {
				waterParticleCounter += velocity.mag();
			}
		} else {
			waterParticleCounter = 0;
		}
	}
	
	/**
	 * Will remove this entity from its world.
	 */
	public void remove() {
		getWorld().remove(this);
	}

	/**
	 * Will load the textures for all entities.
	 */
	public static void load() {
		Player.load();
		Sheep.load();
		Dynamite.load();
	}

	/**
	 * @return This Entity represented as a byte array.
	 */
	public abstract byte[] toBytes();
	
	/**
	 * @return Will try to create an Entity from the given bytes.
	 */
	public abstract Entity fromBytes(ByteBuffer bytes);
	
	/**
	 * @param bytes The bytes representing an Entity.
	 * @return The Entity represented by the given bytes.
	 */
	public static Entity getEntityFromBytes(ByteBuffer bytes) {
		short id = bytes.getShort();
		EntityType type = null;
		for(EntityType t : EntityType.values()) {
			if(t.getID() == id) {
				type = t;
				break;
			}
		}
		
		if(type == null)
			throw new IllegalArgumentException("The given bytes represent an entity with an ID ("+id+") that doesn't exist!");
		
		 return type.getEntity().fromBytes(bytes);
	}
	
}

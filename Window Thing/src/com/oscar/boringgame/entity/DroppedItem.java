package com.oscar.boringgame.entity;

import java.nio.ByteBuffer;

import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.renderer.helper.Artist;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public class DroppedItem extends Entity {
	/** The dropped item's hitbox. */
	private static final Area hitbox = new Area(-0.375f, -0.375f, 0.375f, 0.375f);
	/** The dropped item's display area. */
	private static final Area display = new Area(-0.375f, -0.65f, 0.365f, 0.1f);
	/** The texture coordinates of the shadow for the DroppedItem. */
	private static final float[] shadow = Block.textureArray(5, 1, 20f/16, 10f/16);
	/** The max amount of ticks a DroppedItem can live through before despawning. */
	private static final int maxLiveableTicks = 60 * 180;
	
	/** The ItemStack in this DroppedItem. */
	private final ItemStack item;
	/** The minimum amount of ticks this dropped item should live before being pick-up-able */
	private int pickupMin;
	/** The number of ticks this item lived. */
	private int ticksLived;

	/**
	 * Will create a sample dropped item, with no information attached to it.
	 */
	protected DroppedItem() {
		super();
		this.item = null;
	}
	
	/**
	 * Will create a DroppedItem at the given location with a random UUID and containing the given item. The minimum
	 * pickup time is set to 120 ticks by default.
	 * @param item The item contained in the DroppedItem.
	 * @param location The Location at which to spawn the DroppedItem.
	 */
	public DroppedItem(ItemStack item, Location location) {
		super(location);
		this.name = "Dropped "+item.getType().getName();
		this.item = item;
		this.pickupMin = 60 * 2;
		this.ticksLived = 0;
	}

	/**
	 * Will create a DroppedItem from the given bytes.
	 * @param bytes The bytes to read from.
	 */
	protected DroppedItem(ByteBuffer bytes) {
		super(BoringUtil.getUUID(bytes));
		hp = bytes.getFloat();
		location.set(new Location(bytes));
		velocity.set(new PVector(bytes));
		name = BoringUtil.getString(bytes, separator);
		pickupMin = bytes.getInt();
		ticksLived = bytes.getInt();
		item = new ItemStack(bytes);
	}
	
	@Override
	public short getID() {
		return 2;
	}
	
	@Override
	public int getMaxHP() {
		return 10;
	}
	
	@Override
	public void tick() {
		ticksLived++;
		if(ticksLived > maxLiveableTicks || (getWorld().isInLoadedChunk(location) && !location.getBlock().isWalkable()))
			remove();
		super.tick();
	}
	
	@Override
	public Texture getTexture() {
		return Artist.BLOCK_ATLAS;
	}
	
	@Override
	public float[] getShadowOffsets() {
		return shadow;
	}
	
	@Override
	public EntityType getType() {
		return EntityType.DROPPED_ITEM;
	}

	@Override
	public Area getHitbox() {
		return hitbox;
	}
	
	@Override
	public Area getDisplay() {
		return display;
	}
	
	@Override
	public float[] getTextureCoordinates() {
		return item.getType().getTextureCoordinates();
	}

	/**
	 * @return The ItemStack in this DroppedItem.
	 */
	public ItemStack getItem() {
		return item;
	}
	
	/**
	 * @return If this DroppedItem can already be picked up.
	 */
	public boolean canBePickedUp() {
		return ticksLived >= pickupMin;
	}

	/**
	 * @return The minimum amount of time in seconds this item should live before it can be picked up.
	 */
	public float getPickupTime() {
		return pickupMin;
	}

	/**
	 * Will set the minimum amount of time in seconds this item should live before it can be picked up.
	 * @param x The new value for the minimum pickup time.
	 */
	public void setPickupTime(int x) {
		pickupMin = x;
	}
	
	@Override
	public byte[] toBytes() {
		byte[] idBytes = BoringUtil.toBytes(getType().getID());
		byte[] uuidBytes = BoringUtil.toBytes(uuid);
		byte[] healthBytes = BoringUtil.toBytes(hp);
		byte[] locationBytes = location.toBytes();
		byte[] velocityBytes = velocity.toBytes();
		byte[] nameBytes = BoringUtil.toBytes(name);
		byte[] pickupMinBytes = BoringUtil.toBytes(pickupMin);
		byte[] ticksLivedBytes = BoringUtil.toBytes(ticksLived);
		byte[] itemBytes = item.toBytes();
		
		return BoringUtil.concatAll(idBytes, uuidBytes, healthBytes, locationBytes, velocityBytes, nameBytes, separator, pickupMinBytes, ticksLivedBytes, itemBytes);
	}
	
	@Override
	public DroppedItem fromBytes(ByteBuffer bytes) {
		return new DroppedItem(bytes);
	}
}

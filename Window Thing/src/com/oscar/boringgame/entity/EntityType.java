package com.oscar.boringgame.entity;

public enum EntityType implements Comparable<EntityType> {
	PLAYER(new Player()), 
	SHEEP(new Sheep()),
	DROPPED_ITEM(new DroppedItem()),
	DYNAMITE(new Dynamite())
	; 
	
	private final Entity entity;
	
	EntityType(Entity entity){
		this.entity = entity;
	}
	
	/**
	 * @return This entity type's ID.
	 */
	public short getID() {
		return entity.getID();
	}
	
	/**
	 * @return A sample entity of this type, that can be used to create new entities.
	 */
	public Entity getEntity() {
		return entity;
	}
	
}
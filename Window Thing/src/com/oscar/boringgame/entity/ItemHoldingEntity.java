package com.oscar.boringgame.entity;

import com.oscar.boringgame.item.ItemStack;

public interface ItemHoldingEntity {
	

	/**
	 * @return The ItemStack currently selected by the Player.
	 */
	ItemStack getHandItem();

}

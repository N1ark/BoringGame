package com.oscar.boringgame.entity;

import static java.lang.Math.min;

import java.nio.ByteBuffer;

import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public class Dynamite extends Entity {
	
	/** The texture coordinates for the dynamite, organized as such: texture = textures[fuseFrame] */
	private static final float[][] textures;
	/** The texture object with all the dynamite's textures. */
	private static Texture textureAtlas;
	/** The texture coordinate's for the dynamite's shadow. */
	private static final float[] shadow = {44f/64, 54f/64, 1,1};
	/** The dynamite's hitbox. */
	private static final Area hitbox = new Area(-0.09f, -0.09f, 0.09f, 0.09f);
	/** The dynamite's display area. */
	private static final Area display = Area.getAreaRelative(-1.5f/16, -11f/16, 6f/16, 13f/16);
	
	/** The dynamite's fuse, which is the amount of ticks before it explodes. */
	private int fuse;
	/** The range in which blocks may be damaged from the explosion. */
	private float explosionRange;
	
	static {
		textures = new float[9][4];
		for(int i = 0; i < textures.length; i++)
			textures[i] = textureCoordinates(textures.length-i);
	}
	
	/**
	 * Will create a Dynamite with no data.
	 */
	public Dynamite() {}
	
	/**
	 * Will create a Dynamite at the specified location with a random UUID, a default fuse of 225 ticks and a default explosion range of 3 blocks.
	 * @param loc The Location to spawn the Dynamite at.
	 */
	public Dynamite(Location loc) {
		super(loc);
		name = "Dynamite";
		fuse = 225;
		explosionRange = 3;
	}
	
	/**
	 * Will create a Dynamite from the given Bytes.
	 * @param bytes The bytes to read the dynamite's data from.
	 */
	public Dynamite(ByteBuffer bytes) {
		super(BoringUtil.getUUID(bytes));
		hp = bytes.getFloat();
		location.set(new Location(bytes));
		velocity.set(new PVector(bytes));
		name = BoringUtil.getString(bytes, separator);
		fuse = bytes.getInt();
		explosionRange = bytes.getFloat();
	}
	
	@Override
	public void tick() {
		super.tick();
		fuse--;
		if(fuse <= 0) {
			World world = getWorld();

			float dist;
			for(Entity e : world.getEntities())
				if((dist = e.getLocation().distance(location)) < explosionRange)
					e.addHP(-(explosionRange-dist)/explosionRange*30);
			
			for(int x = (int) Math.floor(location.x - explosionRange); x < Math.ceil(location.x+explosionRange); x++) {
				for(int y = (int) Math.floor(location.y - explosionRange); y < Math.ceil(location.y+explosionRange); y++) {
					dist = location.distance(x, y);
					if(dist > explosionRange)
						continue;
					
					Block block = world.getBlock(x, y);
					int dur = block.getDurability();
					if(dur > 0) {
						float odds = 15f/dur - 0.001f*dur + 1.03f - dist / (explosionRange*4);
						if(odds > BoringMath.random().nextFloat())
							world.setBreakState(5, x, y);
					}
				}
			}
			
			world.remove(this);
		}
	}
	
	@Override
	public short getID() {
		return 3;
	}
	
	@Override
	public int getMaxHP() {
		return 70;
	}
	
	@Override
	public EntityType getType() {
		return EntityType.DYNAMITE;
	}
	
	@Override
	public Area getHitbox() {
		return hitbox;
	}
	
	@Override
	public Area getDisplay() {
		return display;
	}

	@Override
	public Texture getTexture() {
		return textureAtlas;
	}

	@Override
	public float[] getTextureCoordinates() {
		return textures[min(fuse/25+1, 8)];
	}

	@Override
	public float[] getShadowOffsets() {
		return shadow;
	}

	@Override
	public byte[] toBytes() {
		byte[] idBytes = BoringUtil.toBytes(getType().getID());
		byte[] uuidBytes = BoringUtil.toBytes(uuid);
		byte[] healthBytes = BoringUtil.toBytes(hp);
		byte[] locationBytes = location.toBytes();
		byte[] velocityBytes = velocity.toBytes();
		byte[] nameBytes = BoringUtil.toBytes(name);
		byte[] fuseBytes = BoringUtil.toBytes(fuse);
		byte[] explosionRangeBytes = BoringUtil.toBytes(explosionRange);
		
		return BoringUtil.concatAll(idBytes, uuidBytes, healthBytes, locationBytes, velocityBytes, nameBytes, separator, fuseBytes, explosionRangeBytes);
	}

	@Override
	public Dynamite fromBytes(ByteBuffer bytes) {
		return new Dynamite(bytes);
	}
	
	/**
	 * Will load all the textures for the Dynamite.
	 */
	public static void load() {
		textureAtlas = new Texture("entity/dynamite_atlas");
	}

	private static float[] textureCoordinates(int x) {
		float halfPixel = 0.0001f;
		return new float[] {x * 6f / 64 + halfPixel, halfPixel, (x+1) * 6f / 64 - halfPixel, 13f / 64 - halfPixel};
	}
}

package com.oscar.boringgame.entity;

import java.nio.ByteBuffer;

import com.oscar.boringgame.item.CommonItem;
import com.oscar.boringgame.renderer.helper.Texture;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.BoringUtil;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;
import com.oscar.boringgame.util.Side;

public class Sheep extends Entity {
	/** The coordinates for the sheep's field of view. This area should be translated to the sheep's location to be usable. */
	private static final Area fieldOfView = new Area(-7, -7, 7, 7);
	/** The sheep's hitbox. */
	private static final Area hitbox = new Area(-0.3, -0.4, 0.3, 0);
	/** The sheep's display area. */
	private static final Area display = new Area(-0.5, -0.85, 0.5, 0.05);
	
	/** The sheep's texture atlas. */
	private static Texture textureAtlas;
	/** The sheep's texture coordinates, corresponding to: textureCor = textures[facedSide][stepAnimation][hasWool ? 1 : 0] */
	private static final float[][][][] textures;
	/** The shadow's texture coordinates in the sheep's texture atlas. */
	private static final float[] shadow = {108f/128, 118f/128, 1,1};
	/** The amount of ticks the sheep during which the sheep will try to get to a fixed location target (aka not a targeted entity). After
	 * this amount of ticks, it will loose interest and stop pursuing this goal. */
	private static final int walkingPatience = 60 * 50;
	
	/** The amount of ticks during which this sheep tried to walk to its goal. */
	private int ticksWalked;
	/** The sheep's target, where it will try to walk to. */
	private final Location target;
	/** The sheep's current field of view. This is a final field to avoid having to clone and translate the field of view every tick. */
	private final Area currentFieldOfView;
	/** The entity targetted by the sheep, or null if it isn't targetting anyone. */
	private Entity targetEntity;
	/** The sheep's yaw. */
	private float yaw;
	/** The side faced by the sheep, corresponding to its yaw. */
	private Side facing;
	/** If the sheep has wool. */
	private boolean hasWool;
	/** The walking animation's current frame. */
	private float animStep;

	// Initialize texture coordinates
	static {
		textures = new float[4][4][2][4];
		for(Side s : Side.values()) {
			for(int i = 0; i < 3; i++) {
				int x = (s.ordinal()%2)*3 + i;
				int y = s.ordinal()/2;
				textures[s.ordinal()][i][0] = textureCoordinates(x, y+2);
				textures[s.ordinal()][i][1] = textureCoordinates(x, y);
			}
		}
		
		for(Side s : Side.values()) {
			textures[s.ordinal()][3] = textures[s.ordinal()][2];
			textures[s.ordinal()][2] = textures[s.ordinal()][0];
		}
	}
	
	/**
	 * Will create a sample Sheep, that has no values.
	 */
	protected Sheep() {
		super();
		target = null;
		currentFieldOfView = null;
	}
	
	/**
	 * Will spawn a sheep at the given Location with a random UUID and wool.
	 * @param loc The location the sheep will appear at.
	 */
	public Sheep(Location loc) {
		super(loc);
		name = "Sheep";
		currentFieldOfView = fieldOfView.copyTranslate(location);
		hasWool = true;
		yaw = (float) (BoringMath.random().nextFloat() * 2 * Math.PI);
		facing = Side.closestTo(yaw);
		target = location.clone();
		animStep = 0;
		ticksWalked = 0;
	}
	
	/**
	 * Will create a Sheep from the given bytes.
	 * @param bytes The bytes to read from.
	 */
	protected Sheep(ByteBuffer bytes) {
		super(BoringUtil.getUUID(bytes));
		hp = bytes.getFloat();
		location.set(new Location(bytes));
		velocity.set(new PVector(bytes));
		name = BoringUtil.getString(bytes, separator);
		ticksWalked = bytes.getInt();
		yaw = bytes.getFloat();
		hasWool = bytes.get() == 1;
		
		currentFieldOfView = fieldOfView.copyTranslate(location);
		facing = Side.closestTo(yaw);
		target = location.clone();
		animStep = 0;
	}
	
	@Override
	public short getID() {
		return 1;
	}
	
	@Override
	public int getMaxHP() {
		return 30;
	}
		
	@Override
	public Texture getTexture() {
		return textureAtlas;
	}
	
	@Override
	public EntityType getType() {
		return EntityType.SHEEP;
	}

	@Override
	public Area getHitbox() {
		return hitbox;
	}

	@Override
	public Area getDisplay() {
		return display;
	}
	
	@Override
	public float[] getTextureCoordinates() {
		return textures[facing.ordinal()][(int) animStep][hasWool ? 1 : 0];
	}
	
	@Override
	public float[] getShadowOffsets() {
		return shadow;
	}
		
	/**
	 * Will increment the Sheep walking animation.
	 */
	public void incrementWalk() {
		animStep += velocity.mag();
		animStep %= 4;
	}
	
	/**
	 * @return If this Sheep has wool.
	 */
	public boolean hasWool() {
		return hasWool;
	}
	
	@Override
	public void tick() {
		if(isAttractedByEntity())
			moveTowards(targetEntity.location);
		else if(isMovingTowardsTarget())
			moveTowards(target);
		else {
			animStep = 0;
			lookAround();
			tryToFindTarget();
		}
		
		super.tick();
		currentFieldOfView.set(fieldOfView).translate(location);
	}
	
	private boolean canBeAttractedTo(Entity e) {
		return e instanceof ItemHoldingEntity && 
				((ItemHoldingEntity) e).getHandItem() != null && 
				((ItemHoldingEntity) e).getHandItem().getType() == CommonItem.CARROT && 
				location.distance(e.location) < 7;
	}
	
	/**
	 * @return If the sheep is currently attracted by an entity.
	 */
	private boolean isAttractedByEntity() {
		if(targetEntity != null) {
			if(canBeAttractedTo(targetEntity))
				return true;
			targetEntity = null;
			target.set(location);
		}
		
		for(Entity e : getWorld().visibleEntities(currentFieldOfView)) {
			if(canBeAttractedTo(e)) {
				if(targetEntity == null || location.distance(e.location) < location.distance(targetEntity.location)) {
					targetEntity = e;
					ticksWalked = 0;
				}
			}
		}
		
		return targetEntity != null;
	}
	
	
	/**
	 * @return If the sheep's target is far enough for it to want to go there.
	 */
	private boolean isMovingTowardsTarget() {
		boolean isFar = location.distance(target) > 0.3;
		if(isFar && ticksWalked > walkingPatience) {
			target.set(location);
			return false;
		}
		return isFar;
	}
	
	/**
	 * Will add the required velocity to the sheep, for it to go towards the target. This will also
	 * recalculate its yaw and facing side.
	 * @param loc The location to which the sheep is walking.
	 */
	private void moveTowards(Location loc) {
		if(location.distance(loc) < 0.4)  return;
		
		float speed = convertWalkSpeed(0.009f);
		PVector walked = location.vectorTo(loc);
		walked.limit(speed);
		velocity.add(walked);
		yaw = walked.getAngle();
		facing = Side.closestTo(yaw);
		ticksWalked++;
	}
	
	/**
	 * Will add a random value to the sheep's yaw.
	 */
	private void lookAround() {
		if(BoringMath.random().nextFloat() < 0.01) {
			yaw += (BoringMath.random().nextFloat() - 0.5f) * 10;
			facing = Side.closestTo(yaw);
		}			
	}
	
	/**
	 * Will randomly decide to set a new target for the sheep.
	 */
	private void tryToFindTarget() {
		if(BoringMath.random().nextFloat() < 0.005) {
			int attempts = 0;
			Area currentHitbox = hitbox.clone();
			float disX = 0;
			float disY = 0;
			do {
				currentHitbox.translate(-disX, -disY);
				attempts++;
				disX = (BoringMath.random().nextFloat() - 0.5f) * 10;
				disY = (BoringMath.random().nextFloat() - 0.5f) * 10;
				currentHitbox.translate(disX, disY);
			} while(attempts < 10 && getWorld().chunkExists(currentHitbox.x1, currentHitbox.y1) && !getWorld().isWalkable(currentHitbox));
			
			if(attempts < 10) {
				target.set(location.x + disX, location.y + disY);
				ticksWalked = 0;
			}
		}
	}
	
	@Override
	protected boolean tryMoving() {
		boolean moved = super.tryMoving();
		if(moved)
			incrementWalk();
		else
			animStep = 0;
		return moved;
	}
	
	@Override
	public byte[] toBytes() {
		byte[] idBytes = BoringUtil.toBytes(getType().getID());
		byte[] uuidBytes = BoringUtil.toBytes(uuid);
		byte[] healthBytes = BoringUtil.toBytes(hp);
		byte[] locationBytes = location.toBytes();
		byte[] velocityBytes = velocity.toBytes();
		byte[] nameBytes = BoringUtil.toBytes(name);
		byte[] ticksWalkedBytes = BoringUtil.toBytes(ticksWalked);
		byte[] yawBytes = BoringUtil.toBytes(yaw);
		byte[] hasWoolBytes = BoringUtil.toBytes(hasWool);
		
		return BoringUtil.concatAll(idBytes, uuidBytes, healthBytes, locationBytes, velocityBytes, nameBytes, separator, ticksWalkedBytes, yawBytes, hasWoolBytes);
	}
	
	@Override
	public Entity fromBytes(ByteBuffer bytes) {
		return new Sheep(bytes);
	}
	
	/**
	 * Will load all the textures for the Sheep.
	 */
	public static void load() {
		textureAtlas = new Texture("entity/sheep_atlas");
	}
	
	private static float[] textureCoordinates(int x, int y) {
		float halfPixel = 0.0001f;
		return new float[] {x * 20f / 128 + halfPixel, y * 18f / 128 + halfPixel, (x+1) * 20f / 128 - halfPixel, (y+1) * 18f / 128 - halfPixel};
	}
	
}
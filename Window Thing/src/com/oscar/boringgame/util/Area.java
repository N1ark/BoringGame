package com.oscar.boringgame.util;

import static com.oscar.boringgame.util.BoringMath.isBetweenInclusive;

import java.awt.Point;

public class Area implements Cloneable {
	public float x1, y1, x2, y2;
	
	/**
	 * Creates an area, with set corners.
	 * The coordinates will be used so that the first corner always has smaller coordinates.
	 * @param x1 The X coordinate of the first corner.
	 * @param y1 The Y coordinate of the first corner.
	 * @param x2 The X coordinate of the other corner.
	 * @param y2 The Y coordinate of the other corner.
	 */
	public Area(float x1, float y1, float x2, float y2) {
		this.x1 = Math.min(x1,x2);
		this.x2 = Math.max(x1,x2);
		this.y1 = Math.min(y1,y2);
		this.y2 = Math.max(y1,y2);
	}
	
	/**
	 * Creates an area, with set corners.
	 * The coordinates will be used so that the first corner always has smaller coordinates.
	 * Double values will be cast to float.
	 * @param x1 The X coordinate of the first corner.
	 * @param y1 The Y coordinate of the first corner.
	 * @param x2 The X coordinate of the other corner.
	 * @param y2 The Y coordinate of the other corner.
	 */
	public Area(double x1, double y1, double x2, double y2) {
		this((float) x1, (float) y1, (float) x2, (float) y2);
	}
	
	/**
	 * Creates an area that has it's first corner at (0;0)
	 * @param x The X coordinate of the other corner.
	 * @param y The Y coordinate of the other corner.
	 */
	public Area(double x, double y) {
		this(0, 0, (float) x, (float) y);
	}
	
	/**
	 * Creates an area that has it's first corner at (0;0)
	 * @param x The X coordinate of the other corner.
	 * @param y The Y coordinate of the other corner.
	 */
	public Area(float x, float y) {
		this(0, 0, x, y);
	}
	
	/**
	 * 
	 * @param x The X coordinate of the area.
	 * @param y The Y coordinate of the area.
	 * @param w The width of the area.
	 * @param h The height of the area.
	 * @return An area set at the given coordinate, and with the set width and height.
	 */
	public static Area getAreaRelative(double x, double y, double w, double h) {
		return new Area(x,y,x+w,y+h);
	}
	
	/**
	 * 
	 * @param x The X coordinate of the area.
	 * @param y The Y coordinate of the area.
	 * @param w The width of the area.
	 * @param h The height of the area.
	 * @return An area set at the given coordinate, and with the set width and height.
	 */
	public static Area getAreaRelative(float x, float y, float w, float h) {
		return new Area(x,y,x+w,y+h);
	}

	/**
	 * @return The width of the area.
	 */
	public float getWidth() {
		return x2-x1;
	}

	/**
	 * @return The height of the area.
	 */
	public float getHeight() {
		return y2-y1;
	}
	
	/**
	 * Will set the the values of this area to the values of the given area.
	 * @param a The area from which the values should be taken.
	 * @return This same area.
	 */
	public Area set(Area a) {
		this.x1 = a.x1;
		this.x2 = a.x2;
		this.y1 = a.y1;
		this.y2 = a.y2;
		return this;
	}
	
	/**
	 * This method doesn't change the Area's values!
	 * @param posX The X value by which the area should be translated.
	 * @param posY The Y value by which the area should be translated.
	 * @param x The X coordinate to check.
	 * @param y The Y coordinate to check.
	 * @return If the point represented by the given coordinates is inside the area when it is translated by the given values.
	 */
	public boolean isInside(float posX, float posY, float x, float y) {
		return BoringMath.isBetween(x, x1+posX, x2+posX) && BoringMath.isBetween(y, y1+posY, y2+posY);
	}
	
	/**
	 * This method doesn't change the Area's values!
	 * @param posX The X value by which the area should be translated.
	 * @param posY The Y value by which the area should be translated.
	 * @param x The position to check as a vector.
	 * @return If the point represented by the given vector's coordinates is inside the area when it is translated by the given values.
	 */
	public boolean isInside(float posX, float posY, PVector p) {
		return isInside(posX, posY, p.x, p.y);
	}
	
	/**
	 * This method doesn't change the Area's values!
	 * @param posX The X value by which the area should be translated.
	 * @param posY The Y value by which the area should be translated.
	 * @param x The location to check.
	 * @return If the point represented by the given Location's coordinates is inside the area when it is translated by the given values.
	 */
	public boolean isInside(float posX, float posY, Location p) {
		return isInside(posX, posY, p.x, p.y);
	}
	
	/**
	 * @param x The X coordinate to check.
	 * @param y The Y coordinate to check.
	 * @return If the point represented by the given coordinates is inside this area.
	 */
	public boolean isInside(float x, float y) {
		return BoringMath.isBetween(x, x1, x2) && BoringMath.isBetween(y, y1, y2);
	}
	
	/**
	 * @param x The X coordinate to check.
	 * @param y The Y coordinate to check.
	 * @return If the point represented by the given coordinates is inside this area.
	 */
	public boolean isInside(double x, double y) {
		return isInside((float) x, (float) y);
	}
	
	/**
	 * @param l The Location to check.
	 * @return If the given Location is inside this area.
	 */
	public boolean isInside(Location l) {
		return isInside(l.x, l.y);
	}
	
	/**
	 * @param p The Point to check.
	 * @return If the point is inside this area.
	 */
	public boolean isInside(Point p) {
		return isInside(p.x, p.y);
	}
	
	/**
	 * @param area The other area to check.
	 * @return If the given area is fully inside this area.
	 */
	public boolean isInside(Area area) {
		return isInside(area.x1, area.y1) && isInside(area.x2, area.y2);
	}
	
	/**
	 * @param x The X coordinate to check.
	 * @param y The Y coordinate to check.
	 * @return If the given coordinates are included in this area, with inclusive comparisons.
	 */
	public boolean isInsideInclusive(float x, float y) {
		return isBetweenInclusive(x, x1, x2) && isBetweenInclusive(y, y1, y2);
	}
	
	/**
	 * @param area The other area to check.
	 * @return If this area and the given area intersect. The check is exclusive, so it will return false if just the edges are touching.
	 */
	public boolean intersects(Area area) {
		return x1 < area.x2 && x2 > area.x1 && y1 < area.y2 && y2 > area.y1;
	}
	
	/**
	 * @param area The other area to check.
	 * @param posX The X coordinate by which this area should be translated.
	 * @param posY The Y coordinate by which this area should be translated.
	 * @return If this area and the given area intersect when this area is translated by the given values. The check is exclusive, so it will
	 * return false if just the edges are touching.
	 */
	public boolean intersects(Area area, float posX, float posY) {
		return x1 + posX < area.x2 && x2 + posX > area.x1 && y1 + posY < area.y2 && y2 + posY > area.y1;
	}
	
	/**
	 * Will translate this area by the given distance on the X axis.
	 * @param x The distance moved.
	 * @return The same area.
	 */
	public Area translateX(float x) {
		x1 += x;
		x2 += x;
		return this;
	}
	
	/**
	 * Will translate this area by the given distance on the Y axis.
	 * @param y The distance moved.
	 * @return The same area.
	 */
	public Area translateY(float y) {
		y1 += y;
		y2 += y;
		return this;
	}
	
	/**
	 * Will translate this area by the given vector.
	 * @param x The distance moved on the X axis.
	 * @param y The distance moved on the Y axis.
	 * @return The same area.
	 */
	public Area translate(float x, float y) {
		return translateX(x).translateY(y);
	}
	
	/**
	 * Will translate this area by considering the given Location as a Vector.
	 * @param l The "vector" moved.
	 * @return The same area.
	 */
	public Area translate(Location l) {
		return translate(l.x, l.y);
	}

	/**
	 * Will translate this area by the given Vector.
	 * @param v The vector moved.
	 * @return The same area.
	 */
	public Area translate(PVector v) {
		return translate(v.x, v.y);
	}
	
	/**
	 * Will copy this area and translate it by the given Location as a Vector.
	 * @param l The "vector" moved.
	 * @return A clone of this area, translated by the given vector.
	 */
	public Area copyTranslate(Location l) {
		return clone().translate(l);
	}
	
	/**
	 * Will copy this area and translate this area by the given vector.
	 * @param x The distance moved on the X axis.
	 * @param y The distance moved on the Y axis.
	 * @return A clone of this area, translated by the given vector.
	 */
	public Area copyTranslate(float x, float y) {
		return clone().translate(x, y);
	}
	
	/**
	 * Will create an Area like this one, but rotated by 90 degrees <b>around the origin</b>.
	 * @param clockwise If the rotation is clockwise.
	 * @return A clone of this area, rotated by 90 degrees.
	 */
	public Area cloneRotateRightAngle(boolean clockwise) {
		return clockwise ? new Area(y1, -x1, y2, -x2) : new Area(-y1, x1, -y2, x2);
	}
	
	/**
	 * Wether or not this area intersects with or contains the segment AB (with A(ax, ay) and B(bx, by)).
	 * @param ax The X coordinate of one point of the segment.
	 * @param ay The Y coordinate of one point of the segment.
	 * @param bx The X coordinate of the other point of the segment.
	 * @param by The Y coordinate of the other point of the segment.
	 * @return If there is an intersection between this area and the segment.
	 */
	public boolean intersectsWith(float ax, float ay, float bx, float by) {
		
		if(isInside(ax, ay) || isInside(ax, ay))
			return true;
		
		if(BoringMath.doIntersect(x1, y1, x2, y1, ax, ay, bx, by))
			return true;
		if(BoringMath.doIntersect(x1, y2, x2, y2, ax, ay, bx, by))
			return true;
		if(BoringMath.doIntersect(x1, y1, x1, y2, ax, ay, bx, by))
			return true;
		if(BoringMath.doIntersect(x2, y1, x2, y2, ax, ay, bx, by))
			return true;
		
		return false;
	}
	
	/**
	 * Will expand this Area bu the given value in all four directions.
	 * @param add The added value in all directions.
	 * @return This same area.
	 */
	public Area expand(float add) {
		x1 -=  add;
		y1 -= add;
		x2 += add;
		y2 += add;
		return this;
	}
	
	/**
	 * Will divide all values of this Area by the given number.
	 * @param value The value by which this Area will be divided.
	 * @return This same Area.
	 */
	public Area divide(float value) {
		x1 /= value;
		y1 /= value;
		x2 /= value;
		y2 /= value;
		return this;
	}
	
	/**
	 * Will multiply all values of this Area by the given number.
	 * @param value The value by which this Area will be multiplied.
	 * @return This same Area.
	 */
	public Area mult(float value) {
		x1 *= value;
		y1 *= value;
		x2 *= value;
		y2 *= value;
		return this;
	}
	
	/**
	 * Will floor all values of this Area.
	 * @return This same Area.
	 */
	public Area floor() {
		x1 = (float) Math.floor(x1);
		y1 = (float) Math.floor(y1);
		x2 = (float) Math.floor(x2);
		y2 = (float) Math.floor(y2);
		return this;
	}
	
	/**
	 * Will ceil all values of this Area.
	 * @return This same Area.
	 */
	public Area ceil() {
		x1 = (float) Math.ceil(x1);
		y1 = (float) Math.ceil(y1);
		x2 = (float) Math.ceil(x2);
		y2 = (float) Math.ceil(y2);
		return this;
	}
	
	/**
	 * Will round all values of this Area.
	 * @return This same Area.
	 */
	public Area round() {
		x1 = Math.round(x1);
		y1 = Math.round(y1);
		x2 = Math.round(x2);
		y2 = Math.round(y2);
		return this;
	}
	
	/**
	 * Will "maximize" the values of this Area, by flooring the smaller values <code>x1</code> and <code>y1</code> while
	 * ceiling the larger values <code>x2</code> and <code>y2</code>.
	 * @return This same Area.
	 */
	public Area maximize() {
		x1 = (float) Math.floor(x1);
		y1 = (float) Math.floor(y1);
		x2 = (float) Math.ceil(x2);
		y2 = (float) Math.ceil(y2);
		return this;
	}
	
	/**
	 * Will add the given values to the <code>x2</code> and <code>y2</code> values of this area.
	 * @param x The added value to <code>x2</code>
	 * @param y The added value to <code>y2</code>
	 * @return This same area.
	 */
	public Area add(float x, float y) {
		x2 += x;
		y2 += y;
		return this;
	}
	
	/**
	 * Will map the given X coordinate from this X coordinate system to the given Area's coordinate system.
	 * @param x The X coordinate to transform.
	 * @param target The area whose coordinate system will be used for the new X value.
	 * @return The equivalent of the given X coordinate in the given Area's coordinate system.
	 */
	public float mapX(float x, Area target) {
		return target.x1 + (x - x1) * target.getWidth() / getWidth();
	}
	
	/**
	 * Will map the given Y coordinate from this Y coordinate system to the given Area's coordinate system.
	 * @param y The Y coordinate to transform.
	 * @param target The area whose coordinate system will be used for the new Y value.
	 * @return The equivalent of the given Y coordinate in the given Area's coordinate system.
	 */
	public float mapY(float y, Area target) {
		return target.y1 + (y - y1) * target.getHeight() / getHeight();
	}
	
	/**
	 * Will create an Area symmetrical to this one around the X axis.
	 * @return A clone of this area, but with an X axis symetry.
	 */
	public Area cloneSymetryX() {
		return new Area(x1, -y1, x2, -y2);
	}
	
	/**
	 * Will create an Area symmetrical to this one around the Y axis.
	 * @return A clone of this area, but with an Y axis symetry.
	 */
	public Area cloneSymetryY() {
		return new Area(-x1, y1, -x2, y2);
	}
	
	@Override
	public Area clone() {
		return new Area(x1, y1, x2, y2);
	}

	@Override
	public String toString() {
		return "X:["+x1+";"+x2+"] Y:["+y1+";"+y2+"]";
	}

}

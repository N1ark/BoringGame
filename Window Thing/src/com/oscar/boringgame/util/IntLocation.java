package com.oscar.boringgame.util;

import static java.lang.Math.floor;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class IntLocation {
	
	public static final int BYTES = Integer.BYTES * 2;
	
	public int x, y;
	
	public IntLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public IntLocation(float x, float y) {
		this((int) floor(x), (int) floor(y));
	}
	
	public IntLocation(Location loc) {
		this((int) floor(loc.x), (int) floor(loc.y));
	}
	
	@Override
	public boolean equals(Object obj) {
		return 
				obj instanceof IntLocation &&
				((IntLocation) obj).x == x &&
				((IntLocation) obj).y == y;
	}
	
	/**
	 * @return This IntLocation's exact representation as a byte array of size <code>IntLocation.BYTES</code>.
	 */
	public byte[] toBytes() {
		byte[] bytesX = BoringUtil.toBytes(x);
		byte[] bytesY = BoringUtil.toBytes(y);
		
		return BoringUtil.concatAll(bytesX, bytesY);
	}
	
	/**
	 * Creates an IntLocation represented by the given bytes.
	 * @param bytes The location's bytes.
	 */
	public IntLocation(byte[] bytes) {
		if(bytes.length != BYTES)
			throw new IllegalArgumentException("The given array length (" + bytes.length + ") must be of length IntLocation.BYTES (" + BYTES + ")");
		
		this.x = BoringUtil.toInt(Arrays.copyOfRange(bytes, 0, Integer.BYTES));
		this.y = BoringUtil.toInt(Arrays.copyOfRange(bytes, Integer.BYTES, Integer.BYTES*2));
	}
	
	/**
	 * Creates an IntLocation represented by the given byte byffer.
	 * @param bytes The location's bytes.
	 */
	public IntLocation(ByteBuffer bytes) {
		if(bytes.remaining() < BYTES)
			throw new IllegalArgumentException("The given buffer length (" + bytes.remaining() + ") must be at least of length IntLocation.BYTES (" + BYTES + ")");
		this.x = bytes.getInt();
		this.y = bytes.getInt();
	}
}

package com.oscar.boringgame.util;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Pattern;

public class BoringUtil {

	/**
	 * Will concatenate the given arrays into a single array, whose length is equal to the sum of all the given arrays' length, and that contains
	 * a copy of all the objects of the given arrays.
	 * @param first The first array to concatenate.
	 * @param rest The other arrays to concatenate.
	 * @return The resulting array.
	 */
	@SafeVarargs
	public static <T> T[] concatAll(T[] first, T[]... rest) {
		int totalLength = first.length;
		for (T[] array : rest)
			totalLength += array.length;
		T[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (T[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}
	
	/**
	 * Will concatenate the given arrays into a single array, whose length is equal to the sum of all the given arrays' length, and that contains
	 * a copy of all the objects of the given arrays.
	 * @param first The first array to concatenate.
	 * @param rest The other arrays to concatenate.
	 * @return The resulting array.
	 */
	@SafeVarargs
	public static int[] concatAll(int[] first, int[]... rest) {
		int totalLength = first.length;
		for (int[] array : rest)
			totalLength += array.length;
		
		int[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (int[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}
	
	/**
	 * Will concatenate the given arrays into a single array, whose length is equal to the sum of all the given arrays' length, and that contains
	 * a copy of all the objects of the given arrays.
	 * @param first The first array to concatenate.
	 * @param rest The other arrays to concatenate.
	 * @return The resulting array.
	 */
	@SafeVarargs
	public static byte[] concatAll(byte[] first, byte[]... rest) {
		int totalLength = first.length;
		for (byte[] array : rest)
			totalLength += array.length;
		
		byte[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (byte[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}
	

	/**
	 * @param value The value to transform to bytes.
	 * @return The given short as an array of bytes.
	 */
	public static byte[] toBytes(short value) {
		return ByteBuffer.allocate(Short.BYTES).putShort(value).array();
	}
	
	/**
	 * @param value The value to transform to bytes.
	 * @return The given int as an array of bytes.
	 */
	public static byte[] toBytes(int value) {
		return ByteBuffer.allocate(Integer.BYTES).putInt(value).array();
	}

	/**
	 * @param value The value to transform to bytes.
	 * @return The given float as an array of bytes.
	 */
	public static byte[] toBytes(float value) {
		return ByteBuffer.allocate(Float.BYTES).putFloat(value).array();
	}
	
	/**
	 * @param value The value to transform to bytes.
	 * @return The given long as an array of bytes.
	 */
	public static byte[] toBytes(long value) {
		return ByteBuffer.allocate(Long.BYTES).putLong(value).array();
	}
	
	/**
	 * @param value The value to transform to bytes.
	 * @return The given boolean as an array of bytes.
	 */
	public static byte[] toBytes(boolean value) {
		return new byte[] {(byte) (value ? 1 : 0)};
	}
	
	/**
	 * @param value The value to transform to bytes.
	 * @return The given UUID as an array of bytes.
	 */
	public static byte[] toBytes(UUID uuid) {
		ByteBuffer bb = ByteBuffer.allocate(Long.BYTES * 2);
		bb.putLong(uuid.getMostSignificantBits());
		bb.putLong(uuid.getLeastSignificantBits());
		return bb.array();
	}
	
	/**
	 * Will read an UUID from the ByteBuffer, and increments its position by 16.
	 * @param bytes The bytes to read from.
	 * @return The read UUID.
	 */
	public static UUID getUUID(ByteBuffer bytes) {
		long firstLong = bytes.getLong();
		long secondLong = bytes.getLong();
		return new UUID(firstLong, secondLong);
	}
	
	/**
	 * Will decode the given ByteBuffer's content until the stopper sequence is found or the end of the buffer is reached, 
	 * and will then increment the buffer's position to right after the stopping sequence.
	 * <br> For instance, for a byte array created as <code>string1Bytes, separator, string2Bytes, separator</code>, to retrieve
	 * the Strings the correct sequence would be <code>string1 = getString(bytes, separator); string2 = getString(bytes, separator)</code>.
	 * @param bytes The bytes to read from.
	 * @param stopper The stopping sequence.
	 * @return The String generated.
	 */
	public static String getString(ByteBuffer bytes, byte[] stopper) {
		int lastPos;
		findEnd: for(lastPos = bytes.position(); lastPos < bytes.capacity() - stopper.length + 1; lastPos++) {
			for(int i = 0; i < stopper.length; i++)
				if(bytes.get(lastPos + i) != stopper[i])
					continue findEnd;
			break;
		}
		
		String string = new String(Arrays.copyOfRange(bytes.array(), bytes.position(), lastPos));
		bytes.position(lastPos + stopper.length);
		return string;
	}
	
	/**
	 * @param value The value to transform to bytes.
	 * @return The given String as an array of bytes.
	 */
	public static byte[] toBytes(String string) {
		return string.getBytes(StandardCharsets.UTF_8);
	}
	
	/**
	 * @param bytes The bytes used to create the int.
	 * @return The int represented by the given bytes.
	 */
	public static int toInt(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getInt();
	}
	
	/**
	 * @param bytes The bytes used to create the float.
	 * @return The float represented by the given bytes.
	 */
	public static float toFloat(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getFloat();
	}
	
	/**
	 * @param bytes The bytes used to create the short.
	 * @return The short represented by the given bytes.
	 */
	public static short toShort(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getShort();
	}
	
	/**
	 * @param bytes The bytes used to create the long.
	 * @return The long represented by the given bytes.
	 */
	public static long toLong(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getLong();
	}
	
	/**
	 * @param bytes The bytes used to create the boolean.
	 * @return The boolean represented by the given bytes.
	 */
	public static boolean toBoolean(byte[] bytes) {
		return bytes[0] == 1;
	}
	
	/**
	 * @param bytes The bytes used to create the UUID.
	 * @return The UUID represented by the given bytes.
	 */
	public static UUID toUUID(byte[] bytes) {
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		long firstLong = bb.getLong();
		long secondLong = bb.getLong();
		return new UUID(firstLong, secondLong);
	}
	
	/**
	 * @param bytes The bytes used to create the String.
	 * @return The String represented by the given bytes.
	 */
	public static String toString(byte[] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}
	
	/**
	 * @param bytes The bytes to convert to booleans.
	 * @return Will transform the given array of bytes into an array of booleans, that's eight times the size of the input array.
	 */
	public static boolean[] booleansFromBytes(byte[] bytes) {
		boolean[] bits = new boolean[bytes.length * 8];
		for (int i = 0; i < bits.length; i++)
			if ((bytes[i / 8] & (1 << (7 - (i % 8)))) > 0)
				bits[i] = true;
		return bits;
	}
	
	/**
	 * @param booleans The booleans to transform into bytes. The size of this array must be a multiple of 8.
	 * @return Will transform the given array of bytes to an array of bytes, by considering each boolean to be a bit. The returned array's length
	 * will have a length of <code>booleans.length / Byte.SIZE</code>.
	 */
	public static byte[] bytesFromBooleans(boolean[] booleans) {
		if(booleans.length%8 != 0)
			throw new IllegalArgumentException("The length of the boolean array ("+booleans.length+") must be a multiple of Byte.SIZE (" + Byte.SIZE + ")");
		
		byte[] bytes = new byte[booleans.length / Byte.SIZE];
		int index = 0;
		byte byteIndex = 0;
		
		for(boolean b : booleans) {
			
			if(b) {
			bytes[byteIndex] += (1 << (7 - index));
			}
				
			index ++;
			if(index == Byte.SIZE) {
				index = 0;
				byteIndex ++;
			}
		}
		
		return bytes;
	}
	
	/**
	 * Will capitalize each word in this String, mean each word will be in loercase except the first character of each word
	 * that will be upper case.
	 * @param string The String to capitalize.
	 * @return The capitalized String.
	 */
	public static String capitalize(String string) {
		String x = "";
		boolean first = false;
		
		for(String word : string.split(Pattern.quote(" "))) {
			if(first)
				x+=' ';
			
			x += word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
			
			first = true;
		}
		
		return x;
	}
}

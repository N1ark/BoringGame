package com.oscar.boringgame.util;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class PVector {
	
	/** The amount of bytes needed to save a PVector. */
	public static final int BYTES = Float.BYTES * 2;
	
	/** The x component of the vector. */
	public float x;

	/** The y component of the vector. */
	public float y;


	/**
	 * Constructor for an empty vector: x, y, and z are set to 0.
	 */
	public PVector() {
	}

	/**
	 * Constructor for a 2D vector
	 * 
	 * @param x the x coordinate.
	 * @param y the y coordinate.
	 */
	public PVector(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Constructor for a 2D vector
	 * 
	 * @param x the x coordinate.
	 * @param y the y coordinate.
	 */
	public PVector(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}

	/**
	 * Set x, y, and z coordinates.
	 * 
	 * @param x the x coordinate.
	 * @param y the y coordinate.
	 * @param z the z coordinate.
	 */
	public void set(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Set x, y, and z coordinates from a Vector3D object.
	 * 
	 * @param v the PVector object to be copied
	 */
	public void set(PVector v) {
		this.x = v.x;
		this.y = v.y;
	}

	/**
	 * Set the x, y (and maybe z) coordinates using a float[] array as the source.
	 * 
	 * @param source array to copy from
	 */
	public void set(float[] source) {
		if (source.length >= 2) {
			this.x = source[0];
			this.y = source[1];
		}
	}

	/**
	 * Get a copy of this vector.
	 */
	public PVector get() {
		return new PVector(this.x, this.y);
	}

	public float[] get(float[] target) {
		if (target == null) {
			return new float[] { this.x, this.y };
		}
		if (target.length >= 2) {
			target[0] = this.x;
			target[1] = this.y;
		}
		return target;
	}

	/**
	 * Calculate the magnitude (length) of the vector
	 * 
	 * @return the magnitude of the vector
	 */
	public float mag() {
		return (float) Math.sqrt(this.x * this.x + this.y * this.y);
	}

	/**
	 * Add a vector to this vector
	 * 
	 * @param v the vector to be added
	 */
	public PVector add(PVector v) {
		this.x += v.x;
		this.y += v.y;
		return this;
	}

	public PVector add(float x, float y) {
		this.x += x;
		this.y += y;
		return this;
	}
	
	public PVector cloneAdd(float x, float y) {
		return clone().add(x, y);
	}
	
	public PVector cloneAdd(PVector v) {
		return clone().add(v);
	}

	/**
	 * Add two vectors
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return a new vector that is the sum of v1 and v2
	 */
	static public PVector add(PVector v1, PVector v2) {
		return add(v1, v2, null);
	}

	/**
	 * Add two vectors into a target vector
	 * 
	 * @param v1     a vector
	 * @param v2     another vector
	 * @param target the target vector (if null, a new vector will be created)
	 * @return a new vector that is the sum of v1 and v2
	 */
	static public PVector add(PVector v1, PVector v2, PVector target) {
		if (target == null) {
			target = new PVector(v1.x + v2.x, v1.y + v2.y);
		} else {
			target.set(v1.x + v2.x, v1.y + v2.y);
		}
		return target;
	}

	/**
	 * Subtract a vector from this vector
	 * 
	 * @param v the vector to be subtracted
	 */
	public void sub(PVector v) {
		this.x -= v.x;
		this.y -= v.y;
	}
	
	public void sub(float x, float y) {
		this.x -= x;
		this.y -= y;
	}

	/**
	 * Subtract one vector from another
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return a new vector that is v1 - v2
	 */
	static public PVector sub(PVector v1, PVector v2) {
		return sub(v1, v2, null);
	}

	static public PVector sub(PVector v1, PVector v2, PVector target) {
		if (target == null) {
			target = new PVector(v1.x - v2.x, v1.y - v2.y);
		} else {
			target.set(v1.x - v2.x, v1.y - v2.y);
		}
		return target;
	}

	/**
	 * Multiply this vector by a scalar
	 * 
	 * @param n the value to multiply by
	 */
	public void mult(float n) {
		this.x *= n;
		this.y *= n;
	}

	/**
	 * Multiply a vector by a scalar
	 * 
	 * @param v a vector
	 * @param n scalar
	 * @return a new vector that is v1 * n
	 */
	static public PVector mult(PVector v, float n) {
		return mult(v, n, null);
	}

	/**
	 * Multiply a vector by a scalar, and write the result into a target PVector.
	 * 
	 * @param v      a vector
	 * @param n      scalar
	 * @param target PVector to store the result
	 * @return the target vector, now set to v1 * n
	 */
	static public PVector mult(PVector v, float n, PVector target) {
		if (target == null) {
			target = new PVector(v.x * n, v.y * n);
		} else {
			target.set(v.x * n, v.y * n);
		}
		return target;
	}

	/**
	 * Multiply each element of one vector by the elements of another vector.
	 * 
	 * @param v the vector to multiply by
	 */
	public void mult(PVector v) {
		this.x *= v.x;
		this.y *= v.y;
	}

	/**
	 * Multiply each element of one vector by the individual elements of another
	 * vector, and return the result as a new PVector.
	 */
	static public PVector mult(PVector v1, PVector v2) {
		return mult(v1, v2, null);
	}

	/**
	 * Multiply each element of one vector by the individual elements of another
	 * vector, and write the result into a target vector.
	 * 
	 * @param v1     the first vector
	 * @param v2     the second vector
	 * @param target PVector to store the result
	 */
	static public PVector mult(PVector v1, PVector v2, PVector target) {
		if (target == null) {
			target = new PVector(v1.x * v2.x, v1.y * v2.y);
		} else {
			target.set(v1.x * v2.x, v1.y * v2.y);
		}
		return target;
	}

	/**
	 * Divide this vector by a scalar
	 * 
	 * @param n the value to divide by
	 */
	public void div(float n) {
		this.x /= n;
		this.y /= n;
	}

	/**
	 * Divide a vector by a scalar and return the result in a new vector.
	 * 
	 * @param v a vector
	 * @param n scalar
	 * @return a new vector that is v1 / n
	 */
	static public PVector div(PVector v, float n) {
		return div(v, n);
	}

	static public PVector div(PVector v, float n, PVector target) {
		if (target == null) {
			target = new PVector(v.x / n, v.y / n);
		} else {
			target.set(v.x / n, v.y / n);
		}
		return target;
	}

	/**
	 * Divide each element of one vector by the elements of another vector.
	 */
	public void div(PVector v) {
		this.x /= v.x;
		this.y /= v.y;
	}

	/**
	 * Multiply each element of one vector by the individual elements of another
	 * vector, and return the result as a new PVector.
	 */
	static public PVector div(PVector v1, PVector v2) {
		return div(v1, v2, null);
	}

	/**
	 * Divide each element of one vector by the individual elements of another
	 * vector, and write the result into a target vector.
	 * 
	 * @param v1     the first vector
	 * @param v2     the second vector
	 * @param target PVector to store the result
	 */
	static public PVector div(PVector v1, PVector v2, PVector target) {
		if (target == null) {
			target = new PVector(v1.x / v2.x, v1.y / v2.y);
		} else {
			target.set(v1.x / v2.x, v1.y / v2.y);
		}
		return target;
	}

	/**
	 * Calculate the Euclidean distance between two points (considering a point as a
	 * vector object)
	 * 
	 * @param v another vector
	 * @return the Euclidean distance between
	 */
	public float dist(PVector v) {
		float dx = this.x - v.x;
		float dy = this.y - v.y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}
	
	/**
	 * Calculate the Euclidean distance between two points (considering a point as a
	 * vector object)
	 * 
	 * @param x the x coordinate of the other point
	 * @param y the y coordinate of the other point
	 * @return the Euclidean distance between
	 */
	public float dist(float x, float y) {
		float dx = this.x - x;
		float dy = this.y - y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Calculate the Euclidean distance between two points (considering a point as a
	 * vector object)
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return the Euclidean distance between v1 and v2
	 */
	static public float dist(PVector v1, PVector v2) {
		float dx = v1.x - v2.x;
		float dy = v1.y - v2.y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Will create a unit vector with the given angle.
	 * @param angle The angle of the vector.
	 * @return The created vector.
	 */
	static public PVector fromAngle(double angle) {
		return new PVector((float) Math.cos(angle), (float) Math.sin(angle));
	}

	/**
	 * Calculate the dot product with another vector
	 * 
	 * @return the dot product
	 */
	public float dot(PVector v) {
		return this.x * v.x + this.y * v.y;
	}

	public float dot(float x, float y) {
		return this.x * x + this.y * y;
	}

	static public float dot(PVector v1, PVector v2) {
		return v1.x * v2.x + v1.y * v2.y;
	}

	/**
	 * Normalize the vector to length 1 (make it a unit vector)
	 * @return this vector.
	 */
	public PVector normalize() {
		float m = mag();
		if (m != 0 && m != 1) {
			div(m);
		}
		return this;
	}

	/**
	 * Normalize this vector, storing the result in another vector.
	 * 
	 * @param target Set to null to create a new vector
	 * @return a new vector (if target was null), or target
	 */
	public PVector normalize(PVector target) {
		if (target == null) {
			target = new PVector();
		}
		float m = mag();
		if (m > 0) {
			target.set(this.x / m, this.y / m);
		} else {
			target.set(this.x, this.y);
		}
		return target;
	}

	/**
	 * Limit the magnitude of this vector
	 * 
	 * @param max the maximum length to limit this vector
	 */
	public void limit(float max) {
		if (mag() > max) {
			normalize();
			mult(max);
		}
	}

	/**
	 * Calculate the angle of rotation for this vector (only 2D vectors)
	 * 
	 * @return the angle of rotation
	 */
	public float heading2D() {
		float angle = (float) Math.atan2(-this.y, this.x);
		return -1 * angle;
	}

	/**
	 * Calculate the angle between two vectors, using the dot product
	 * 
	 * @param v1 a vector
	 * @param v2 another vector
	 * @return the angle between the vectors
	 */
	static public float angleBetween(PVector v1, PVector v2) {
		double dot = v1.x * v2.x + v1.y * v2.y;
		double v1mag = Math.sqrt(v1.x * v1.x + v1.y * v1.y);
		double v2mag = Math.sqrt(v2.x * v2.x + v2.y * v2.y);
		return (float) Math.acos(dot / (v1mag * v2mag));
	}

	@Override
	public String toString() {
		return "[ " + this.x + ", " + this.y + " ]";
	}
	
	/**
	 * @return The angle of the vector, relative to the X axis, in radians.
	 */
	public float getAngle() {
		return (float) Math.atan2(y, x);
	}
	
	/**
	 * @return The closest Side value that represents the angle of the vector.
	 */
	public Side getSide() {
		return Side.closestTo(getAngle());
	}
	
	@Override
	public boolean equals(Object obj) {
		return 
				obj instanceof PVector && 
				x == ((PVector) obj).x && 
				y == ((PVector) obj).y;
	}
	
	@Override
	protected PVector clone() {
		return new PVector(x, y);
	}

	/**
	 * Will set the vector's values to 0.
	 */
	public void nullify() {
		x = 0;
		y = 0;
	}
	
	/**
	 * @return If this vector's values are both equal to 0.
	 */
	public boolean isNull() {
		return x == 0 && y == 0;
	}
	
	/**
	 * @return This PVector's exact representation as a byte array of size <code>PVector.BYTES</code>.
	 */
	public byte[] toBytes() {
		
		byte[] bytesX = BoringUtil.toBytes(x);
		byte[] bytesY = BoringUtil.toBytes(y);
		
		return BoringUtil.concatAll(bytesX, bytesY);
	}
	
	/**
	 * Creates a PVector represented by the given bytes.
	 * @param bytes The vector's bytes.
	 */
	public PVector(byte[] bytes) {
		if(bytes.length != BYTES)
			throw new IllegalArgumentException("The given array length (" + bytes.length + ") must be of length PVector.BYTES (" + BYTES + ")");
		
		this.x = BoringUtil.toFloat(Arrays.copyOfRange(bytes, 0, Float.BYTES));
		this.y = BoringUtil.toFloat(Arrays.copyOfRange(bytes, Float.BYTES, Float.BYTES*2));
	}
	
	/**
	 * Creates a PVector represented by the given byte byffer.
	 * @param bytes The vector's bytes.
	 */
	public PVector(ByteBuffer bytes) {
		if(bytes.remaining() < BYTES)
			throw new IllegalArgumentException("The given buffer length (" + bytes.remaining() + ") must be of length PVector.BYTES (" + BYTES + ")");
		this.x = bytes.getFloat();
		this.y = bytes.getFloat();
	}
}

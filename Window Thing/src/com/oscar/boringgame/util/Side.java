package com.oscar.boringgame.util;

public enum Side {
	NORTH( 0,-1), 
	SOUTH( 0, 1), 
	EAST ( 1, 0), 
	WEST (-1, 0);
	
	public final int x, y;
	
	Side(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public Location toLocation() {
		return new Location(x, y, null);
	}
	public PVector toVector() {
		return new PVector(x, y);
	}

	/**
	 * 
	 * @param angle The angle that is compared, in radians.
	 * @return The Side that most cloesly represents the given angle.
	 */
	public static Side closestTo(float radians) {
		float angle = (float) Math.toDegrees(radians);
		if(angle < 0)
			while(angle < 0)
				angle += 360;
		else
			angle %= 360;
		
		angle += 45; // we add 45, to avoid having double comparisons
		
		if(angle < 90)
			return EAST;
		else if(angle < 180)
			return SOUTH;
		else if(angle < 270)
			return WEST;
		else if(angle < 360)
			return NORTH;
		else
			return EAST;
	}
}

package com.oscar.boringgame.util;

import static java.lang.Math.abs;
import static java.lang.Math.floor;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BoringMath {

	private static final Random random = new Random();

	public static Random random() {
		return random;
	}

	public static boolean isBetweenExclusive(float number, float min, float max) {
		return (number > min && number < max);
	}

	public static boolean isBetweenInclusive(float number, float min, float max) {
		return (number >= min && number <= max);
	}

	public static boolean isBetween(float number, float min, float max) {
		return (number >= min && number < max);
	}

	public static boolean isInside(Point p, float x, float y, float width, float height) {
		return isBetween(p.x, x, x + width) && isBetween(p.y, y, y + height);
	}

	public static boolean isInside(Point p, Point q, float width, float height) {
		return isBetween(p.x, q.x, q.x + width) && isBetween(p.y, q.y, q.y + height);
	}

	public static boolean isInside(float x, float y, float x1, float y1, float x2, float y2) {
		return isBetween(x, x1, x2) && isBetween(y, y1, y2);
	}

	public static float getRadAngle(float x1, float y1, float x2, float y2) {
		return (float) Math.atan2(y2 - y1, x2 - x1);
	}

	public static float getRadAngle(Location center, Location target) {
		return getRadAngle(center.x, center.y, target.x, target.y);
	}

	public static float getAngle(float x1, float y1, float x2, float y2) {
		float angle = getRadAngle(x1, y1, x2, y2);
		angle = (float) Math.toDegrees(angle);
		return angle;
	}

	public static float getAngle(Location center, Location target) {
		return getAngle(center.x, center.y, target.x, target.y);
	}

	public static float distance(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}

	public static int nextInt(int min, int max) {
		return min + random.nextInt(max - min);
	}

	public static float nextFloat(float min, float max) {
		return min + random.nextFloat() * (max - min);
	}

	public static int nextInt(int min, int max, long seed) {
		return min + new Random(seed).nextInt(max - min);
	}

	public static float nextFloat(float min, float max, long seed) {
		return min + new Random(seed).nextFloat() * (max - min);
	}

	public static int nextInt(int min, int max, Random rand) {
		return min + rand.nextInt(max - min);
	}

	public static float nextFloat(float min, float max, Random rand) {
		return min + rand.nextFloat() * (max - min);
	}
	
	public static float adjustBetween(float value, float min, float max) {
		return value < min ? min : value > max ? max : value;
	}
	
	public static int adjustBetween(int value, int min, int max) {
		return value < min ? min : value > max ? max : value;
	}
	
	public static boolean doIntersect(float ax1, float ay1, float ax2, float ay2, float bx1, float by1, float bx2, float by2) {
		
		// width of first segment
		float sx1 = ax2 - ax1;
		// height of first segment
	    float sy1 = ay2 - ay1;
	    // width of second segment
	    float sx2 = bx2 - bx1;
	    // height of second segment
	    float sy2 = by2 - by1;

	    float s = (-sy1 * (ax1 - bx1) + sx1 * (ay1 - by1)) / (-sx2 * sy1 + sx1 * sy2);
	    float t = ( sx2 * (ay1 - by1) - sy2 * (ax1 - bx1)) / (-sx2 * sy1 + sx1 * sy2);

	    if (0 <= s && s <= 1 && 0 <= t && t <= 1) {
	        // Collision detected
	        return true;
	    }

	    return false;
	}
	
	/**
	 * Will raytrace the given line on a grid, and return a List with all the int coordinates of the grid by which the line intersects.
	 * <br>This is based on <a href="http://playtechs.blogspot.com/2007/03/raytracing-on-grid.html">this website</a>.
	 * @param x0 The X coordinate of the first point of the segment.
	 * @param y0 The Y coordinate of the first point of the segment.
	 * @param x1 The X coordinate of the other point of the segment.
	 * @param y1 The Y coordinate of the other point of the segment.
	 * @return A List with all the coordinates by which the Line passes, in int array formatted as: {x,y}
	 */
	public static List<int[]> raytraceLine(float x0, float y0, float x1, float y1){
		List<int[]> found = new ArrayList<>();
		
		float dx = abs(x1 - x0);
		float dy = abs(y1 - y0);

		int x = (int) (floor(x0));
		int y = (int) (floor(y0));

		float dt_dx = 1f / dx;
		float dt_dy = 1f / dy;


		int n = 1;
		int x_inc, y_inc;
		float t_next_vertical, t_next_horizontal;

		if (dx == 0) {
			x_inc = 0;
			t_next_horizontal = dt_dx; // infinity
		} else if (x1 > x0) {
			x_inc = 1;
			n += (int) (floor(x1)) - x;
			t_next_horizontal = (float) ((floor(x0) + 1 - x0) * dt_dx);
		} else {
			x_inc = -1;
			n += x - (int) (floor(x1));
			t_next_horizontal = (float) ((x0 - floor(x0)) * dt_dx);
		}

		if (dy == 0) {
			y_inc = 0;
			t_next_vertical = dt_dy; // infinity
		} else if (y1 > y0) {
			y_inc = 1;
			n += (int) (floor(y1)) - y;
			t_next_vertical = (float) ((floor(y0) + 1 - y0) * dt_dy);
		} else {
			y_inc = -1;
			n += y - (int) (floor(y1));
			t_next_vertical = (float) ((y0 - floor(y0)) * dt_dy);
		}

		for (; n > 0; --n) {
			found.add(new int[] {x, y});

			if (t_next_vertical < t_next_horizontal) {
				y += y_inc;
				t_next_vertical += dt_dy;
			} else {
				x += x_inc;
				t_next_horizontal += dt_dx;
			}
		}
		
		return found;
	}
}

package com.oscar.boringgame.util;

import com.oscar.boringgame.item.Item;
import com.oscar.boringgame.item.ItemStack;

public class Loot {
	private final Item type;
	private final float odds;
	private final int min;
	private final int max;
	public Loot(Item type) {
		this.type = type;
		odds = 1;
		min = 0;
		max = 1;
	}
	public Loot(Item type, float odds) {
		this.type = type;
		this.odds = odds;
		min = 0;
		max = 1;
	}
	public Loot(Item type, int min, int max) {
		this.type = type;
		this.min = min;
		this.max = max;
		odds = 1;
	}
	public Loot(Item type, float odds, int min, int max) {
		this.type = type;
		this.min = min;
		this.max = max;
		this.odds = odds;
	}
	
	/**
	 * @return The Item associated to this loot.
	 */
	public Item getItem() {
		return type;
	}
	
	/**
	 * @return The minimum amount of Items that can be dropped.
	 */
	public int getMin() {
		return min;
	}
	
	/**
	 * @return The maximum amount of Items that can be dropped.
	 */
	public int getMax() {
		return max;
	}
	
	/**
	 * @return The odds of an Item dropping.
	 */
	public float getOdds() {
		return odds;
	}
	/**
	 * Will take into account the probability of the loot appearing, the min and max values and the Item type to create a new ItemStack.
	 * <br> The amount of the generated ItemStack will be included in [min;max].
	 * @return The generated ItemStack, <strong>or null if the odds were too low</strong>
	 */
	public ItemStack generateItem() {
		if(odds == 1 || BoringMath.random().nextFloat() < odds) {
			int amount = min + BoringMath.random().nextInt(max-min+1);
			if(amount < 1)
				return null;
			
			ItemStack item = new ItemStack(type);
			item.setAmount(amount);
			return item;
		}
		return null;
	}
}

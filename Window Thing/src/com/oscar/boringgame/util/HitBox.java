package com.oscar.boringgame.util;

public class HitBox implements Cloneable{
	Area[] parts;
	public HitBox(Area... parts) {
		this.parts = parts;
	}
	public HitBox(){
		parts = new Area[0];
	}
	public Area[] getParts() {
		return parts.clone();
	}
	public HitBox addPart(Area a) {
		Area[] old = parts.clone();
		parts = new Area[parts.length + 1];
		for(int x = 0; x < old.length; x ++)
			parts[x] = old[x];
		parts[parts.length-1] = a;
		return this;
	}
	public boolean collidesWith(Location l) {
		for(Area a : parts)
			if(a.isInside(l))
				return true;
		return false;
	}
	public boolean intersectsWith(Area a) {
		for(Area b : parts)
			if(b.intersects(a))
				return true;
		return false;
	}
	public HitBox copyTranslate(float x, float y) {
		Area[] areas = new Area[parts.length];
		for(int i = 0; i < areas.length; i++)
			areas[i] = parts[i].copyTranslate(x, y);
		return new HitBox(parts);
	}
	@Override
	public HitBox clone() {
		return new HitBox(parts.clone());
	}
}

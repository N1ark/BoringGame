package com.oscar.boringgame.util;

public enum EquationType {
	/**
	 * Equation A
	 * Arguments:
	 * A
	 */
	A,
	
	/**
	 * Equation A * X
	 * Arguments:
	 * A
	 */
	AX,
	
	/**
	 * Equation A * X + B
	 * Arguments:
	 * A,
	 * B
	 */
	AXB,
	
	/**
	 * Equation A * X + B * X
	 * Arguments:
	 * A,
	 * B
	 */
	AXBX,
	
	/**
	 * Equation A * (X ^ 2) + B * X + C
	 * Arguments:
	 * A,
	 * B,
	 * C
	 */
	AXPOWBXC,
	
	/**
	 * Equation A * (X ^ E) + B * X + C
	 * Arguments:
	 * A,
	 * E,
	 * B,
	 * C
	 */
	AXPOWEBXC,
	
	/**
	 * Equation sin(X)
	 * Arguments:
	 * None
	 */
	SINX,
	
	/**
	 * Equation cos(X)
	 * Arguments:
	 * None
	 */
	COSX,
	
	/**
	 * Equation A * sin(X)
	 * Arguments:
	 * A
	 */
	ASINX,
	
	/**
	 * Equation A * cos(X)
	 * Arguments:
	 * A
	 */
	ACOSX,
	
	/**
	 * Equation sin(A * X)
	 * Arguments:
	 * A
	 */
	SINAX,
	
	/**
	 * Equation cos(A * X)
	 * Arguments:
	 * A
	 */
	COSAX,
	/**
	 * Equation [random A~B]
	 * Arguments:
	 * A,
	 * B
	 */
	RAND,
	
	/**
	 * Equation [random A~B] * X
	 * Arguments:
	 * A,
	 * B
	 */
	RANDX
	;
	
	public static class Equation implements Cloneable{
		public final EquationType type;
		public final float[] args;
		
		public Equation(EquationType type, float... args) {
			this.type = type;
			if(type == RANDX || type == RAND)
				this.args = new float[] {args[0], args[1], BoringMath.nextFloat(Math.min(args[0], args[1]), Math.max(args[0], args[1]))};
			else
				this.args = args;
		}
		
		public float calc(float x) {
			switch(type) {
			case A:
				return args[0];
			case AX:
				return args[0] * x;
			case AXB:
				return (args[0] * x) + args[1];
			case AXBX:
				return (args[0] * x) + (args[1] * x);
			case AXPOWBXC:
				return (float) ((args[0] * Math.pow(x, 2)) + (args[1] * x) + args[2]);
			case AXPOWEBXC:
				return (float) ((args[0] * Math.pow(x, args[1])) + (args[2] * x) + args[3]);
			case SINX:
				return (float) Math.sin(x);
			case COSX:
				return (float) Math.cos(x);
			case ASINX:
				return (float) (args[0] * Math.sin(x));
			case ACOSX:
				return (float) (args[0] * Math.cos(x));
			case SINAX:
				return (float) Math.sin(args[0] * x);
			case COSAX:
				return (float) Math.cos(args[0] * x);
			case RAND:
				return args[2];
			case RANDX:
				return args[2] * x;
			}
			return x;
		}
		
		@Override
		public Equation clone() {
			return new Equation(type, args);
		}
	}
}

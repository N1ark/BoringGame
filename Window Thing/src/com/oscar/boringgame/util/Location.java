package com.oscar.boringgame.util;

import java.nio.ByteBuffer;
import java.util.Arrays;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.map.World;

public class Location implements Cloneable {
	
	public static final int BYTES = Float.BYTES * 2;
	
	public float x, y;
	
	public World w;


	public Location(double x, double y, World w) {
		this((float) x, (float) y, w);
	}


	public Location(float x, float y, World w) {
		this.x = x;
		this.y = y;
		this.w = w;
	}
	
	public Location(IntLocation loc, World w) {
		this.x = loc.x;
		this.y = loc.y;
		this.w = w;
	}

	public World getWorld() {
		return w;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void setWorld(World w) {
		this.w = w;
	}

	public void set(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Location middle(Location l) {
		return new Location((x + l.x) / 2, (y + l.y) / 2, w);
	}

	public float distance(float x, float y) {
		return (float) Math.sqrt((this.x - x) * (this.x - x) + (this.y - y) * (this.y - y));
	}

	public float distance(Location l) {
		return distance(l.x, l.y);
	}

	public SolidBlock getBlock() {
		return w.getBlock(x, y);
	}

	public SurfaceBlock getUpperBlock() {
		return w.getUpperBlock(x, y);
	}

	public SurfaceBlock getVisibleUpperBlock() {
		return w.getVisibleUpperBlock(x, y);
	}

	public Biome getBiome() {
		return w.getBiome(x, y);
	}

	public Location toInt() {
		x = (int) x;
		y = (int) y;
		return this;
	}

	public Location ceil() {
		x = (float) Math.ceil(x);
		y = (float) Math.ceil(y);
		return this;
	}

	public Location floor() {
		x = (float) Math.floor(x);
		y = (float) Math.floor(y);
		return this;
	}

	public Location round() {
		x = Math.round(x);
		y = Math.round(y);
		return this;
	}

	public Location add(Location l) {
		x += l.x;
		y += l.y;
		return this;
	}

	public Location add(int x, int y) {
		this.x += x;
		this.y += y;
		return this;
	}

	public Location add(float x, float y) {
		this.x += x;
		this.y += y;
		return this;
	}

	public Location add(PVector p) {
		x += p.x;
		y += p.y;
		return this;
	}

	@Override
	public Location clone() {
		return new Location(x, y, w);
	}

	@Override
	public boolean equals(Object obj) {
		return 
				obj instanceof Location && 
				((Location) obj).x == x && 
				((Location) obj).y == y;
	}

	@Override
	public String toString() {
		return "[" + x + "/" + y + "]";
	}

	public PVector toVector() {
		return new PVector(x, y);
	}

	public void set(Location l) {
		this.x = l.x;
		this.y = l.y;
		this.w = l.w;
	}

	public PVector vectorTo(Location l) {
		return new PVector(l.x - x, l.y - y);
	}

	/**
	 * @return This Location's exact representation (without the World) as a byte array of size <code>Location.BYTES</code>.
	 */
	public byte[] toBytes() {
		
		byte[] bytesX = BoringUtil.toBytes(x);
		byte[] bytesY = BoringUtil.toBytes(y);
		
		return BoringUtil.concatAll(bytesX, bytesY);
	}
	
	/**
	 * Creates a Location represented by the given bytes.
	 * @param bytes The location's bytes.
	 */
	public Location(byte[] bytes) {
		if(bytes.length != BYTES)
			throw new IllegalArgumentException("The given array length (" + bytes.length + ") must be of length Location.BYTES (" + BYTES + ")");
		
		this.x = BoringUtil.toFloat(Arrays.copyOfRange(bytes, 0, Float.BYTES));
		this.y = BoringUtil.toFloat(Arrays.copyOfRange(bytes, Float.BYTES, Float.BYTES*2));
	}
	
	/**
	 * Creates an Location represented by the given byte byffer.
	 * @param bytes The location's bytes.
	 */
	public Location(ByteBuffer bytes) {
		if(bytes.remaining() < BYTES)
			throw new IllegalArgumentException("The given buffer length (" + bytes.remaining() + ") must be at least of length Location.BYTES (" + BYTES + ")");
		this.x = bytes.getFloat();
		this.y = bytes.getFloat();
	}
	
}

package com.oscar.boringgame.map;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;

public class Chunk {
	
	public static final int SIZE = 32;
	
	private Biome[][] biomes;
	private SolidBlock[][] ground;
	private SurfaceBlock[][] surface;
	private byte[][] breaking;
	// This could possibly be replaced by a BitSet, if one day memory usage becomes a concern [?]
	private boolean[][] blockUpdates;
	
	
	public Chunk() {
		this.ground = new SolidBlock[SIZE][SIZE];
		this.surface = new SurfaceBlock[SIZE][SIZE];
		this.breaking = new byte[SIZE][SIZE];
		this.biomes = new Biome[SIZE][SIZE];
		this.blockUpdates = new boolean[SIZE][SIZE];
		
		for(int i = 0; i < Chunk.SIZE; i++) {
			for(int j = 0; j < Chunk.SIZE; j++) {
				ground[i][j] = SolidBlock.ERROR;
				surface[i][j] = AIR.AIR;
				biomes[i][j] = Biome.PLAIN;
			}
		}
	}
	
	/**
	 * Will set a block in this chunk, at the given inner coordinates (ranging from 0 to <code>Chunk.SIZE</code>).
	 * @param b The block to place.
	 * @param x The inner x coordinate of the block.
	 * @param y The inner y coordinate of the block.
	 */
	public void setblock(Block b, int x, int y) {
		if(b instanceof SolidBlock)
			setblock((SolidBlock) b, x, y);
		else
			setblock((SurfaceBlock) b, x, y);
	}
	
	/**
	 * Will set a ground block in this chunk, at the given inner coordinates (ranging from 0 to <code>Chunk.SIZE</code>).
	 * @param b The block to place.
	 * @param x The inner x coordinate of the block.
	 * @param y The inner y coordinate of the block.
	 */
	public void setblock(SolidBlock b, int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried setting ground block to outside chunk (" + x + "/" + y + ")").printStackTrace();
			return;
		}
		ground[x][y] = b;
	}
	
	/**
	 * Will set a ground block in this chunk, at the given inner coordinates (ranging from 0 to <code>Chunk.SIZE</code>).
	 * @param b The block to place.
	 * @param x The inner x coordinate of the block.
	 * @param y The inner y coordinate of the block.
	 */
	public void setblock(SurfaceBlock b, int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried setting surface block to outside chunk (" + x + "/" + y + ")").printStackTrace();
			return;
		}
		surface[x][y] = b;
	}
	
	/**
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @return The ground block at the given inner coordinates, or <code>SolidBlock.ERROR</code> if the coordinates aren't inside the chunk.
	 */
	public SolidBlock getBlock(int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried getting ground block from outside chunk (" + x + "/" + y + ")").printStackTrace();
			return SolidBlock.ERROR;
		}
		return ground[x][y];
	}
	
	/**
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @return The ground block at the given inner coordinates, or <code>AIR.AIR</code> if the coordinates aren't inside the chunk.
	 */
	public SurfaceBlock getUpperBlock(int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried getting surface block from outside chunk (" + x + "/" + y + ")").printStackTrace();
			return AIR.AIR;
		}
		return surface[x][y];
	}
	
	/**
	 * Will set the biome at the given coordinates.
	 * @param b The biome that will be set.
	 * @param x The x coordinate of the location.
	 * @param y The y coordinate of the location.
	 */
	public void setBiome(Biome b, int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried setting a biome to outside chunk (" + x + "/" + y + ")").printStackTrace();
			return;
		}
		biomes[x][y] = b;
	}
	
	/**
	 * @param x The x coordinate of the location.
	 * @param y The y coordinate of the location.
	 * @return The biome at the given inner coordinates, or <code>null</code> if the coordinates aren't inside the chunk.
	 */
	public Biome getBiome(int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried getting a biome from outside chunk (" + x + "/" + y + ")").printStackTrace();
			return null;
		}
		return biomes[x][y];
	}
	
	/**
	 * Will set the break state of the block at the given inner coordinates. If the given coordinates aren't inside the chunk, will do nothing.
	 * @param state The new break state.
	 * @param x The x coordinate of the location.
	 * @param y The y coordinate of the location.
	 */
	public void setBreakState(int state, int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried setting break state to outside chunk (" + x + "/" + y + ")").printStackTrace();
			return;
		}
		breaking[x][y] = (byte) state;
	}
	
	/**
	 * Will change the break state of the block at the given inner coordinates by the given amount. If the given coordinates aren't inside the chunk, will do nothing.
	 * @param added The new break state.
	 * @param x The x coordinate of the location.
	 * @param y The y coordinate of the location.
	 */
	public void addBreakState(int added, int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried adding break state to outside chunk (" + x + "/" + y + ")").printStackTrace();
			return;
		}
		breaking[x][y] += added;
	}
	
	/**
	 * @param x The x coordinate of the location.
	 * @param y The y coordinate of the location.
	 * @return The breaking state of the block at the given inner coordinates, or 0 if the coordinates aren't inside the chunk.
	 */
	public int getBreakState(int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried getting a break state from outside chunk (" + x + "/" + y + ")").printStackTrace();
			return 0;
		}
		return breaking[x][y];
	}
	
	/**
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @return If the block at the given inner coordinates needs a block update.
	 */
	public boolean needsBlockUpdate(int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried getting a block update from outside chunk (" + x + "/" + y + ")").printStackTrace();
			return false;
		}
		return blockUpdates[x][y];
	}
	
	/**
	 * Will set wether or not the block at the given coordinates needs a block update.
	 * @param state If a block update is needed.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 */
	public void blockUpdate(boolean state, int x, int y) {
		if(!isInside(x, y)) {
			new IllegalArgumentException("Tried setting a block update to outside chunk (" + x + "/" + y + ")").printStackTrace();
			return;
		}
		blockUpdates[x][y] = state;
	}
	
	/**
	 * Will look through all the blocks in the chunk and see if they need a block update.
	 * @return The total amount of needed block updates in the chunk.
	 */
	public int amountOfBlockUpdates() {
		int total = 0;
		for(int x = 0; x < SIZE; x++)
			for(int y = 0; y < SIZE; y++)
				if(blockUpdates[x][y])
					total++;
		
		return total;
	}
	
	public boolean needsABlockUpdate() {
		for(int x = 0; x < SIZE; x++)
			for(int y = 0; y < SIZE; y++)
				if(blockUpdates[x][y])
					return true;
		return false;
	}
	
	/**
	 * Will set all the values in the given array to the Chunk's needed block updates.
	 * @param array The array to store the needed block updates. It must have a length of <code>Chunk.SIZE</code>, and so must all its inner arrays.
	 */
	public void blockUpdatesToArray(boolean[][] array) {
		if(array.length != SIZE) {
			new IllegalArgumentException("The given array must have a size of " + Chunk.SIZE + "! It has a size of " + array.length).printStackTrace();
			return;
		}
		
		for(int x = 0; x < SIZE; x++) {
			if(array[x].length != SIZE) {
				new IllegalArgumentException("The given inner array of index " + x +" must have a size of " + Chunk.SIZE + "! It has a size of " + array[x].length).printStackTrace();
				return;
			}
			for(int y = 0; y < SIZE; y++)
				array[x][y] = blockUpdates[x][y];
		}
	}
	
	/**
	 * Will remove all existant necessary block updates.
	 */
	public void clearUpdates() {
		for(int x = 0; x < SIZE; x++)
			for(int y = 0; y < SIZE; y++)
				blockUpdates[x][y] = false;
	}
	
	/**
	 * Will return wether the given coordinates can be considered as inner coordinates for a chunk.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return If the given coordinate is inside the Chunk, which means that the given coordinates are included between 0 and <code>Chunk.size-1</code>.
	 */
	private static boolean isInside(int x, int y) {
		return 0 <= x && x < SIZE && 0 <= y && y < SIZE;
	}
	
}

package com.oscar.boringgame.map.generator;

import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.map.Chunk;
import com.oscar.boringgame.map.World;

public class TestWorldGenerator extends WorldGenerator {
	
	public TestWorldGenerator(World world) {
		super(world);
	}
	
	@Override
	public WorldGeneratorType getType() {
		return WorldGeneratorType.TEST;
	}

	@Override
	public void generateData(Chunk chunk, int x, int y) {
		for(int i = 0; i < Chunk.SIZE; i++) {
			for(int j = 0; j < Chunk.SIZE; j++) {
				chunk.setblock(SolidBlock.GRASS, i, j);
				chunk.setblock(AIR.AIR, i, j);
				chunk.setBiome(Biome.PLAIN, i, j);
			}
		}
	}

}

package com.oscar.boringgame.map.generator;

import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.map.Chunk;
import com.oscar.boringgame.map.World;

public abstract class WorldGenerator {
	
	protected final World world;
	protected final long seed;
	
	WorldGenerator(World world) {
		this.seed = world.seed;
		this.world = world;
	}
	
	/**
	 * @return The type of this WorldGenerator.
	 */
	public abstract WorldGeneratorType getType();
	
	/**
	 * Will generate the terrain for the given Chunk, at the given chunk position. To override this method,
	 * the class should rather implement <code>generateData(Chunk, int, int)</code>.
	 * @param chunk The chunk to generate.
	 * @param x The X coordinate of the chunk.
	 * @param y The Y coordinate of the chunk.
	 */
	public final void generateTerrain(Chunk chunk, int x, int y) {
		long chrono = System.currentTimeMillis();
		System.out.print("Generating chunk at " + x + "/" + y + "... ");
		generateData(chunk, x, y);
		System.out.println("Done! Took " + (System.currentTimeMillis() - chrono) + "ms");
	}
	
	/**
	 * An inner method, that will actually do all the generation for the chunk.
	 * @param chunk The chunk to generate.
	 * @param x The X coordinate of the chunk.
	 * @param y The Y coordinate of the chunk.
	 */
	protected abstract void generateData(Chunk chunk, int x, int y);
	
	/**
	 * Fills the upper part of a chunk with Air.
	 * @param chunk The filled chunk.
	 */
	protected void fillAir(Chunk chunk) {
		for(int x = 0; x < Chunk.SIZE; x++)
			for(int y = 0; y < Chunk.SIZE; y++)
				chunk.setblock(AIR.AIR, x, y);
	}
	
	/**
	 * @param value The value to floor.
	 * @return Will floor the given value according to <code>Math.floor</code>, but will return an int.
	 */
	protected static int cfloor(float x) {
		return (int) Math.floor(x);
	}
	
	/**
	 * @param world The world for which the generator will be made.
	 * @param id The ID of the generator type.
	 * @return A world generator made for the given world, that is of the type specified by the given ID.
	 */
	public static WorldGenerator getWorldGenerator(World world, byte id) {
		WorldGeneratorType type = null;
		for(WorldGeneratorType t : WorldGeneratorType.values())
			if(t.getID() == id)
				type = t;
		if(type == null)
			throw new IllegalArgumentException("The given ID ("+id+")doesn't correspond to an existing WorldGeneratorType!");
		
		switch(type) {
		case DEFAULT:
			return new DefaultWorldGenerator(world);
		case TEST:
			return new TestWorldGenerator(world);
		default:
			throw new IllegalArgumentException("The given ID ("+id+")doesn't correspond to an existing WorldGeneratorType!");
		}
	}
	
}

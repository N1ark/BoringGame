package com.oscar.boringgame.map.generator;

public enum WorldGeneratorType {
	
	TEST(0),
	DEFAULT(1);
	
	private final byte id;
	
	WorldGeneratorType(int id){
		this.id = (byte) id;
	}
	
	/**
	 * @return This generator's id.
	 */
	public byte getID() {
		return id;
	}
	
}

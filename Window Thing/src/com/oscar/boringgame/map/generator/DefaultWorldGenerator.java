package com.oscar.boringgame.map.generator;

import static com.oscar.boringgame.util.BoringMath.isBetween;
import static java.lang.Math.abs;
import static java.lang.Math.floor;
import static java.lang.Math.signum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.oscar.boringgame.blocks.CosmeticBlock;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlock;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.entity.Sheep;
import com.oscar.boringgame.map.Biome;
import com.oscar.boringgame.map.Chunk;
import com.oscar.boringgame.map.World;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.OpenSimplexNoise;
import com.oscar.boringgame.util.PVector;
import com.oscar.boringgame.util.PoissonDisk;

public class DefaultWorldGenerator extends WorldGenerator {
	
	/** The scale used for the river noise. */
	private static final float riverSize = 48;
	
	/** The scale used for the temperature and humidity variation. */
	private static final float tempSize = 150;
	
	/** The minimum distance between two biome centers. */
	private static final int biomeSize = 10;

	/** A random seed generated based on the classic seed, used for randomness depending on the X axis on the positive side. */
	private final long xpSeed;
	
	/** A random seed generated based on the classic seed, used for randomness depending on the Y axis on the positive side. */
	private final long ypSeed;
	
	/** A random seed generated based on the classic seed, used for randomness depending on the X axis on the negative side. */
	private final long xnSeed;
	
	/** A random seed generated based on the classic seed, used for randomness depending on the Y axis on the negative side. */
	private final long ynSeed;
	
	/** Noise for the rivers. */
	private final OpenSimplexNoise rivers;
	
	/** Noise with the temperatures of the world. */
	private final OpenSimplexNoise temperature;
	
	/** Noise with the humidity of the world. */
	private final OpenSimplexNoise humidity;
	
	/** Noise used for flower and ore placement. */
	private final OpenSimplexNoise flowers;
	
	public DefaultWorldGenerator(World world) {
		super(world);
		
		Random random = new Random(seed);
		rivers = new OpenSimplexNoise(random.nextLong());
		temperature = new OpenSimplexNoise(random.nextLong());
		humidity = new OpenSimplexNoise(random.nextLong());
		flowers = new OpenSimplexNoise(random.nextLong());
		xpSeed = random.nextLong();
		ypSeed = random.nextLong();
		xnSeed = random.nextLong();
		ynSeed = random.nextLong();
	}
	
	@Override
	public WorldGeneratorType getType() {
		return WorldGeneratorType.DEFAULT;
	}
	
	@Override
	public void generateData(Chunk chunk, int chunkX, int chunkY) {		
		long chunkSeed = getChunkRandomSeed(chunkX, chunkY);
		Random rand = new Random(chunkSeed);
		
		fillAir(chunk);
		
		// Get all biome centers (including surrounding chunks)
		int gridSize = PoissonDisk.gridSize(biomeSize, Chunk.SIZE);
		float unitSize = PoissonDisk.gridUnitSize(biomeSize);
		PVector[][] biomeCenters = new PVector[gridSize*3][gridSize*3];
		HashMap<PVector, List<Float>> temperaturesPerBiome = new HashMap<>();
		HashMap<PVector, List<Float>> humidityPerBiome = new HashMap<>();
		HashMap<PVector, Biome> biomePerCenter = new HashMap<>();
		PVector[][] closestBiomePoint = new PVector[Chunk.SIZE][Chunk.SIZE];
		boolean[] usedBiomes = new boolean[Biome.values().length];
		
		for(int x = -1; x <= 1; x++) {
			for(int y = -1; y <= 1; y++) {
				PVector[][] points = PoissonDisk.fastPoissonDiskSampling(getChunkRandomSeed(chunkX+x, chunkY+y), biomeSize, Chunk.SIZE);
				for(int i = 0; i < gridSize; i++)
					for(int j = 0; j < gridSize; j++) {
						PVector point = points[i][j];
						
						
						biomeCenters[i + gridSize * (x+1)][j + gridSize * (y+1)] = point;
						
						if(point == null)
							continue;
						
						point.x += Chunk.SIZE * x;
						point.y += Chunk.SIZE * y;
						temperaturesPerBiome.put(point, new ArrayList<>());
						humidityPerBiome.put(point, new ArrayList<>());
					}
			}
		}
		
		// Map each coordinate to a biome center, and add all temperatures
		for (int x = -Chunk.SIZE; x < Chunk.SIZE * 2; x++) {
			int globX = Chunk.SIZE * chunkX + x;
			for (int y = -Chunk.SIZE; y < Chunk.SIZE * 2; y++) {
				int globY = Chunk.SIZE * chunkY + y;
				
				PVector closest = null;
				float closestDist = Float.MAX_VALUE;
				
				int arrayX = (int) floor(x/unitSize) + gridSize;
				int arrayY = (int) floor(y/unitSize) + gridSize;
				
				for(int ix = -1; ix <= 1; ix++)
					for(int iy = -1; iy <= 1; iy++) {
						int nArrayX = arrayX + ix;
						int nArrayY = arrayY + iy;
											
						if(nArrayX < 0 || nArrayY < 0 || gridSize * 3 <= nArrayX || gridSize * 3 <= nArrayY)
							continue;
						
						PVector point = biomeCenters[nArrayX][nArrayY];
						if(point == null)
							continue;
						
						float distance = point.dist(x, y);
						if(distance < closestDist) {
							closest = point;
							closestDist = distance;
						}
					}
				
				if(closest == null) { // recheck with larger distance
					for(int ix = -2; ix <= 2; ix++)
						for(int iy = -2; iy <= 2; iy++) {
							int nArrayX = arrayX + ix;
							int nArrayY = arrayY + iy;
											
							if(nArrayX < 0 || gridSize * 3 <= nArrayX || nArrayY < 0 || gridSize * 3 <= nArrayY)
								continue;
							
							PVector point = biomeCenters[nArrayX][nArrayY];
							if(point == null)
								continue;
							
							float distance = point.dist(x, y);
							if(distance < closestDist) {
								closest = point;
								closestDist = distance;
							}
						}
					biomeCenters[arrayX][arrayY] = closest; // will set the closest found point to this grid position, to avoid furhther problems
				}

				if(isBetween(x, 0, Chunk.SIZE) && isBetween(y, 0, Chunk.SIZE))
					closestBiomePoint[x][y] = closest;
				
				float heat = (float) temperature.eval(globX / tempSize, globY / tempSize);
				temperaturesPerBiome.get(closest).add(heat);
				
				float humi = (float) humidity.eval(globX / tempSize, globY / tempSize);
				humidityPerBiome.get(closest).add(humi);
			}
		}
		
		// Make temperature and humidity average
		for(PVector p : temperaturesPerBiome.keySet()) {
			List<Float> temperatures = temperaturesPerBiome.get(p);
			List<Float> humidities = humidityPerBiome.get(p);
			float temperature = 0;
			float humidity = 0;
			int count = temperatures.size();
			
			for(int i = 0; i < temperatures.size(); i++) {
				temperature += temperatures.get(i);
				humidity += humidities.get(i);
			}
			
			temperature /= count;
			humidity /= count;
			
			Biome biome = Biome.biomeFor(Biome.convertTemperature(temperature), Biome.convertHumidity(humidity));
			usedBiomes[biome.ordinal()] = true;
			biomePerCenter.put(p, biome);
			
			temperatures.clear();
			temperatures.add(temperature);
			
			humidities.clear();
			humidities.add(humidity);
		}
		
		// Do ground blocks
		for (int x = 0; x < Chunk.SIZE; x++) {
			int globX = Chunk.SIZE * chunkX + x;
			for (int y = 0; y < Chunk.SIZE; y++) {
				int globY = Chunk.SIZE * chunkY + y;
				
				float waterValue = (float) rivers.eval(globX / riverSize, globY / riverSize);
				float temperature = temperaturesPerBiome.get(closestBiomePoint[x][y]).get(0);
				Biome biome = biomePerCenter.get(closestBiomePoint[x][y]);
				SolidBlock groundBlock = biome.getGround();
				
				if(temperature > .4) {
					temperature = (temperature - 0.4f)*3;
					temperature *= temperature;
					waterValue -= temperature;
				}
				
				switch(biome) {
				case PLAIN: case FLOWERY_FOREST: case FOREST: case JUNGLE: case SWAMP: case TAIGA: case TOUNDRA:
					if (waterValue > 0.44)
						groundBlock = SolidBlock.DEEP_WATER;
					else if (waterValue > 0.3)
						groundBlock = SolidBlock.WATER;
					break;
					
				case MOUNTAINS:
					if (waterValue > 0.44)
						groundBlock = SolidBlock.DEEP_WATER;
					else if (waterValue > 0.35)
						groundBlock = SolidBlock.WATER;
					break;

				case ICE_PLAINS: case ETERNAL_SNOW:
					if (waterValue > 0.3)
						groundBlock = SolidBlock.ICE;
					break;

				case SAVANNA:
					if (waterValue > 0.48)
						groundBlock = SolidBlock.DEEP_WATER;
					else if (waterValue > 0.37)
						groundBlock = SolidBlock.WATER;
					break;

				case DEAD_LANDS:
					if (waterValue > 0.37 && rand.nextInt(10) < 8)
						groundBlock = SolidBlock.DIRT;
					break;

				case DESERT:
					if (waterValue > 0.38 && rand.nextInt(10) < 4)
						groundBlock = SolidBlock.DIRT;
					break;
				
				}
				
				chunk.setblock(groundBlock, x, y);
				chunk.setBiome(biome, x, y);
			}
		}
		
		// Tree planting
		for(Biome biome : Biome.values()) {
			if(!usedBiomes[biome.ordinal()])
				continue;
			
			for(int i = 0; i < biome.getPoissonSurfaceBlocks().length; i++) {
				SurfaceBlock block = biome.getPoissonSurfaceBlocks()[i];
				int distance = biome.getPoissonSurfaceBlocksMinDistance()[i];
				List<PVector> positions = PoissonDisk.fastPoissonDiskSamplingList(rand.nextLong(), distance, Chunk.SIZE);
				
				for(PVector p : positions) {
					int x = cfloor(p.x);
					int y = cfloor(p.y);
					
					if(chunk.getBiome(x, y) != biome)
						continue;

					
					if(!isPlacable(chunk, block, x, y))
						continue;
					
					if(block instanceof DataHolderBlock) {
						int globX = Chunk.SIZE * chunkX + x;
						int globY = Chunk.SIZE * chunkY + y;
						block = ((DataHolderBlock) block).newInstance(globX, globY, world);
					}
					
					chunk.setblock(block, x, y);
				}
				
			}
		}
		
		// Cosmetic blocks
		for (int x = 0; x < Chunk.SIZE; x++) {
			int globX = Chunk.SIZE * chunkX + x;
			for (int y = 0; y < Chunk.SIZE; y++) {
				int globY = Chunk.SIZE * chunkY + y;
				
				Biome biome = chunk.getBiome(x, y);
				SolidBlock ground = chunk.getBlock(x, y);
				float value = (float) flowers.eval(globX, globY);
				float[] odds = biome.getRandomSurfaceBlocksOdds();
				
				for(int i = 0; i < biome.getRandomSurfaceBlocks().length; i++) {
					SurfaceBlock[] blocks = biome.getRandomSurfaceBlocks()[i];
					float odd = odds[i];
					
					if(signum(odd) == signum(value) && abs(odd) < abs(value)) {
						SurfaceBlock block = blocks[blocks.length == 1 ? 0 : rand.nextInt(blocks.length)];
						if(block instanceof DataHolderBlock)
							block = ((DataHolderBlock) block).newInstance(globX, globY, world);
						
						if(isPlacable(chunk, block, x, y))
							chunk.setblock(block, x, y);
						
						break;
					}
				}
				
				if(biome == Biome.MOUNTAINS) {
					if(ground != SolidBlock.STONE)
						break;
					
					SolidBlock ore;
					if(value > 0.84)
						ore = SolidBlock.IRON_ORE;
					else if(value < -0.82)
						ore = SolidBlock.COAL_ORE;
					else
						continue;
					
					for(int bx = x-1; bx <= x+1; bx++) {
						for(int by = y-1; by <= y+1; by++) {
							if(
									bx < 0 ||
									by < 0 ||
									bx >= Chunk.SIZE ||
									by >= Chunk.SIZE ||
									chunk.getBlock(bx, by) != SolidBlock.STONE
							)
								continue;
							
							if(rand.nextInt(3) != 0)
								chunk.setblock(ore, bx, by);
						}
					}
				}
				
			}
		}
		
		for (int x = 0; x < Chunk.SIZE; x++) {
			for (int y = 0; y < Chunk.SIZE; y++) {
				SurfaceBlock surface = chunk.getUpperBlock(x, y);
				if(surface instanceof DataHolderBlockInstance) {
					int globX = Chunk.SIZE * chunkX + x;
					int globY = Chunk.SIZE * chunkY + y;
					world.blockUpdate(globX, globY);
				}
			}
		}
		
		// Animals
		List<PVector> animalGroups = PoissonDisk.fastPoissonDiskSamplingList(rand.nextLong(), 26, Chunk.SIZE);
		for(PVector p : animalGroups) {
			float value = rand.nextFloat();
			if(value < 0.3f && chunk.getBiome((int) floor(p.x), (int) floor(p.y)) == Biome.PLAIN) {
				int count = 2 + rand.nextInt(4); // [2;5]
				for(int i = 0; i < count; i++) {
					float x = p.x + BoringMath.nextFloat(-3, 3, rand);
					float y = p.y + BoringMath.nextFloat(-3, 3, rand);
					
					if(
							x < 0 ||
							y < 0 ||
							x >= Chunk.SIZE ||
							y >= Chunk.SIZE ||
							!chunk.getBlock((int) floor(x), (int) floor(y)).isWalkable()
					)
						continue;
					
					float globX = Chunk.SIZE * chunkX + x;
					float globY = Chunk.SIZE * chunkY + y;

					
					new Sheep(new Location(globX, globY, world));
				}
			}
		}
	}
	
	
	/**
	 * @param chunkX The X position of the chunk.
	 * @param chunkY The Y position of the chunk.
	 * @return A seed based on the chunk's position and the world's seed.
	 */
	private long getChunkRandomSeed(int chunkX, int chunkY) {
		Random xRand = new Random(chunkX < 0 ? xnSeed : xpSeed);
		for(int x = 0; x < Math.abs(chunkX); x++)
			xRand.nextBoolean();
		Random yRand = new Random(chunkY < 0 ? ynSeed : ypSeed);
		for(int y = 0; y < Math.abs(chunkY); y++)
			yRand.nextBoolean();
		
		int val1 = xRand.nextInt();
		int val2 = yRand.nextInt();
		long chunkSeed = (long) val1 << 32 | val2 & 0xFFFFFFFFL;

		return chunkSeed;
	}
	
	/**
	 * Will check the given chunk's coordinates, and see if a CosmeticBlock can be placed there, which means that
	 * there is either an Air or an other CosmeticBlock there, and there is no tree on it.
	 * @param c The Chunk checked.
	 * @param b The surface block that should be placed.
	 * @param x The X coordinate in the chunk.
	 * @param y The Y coordinate in the chunk.
	 * @return If the given location can have cosmetics placed on top.
	 */
	private static boolean isPlacable(Chunk c, SurfaceBlock b, int x, int y) {
		
		SurfaceBlock surface;
		for(int bx = x-2; bx <= x; bx++)
			for(int by = y-1; by <= y; by++)
				if(
					bx >= 0 &&
					by >= 0 &&
					(surface = c.getUpperBlock(bx, by)) instanceof SurfaceHitboxBlock &&
					((SurfaceHitboxBlock) surface).getDimension().isInside(bx, by, x, y)
				)
					return false;
		
		if(!(b instanceof SurfaceHitboxBlock))
			return b.canPlaceOn(c.getBlock(x, y));
		
		SurfaceHitboxBlock hitbox = (SurfaceHitboxBlock) b;
		for(int bx = x ; bx < x+hitbox.getDimension().getWidth(); bx++)
			for(int by = y; by < y+hitbox.getDimension().getHeight(); by++)
				if(
						bx >= Chunk.SIZE ||
						by >= Chunk.SIZE ||
						!b.canPlaceOn(c.getBlock(bx, by)) ||
						!(c.getUpperBlock(bx, by) == AIR.AIR ||
						c.getUpperBlock(bx, by) instanceof CosmeticBlock)
				)
					return false;
		return true;
	}
	
}

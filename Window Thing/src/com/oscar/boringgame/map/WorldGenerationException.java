package com.oscar.boringgame.map;

public class WorldGenerationException extends Exception {
	private static final long serialVersionUID = -6177094395138337264L;
	
	private final String message;
	
	public WorldGenerationException(String message) {
		super();
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
}

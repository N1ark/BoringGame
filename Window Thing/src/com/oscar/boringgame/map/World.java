package com.oscar.boringgame.map;

import static java.lang.Math.abs;
import static java.lang.Math.ceil;
import static java.lang.Math.floor;
import static java.lang.Math.floorDiv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentHashMap.KeySetView;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.zip.DataFormatException;

import com.oscar.boringgame.blocks.ComplexFurnitureBlock;
import com.oscar.boringgame.blocks.CosmeticBlock;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.AIR;
import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;
import com.oscar.boringgame.blocks.blocktypes.SurfaceHitboxBlock;
import com.oscar.boringgame.entity.DroppedItem;
import com.oscar.boringgame.entity.Entity;
import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.events.BlockBreak;
import com.oscar.boringgame.events.BlockBreak.BlockBreakEvent;
import com.oscar.boringgame.files.WorldIO;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.map.generator.WorldGenerator;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.renderer.RenderingInfo;
import com.oscar.boringgame.util.Area;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.IntLocation;
import com.oscar.boringgame.util.Location;

public class World {
	
	/** The amount of milliseconds between the start of two completed world ticks. */
	private static final int TICK_RATE = 50;
	
	/** The amount of chunks around the ticking entity that should receive their block udpdates. */
	private static final int TICK_RANGE = 3;
	
	/** The amount of milliseconds between each attempt to unload/load the chunks around the ticking entity. */
	private static final int LOAD_CHUNK_RATE = 1000;
	
	/** The distance around the ticking entity's chunk position where chunks must be loaded. Closer than that and they
	 * must be loaded. */
	private static final int MIN_LOADED_CHUNK_DIST = 3;
	
	/** The distance around the ticking entity's chunk position where chunks can be loaded. Further than that and they
	 * should automatically be unloaded and saved. */
	private static final int MAX_LOADED_CHUNK_DIST = 4;
	
	/** The world's seed. */
	public final long seed;
	/** The world's name, used for saving files. */
	private final String name;
	/** The map with all the currently loaded chunks. */
	private final ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Chunk>> chunks;
	/** The generator used to create new chunks. Can be null if no chunks should be generated. */
	private WorldGenerator generator;
	/** The debug object to which the world can give information such as ticking speed. */
	private RenderingInfo debug;
	/** The entity used to know what chunks to tick and load/unload. */
	private Entity tickingEntity;
	/** All the entities in the world. */
	private final List<Entity> entities;
	/** All the particles in the world. */
	private final List<ParticleEntity> particles;
	/** A list with the coordinates of the chunks that must be loaded. */
	private final List<IntLocation> loadingChunks;
	
	/** The amount of ticks done by the world. This is used for tasks that should periodically happen at a frequency lower than the
	 * world's ticking. */
	private int tickCount = 0;
	/** If the world is ticking. If true, <code>ticker.isAlive() && chunkLoader.isAlive()</code> should also true. If false,
	 * then they shouldn't be running but there is no guarantee these threads aren't actually null. */
	private boolean isTicking;
	/** The thread used for ticking blocks and generating chunks. */
	private Thread ticker;
	/** The thread used for loading/unloading chunks. */
	private Thread chunkLoader;
	/** The Area in chunk coordinates with all the currently visible */
	private Area currentlyViewedChunks;
	
	/**
	 * Will create a world with the given name, and a randomly generated seed.
	 * @param name The world's name.
	 */
	public World(String name) {
		this(name, new Random().nextLong());
	}
	
	/**
	 * Will create a world with the given name and the given seed.
	 * @param name The world's name.
	 * @param seed The seed, used by the WorldGenerator.
	 */
	public World(String name, long seed) {
		this.name = name;
		this.seed = seed;
		this.chunks = new ConcurrentHashMap<>();
		this.entities = new CopyOnWriteArrayList<>();
		this.particles = new CopyOnWriteArrayList<>();
		this.loadingChunks = new CopyOnWriteArrayList<>();
	}

	private class Ticking extends Thread {
		
		/** An array to store all the needed updates beforehand, to avoid setting and updating blocks during the same tick. */
		private final boolean[][][][] updateKeeper = new boolean[TICK_RANGE*2+1][TICK_RANGE*2+1][Chunk.SIZE][Chunk.SIZE];
		/** An array to know if the chunk at a relative position to the ticking entity should be updated. */
		private final boolean[][] needsUpdate = new boolean[TICK_RANGE*2+1][TICK_RANGE*2+1];
		
		@Override
		public void run() {
			long chrono;
			while(isTicking) {
				chrono = System.currentTimeMillis();
				tickCount ++;
				
				if(tickingEntity != null) {
					int chunkX = floorDiv((int) floor(tickingEntity.getLocation().x), Chunk.SIZE);
					int chunkY = floorDiv((int) floor(tickingEntity.getLocation().y), Chunk.SIZE);

					// Get all needed updates in the ticked chunks.
					for(int lX = -TICK_RANGE; lX <= TICK_RANGE; lX++) {
						for(int lY = -TICK_RANGE; lY <= TICK_RANGE; lY++) {
							Chunk chunk = getChunkOfCoordinatesIf(chunkX+lX, chunkY+lY);
							if(chunk == null || !chunk.needsABlockUpdate()) {
								needsUpdate[lX+TICK_RANGE][lY+TICK_RANGE] = false;
								continue;
							}
							
							needsUpdate[lX+TICK_RANGE][lY+TICK_RANGE] = true;
							chunk.blockUpdatesToArray(updateKeeper[lX+TICK_RANGE][lY+TICK_RANGE]);
							chunk.clearUpdates();
						}
					}
					
					// Do the actual ticking
					for(int lX = -TICK_RANGE; lX <= TICK_RANGE; lX++) {
						for(int lY = -TICK_RANGE; lY <= TICK_RANGE; lY++) {
							if(!needsUpdate[lX+TICK_RANGE][lY+TICK_RANGE])
								continue;

							int cx = chunkX + lX;
							int cy = chunkY + lY;
							
							Chunk chunk = chunks.containsKey(cx) ? chunks.get(cx).get(cy) : null;
							boolean[][] updates = updateKeeper[lX+TICK_RANGE][lY+TICK_RANGE];
							
							// Ticks all the blocks
							for(int x = 0; x < Chunk.SIZE; x++) {
								for(int y = 0; y < Chunk.SIZE; y++) {
									if(!updates[x][y])
										continue;
																		
									SolidBlock ground = chunk.getBlock(x, y);
									SurfaceBlock surface = chunk.getUpperBlock(x, y);
									
									if(surface instanceof DataHolderBlockInstance)
										((DataHolderBlockInstance) surface).tick(tickCount);
								
									else if(ground == SolidBlock.WATER || ground == SolidBlock.DEEP_WATER) {
										// Don't do any further checking if it's not a "water tick".
										if(tickCount % 13 != 0) {
											chunk.blockUpdate(true, x, y);
											continue;
										}
										
										boolean checkLater = false;
										// loop through the 4 directly neighbouring blocks
										for(int i = 1; i < 9; i+=2) {
											Chunk c = chunk;
											int bx = x+i%3-1;
											int by = y+i/3-1;
											
											// Adjust chunk, in case the block is in another chunk
											if(bx < 0 || bx >= Chunk.SIZE || by < 0 || by >= Chunk.SIZE) {
												c = getChunkOfCoordinatesIf(cx + floorDiv(bx, Chunk.SIZE), cy + floorDiv(by, Chunk.SIZE));
												if(c == null)
													continue;
												bx = convert(bx);
												by = convert(by);
											}
											
											if(c.getBlock(bx, by) == SolidBlock.BEDROCK) {
												c.setblock(SolidBlock.WATER, bx, by);
												c.blockUpdate(true, bx, by);
												checkLater = true;
											}
										}
										if(checkLater)
											chunk.blockUpdate(true, x, y);
									}
								}
							}
						}
					}
				}
				
				// Generate/Load needed chunks
				if(generator != null) {
					for(IntLocation location : loadingChunks) {
						// Generate the chunk
						if(!WorldIO.chunkIsSaved(World.this, location.x, location.y)) {
							Chunk chunk = chunks.get(location.x).get(location.y);
							generator.generateTerrain(chunk, location.x, location.y);
						}
						// Load the chunk
						else {
							try {
								Chunk chunk = WorldIO.loadChunk(World.this, location.x, location.y);
								chunks.get(location.x).put(location.y, chunk);
							} catch (IllegalStateException | IllegalArgumentException | IOException | DataFormatException e) {
								System.err.println("Exception when trying to load the chunk: ");
								e.printStackTrace();
								continue;
							}
						}
						loadingChunks.remove(location);
					}
				}
				
				chrono = System.currentTimeMillis()-chrono;
				if(debug != null)
					debug.tickSpeed = (int) chrono;
				
				try {
					Thread.sleep(TICK_RATE <= chrono ? 0 : TICK_RATE - chrono);
				} catch (InterruptedException e) {
					System.err.println("Error when making world ticking sleep:");
					e.printStackTrace();
				}
			}
		}
	}
	
	private class ChunkLoading extends Thread {
		
		@Override
		public void run() {
			long chrono;
			while(isTicking) {
				chrono = System.currentTimeMillis();
				
				if(tickingEntity != null) {
					int chunkX = floorDiv((int) floor(tickingEntity.getLocation().x), Chunk.SIZE);
					int chunkY = floorDiv((int) floor(tickingEntity.getLocation().y), Chunk.SIZE);

					// Unload everything that's too far
					for(int x : chunks.keySet()) {
						for(int y : chunks.get(x).keySet()) {								
							if(
									(abs(chunkX - x) > MAX_LOADED_CHUNK_DIST ||
											abs(chunkY - y) > MAX_LOADED_CHUNK_DIST) &&
									!currentlyViewedChunks.isInsideInclusive(x, y)
									) {
								Chunk chunk = chunks.get(x).get(y);
								try {
									WorldIO.saveChunk(World.this, x, y, chunk);

									chunks.get(x).remove(y);
									if(chunks.get(x).keySet().size() == 0)
										chunks.remove(x);
								} catch (IllegalStateException | IOException e) {
									System.err.println("Exception when trying to save and unload the chunk: ");
									e.printStackTrace();
								}
							}
						}
					}

					// Load everything that's too near and that isn't loaded.
					for(int x = chunkX - MIN_LOADED_CHUNK_DIST; x <= chunkX + MIN_LOADED_CHUNK_DIST; x++) {
						for(int y = chunkY - MIN_LOADED_CHUNK_DIST; y <= chunkY + MIN_LOADED_CHUNK_DIST; y++) {
							getChunk(x*Chunk.SIZE, y*Chunk.SIZE); // kind of hacky, but will add the chunk to the generation queue.
						}
					}
				}
				
				chrono = System.currentTimeMillis()-chrono;
				
				try {
					Thread.sleep(LOAD_CHUNK_RATE <= chrono ? 0 : LOAD_CHUNK_RATE - chrono);
				} catch (InterruptedException e) {
					System.err.println("Error when making world loading/unloading sleep:");
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * @return This world's name. Mainly used for saving/loading files, and displaying the world in GUI.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Will set the generator for this world.
	 * @param generator The WorldGenerator used for making all chunks.
	 */
	public void setGenerator(WorldGenerator generator) {
		this.generator = generator;
	}
	
	/**
	 * @return The used WorldGenerator by the World.
	 */
	public WorldGenerator getGenerator() {
		return generator;
	}
	
	/**
	 * Will set the Area that contains all the currently renderer chunks, that should not be unloaded.
	 * @param area The area containing the displayed chunks.
	 */
	public void setCurrentlySeenChunks(Area area) {
		currentlyViewedChunks = area;
	}
	
	/**
	 * Will set the entity around which block updates should be done.
	 * @param e The entity concerned.
	 */
	public void setTickingEntity(Entity e) {
		tickingEntity = e;
	}
	
	/**
	 * @return The entity around which block updates should be done.
	 */
	public Entity getTickingEntity() {
		return tickingEntity;
	}
	
	/**
	 * @return The amount of currently loaded chunks.
	 */
	public int getLoadedChunks() {
		int total = 0;
		for(int x : chunks.keySet())
			total += chunks.get(x).size();
		return total;
	}
	
	/**
	 * Programs a block update that will look at the given Location in the next tick.
	 * @param x The X coordinate of the block to update.
	 * @param y The Y coordinate of the block to update.
	 */
	public void blockUpdate(int x, int y) {
		Chunk c = getChunkIf(x, y);
		if(c != null)
			c.blockUpdate(true, convert(x), convert(y));
	}
	
	/**
	 * Programs a block update that will look at the given Location in the next tick.
	 * @param x The X coordinate of the block to update.
	 * @param y The Y coordinate of the block to update.
	 */
	public void blockUpdate(float x, float y) {
		blockUpdate((int) floor(x), (int) floor(y));
	}

	/**
	 * @return The current amount of block updates waiting to be made.
	 */
	public int amountOfBlockUpdates() {
		if(tickingEntity == null) {
			new IllegalAccessError("Tried getting the number of needed block updates but the ticking entity was null!").printStackTrace();
			return 0;
		}
		
		int total = 0;
		int chunkX = floorDiv((int) floor(tickingEntity.getLocation().x), Chunk.SIZE);
		int chunkY = floorDiv((int) floor(tickingEntity.getLocation().y), Chunk.SIZE);
		
		for(int cx = chunkX - TICK_RANGE; cx <= chunkX + TICK_RANGE; cx++) {
			for(int cy = chunkY - TICK_RANGE; cy <= chunkY + TICK_RANGE; cy++) {
				Chunk chunk = chunks.containsKey(cx) ? chunks.get(cx).get(cy) : null;
				if(chunk != null)
					total += chunk.amountOfBlockUpdates();
			}
		}
		
		return total;
	}
	
	/**
	 * If the given position, is inside a hitbox.
	 * <br>This will take into account all the surface blocks in a radius of 5 blocks.
	 * @param posX The X coordinate to check.
	 * @param posY The Y coordinate to check.
	 * @return If it is inside a hitbox.
	 */
	public boolean isFurnitureHitbox(float posX, float posY) {
		for(int x = (int) (posX-5); x < posX+5; x++)
			for(int y = (int) (posY-5); y < posY+5; y++) {
				
				SurfaceBlock b = getUpperBlockIf(x, y);
				if(b == null || !(b instanceof SurfaceHitboxBlock)) continue;
				
				if(b instanceof ComplexFurnitureBlock){
					for(Area a : ((ComplexFurnitureBlock) b).getContactHitbox().getParts())
						if(a.isInside(x, y, posX, posY))
							return true;
				} else if(((SurfaceHitboxBlock) b).getHitbox().isInside(x, y, posX, posY))
					return true;
			}
		return false;
	}
	
	/**
	 * If the given Location, considered as a point, is inside a hitbox.
	 * <br>This will take into account all the surface blocks in a radius of 5 blocks.
	 * @param l The Location to check.
	 * @return If it is inside a Hitbox.
	 */
	public boolean isFurnitureHitbox(Location l) {
		return isFurnitureHitbox(l.x, l.y);
	}
	
	/**
	 * If the given Area intersects with any hitbox.
	 * <br>This will take into account all the surface blocks in a radius of 5 blocks.
	 * @param a The area to check.
	 * @return If it is inside a Hitbox.
	 */
	public boolean isFurnitureHitbox(Area a) {
		for(int x = (int) (a.x1 - 5); x < a.x2 + 5; x++) {
			for(int y = (int) (a.y1 - 5); y < a.y2 + 5; y++) {
				SurfaceBlock block = getUpperBlockIf(x, y);
				
				if(block instanceof ComplexFurnitureBlock) {
					for(Area area : ((ComplexFurnitureBlock) block).getContactHitbox().getParts()) 
						if(area.intersects(a, x, y))
							return true;
				}
				
				else if(block instanceof SurfaceHitboxBlock) {
					if(((SurfaceHitboxBlock) block).getHitbox().intersects(a, x, y))
						return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The ground block at the given coordinates, or null if it wasn't generated.
	 */
	public SolidBlock getBlockIf(int x, int y) {
		Chunk c = getChunkIf(x, y);
		return c == null ? null : c.getBlock(convert(x), convert(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The ground block at the given coordinates, or null if it wasn't generated.
	 */
	public SolidBlock getBlockIf(float x, float y) {
		return getBlockIf((int) floor(x), (int) floor(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The ground block at the given coordinates.
	 */
	public SolidBlock getBlock(int x, int y) {
		return getChunk(x, y).getBlock(convert(x), convert(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The ground block at the given coordinates.
	 */
	public SolidBlock getBlock(float x, float y) {
		return getBlock((int) floor(x), (int) floor(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The upper block at the given coordinates, not taking into account the Blocks' display size, or null if it wasn't generated.
	 */
	public SurfaceBlock getUpperBlockIf(int x, int y) {
		Chunk c = getChunkIf(x, y);
		return c == null ? null : c.getUpperBlock(convert(x), convert(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The upper block at the given coordinates, not taking into account the Blocks' display size, or null if it wasn't generated.
	 */
	public SurfaceBlock getUpperBlockIf(float x, float y) {
		return getUpperBlockIf((int) floor(x), (int) floor(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The upper block at the given coordinates, not taking into account the Blocks' display size.
	 */
	public SurfaceBlock getUpperBlock(int x, int y) {
		return getChunk(x, y).getUpperBlock(convert(x), convert(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The upper block at the given coordinates, not taking into account the Blocks' display size.
	 */
	public SurfaceBlock getUpperBlock(float x, float y) {
		return getUpperBlock((int) floor(x), (int) floor(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The upper block at the given coordinates, <b>taking into account the Blocks' display size.</b>
	 */
	public SurfaceBlock getVisibleUpperBlock(int x, int y) {
		int chunkX = floorDiv(x, Chunk.SIZE);
		int chunkY = floorDiv(y, Chunk.SIZE);
		Chunk chunk = getChunkOfCoordinatesIf(chunkX, chunkY);
		getChunkOfCoordinates(chunkX, chunkY);
		for(int i = x-4; i < x+3; i++) {
			for(int j = y-3; j < y+3; j++) {
				Chunk c = chunk;
				if(floorDiv(i, Chunk.SIZE) != chunkX || floorDiv(j, Chunk.SIZE) != chunkY)
					c = getChunkIf(i, j);
				if(c == null)
					continue;

				SurfaceBlock b = c.getUpperBlock(convert(i), convert(j));
				if(b == AIR.AIR) continue;
					
				if(b instanceof SurfaceHitboxBlock && ((SurfaceHitboxBlock) b).getDimension().isInside(i, j, x, y))
					return b;
					
				else if(b instanceof CosmeticBlock && BoringMath.isBetween(x, i, i+1) && BoringMath.isBetween(y, j, j+1))
					return b;
			}
		}
		
		return getChunk(x, y).getUpperBlock(convert(x), convert(y));
	}
	
	/**
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The upper block at the given coordinates, <b>taking into account the Blocks' display size.</b>
	 */	
	public SurfaceBlock getVisibleUpperBlock(float x, float y) {
		return getVisibleUpperBlock((int) floor(x), (int) floor(y));
	}
	
	public void incrementBreakState(int x, int y) {
		getChunk(x, y).addBreakState(1, convert(x), convert(y));
	}
	
	/**
	 * 
	 * Will set the breaking state of the block at the given location to the given value.
	 * @param value The breaking value of the block.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @return If the block broke.
	 */
	public boolean setBreakState(int value, int x, int y) {
		Chunk c = getChunk(x, y);
		int cx = convert(x);
		int cy = convert(y);
		
		if(value < 5) {
			c.setBreakState(value, cx, cy);
			return false;
		}
		
		c.setBreakState(0, cx, cy);
		Block broken = c.getBlock(cx, cy);
		if(c.getUpperBlock(cx, cy) != AIR.AIR) {
			broken = c.getUpperBlock(cx, cy);
			c.setblock(AIR.AIR, cx, cy);
		}
		else {
			main: for(int i = x-3; i <= x+3; i++) {
				for(int j = y-3; j <= y+3; j++) {
					Chunk used = c;
					if(floorDiv(i, Chunk.SIZE) != floorDiv(x, Chunk.SIZE) || floorDiv(j, Chunk.SIZE) != floorDiv(y, Chunk.SIZE))
						used = getChunk(i, j);
					if(used.getUpperBlock(convert(i), convert(j)) != AIR.AIR) {
						SurfaceBlock b = used.getUpperBlock(convert(i), convert(j));
						if(
								b instanceof SurfaceHitboxBlock && 
								BoringMath.isBetween(x, i, i+((SurfaceHitboxBlock) b).getDimension().x2) && 
								BoringMath.isBetween(y, j, j+((SurfaceHitboxBlock) b).getDimension().y2)
						) {
							broken = b;
							c.setblock(AIR.AIR, convert(i), convert(j));
							break main;
						}
					}
				}
			}
		}

		if(broken == c.getBlock(cx, cy))
			c.setblock(SolidBlock.BEDROCK, cx, cy);
		
		BlockBreak.onBlockBreak(new BlockBreakEvent(broken, new Location(x, y, this), null));
		blockUpdate(x,   y  );
		blockUpdate(x+1, y  );
		blockUpdate(x-1, y  );
		blockUpdate(x,   y+1);
		blockUpdate(x,   y-1);
		
		return true;
	}
	
	/**
	 * Will reset the break state of the block at the specified location.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 */
	public void resetBreakState(int x, int y) {
		getChunk(x, y).setBreakState(0, convert(x), convert(y));
	}
	
	/**
	 * Will reset the break state of the block at the specified location.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 */
	public void resetBreakState(float x, float y) {
		resetBreakState((int) floor(x), (int) floor(y));
	}
	
	/**
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @return Will return the break state of the block at the specified location, usually ranging from 0 to 4 (if >4, it should be broken).
	 */
	public int getBreakState(int x, int y) {
		return getChunk(x, y).getBreakState(convert(x), convert(y));
	}
	
	/**
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @return Will return the break state of the block at the specified location, usually ranging from 0 to 4 (if >4, it should be broken).
	 */
	public int getBreakState(float x, float y) {
		return getBreakState((int) floor(x), (int) floor(y));
	}
	
	/**
	 * Will set the given Block at the given coordinates (the method decides if it should put it on the lower or higher layer). 
	 * <br>This will also trigger block updates around the block and on it.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @param b The block that will be placed.
	 */
	public void setblock(int x, int y, Block b) {
		getChunk(x, y).setblock(b, convert(x), convert(y));
		
		blockUpdate(x,   y  );
		blockUpdate(x+1, y  );
		blockUpdate(x-1, y  );
		blockUpdate(x,   y+1);
		blockUpdate(x,   y-1);
	}

	/**
	 * Will set the given Block at the given coordinates (the method decides if it should put it on the lower or higher layer). 
	 * <br>This will also trigger block updates around the block and on it.
	 * @param x The x coordinate of the block.
	 * @param y The y coordinate of the block.
	 * @param b The block that will be placed.
	 */
	public void setblock(float x, float y, Block b) {
		setblock((int) floor(x), (int) floor(y), b);
	}
	
	/**
	 * Will set the given Block at the given Location (the method decides if it should put it on the lower or higher layer). 
	 * <br>This will also trigger block updates around the block and on it.
	 * @param l The location at which the block will be placed.
	 * @param b The block that will be placed.
	 */
	public void setblock(Location l, Block b) {
		setblock((int) floor(l.x), (int) floor(l.y), b);
	}
	
	/**
	 * @param x The x coordinate that's checked.
	 * @param y The y coordinate that's checked.
	 * @return Will return the biome at the specified coordinates.
	 */
	public Biome getBiome(int x, int y) {
		return getChunk(x, y).getBiome(convert(x), convert(y));
	}
	
	/**
	 * @param x The x coordinate that's checked.
	 * @param y The y coordinate that's checked.
	 * @return Will return the biome at the specified coordinates.
	 */
	public Biome getBiome(float x, float y) {
		return getBiome((int) floor(x), (int) floor(y));
	}
	
	/**
	 * @param l The location that's checked.
	 * @return Will return the biome at the specified location.
	 */
	public Biome getBiome(Location l) {
		return getBiome((int) floor(l.x), (int) floor(l.y));
	}

	/**
	 * Will drop an Item at the given Location.
	 * @param item The dropped Item.
	 * @param l The location at which it will be dropped.
	 * @return The entity corresponding to the dropped item.
	 */
	public DroppedItem dropItem(ItemStack item, Location l) {
		DroppedItem d = new DroppedItem(item, l);
		return d;
	}
	
	/**
	 * Will create a List with all the DroppedItems around the Player in a square area of 6 blocks.
	 * @param p The Player that will be used.
	 * @return The List with all the nearby DroppedItems.
	 */
	public List<DroppedItem> visibleItems(Player p){
		List<DroppedItem> x = new ArrayList<>();
		Area check = new Area(-3, -3, 3, 3).translate(p.getLocation());
		for(Entity e : entities)
			if(e instanceof DroppedItem && check.isInside(e.getLocation()))
				x.add((DroppedItem) e);
		return x;	
	}
	
	/**
	 * @return The list containing all the Entities in the world.
	 */
	public List<Entity> getEntities(){
		return entities;
	}
	
	/**
	 * Will create a List with all the Entities that are contained in the given Area.
	 * @param a The area checked.
	 * @return The List with all the contained Entities.
	 */
	public List<Entity> visibleEntities(Area a){
		List<Entity> x = new ArrayList<Entity>();
		for(Entity e : entities)
			if(a.isInside(e.getLocation()))
				x.add(e);
		return x;
	}
	
	/**
	 * @return The list containing all the ParticleEntities in the world.
	 */
	public List<ParticleEntity> getParticles(){
		return particles;
	}
	
	/**
	 * Will create a List with all the ParticleEntities that are contained in given Area.
	 * @param a The area checked.
	 * @return The list with all the contained ParticleEntities.
	 */
	public List<ParticleEntity> visibleParticles(Area a){
		List<ParticleEntity> x = new  ArrayList<>();
		for(ParticleEntity e : particles)
			if(a.isInside(e.getLocation()))
				x.add(e);
		return x;
	}
	
	/**
	 * Will add an Entity to the World.
	 * @param e The added Entity.
	 */
	public void add(Entity e) {
		entities.add(e);
	}
	
	/**
	 * Will remove an Entity from the World.
	 * @param e The removed Entity.
	 */
	public void remove(Entity e) {
		entities.remove(e);
	}
	
	/**
	 * Will add a ParticleEntity to the World.
	 * @param e The added ParticleEntity.
	 */
	public void add(ParticleEntity e) {
		particles.add(e);
	}
	
	/**
	 * Will remove a ParticleEntity from the World.
	 * @param e The removed ParticleEntity.
	 */
	public void remove(ParticleEntity e) {
		particles.remove(e);
	}
	
	/**
	 * @return If the World's ticking is enabled.
	 */
	public boolean isTicking() {
		return isTicking;
	}
	
	/**
	 * If the World isn't already ticking, will start its ticking.
	 */
	public void startTicking() {
		if(!isTicking) {
			isTicking = true;
			ticker = new Ticking();
			chunkLoader = new ChunkLoading();

			ticker.setName("Ticking thread");
			chunkLoader.setName("Chunk thread");

			ticker.start();
			chunkLoader.start();
		}
	}
	
	/**
	 * If the World is already ticking, will stop its ticking.
	 */
	public void stopTicking() {
		isTicking = false;
	}
	
	/**
	 * Will set the RenderingInfo object used for this world, that will specify the tick speed of the world.
	 * @param debug The RenderingInfo object.
	 */
	public void setTickingInfo(RenderingInfo debug) {
		this.debug = debug;
	}
	
	/**
	 * Will look through all the blocks surrounding and inside this Area, to see if the given Area can be walked by an Entity.
	 * @param hitbox The area to check.
	 * @return If an Entity could walk inside the given Area.
	 */
	public boolean isWalkable(Area hitbox) {
		for(int x = (int) floor(hitbox.x1); x < ceil(hitbox.x2); x++)
			for(int y = (int) floor(hitbox.y1); y < ceil(hitbox.y2); y++) {
				SolidBlock b;
				if((b = getBlockIf(x, y)) == null || !b.isWalkable())
					return false;
			}
		return !isFurnitureHitbox(hitbox);
	}
	
	/**
	 * @param x The X coordinate of the chunk in <strong>world coordinates</strong>.
	 * @param y The Y coordinate of the chunk in <strong>world coordinates</strong>.
	 * @return If the chunk at the given <strong>world coordinates</strong> exists.
	 */
	public boolean chunkExists(int x, int y) {
		return 
				chunks.containsKey(floorDiv(x, Chunk.SIZE)) && 
				chunks.get(floorDiv(x, Chunk.SIZE)).containsKey(floorDiv(y, Chunk.SIZE));
	}
	
	/**
	 * @param x The X coordinate of the chunk in <strong>world coordinates</strong>.
	 * @param y The Y coordinate of the chunk in <strong>world coordinates</strong>.
	 * @return If the chunk at the given <strong>world coordinates</strong> exists.
	 */
	public boolean chunkExists(float x, float y) {
		return chunkExists((int) floor(x), (int) floor(y));
	}
	
	/**
	 * Checks if the given coordinates are in the world, and if not, will generate a chunk at the given coordinates.
	 * @param x The checked x coordinate.
	 * @param y The checked y coordinate.
	 * @return The chunk at the given world coordinates.
	 */
	public Chunk getChunk(int x, int y) {
		int chunkX = floorDiv(x, Chunk.SIZE);
		int chunkY = floorDiv(y, Chunk.SIZE);
		
		if(!chunks.containsKey(chunkX))
			chunks.put(chunkX, new ConcurrentHashMap<>());
		
		if(!chunks.get(chunkX).containsKey(chunkY)) {
			Chunk chunk = new Chunk();
			chunks.get(chunkX).put(chunkY, chunk);
	
			IntLocation intLoc = new IntLocation(chunkX, chunkY);
			if(!loadingChunks.contains(intLoc))
				loadingChunks.add(intLoc);
						
			return chunk;
		}
		
		return chunks.get(chunkX).get(chunkY);
	}
	
	/**
	 * Checks if the given <strong>chunk</strong> coordinates are in the world, and if not, will generate a chunk at the given coordinates.
	 * <br><strong>Please do not mix this method with getChunk(int, int). This method will consider that the given coordinates are
	 * the coordinates of the chunk, which are NOT equal to block coordinates.</strong>
	 * @param chunkX The checked x coordinate.
	 * @param chunkY The checked y coordinate.
	 * @return The chunk at the given <strong>chunk</strong> coordinates.
	 */
	public Chunk getChunkOfCoordinates(int chunkX, int chunkY) {
		if(!chunks.containsKey(chunkX))
			chunks.put(chunkX, new ConcurrentHashMap<>());
		
		if(!chunks.get(chunkX).containsKey(chunkY)) {
			Chunk chunk = new Chunk();
			chunks.get(chunkX).put(chunkY, chunk);
	
			IntLocation intLoc = new IntLocation(chunkX, chunkY);
			if(!loadingChunks.contains(intLoc))
				loadingChunks.add(intLoc);
			
			return chunk;
		}
		
		return chunks.get(chunkX).get(chunkY);
	}
	
	/**
	 * Checks if the given <strong>chunk</strong> coordinates are in the world, and if not, will return null.
	 * <br><strong>Please do not mix this method with getChunk(int, int). This method will consider that the given coordinates are
	 * the coordinates of the chunk, which are NOT equal to block coordinates.</strong>
	 * @param chunkX The checked x coordinate.
	 * @param chunkY The checked y coordinate.
	 * @return The chunk at the given <strong>chunk</strong> coordinates, or null if it wasn't generated.
	 */
	public Chunk getChunkOfCoordinatesIf(int chunkX, int chunkY) {
		return chunks.containsKey(chunkX) ? chunks.get(chunkX).get(chunkY) : null;
	}
	
	/**
	 * Checks if the given coordinates are in the world, and if not, will return null instead of generating a chunk.
	 * @param x The checked x coordinate.
	 * @param y The checked y coordinate.
	 * @return The chunk at the given world coordinates, or null if it wasn't generated.
	 */
	private Chunk getChunkIf(int x, int y) {
		int chunkX = floorDiv(x, Chunk.SIZE);
		int chunkY = floorDiv(y, Chunk.SIZE);
		
		if(!chunks.containsKey(chunkX))
			return null;
		
		return chunks.get(chunkX).get(chunkY);
	}
	
	/**
	 * @param value The world coordinate value.
	 * @return Will convert the value in a world coordinate system to a chunk coordinate system.
	 */
	private static int convert(int value) {
		int mod = value % Chunk.SIZE;
		return 
				value < 0 && mod != 0 ? 
						mod + Chunk.SIZE : 
						mod;
	}

	/**
	 * @return The keys to access all existing chunks on the X axis.
	 */
	public KeySetView<Integer, ConcurrentHashMap<Integer, Chunk>> getChunkXKeys() {
		return chunks.keySet();
	}
	
	/**
	 * @return The keys to access all existing chunks on the Y axis on the specified X axis.
	 */
	public KeySetView<Integer, Chunk> getChunkYKeys(int x) {
		if(!chunks.containsKey(x))
			throw new IllegalArgumentException("The given X coordinate ("+x+") isn't a valid key for the X coordinate!");
		return chunks.get(x).keySet();
	}

	/**
	 * Will set the chunk at the specified chunk coordinates.
	 * @param chunk The chunk to place in the world.
	 * @param x The X chunk coordinate of the chunk's location.
	 * @param y The Y chunk coordinate of the chunk's location.
	 */
	public void setChunk(Chunk chunk, int x, int y) {
		if(!chunks.containsKey(x))
			chunks.put(x, new ConcurrentHashMap<>());
		chunks.get(x).put(y, chunk);
	}

	/**
	 * @param location The location to check.
	 * @return If the given location is in a currently loaded chunk.
	 */
	public boolean isInLoadedChunk(Location location) {
		int x = floorDiv((int) floor(location.x), Chunk.SIZE);
		if(!chunks.containsKey(x))
			return false;
		int y = floorDiv((int) floor(location.y), Chunk.SIZE);
		if(!chunks.get(x).containsKey(y))
			return false;
		
		for(IntLocation loc : loadingChunks)
			if(loc.x == x && loc.y == y)
				return false;
		return true;
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.println("Unloaded world \"" + name + "\".");
		super.finalize();
	}

	/**
	 * Will generate an image centered on the Player, that shows the entire world.
	 * @param radius The amount of blocks displayed on a side.
	 * @param size The resolution in pixels of the image.
	 * @return The generated image. This may take some time, so this method is better used on a secondary Thread.
	 */
//	public BufferedImage exportToPng(int radius, int size) {
//		if(radius > SIZE*2)
//			radius = SIZE*2;
//		double scale = (double)size/radius;
//		int px = (int) Main.PLAYER.getLocation().x;
//		int py = (int) Main.PLAYER.getLocation().y;
//		HashMap<Block, Image> scaledImages = new HashMap<>();
//
//		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
//		JPanel observer = new JPanel();
//		ArrayList<ImageDisplay> layer1 = new ArrayList<>();
//		ArrayList<ImageDisplay> layer2 = new ArrayList<>();
//
//		for (int y = py-radius/2; y < py+radius/2; y++){
//			for (int x = px-radius/2; x < px+radius/2; x++){
//				try {
//					Block b = ground.get(x).get(y);
//					int xd = (int) (size/2+scale*(x-px));
//					int yd = (int) (size/2+scale*(y-py));
//					if(!scaledImages.containsKey(b))
//						scaledImages.put(b, b.getTexture().getScaledInstance((int) scale, (int) scale, Image.SCALE_FAST));
//					layer1.add(new ImageDisplay(scaledImages.get(b), xd, yd));
//					if(surface.get(x).get(y) != AIR.AIR) {
//						b = surface.get(x).get(y);
//						if(!scaledImages.containsKey(b)) {
//							if(b instanceof SurfaceHitboxBlock)
//								scaledImages.put(b, b.getTexture().getScaledInstance(
//										(int) (scale * ((SurfaceHitboxBlock) b).getDisplay().getWidth()), 
//										(int) (scale * ((SurfaceHitboxBlock) b).getDisplay().getHeight()), 
//										Image.SCALE_FAST));
//							else
//								scaledImages.put(b, b.getTexture().getScaledInstance(
//										(int) scale, 
//										(int) scale, 
//										Image.SCALE_FAST));
//						}
//						if(b instanceof SurfaceHitboxBlock) {
//							xd += ((SurfaceHitboxBlock) b).getDisplay().x1*scale;
//							yd += ((SurfaceHitboxBlock) b).getDisplay().y1*scale;
//						}	
//						layer2.add(new ImageDisplay(scaledImages.get(b), xd, yd));
//					}
//				} catch (NullPointerException e) {/**/}
//			}
//		}
//		Graphics g = image.getGraphics();
//		for(ImageDisplay img : layer1)
//			g.drawImage(img.i, img.x, img.y, observer);
//		for(ImageDisplay img : layer2)
//			g.drawImage(img.i, img.x, img.y, observer);
//		
//		return image;
//	}
}

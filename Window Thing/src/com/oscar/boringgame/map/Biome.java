	package com.oscar.boringgame.map;

import static java.lang.Math.abs;
import static java.lang.Math.round;

import com.oscar.boringgame.blocks.CosmeticBlock;
import com.oscar.boringgame.blocks.FurnitureBlock;
import com.oscar.boringgame.blocks.SolidBlock;
import com.oscar.boringgame.blocks.blocktypes.DataHolderBlockInstance;
import com.oscar.boringgame.blocks.blocktypes.SurfaceBlock;

public enum Biome {
	
	ETERNAL_SNOW (
		0, "Eternal Snows", 6, 90, 
		new float[] {215/255f, 255/255f, 255/255f},
		SolidBlock.SNOW,
		new SurfaceBlock[0], new int[0],
		new SurfaceBlock[][] {{CosmeticBlock.FROST_FLAKES}}, new float[] {0.7f}
	),
	MOUNTAINS (
		1, "Mountains", 3, 34,
		new float[] {148/255f, 149/255f, 155/255f},
		SolidBlock.STONE,
		new SurfaceBlock[0], new int[0],
		new SurfaceBlock[0][0], new float[0] 
	),
	ICE_PLAINS (
		2, "Ice Plains", 5, 72,
		new float[] {145/255f, 233/255f, 180/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[] {FurnitureBlock.PINE}, new int[] {12},
		new SurfaceBlock[][] {{CosmeticBlock.FROST_FLAKES}}, new float[] {0.78f}
	),
	TOUNDRA (
		3, "Toundra", 0, 0,
		new float[] {74/255f, 119/255f, 101/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[0], new int[0],
		new SurfaceBlock[0][0], new float[0]
	),
	SWAMP (
		4, "Swamp", 16, 100,
		new float[] {83/255f, 137/255f, 47/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[0], new int[0],
		new SurfaceBlock[0][0], new float[0]
	),
	FOREST (
		5, "Forest", 17, 70, 
		new float[] {44/255f, 174/255f, 40/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[] {FurnitureBlock.OAK}, new int[] {4},
		new SurfaceBlock[][] {{DataHolderBlockInstance.STRAWBERRY_BUSH}, {CosmeticBlock.RED_FLOWERS, CosmeticBlock.MIXED_FLOWERS}}, new float[] {-0.75f, 0.7f}
	),
	PLAIN (
		6, "Plains", 15, 50,
		new float[]  {75/255f, 199/255f, 71/255f},
		SolidBlock.GRASS, 
		new SurfaceBlock[] {FurnitureBlock.OAK}, new int[] {10},
		new SurfaceBlock[][] {{CosmeticBlock.RED_FLOWERS, CosmeticBlock.MIXED_FLOWERS, CosmeticBlock.TULIPS}}, new float[] {0.6f}
	),
	FLOWERY_FOREST (
		7, "Flowery Forest", 15, 60,
		new float[] {17/255f, 137/255f, 13/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[] {FurnitureBlock.OAK}, new int[] {7},
		new SurfaceBlock[][] {{CosmeticBlock.MIXED_FLOWERS, CosmeticBlock.MIXED_FLOWERS, CosmeticBlock.RED_FLOWERS, CosmeticBlock.TULIPS}}, new float[] {0.5f}
		
	),
	TAIGA (
		8, "Taiga", 10, 22,
		new float[] {47/255f, 137/255f, 101/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[] {FurnitureBlock.PINE}, new int[] {5},
		new SurfaceBlock[0][0], new float[0]
	),
	DEAD_LANDS (
		9, "Dead Lands", 18, 2,
		new float[] {165/255f, 129/255f, 101/255f},
		SolidBlock.DRY_DIRT,
		new SurfaceBlock[0], new int[0],
		new SurfaceBlock[][] {{CosmeticBlock.DEAD_BUSH}, {FurnitureBlock.CACTUS}}, new float[] {0.7f, -0.82f}
	),
	JUNGLE (
		10, "Jungle", 30, 90,
		new float[] {57/255f, 219/255f, 24/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[] {DataHolderBlockInstance.LYCHEE_TREE}, new int[] {6},
		new SurfaceBlock[][] {{CosmeticBlock.HIBICUS_BUSH, CosmeticBlock.RED_FLOWERS}}, new float[] {0.66f}
	),
	SAVANNA (
		11, "Savanna", 30, 50,
		new float[] {157/255f, 178/255f, 72/255f},
		SolidBlock.GRASS,
		new SurfaceBlock[] {FurnitureBlock.BAOBAB}, new int[] {20},
		new SurfaceBlock[0][0], new float[0]
	),
	DESERT (
		12, "Desert", 30, 0,
		new float[] {229/255f, 220/255f, 159/255f},
		SolidBlock.SAND,
		new SurfaceBlock[0], new int[0],
		new SurfaceBlock[][] {{FurnitureBlock.CACTUS}, {CosmeticBlock.DEAD_BUSH}}, new float[] {-0.73f, 0.75f}
	),
	;
	
	/** The max temperature that can be generated (the minimum is 0). */
	private static final int MAX_TEMP = 30;
	/** The max humidity that can be generated (the minimum is 0). */
	private static final int MAX_HUM = 100;
	
	/** The biome's id. */
	private final byte id;
	/** The biome's name. */
	private final String name;
	/** The biome's ideal temperature. */
	private final int temperature;
	/** The biome's ideal humidity. */
	private final int humidity;
	/** The color of grass in this biome ([r, g, b], values ranging from 0 to 1). */
	private final float[] grassColor;
	
	/** The block used to fill the ground in this biome. */
	private final SolidBlock ground;
	/** The surface blocks that should be evenly placed on this biome, according to a PoissonDisk distribution
	 * of minimum distance specified by the value at the corresponding index of surfaceBlocksMinDistance. */
	private final SurfaceBlock[] surfaceBlocks;
	/** The minimum distance that separates two surface blocks at this index of surfaceBlocks. */
	private final int[] surfaceBlocksMinDistance;
	/** The decoration blocks that should randomly be placed in the biome, with a probability specified by the
	 * value at the same index of decorationBlocksOdds. If decorationBlocks[index].length > 1, then one of its
	 * elements is randomly picked. */
	private final SurfaceBlock[][] decorationBlocks;
	/** The odds (ranging from 0 to 1) of randomly placing the decoration block at a given index of decorationBlocks. */
	private final float[] decorationBlocksOdds;
	
	Biome(int id, String name, int temperature, int humidity, float[] grassColor, 
			SolidBlock ground, SurfaceBlock[] surfaceBlocks, int[] surfaceBlocksMinDistance,
			SurfaceBlock[][] decorationBlocks, float[] decorationBlocksOdds){
		if(grassColor.length != 3)
			throw new IllegalArgumentException("grassColor should have a length of 3!");
		if(surfaceBlocks.length != surfaceBlocksMinDistance.length)
			throw new IllegalArgumentException("surfaceBlocks and surfaceBlocksMinDistance should be the same length!");
		if(decorationBlocks.length != decorationBlocksOdds.length)
			throw new IllegalArgumentException("decorationBlocks and decorationBlocksOdds should be the same length!");
		
		this.id = (byte) id;
		this.name = name;
		this.temperature = temperature;
		this.humidity = humidity;
		this.grassColor = grassColor;

		this.ground = ground;
		this.surfaceBlocks = surfaceBlocks;
		this.surfaceBlocksMinDistance = surfaceBlocksMinDistance;
		this.decorationBlocks = decorationBlocks;
		this.decorationBlocksOdds = decorationBlocksOdds;
	}
	
	/**
	 * @return The biome's ID.
	 */
	public byte getID() {
		return id;
	}
	
	/**
	 * @return The biome's user friendly name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The temperature around which this biome tends to appear.
	 */
	public int getTemperature() {
		return temperature;
	}
	
	/**
	 * @return The temperature around which this biome tends to appear.
	 */
	public int getHumidity() {
		return humidity;
	}
	
	/**
	 * @return The colors needed for the grass in this biome, in an array as [R, G, B].
	 */
	public float[] getGrassColor() {
		return grassColor;
	}
	
	/**
	 * @return The ground blocks used for this biome.
	 */
	public SolidBlock getGround() {
		return ground;
	}
	
	/**
	 * 
	 * @return The surface blocks that should be evenly placed on this biome, according to a PoissonDisk distribution
	 * of minimum distance specified by the value at the corresponding index of <code>getPoissonSurfaceBlocksMinDistance()</code>.
	 */
	public SurfaceBlock[] getPoissonSurfaceBlocks() {
		return surfaceBlocks;
	}
	
	/**
	 * @return The minimum distance that separates two surface blocks at this index of <code>getPoissonSurfaceBlocks()</code>.
	 */
	public int[] getPoissonSurfaceBlocksMinDistance() {
		return surfaceBlocksMinDistance;
	}
	
	/**
	 * @return The decoration blocks that should randomly be placed in the biome, with a probability specified by the
	 * value at the same index of <code>getRandomSurfaceBlocksOdds()</code>. 
	 * If <code>getRandomSurfaceBlocksOdds[index].length > 1</code>, then one of its elements should be randomly picked.
	 */
	public SurfaceBlock[][] getRandomSurfaceBlocks(){
		return decorationBlocks;
	}
	
	/**
	 * @return The odds (ranging from 0 to 1) of randomly placing the decoration block at a given index of <code>getRandomSurfaceBlocks()</code>.
	 */
	public float[] getRandomSurfaceBlocksOdds() {
		return decorationBlocksOdds;
	}
	
	/**
	 * Will represent all biomes in a 2D space, and by using temperature and humidity as the two axis, will look
	 * for the closest biome to the given properties.
	 * @param temp The temperature that the biome should try to match.
	 * @param hum The humidity that the biome should try to match.
	 * @return
	 */
	public static Biome biomeFor(float temp, float hum) {
		Biome bestBiome = PLAIN;
		float bestDist = Float.MAX_VALUE;
	
		for(Biome biome : values()) {
			float dist = abs(temp - biome.temperature)/MAX_TEMP + abs(hum - biome.humidity)/MAX_HUM;
			if(dist < bestDist) {
				bestDist = dist;
				bestBiome = biome;
			}
		}
		
		
		return bestBiome;
	}
	
	/**
	 * Will map the given value from the range [-1;1] to the range [0;30], that would represent a temperature
	 * that would more or less equate to celcius degrees.
	 * @param value The input value.
	 * @return The mapped value.
	 */
	public static int convertTemperature(float value) {
		return round((value+1) * 15);
	}
	
	/**
	 * Will map the given value from the range [-1;1] to the range [0;100], that would represent the humidity
	 * percentage of a location.
	 * @param value The input value.
	 * @return The mapped value.
	 */
	public static int convertHumidity(float value) {
		return round((value+1) * 50);
	}
	
	/**
	 * @param id The id to look for.
	 * @return The Biome with this id, or null if none is found.
	 */
	public static Biome getFromID(byte id) {
		for(Biome b : values())
			if(b.id == id)
				return b;
		return null;
	}
}

package com.oscar.boringgame.events;

import com.oscar.boringgame.blocks.blocktypes.Block;
import com.oscar.boringgame.entity.DroppedItem;
import com.oscar.boringgame.entity.Entity;
import com.oscar.boringgame.item.ItemStack;
import com.oscar.boringgame.particle.BlockBreakingParticle;
import com.oscar.boringgame.particle.ParticleEntity;
import com.oscar.boringgame.util.BoringMath;
import com.oscar.boringgame.util.Location;
import com.oscar.boringgame.util.PVector;

public class BlockBreak {
	public static void onBlockBreak(BlockBreakEvent e) {
		for(ItemStack i : e.block.getDrops()) {
			Location l = e.location.clone();
			l.x += BoringMath.nextFloat(0.4f, 0.6f);
			l.y += BoringMath.nextFloat(0.4f, 0.6f);
			
			PVector velocity = PVector.fromAngle(BoringMath.random().nextFloat()*2*Math.PI);
			velocity.mult(0.1f);
			
			DroppedItem dropped = l.getWorld().dropItem(i, l);
			dropped.setPickupTime(30);
			dropped.setVelocity(velocity);
		}
		
		int particles = BoringMath.nextInt(10, 15);
		for(int i = 0; i < particles; i++) {
			ParticleEntity particle = new BlockBreakingParticle(
					e.block, e.location.getWorld(), 
					e.location.x + BoringMath.random().nextFloat(), 
					e.location.y + BoringMath.random().nextFloat());
			e.location.getWorld().add(particle);
		}
	}
	
	public static class BlockBreakEvent{
		private final Block block;
		private final Location location;
		private final Entity breaker;
		
		public BlockBreakEvent(Block b, Location l) {
			this(b, l, null);
		}
		
		public BlockBreakEvent(Block b, Location l, Entity breaker) {
			block = b;
			location = l;
			this.breaker = breaker;
		}
		
		/**
		 * @return The Entity that broke the Block.
		 */
		public Entity getBreaker() {
			return breaker;
		}
		
		/**
		 * @return The Block that was broken.
		 */
		public Block getBlock() {
			return block;
		}
		
		/**
		 * @return The Location at which the Block was broken.
		 */
		public Location getLocation() {
			return location;
		}
	}
}

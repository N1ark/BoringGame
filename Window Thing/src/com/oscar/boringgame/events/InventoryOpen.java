package com.oscar.boringgame.events;

import com.oscar.boringgame.entity.Player;
import com.oscar.boringgame.item.Inventory;

public class InventoryOpen {
	public static void onInventoryOpen(@SuppressWarnings("unused") InventoryOpenEvent e) {
		/* Not used... yet */
	}
	public static class InventoryOpenEvent{
		private final Player player;
		private final Inventory inventory;
		public InventoryOpenEvent(Player p, Inventory i) {
			player = p;
			inventory = i;
		}
		public Player getPlayer() {
			return player;
		}
		public Inventory getInventory() {
			return inventory;
		}
	}
}
